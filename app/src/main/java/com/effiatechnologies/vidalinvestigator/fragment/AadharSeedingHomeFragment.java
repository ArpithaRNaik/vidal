package com.effiatechnologies.vidalinvestigator.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.activity.AadharSeedingActivity;
import com.effiatechnologies.vidalinvestigator.activity.AadharSeedingReportActivity;
import com.effiatechnologies.vidalinvestigator.activity.AddNewAadharSeedingMemberActivity;
import com.effiatechnologies.vidalinvestigator.activity.LocalAadharSeedingActivity;
import com.effiatechnologies.vidalinvestigator.activity.LocallyAddedMemberDetailsActivity;
import com.effiatechnologies.vidalinvestigator.helper.UrnDescriptionSqliteHelper;
import com.effiatechnologies.vidalinvestigator.helper.UrnDetailsHelperTable;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import java.io.File;


/**
 * Created by Administrator on 05-01-2017.
 */

public class AadharSeedingHomeFragment extends Fragment {

   // private static final String TAG = "AadharSeedingHomeFragment";

    //private AdminHomeFragment.AdminHomePageServices mApiService = null;

    private Activity mActivity = null;
    private Context mContext = null;

    private UrnDescriptionSqliteHelper SQLITEHELPER = null;
    private SQLiteDatabase SQLITEDATABASE;
    String GetSQliteQuery;
    Cursor cursor;
    String DeleteQuery;
    private UrnDetailsHelperTable urnDetailsHelperTable;
    private static final String DB_NAME_URL_DETAILS = "URNDetails.db";
    private static final String DB_NAME_SAVED_URL = "URNDescription.db";

    private LinearLayout layout_AadharSeeding = null;
    private LinearLayout layout_AadharReport = null;
    private LinearLayout layout_AddMember = null;
    private LinearLayout layout_LocalAadharSeeding = null;
    private LinearLayout layout_LocallyAddedData = null;
    private String LoginId;

    public AadharSeedingHomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Log.d(TAG, "inside onCreate");
        mActivity = getActivity();
        mContext = getContext();
        SQLITEHELPER = new UrnDescriptionSqliteHelper(mContext);
        mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        urnDetailsHelperTable = new UrnDetailsHelperTable(getContext());

        LoginId = SharedPreferenceUtil.getLoginId(mContext);
       // Log.d(TAG, "LoginId" + LoginId);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_aadhar_seeding_home_page, container, false);
        // Log.d(TAG, "inside onCreateView");


        layout_AadharSeeding = (LinearLayout) view.findViewById(R.id.layout_aadharSeeding);
        layout_AadharReport = (LinearLayout) view.findViewById(R.id.layout_aadharReport);
        //layout_AddMember = (LinearLayout) view.findViewById(R.id.layout_AddaadharSeedingMember);
      //  layout_LocalAadharSeeding = (LinearLayout) view.findViewById(R.id.layout_LocalAddaadharSeeding);
      //  layout_LocallyAddedData = (LinearLayout) view.findViewById(R.id.layout_LocalDataDisplay);
        layout_AadharSeeding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aadharIntent = new Intent(mContext, AadharSeedingActivity.class);
                startActivity(aadharIntent);
            }
        });
        /*if (LoginId.contains("DE Operator"))
        {
            layout_AadharReport.setVisibility(View.GONE);
        }
        else
        {
            layout_AadharReport.setVisibility(View.VISIBLE);

        }*/
        layout_AadharReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aadharIntent = new Intent(mContext, AadharSeedingReportActivity.class);
                startActivity(aadharIntent);
            }
        });
       /* layout_AddMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aadharIntent = new Intent(mContext, AddNewAadharSeedingMemberActivity.class);
                startActivity(aadharIntent);
            }
        });*/
       /* layout_LocalAadharSeeding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aadharIntent = new Intent(mContext, LocalAadharSeedingActivity.class);
                startActivity(aadharIntent);
            }
        });
        layout_LocallyAddedData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent aadharIntent = new Intent(mContext, LocallyAddedMemberDetailsActivity.class);
                startActivity(aadharIntent);
            }
        });*/


        return view;
    }



    private void deleteSqliteData() {
        //  Log.d(TAG, "insied deleteSqliteData");
        SQLITEDATABASE = mContext.openOrCreateDatabase("URNDescription.db", Context.MODE_PRIVATE, null);
        //  Log.d(TAG, "SQLITEDATABASE : " + SQLITEDATABASE);
        // SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();
        GetSQliteQuery = "SELECT * FROM URNDescriptionTable";
        //  Log.d(TAG, "GetSQliteQuery : " + GetSQliteQuery);
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        //  Log.d(TAG, "cursor : " + cursor);
        cursor.moveToFirst();
        DeleteQuery = "DELETE FROM URNDescriptionTable";
        SQLITEDATABASE.execSQL(DeleteQuery);
        //  Toast.makeText(this, "Record Deleted", Toast.LENGTH_LONG).show();
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        /*SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();

        SQLITEDATABASE.execSQL("DELETE FROM QuestionsAnswerTable"); //delete all rows in a table*/
        SQLITEDATABASE.close();
    }

    private void deleteSqliteList() {

        SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();
        String countAssCls = "SELECT count(*) FROM URNDescriptionTable";
        //  Log.d(TAG, "countAssCls : " + countAssCls);
        Cursor mcursorAss = SQLITEDATABASE.rawQuery(countAssCls, null);
        mcursorAss.moveToFirst();
        int icountAss = mcursorAss.getInt(0);
        if (icountAss > 0) {
            //  Log.d(TAG, "icountAss is greater than 0 : " + icountAss);
            SQLITEDATABASE.execSQL("DELETE FROM URNDescriptionTable"); //delete all rows in a table
            SQLITEDATABASE.close();
        } else {
            //  Log.d(TAG, "inside icountAss less than 0 ");
            //  finish();

        }
    }
    private void deleteURNSqliteData() {
        //  Log.d(TAG, "insied deleteSqliteData");
        SQLITEDATABASE = mContext.openOrCreateDatabase("URNDetails.db", Context.MODE_PRIVATE, null);
        //  Log.d(TAG, "SQLITEDATABASE : " + SQLITEDATABASE);
        // SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();
        GetSQliteQuery = "SELECT * FROM URNDetailsTable";
        //  Log.d(TAG, "GetSQliteQuery : " + GetSQliteQuery);
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        //  Log.d(TAG, "cursor : " + cursor);
        cursor.moveToFirst();
        DeleteQuery = "DELETE FROM URNDetailsTable";
        SQLITEDATABASE.execSQL(DeleteQuery);
        //  Toast.makeText(this, "Record Deleted", Toast.LENGTH_LONG).show();
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        /*SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();

        SQLITEDATABASE.execSQL("DELETE FROM QuestionsAnswerTable"); //delete all rows in a table*/
        SQLITEDATABASE.close();
    }

    private void deleteURNSqliteList() {

        SQLITEDATABASE = urnDetailsHelperTable.getWritableDatabase();
        String countAssCls = "SELECT count(*) FROM URNDetailsTable";
        //  Log.d(TAG, "countAssCls : " + countAssCls);
        Cursor mcursorAss = SQLITEDATABASE.rawQuery(countAssCls, null);
        mcursorAss.moveToFirst();
        int icountAss = mcursorAss.getInt(0);
        if (icountAss > 0) {
            //  Log.d(TAG, "icountAss is greater than 0 : " + icountAss);
            SQLITEDATABASE.execSQL("DELETE FROM URNDetailsTable"); //delete all rows in a table
            SQLITEDATABASE.close();
        } else {
            //  Log.d(TAG, "inside icountAss less than 0 ");
            //  finish();

        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
       // Log.d(TAG, "inside ondetach");
        doesDatabaseExist(mContext, DB_NAME_URL_DETAILS);
        doesURLDatabaseExist(mContext, DB_NAME_SAVED_URL);

    }

    private boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        if (dbFile.exists()) {
           //  Log.d(TAG, "inside if 1 ");
            deleteURNSqliteData();
            deleteURNSqliteList();

        } else {
            // Log.d(TAG, "inside else 2 ");
        }
        return dbFile.exists();
    }

    private boolean doesURLDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        if (dbFile.exists()) {
            // Log.d(TAG, "inside if 3 ");
            deleteSqliteList();
            deleteSqliteData();
        } else {
           //  Log.d(TAG, "inside else 4 ");
        }
        return dbFile.exists();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Log.d(TAG, "inside onAttach");
    }


}
