package com.effiatechnologies.vidalinvestigator.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;

import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.activity.HomePageNavigationActivity;
import com.effiatechnologies.vidalinvestigator.activity.PendingClaimsActivity;
import com.effiatechnologies.vidalinvestigator.adapter.AdminAddQueStateSpinnerAdapter;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.helper.SqliteStatesHelper;
import com.effiatechnologies.vidalinvestigator.model.InputToGetStates;
import com.effiatechnologies.vidalinvestigator.model.SaveQuestionsInput;
import com.effiatechnologies.vidalinvestigator.model.StatesModel;
import com.effiatechnologies.vidalinvestigator.model.UserResponse;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Arpitha on 04-02-2017.
 */

public class InvestigateSelectClaimsFragment extends Fragment {

   // private static final String TAG = "InvestigateSelectClaimsFragment";

    private FacultyTestExamServices mApiService = null;
    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;

    private Activity mActivity = null;
    private Context mContext = null;

    private FragmentManager mFragmentManager = null;
    private FragmentTransaction mFragmentTransaction = null;

    private Spinner spinnerTransactionCode;
    private Spinner spinnerInvestigationStatus;
    private Spinner spinnerHospitalDistrict;
    private Spinner spinnerHospitalNames;
    private EditText etQuestions;
    private CheckBox checkBoxIsActive;
    private Button btnSave;
    private Button btnCancel;
    private Button btnReset;
    private int IsActive = 0;
    private SqliteStatesHelper sqliteStatesHelper;

    private AdminAddQueStateSpinnerAdapter adminAddQueStateSpinnerAdapter;
    private String stateName;
    private int stateCode;

    private SaveQuestionsInput saveQuestionsInput;
    private String questions;
    private List<UserResponse> userResponseList;
    private String resultMesage;
    private int statusCode;

    private int userCode;
    private InputToGetStates inputToGetStates;
    private List<StatesModel> statesModelList;

    private Intent invIntent;


    public InvestigateSelectClaimsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //  Log.d(TAG, "inside onCreate");
        mActivity = getActivity();
        mContext = getContext();
        builder = new AlertDialog.Builder(mContext);
        progressDialog = new ProgressDialog(mContext);
        mApiService = MyApplication.getRetrofit().create(FacultyTestExamServices.class);
        mFragmentManager = getFragmentManager();
       // sqliteStatesHelper = new SqliteStatesHelper(mContext);
      //  statesModelList = sqliteStatesHelper.getProducts();
      //  Log.d(TAG, "statesModelList : " + statesModelList);
        userCode = SharedPreferenceUtil.getUserCode(mContext);
      //  Log.d(TAG, "userCode : " + userCode );
        getStates(userCode);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_investigate_select_claims, container, false);
      //  Log.d(TAG, "inside onCreateView");

        spinnerTransactionCode = (Spinner) view.findViewById(R.id.spinner_TransactionCode);
        spinnerInvestigationStatus = (Spinner) view.findViewById(R.id.spinner_InvestigationStatus);
        spinnerHospitalDistrict = (Spinner) view.findViewById(R.id.spinner_HospitalDistrict);
        spinnerHospitalNames = (Spinner) view.findViewById(R.id.spinner_HospitalNames);

        btnSave = (Button) view.findViewById(R.id.btn_OkInvestigator);
        btnCancel = (Button) view.findViewById(R.id.btn_CancelInvestigator);
        btnReset = (Button) view.findViewById(R.id.btn_ResetInvestigator);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.home_page_fragment_container, new BarGraphFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });


        spinnerTransactionCode.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {

                    stateCode = statesModelList.get(position).getStateCode();
                  //  Log.d(TAG, "stateCode : " + stateCode);

                    stateName = statesModelList.get(position).getStateName();
                  //  Log.d(TAG, "stateName : " + stateName);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  saveQuestions();
                 invIntent = new Intent(mContext, PendingClaimsActivity.class);
                 startActivity(invIntent);
               //  Log.d(TAG, "moving to pendingclaims");
                 mActivity.finish();
            }
        });

        return view;
    }

    private void getStates(int userCode) {

     //   Log.d(TAG, "inside getStates ");


            if (AppUtil.isNetworkAvailable(mContext)) {

                progressDialog.setMessage("Please Wait....");
                progressDialog.show();
                progressDialog.setCancelable(false);

                inputToGetStates = new InputToGetStates();
                inputToGetStates.setUserCode(userCode);


              //  Log.d(TAG, "input to inputToGetStates : " + inputToGetStates);

                Call<List<StatesModel>> call = mApiService.getStatesApi(inputToGetStates);
                call.enqueue(new Callback<List<StatesModel>>() {
                    @Override
                    public void onResponse(Call<List<StatesModel>> call, Response<List<StatesModel>> response) {
                        if (response.isSuccessful()) {
                            progressDialog.dismiss();
                            statesModelList = response.body();
                            statusCode = response.code();
                          //  Log.d(TAG, "statuscode : " +  statusCode );
                          //  Log.d(TAG, "statesModelList : " +  statesModelList );
                          //  Log.d(TAG, "statesModelList size : " +  statesModelList.size() );


                            if (statesModelList.size() >= 1)
                            {
                                // Log.d(TAG, "resultResponseList : "+ resultResponseList);
                                statesModelList.add(0, new StatesModel(0, " Select "));
                                adminAddQueStateSpinnerAdapter = new AdminAddQueStateSpinnerAdapter(getContext(), statesModelList);
                                spinnerTransactionCode.setAdapter(adminAddQueStateSpinnerAdapter);
                                adminAddQueStateSpinnerAdapter.notifyDataSetChanged();
                            }
                            else {
                                progressDialog.dismiss();
                                showDataNotAvailbale();
                            }


                        } else {

                            progressDialog.dismiss();
                            statusCode = response.code();
                          //  Log.d(TAG, statusCode + "failed");
                            showDataNotAvailbale();
                            //  ResponseBody errorBody = response.errorBody();

                        }
                    }

                    @Override
                    public void onFailure(Call<List<StatesModel>> call, Throwable t) {

                        progressDialog.dismiss();
                      //  Log.d(TAG, t.getMessage() + "on failure");
                        showErrorDialog();

                    }
                });
            } else {
                // AppUtil.toastMessage(mContext, "Check Internet connection");
                showInternetNotAvailbale();
            }

    }

    private void saveQuestions() {

      //  Log.d(TAG, "inside ApproveSMS ");

        boolean cancel = false;

        questions  = etQuestions.getText().toString();

        if (TextUtils.isEmpty(questions)) {
            etQuestions.setError("Enter Question");
            cancel = true;
        }
        if (stateCode == 0)
        {
            AppUtil.toastMessage(mContext, "Please Select State");
            cancel = true;
        }
        if (!cancel) {
            if (AppUtil.isNetworkAvailable(mContext)) {

                progressDialog.setMessage("Please Wait....");
                progressDialog.show();
                progressDialog.setCancelable(false);

                saveQuestionsInput = new SaveQuestionsInput();
                saveQuestionsInput.setQuestion(questions);
                saveQuestionsInput.setStateCode(stateCode);
                saveQuestionsInput.setIsActive(IsActive);


                //  Log.d(TAG, "input to saveQuestionsInput : " + saveQuestionsInput);

                Call<List<UserResponse>> call = mApiService.saveQuestionsApi(saveQuestionsInput);
                call.enqueue(new Callback<List<UserResponse>>() {
                    @Override
                    public void onResponse(Call<List<UserResponse>> call, Response<List<UserResponse>> response) {
                        if (response.isSuccessful()) {
                            progressDialog.dismiss();
                            userResponseList = response.body();
                            statusCode = response.code();
                           // Log.d(TAG, "statuscode : " +  statusCode );
                            // Log.d(TAG, "resultResponseList : "+ resultResponseList);
                            for (int i = 0; i < userResponseList.size(); i++) {
                                resultMesage = userResponseList.get(i).getResult();
                                //  Log.d(TAG, "resultApprove : " + resultApprove);
                                if (statusCode  == 200)
                                {
                                    showSMSApprovedDialog(resultMesage);
                                }
                                else
                                {
                                    showErrorDialog();
                                }

                            }

                        } else {

                            progressDialog.dismiss();
                            statusCode = response.code();
                          //  Log.d(TAG, statusCode + "failed");
                            showDataNotAvailbale();
                            //  ResponseBody errorBody = response.errorBody();

                        }
                    }

                    @Override
                    public void onFailure(Call<List<UserResponse>> call, Throwable t) {

                        progressDialog.dismiss();
                      //  Log.d(TAG, t.getMessage() + "on failure");
                        showErrorDialog();

                    }
                });
            } else {
                // AppUtil.toastMessage(mContext, "Check Internet connection");
                showInternetNotAvailbale();
            }
        }
    }
    private void showDataNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Please Try Again!!");
        builder.setMessage("Data Not Found");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }
    private void showInternetNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Network Not Available");
        builder.setMessage("Please check your Internet Connection!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }
    private void showErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Something Wrong!!");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //finish();
            }
        });

        builder.show();
    }
    private void showSMSApprovedDialog(String resultApprove) {

        builder.setCancelable(false);
        builder.setTitle("Message");
        builder.setMessage(resultApprove);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

               //  mActivity.finish();

             //   etQuestions.requestFocus();
                etQuestions.getText().clear();
                getStates(userCode);
                checkBoxIsActive.setChecked(false);
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.admin_dashboard_fragment_container, new AdminHomeFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();


            }
        });

        builder.show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
      //  Log.d(TAG, "inside onDetach");
        /*mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.admin_dashboard_fragment_container, new AdminHomeFragment());
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();*/
        mActivity.finish();

    }



    public interface FacultyTestExamServices {

        @POST(UrlConfig.STATES_QUESTIONS)
        Call<List<StatesModel>> getStatesApi(@Body InputToGetStates inputToGetStates);

        @POST(UrlConfig.SAVE_QUESTIONS)
        Call<List<UserResponse>> saveQuestionsApi(@Body SaveQuestionsInput nativeGraphInput);

    }

}
