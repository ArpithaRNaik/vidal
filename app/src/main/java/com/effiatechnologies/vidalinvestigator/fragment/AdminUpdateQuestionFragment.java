package com.effiatechnologies.vidalinvestigator.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.activity.AdminNavigationActivity;
import com.effiatechnologies.vidalinvestigator.adapter.AdminAddQueStateSpinnerAdapter;
import com.effiatechnologies.vidalinvestigator.adapter.QuestionDetailsAdapter;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.helper.RoleTypesSqliteHelper;
import com.effiatechnologies.vidalinvestigator.model.GetQuestionsResponse;
import com.effiatechnologies.vidalinvestigator.model.IndividualStatesResponse;
import com.effiatechnologies.vidalinvestigator.model.InputToGetStates;
import com.effiatechnologies.vidalinvestigator.model.InputToUpdateQuestionDetailsModel;
import com.effiatechnologies.vidalinvestigator.model.NativeGraphInput;
import com.effiatechnologies.vidalinvestigator.model.QuestionAnswerTableList;
import com.effiatechnologies.vidalinvestigator.model.ResponseResultMessage;
import com.effiatechnologies.vidalinvestigator.model.StatesModel;
import com.effiatechnologies.vidalinvestigator.model.UpdateQuestionsModelList;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Arpitha on 04-02-2017.
 */

public class AdminUpdateQuestionFragment extends Fragment {

   // private static final String TAG = "adminupdatequestion";

    private FacultyTestExamServices mApiService = null;
    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;

    private Activity mActivity = null;
    private Context mContext = null;

    private FragmentManager mFragmentManager = null;
    private FragmentTransaction mFragmentTransaction = null;

    private Spinner spinnerStates;
    private RecyclerView queDetailsRecycler;
    private Button btnQuestionUpdate;
    private LinearLayout layout_QuestionDetails;

    private int userCode;
    private InputToGetStates inputToGetStates;
    private List<StatesModel> statesModelList;
    private int statusCode;
    private AdminAddQueStateSpinnerAdapter adminAddQueStateSpinnerAdapter;
    private String stateName;
    private int stateCode;

    private StatesModel statesModelInput;
    private List<GetQuestionsResponse> getQuestionResponseList;
    private QuestionDetailsAdapter questionDetailsAdapter;
    private RoleTypesSqliteHelper SQLITEHELPER = null;
    private SQLiteDatabase SQLITEDATABASEADMIN;
    private JSONArray resultSetForAssCls;

    private InputToUpdateQuestionDetailsModel inputToUpdateQuestionDetailsModel;
    private List<ResponseResultMessage> responseResultMessageList;

    private List<UpdateQuestionsModelList> updateQuestionsModelListList;


    public AdminUpdateQuestionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //  Log.d(TAG, "inside onCreate");
        mActivity = getActivity();
        mContext = getContext();
        builder = new AlertDialog.Builder(mContext);
        progressDialog = new ProgressDialog(mContext);
        mApiService = MyApplication.getRetrofit().create(FacultyTestExamServices.class);
        mFragmentManager = getFragmentManager();
        userCode = SharedPreferenceUtil.getUserCode(mContext);
      //  Log.d(TAG, "userCode : " + userCode);
        getStates(userCode);
        SQLITEHELPER = new RoleTypesSqliteHelper(mContext);

        //deleteSqliteData();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_update_question, container, false);
     //   Log.d(TAG, "inside onCreateView");

        spinnerStates = (Spinner) view.findViewById(R.id.spinner_StatesNameUpdate);
        queDetailsRecycler = (RecyclerView) view.findViewById(R.id.recycler_state_Question_details);
        btnQuestionUpdate = (Button) view.findViewById(R.id.btn_queUpdate);
        layout_QuestionDetails = (LinearLayout) view.findViewById(R.id.Layout_QuestionDetails);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        queDetailsRecycler.setHasFixedSize(true);
        queDetailsRecycler.setLayoutManager(mLayoutManager);
        queDetailsRecycler.setItemAnimator(new DefaultItemAnimator());

        spinnerStates.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    deleteSqliteData();

                    stateCode = statesModelList.get(position).getStateCode();
                  //  Log.d(TAG, "stateCode : " + stateCode);

                    stateName = statesModelList.get(position).getStateName();
                  //  Log.d(TAG, "stateName : " + stateName);

                    getStateQuestionDetails(stateCode);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnQuestionUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getProducts();
            }
        });

        return view;
    }

    private void getStateQuestionDetails(int stateCode) {

      //  Log.d(TAG, "inside getStateQuestionDetails ");


        if (AppUtil.isNetworkAvailable(mContext)) {

            progressDialog.setMessage("Please Wait....");
            progressDialog.show();
            progressDialog.setCancelable(false);

            statesModelInput = new StatesModel();
            statesModelInput.setStateCode(stateCode);


           // Log.d(TAG, "input to statesModelInput : " + statesModelInput);

            Call<List<GetQuestionsResponse>> call = mApiService.getStatesQuestionDetails(statesModelInput);
            call.enqueue(new Callback<List<GetQuestionsResponse>>() {
                @Override
                public void onResponse(Call<List<GetQuestionsResponse>> call, Response<List<GetQuestionsResponse>> response) {
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        getQuestionResponseList = response.body();
                        statusCode = response.code();
                      //  Log.d(TAG, "statuscode : " + statusCode);
                      //  Log.d(TAG, "getQuestionResponseList : " + getQuestionResponseList);
                      //  Log.d(TAG, "statesModelList size : " + getQuestionResponseList.size());


                        if (getQuestionResponseList.size() >= 1) {
                          //  Log.d(TAG, "getQuestionResponseList : " + getQuestionResponseList);
                            layout_QuestionDetails.setVisibility(View.VISIBLE);
                            questionDetailsAdapter = new QuestionDetailsAdapter(mContext,getActivity(), getQuestionResponseList);
                            queDetailsRecycler.setAdapter(questionDetailsAdapter);
                            questionDetailsAdapter.notifyDataSetChanged();
                        } else {
                            progressDialog.dismiss();
                            layout_QuestionDetails.setVisibility(View.GONE);
                            showDataNotAvailbale();
                        }


                    } else {

                        layout_QuestionDetails.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        statusCode = response.code();
                     //   Log.d(TAG, statusCode + "failed");
                        showDataNotAvailbale();
                        //  ResponseBody errorBody = response.errorBody();

                    }
                }

                @Override
                public void onFailure(Call<List<GetQuestionsResponse>> call, Throwable t) {

                    progressDialog.dismiss();
                  //  Log.d(TAG, t.getMessage() + "on failure");
                    showErrorDialog();

                }
            });
        } else {
            // AppUtil.toastMessage(mContext, "Check Internet connection");
            showInternetNotAvailbale();
        }
    }

    public List<UpdateQuestionsModelList> getProducts() {
        updateQuestionsModelListList = new ArrayList<>();
        //  productList.add(new QuestionAnswerTableList(0,"Select Department",false,0));
        //  String selectQuery = "SELECT * FROM " + TABLE_PRODUCTS;
        String selectQueryForAssCls = "Select * from QuestionsTable ";
        SQLITEDATABASEADMIN = SQLITEHELPER.getReadableDatabase();
        Cursor cursorAssCls = SQLITEDATABASEADMIN.rawQuery(selectQueryForAssCls, null);


        if (cursorAssCls.moveToFirst()) {
            do {
                UpdateQuestionsModelList product = new UpdateQuestionsModelList();
                product.setIds(cursorAssCls.getString(0));
                product.setQuestionId(cursorAssCls.getString(1));
                product.setQuestion(cursorAssCls.getString(2));
                product.setStateCode(cursorAssCls.getString(3));
                product.setIsActive(cursorAssCls.getString(4));
             //   Log.d(TAG, "product : " + product);
                updateQuestionsModelListList.add(product);
            } while (cursorAssCls.moveToNext());
        }
        cursorAssCls.close();
        SQLITEDATABASEADMIN.close();
      //  Log.d(TAG, "product list : " + updateQuestionsModelListList);
        updateQuestionDetails(updateQuestionsModelListList);
        return updateQuestionsModelListList;
    }

    /*public Cursor getAllQuestionsData() {
        Log.d(TAG, "inside getAllQuestionsData");
        String selectQueryForAssCls = "Select * from QuestionsTable ";
        SQLITEDATABASEADMIN = SQLITEHELPER.getReadableDatabase();
        Cursor cursorAssCls = SQLITEDATABASEADMIN.rawQuery(selectQueryForAssCls, null);
        curAssClsJson(cursorAssCls);
        return cursorAssCls;
    }

    public JSONArray curAssClsJson(Cursor cursorAssCls) {
        Log.d(TAG, "inside curAssClsJson");
        resultSetForAssCls = new JSONArray();
        cursorAssCls.moveToFirst();
        while (cursorAssCls.isAfterLast() == false) {
            int totalColumnForAssCls = cursorAssCls.getColumnCount();
            JSONObject rowObjectForAssCls = new JSONObject();
            for (int i = 0; i < totalColumnForAssCls; i++) {
                if (cursorAssCls.getColumnName(i) != null) {
                    try {
                        rowObjectForAssCls.put(cursorAssCls.getColumnName(i), cursorAssCls.getString(i));
                    } catch (Exception e) {
                        // Log.d(TAG, e.getMessage());
                    }
                }
            }
            resultSetForAssCls.put(rowObjectForAssCls);
            cursorAssCls.moveToNext();
        }

        cursorAssCls.close();
        Log.d(TAG, "resultSetForAssCls : " + resultSetForAssCls);
        updateQuestionDetails(resultSetForAssCls);
        return resultSetForAssCls;

    }*/

    private void getStates(int userCode) {

      //  Log.d(TAG, "inside getStates ");

        if (AppUtil.isNetworkAvailable(mContext)) {

            progressDialog.setMessage("Please Wait....");
            progressDialog.show();
            progressDialog.setCancelable(false);

            inputToGetStates = new InputToGetStates();
            inputToGetStates.setUserCode(userCode);


          //  Log.d(TAG, "input to inputToGetStates : " + inputToGetStates);

            Call<List<StatesModel>> call = mApiService.getStates(inputToGetStates);
            call.enqueue(new Callback<List<StatesModel>>() {
                @Override
                public void onResponse(Call<List<StatesModel>> call, Response<List<StatesModel>> response) {
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        statesModelList = response.body();
                        statusCode = response.code();
                      //  Log.d(TAG, "statuscode : " + statusCode);
                      //  Log.d(TAG, "statesModelList : " + statesModelList);
                      //  Log.d(TAG, "statesModelList size : " + statesModelList.size());


                        if (statesModelList.size() >= 1) {
                            // Log.d(TAG, "resultResponseList : "+ resultResponseList);
                            statesModelList.add(0, new StatesModel(0, " Select "));
                            adminAddQueStateSpinnerAdapter = new AdminAddQueStateSpinnerAdapter(getContext(), statesModelList);
                            spinnerStates.setAdapter(adminAddQueStateSpinnerAdapter);
                            adminAddQueStateSpinnerAdapter.notifyDataSetChanged();
                        } else {
                            progressDialog.dismiss();
                            showDataNotAvailbale();
                        }


                    } else {

                        progressDialog.dismiss();
                        statusCode = response.code();
                      //  Log.d(TAG, statusCode + "failed");
                        showDataNotAvailbale();
                        //  ResponseBody errorBody = response.errorBody();

                    }
                }

                @Override
                public void onFailure(Call<List<StatesModel>> call, Throwable t) {

                    progressDialog.dismiss();
                  //  Log.d(TAG, t.getMessage() + "on failure");
                    showErrorDialog();

                }
            });
        } else {
            // AppUtil.toastMessage(mContext, "Check Internet connection");
            showInternetNotAvailbale();
        }

    }

    private void updateQuestionDetails(List<UpdateQuestionsModelList> updateQuestionsModelListList) {

       // Log.d(TAG, "inside getStateQuestionDetails ");


        if (AppUtil.isNetworkAvailable(mContext)) {

            progressDialog.setMessage("Please Wait....");
            progressDialog.show();
            progressDialog.setCancelable(false);

            inputToUpdateQuestionDetailsModel = new InputToUpdateQuestionDetailsModel();
            inputToUpdateQuestionDetailsModel.setQuestionDetailsList(updateQuestionsModelListList);
            inputToUpdateQuestionDetailsModel.setStateCode(String.valueOf(stateCode));


           // Log.d(TAG, "input to inputToUpdateQuestionDetailsModel : " + inputToUpdateQuestionDetailsModel);

            Call<List<ResponseResultMessage>> call = mApiService.updateQuestionDetails(inputToUpdateQuestionDetailsModel);
            call.enqueue(new Callback<List<ResponseResultMessage>>() {
                @Override
                public void onResponse(Call<List<ResponseResultMessage>> call, Response<List<ResponseResultMessage>> response) {
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        responseResultMessageList = response.body();
                        statusCode = response.code();
                       // Log.d(TAG, "statuscode : " + statusCode);
                      //  Log.d(TAG, "responseResultMessageList : " + responseResultMessageList);
                      //  Log.d(TAG, "statesModelList size : " + getQuestionResponseList.size());


                        if (responseResultMessageList.size() >= 1) {
                          //  Log.d(TAG, "getQuestionResponseList : " + responseResultMessageList);
                            showSuccessDialog();
                            deleteSqliteData();

                        } else {
                            progressDialog.dismiss();
                            showDataNotAvailbale();
                            deleteSqliteData();
                        }


                    } else {

                        progressDialog.dismiss();
                        statusCode = response.code();
                      //  Log.d(TAG, statusCode + "failed");
                        deleteSqliteData();
                        showDataNotAvailbale();
                        //  ResponseBody errorBody = response.errorBody();

                    }
                }

                @Override
                public void onFailure(Call<List<ResponseResultMessage>> call, Throwable t) {

                    progressDialog.dismiss();
                   // Log.d(TAG, t.getMessage() + "on failure");
                    deleteSqliteData();
                    showErrorDialog();

                }
            });
        } else {
            // AppUtil.toastMessage(mContext, "Check Internet connection");
            showInternetNotAvailbale();
            deleteSqliteData();
        }
    }

    private void deleteSqliteData() {
       // Log.d(TAG, "insied deleteSqliteData");
        SQLITEDATABASEADMIN = SQLITEHELPER.getWritableDatabase();

        SQLITEDATABASEADMIN.execSQL("DELETE FROM QuestionsTable"); //delete all rows in a table
        SQLITEDATABASEADMIN.close();
    }

    private void showDataNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Please Try Again!!");
        builder.setMessage("Data Not Found");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }
    private void showSuccessDialog() {

        builder.setCancelable(false);
        builder.setTitle("Message ");
        builder.setMessage("Updated Successfully");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();

                getStates(userCode);
               // checkBoxIsActive.setChecked(false);
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.admin_dashboard_fragment_container, new AdminHomeFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
            }
        });

        builder.show();
    }

    private void showInternetNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Network Not Available");
        builder.setMessage("Please check your Internet Connection!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }

    private void showErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Something Wrong!!");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //finish();
            }
        });

        builder.show();
    }

    @Override
    public void onDetach() {
        super.onDetach();
      //  Log.d(TAG, "inside onDetach");
        /*Intent adminIntent = new Intent(mContext, AdminNavigationActivity.class);
        startActivity(adminIntent);*/
        mActivity.finish();

    }

    public interface FacultyTestExamServices {

        @POST(UrlConfig.STATES_QUESTIONS)
        Call<List<StatesModel>> getStates(@Body InputToGetStates inputToGetStates);

        @POST(UrlConfig.GET_STATES_QUESTIONS)
        Call<List<GetQuestionsResponse>> getStatesQuestionDetails(@Body StatesModel statesModel);

        @POST(UrlConfig.UPDATE_QUESTION_DETAILS)
        Call<List<ResponseResultMessage>> updateQuestionDetails(@Body InputToUpdateQuestionDetailsModel inputToUpdateQuestionDetailsModel);

    }

}
