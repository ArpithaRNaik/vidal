package com.effiatechnologies.vidalinvestigator.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.model.AllStateDocResponse;
import com.effiatechnologies.vidalinvestigator.model.NativeGraphInput;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Administrator on 06-12-2016.
 */

public class AllStateDocuments extends Fragment {
  //  private static final String TAG = "allstatedoc";

    private Activity mActivity = null;
    private Context mContext = null;
    private getAllStateDocumentService mApiService = null;
    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;

    private TextView tvStateName = null;
    private TextView tvReceived = null;
    private TextView tvReceivedAmt = null;
    private TextView tvApproved = null;
    private TextView tvApprovedAmt = null;
    private TextView tvPending = null;
    private TextView tvPendingAmt = null;
    private TextView tvClaimsPending = null;
    private TextView tvBeyondAmt = null;
    private TextView tvClaimsPeningForeign = null;
    private TextView tvBeyondForeignAmt = null;
    private TextView tvClaimsRejected = null;
    private TextView tvRejectedAmt = null;
    private TextView tvClaimsPaid = null;
    private TextView tvPaidAmt = null;
    private TextView tvPendingFloatGeneration = null;
    private TextView tvPendingFloatGenAmt = null;
    private int loginStateCode;
    private int loginStateCodess;

    private int selectedStateValue;
    private String selectedStateName;

    private List<AllStateDocResponse> allStateDocResponseList;


    public AllStateDocuments() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //  Log.d(TAG, "inside onCreate");
        mActivity = getActivity();
        mContext = getContext();
        mApiService = MyApplication.getRetrofit().create(getAllStateDocumentService.class);
        builder = new AlertDialog.Builder(mActivity);
        progressDialog = new ProgressDialog(mActivity);
        loginStateCodess = SharedPreferenceUtil.getLoginStateCode(mContext);
      //  Log.d(TAG, "loginStateCode : " + loginStateCode);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_state_document, container, false);

        tvStateName = (TextView) view.findViewById(R.id.tv_StateName);
        tvReceived = (TextView) view.findViewById(R.id.tv_Received);
        tvReceivedAmt = (TextView) view.findViewById(R.id.tv_ReceivedAmt);
        tvApproved = (TextView) view.findViewById(R.id.tv_Approved);
        tvApprovedAmt = (TextView) view.findViewById(R.id.tv_ApprovedAmt);
        tvPending = (TextView) view.findViewById(R.id.tv_Pending);
        tvPendingAmt = (TextView) view.findViewById(R.id.tv_PendingAmt);
        tvClaimsPending = (TextView) view.findViewById(R.id.tv_ClaimsPending);
        tvBeyondAmt = (TextView) view.findViewById(R.id.tv_BeyondAmt);
        tvClaimsPeningForeign = (TextView) view.findViewById(R.id.tv_ClaimsPendingForeign);
        tvBeyondForeignAmt = (TextView) view.findViewById(R.id.tv_BeyondForeignAmt);
        tvClaimsRejected = (TextView) view.findViewById(R.id.tv_ClaimsRejected);
        tvRejectedAmt = (TextView) view.findViewById(R.id.tv_RejectedAmt);
        tvClaimsPaid = (TextView) view.findViewById(R.id.tv_ClaimsPaid);
        tvPaidAmt = (TextView) view.findViewById(R.id.tv_PaidAmt);
        tvPendingFloatGeneration = (TextView) view.findViewById(R.id.tv_PendingFloatGeneration);
        tvPendingFloatGenAmt = (TextView) view.findViewById(R.id.tv_PendingFloatGenAmt);

        Bundle bundle = this.getArguments();
        selectedStateValue = bundle.getInt("stateCode");
        selectedStateName = bundle.getString("stateName");

       // Log.d(TAG, "selectedStateValue : " + selectedStateValue );
       // Log.d(TAG, "selectedStateName : " + selectedStateName);
        if (selectedStateName.contains("Select State"))
        {
            tvStateName.setText("ALL STATES");
        }
        else
        {
            tvStateName.setText(selectedStateName);
        }
        getAllStateDocuments(selectedStateValue);

        /*if (getArguments().getInt("temp") == 8) {
            //  Log.d(TAG, "inside 1");
            loginStateCode = 0;
           // tvStateName.setText("ALL STATES");
            getAllStateDocuments(loginStateCodess);
        } else if (getArguments().getInt("temp") == 9) {
            //  Log.d(TAG, "inside 2");
            loginStateCode = 29;
            tvStateName.setText("KARNATAKA");
            getAllStateDocuments(loginStateCode);
        } else if (getArguments().getInt("temp") == 10) {
            //  Log.d(TAG, "inside 3");
            loginStateCode = 22;
            tvStateName.setText("CHHATTISGARH");
            getAllStateDocuments(loginStateCode);
        } else if (getArguments().getInt("temp") == 11) {
            //  Log.d(TAG, "inside 4");
            loginStateCode = 33;
            tvStateName.setText("TAMIL NADU");
            getAllStateDocuments(loginStateCode);
        } else if (getArguments().getInt("temp") == 12) {
            //   Log.d(TAG, "inside 5");
            loginStateCode = 10;
            tvStateName.setText("BIHAR");
            getAllStateDocuments(loginStateCode);
        } else if (getArguments().getInt("temp") == 13) {
            //  Log.d(TAG, "inside 6");
            loginStateCode = 3;
            tvStateName.setText("PUNJAB");
            getAllStateDocuments(loginStateCode);
        } else if (getArguments().getInt("temp") == 14) {
            // Log.d(TAG, "inside 7");
            loginStateCode = 20;
            tvStateName.setText("JHARKHAND");
            getAllStateDocuments(loginStateCode);
        }
        else if (getArguments().getInt("temp") == 16) {
            // Log.d(TAG, "inside 7");
            loginStateCode = 24;
            tvStateName.setText("GUJARAT");
            getAllStateDocuments(loginStateCode);
        }
*/

       // getAllStateDocuments(loginStateCode);

        return view;
    }

    private void getAllStateDocuments(int loginStateCode) {

      //  Log.d(TAG, "inside getAllStateDocuments");
        progressDialog.setMessage("Please Wait....");
        progressDialog.show();
        progressDialog.setCancelable(false);

        NativeGraphInput nativeGraphInput = new NativeGraphInput();
        // nativeGraphInput.setUserName("Ajitha");
        nativeGraphInput.setStateCode(loginStateCode);
     //   Log.d(TAG, nativeGraphInput + "nativegraphinput");

        Call<List<AllStateDocResponse>> call = mApiService.getAllStateDoc(nativeGraphInput);
        call.enqueue(new Callback<List<AllStateDocResponse>>() {
            @Override
            public void onResponse(Call<List<AllStateDocResponse>> call, Response<List<AllStateDocResponse>> response) {
                //   Log.d(TAG, "Result : " + response.body().toString());
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    allStateDocResponseList = response.body();
                  //  Log.d(TAG, "allStateDocResponseList : " + allStateDocResponseList);
                    if (allStateDocResponseList.size() >= 1)
                    {
                        progressDialog.dismiss();
                        for (int i = 0; i < allStateDocResponseList.size(); i++)
                        {
                            tvStateName.setText(allStateDocResponseList.get(i).getStateName());
                            tvReceived.setText(String.valueOf(allStateDocResponseList.get(i).getReceived()));
                            tvReceivedAmt.setText(String.valueOf(allStateDocResponseList.get(i).getReceivedAmt()));
                            tvApproved.setText(String.valueOf(allStateDocResponseList.get(i).getApproved()));
                            tvApprovedAmt.setText(String.valueOf(allStateDocResponseList.get(i).getApprovedAmt()));
                            tvPending.setText(String.valueOf(allStateDocResponseList.get(i).getPending()));
                            tvPendingAmt.setText(String.valueOf(allStateDocResponseList.get(i).getPendingAmt()));
                            tvClaimsPending.setText(String.valueOf(allStateDocResponseList.get(i).getClaimspendingbeyond25days_N_I()));
                            tvBeyondAmt.setText(String.valueOf(allStateDocResponseList.get(i).getBeyond25DaysAmt()));
                            tvClaimsPeningForeign.setText(String.valueOf(allStateDocResponseList.get(i).getClaimspendingbeyond25days_Foreign()));
                            tvBeyondForeignAmt.setText(String.valueOf(allStateDocResponseList.get(i).getBeyond25DaysForeignAmt()));
                            tvClaimsRejected.setText(String.valueOf(allStateDocResponseList.get(i).getClaimsRejectedNos()));
                            tvRejectedAmt.setText(String.valueOf(allStateDocResponseList.get(i).getRejectedAmt()));
                            tvClaimsPaid.setText(String.valueOf(allStateDocResponseList.get(i).getClaimspaidNos()));
                            tvPaidAmt.setText(String.valueOf(allStateDocResponseList.get(i).getPaidAmt()));
                            tvPendingFloatGeneration.setText(String.valueOf(allStateDocResponseList.get(i).getPendingFloatGeneration()));
                            tvPendingFloatGenAmt.setText(String.valueOf(allStateDocResponseList.get(i).getPendingFloatGenerationAmt()));

                            //gotoNextActivity();
                        }
                    } else {
                        //  Log.d(TAG, "userResponse is null");
                        progressDialog.dismiss();
                        showNoDataFound();

                    }

                } else {
                    //  Log.d(TAG, "response is not successfull");
                    progressDialog.dismiss();
                    showNoDataFound();
                }

            }

            @Override
            public void onFailure(Call<List<AllStateDocResponse>> call, Throwable t) {
                //  Log.d(TAG, "Exception " + t.getMessage());
                progressDialog.dismiss();
                showErrorDialog();
            }
        });
    }
    private void showNoDataFound() {

        builder.setCancelable(false);
        builder.setTitle("Message ");
        builder.setMessage("No Data Found !");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //finish();
            }
        });

        builder.show();
    }

    private void showErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Network Failure");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                mActivity.finish();
            }
        });

        builder.show();
    }

    public interface getAllStateDocumentService {


        @POST(UrlConfig.GET_DAILY_REPORT_LIST)
        Call<List<AllStateDocResponse>> getAllStateDoc(@Body NativeGraphInput nativeGraphInput);

    }
}
