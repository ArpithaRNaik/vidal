package com.effiatechnologies.vidalinvestigator.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidplot.xy.XYPlot;
import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.helper.SqliteStatesHelper;
import com.effiatechnologies.vidalinvestigator.model.AllStateDocResponse;
import com.effiatechnologies.vidalinvestigator.model.NativeGraphInput;
import com.effiatechnologies.vidalinvestigator.model.StatesModel;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Administrator on 25-11-2016.
 */

public class ManagementHomeReportsFragment extends Fragment {

  //  private static final String TAG = "managemenehomereportsfragment";

    private AllStateClaimsGraphService mApiService = null;
    private AlertDialog.Builder builder;
    private ProgressDialog progressDialog;

    private FragmentManager mFragmentManager = null;
    private FragmentTransaction mFragmentTransaction = null;

    private Activity mActivity = null;
    private Context mContext = null;

    private TextView tvReceived = null;
    private TextView tvReceivedAmt = null;
    private TextView tvApproved = null;
    private TextView tvApprovedAmt = null;
    private TextView tvPending = null;
    private TextView tvPendingAmt = null;
    private TextView tvClaimsPending = null;
    private TextView tvBeyondAmt = null;
    private TextView tvClaimsPeningForeign = null;
    private TextView tvBeyondForeignAmt = null;
    private TextView tvClaimsRejected = null;
    private TextView tvRejectedAmt = null;
    private TextView tvClaimsPaid = null;
    private TextView tvPaidAmt = null;
    private TextView tvPendingFloatGeneration = null;
    private TextView tvPendingFloatGenAmt = null;

    private List<AllStateDocResponse> allStateDocResponseList;


    private TextView tvCurrentDate = null;
    private TextView tvStateName = null;


    private SwipeRefreshLayout btnRefresh;

    private String UserName;

    private SqliteStatesHelper sqliteStatesHelper;
    private List<StatesModel> statesModelArrayList = new ArrayList<>();
    private String stateNames;
    private int stateCodes;
    private int roleCode;
    private int loginStateCode;
    private String loginStateName;
    private String formattedDate;


    public ManagementHomeReportsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //  Log.d(TAG, "inside onCreate");
        mActivity = getActivity();
        mContext = getContext();
        builder = new AlertDialog.Builder(mContext);
        progressDialog = new ProgressDialog(mContext);
        mApiService = MyApplication.getRetrofit().create(AllStateClaimsGraphService.class);
        UserName = SharedPreferenceUtil.getUserName(mContext);
        roleCode = SharedPreferenceUtil.getRoleCode(mContext);
        // loginStateCode = SharedPreferenceUtil.getLoginStateCode(mContext);
        // loginStateName = SharedPreferenceUtil.getLoginStateName(mContext);
     //   Log.d(TAG, "roleCode : " + roleCode);
     //   Log.d(TAG, "loginStateCode : " + loginStateCode);
     //   Log.d(TAG, "loginStateName : " + loginStateName);
        mFragmentManager = getFragmentManager();


        sqliteStatesHelper = new SqliteStatesHelper(mContext);
        statesModelArrayList = sqliteStatesHelper.getProducts();
        /*if (statesModelArrayList.size() >= 2) {

            //  Log.d(TAG, "inside if");
            for (int i = 0; i < statesModelArrayList.size(); i++) {
                stateNames = statesModelArrayList.get(i).getStateName();
                 Log.d(TAG, "stateNames : " + stateNames);
                stateCodes = statesModelArrayList.get(i).getStateCode();
                Log.d(TAG, "stateCodes : " + stateCodes);

            }
        }*/
        /*if (statesModelArrayList.size() >= 2) {

            Log.d(TAG, "inside if");
            for (int i = 0; i < statesModelArrayList.size(); i++)
            {
                stateNames = statesModelArrayList.get(i).getStateName();
                Log.d(TAG, "stateNames : " + stateNames);
                stateCodes = statesModelArrayList.get(i).getStateCode();
                Log.d(TAG, "stateCodes : " + stateCodes);

                if (stateCodes == 3)
                {
                    Log.d(TAG, "inside state name punjab");

                    nav_Menu.findItem(R.id.nav_punjabGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_punjabDoc).setVisible(true);

                }
                if (stateCodes == 10)
                {
                    Log.d(TAG, "inside state name bihar");

                    nav_Menu.findItem(R.id.nav_biharGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_biharDoc).setVisible(true);

                }
                if (stateCodes == 20)
                {
                    Log.d(TAG, "inside state name JHARKHAND");

                    nav_Menu.findItem(R.id.nav_jharkhandGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_jharkhandDoc).setVisible(true);

                }
                if (stateCodes == 22)
                {
                    Log.d(TAG, "inside state name CHHATTISGARH");

                    nav_Menu.findItem(R.id.nav_chhattisgarhGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_chhattisgarhDoc).setVisible(true);

                }
                if (stateCodes == 29)
                {
                    Log.d(TAG, "inside state name KARNATAKA");

                    nav_Menu.findItem(R.id.nav_karnatakaGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_karnatakaDoc).setVisible(true);

                }
                if (stateCodes == 33)
                {
                    Log.d(TAG, "inside state name tamil nadu");
                    nav_Menu.findItem(R.id.nav_tamilnaduGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_tamilnaduDoc).setVisible(true);

                }
            }
        } else {
            Log.d(TAG, "inside else");
            //showDataNotAvailbale();
        }*/
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        formattedDate = df.format(c.getTime());
     //   Log.d(TAG, "formattedDate : " + formattedDate);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_page_document, container, false);

        //  Log.d(TAG, "inside onCreateView");

        tvStateName = (TextView) view.findViewById(R.id.tv_StateName_Mgnt);
        tvReceived = (TextView) view.findViewById(R.id.tv_Received_Mgnt);
        tvReceivedAmt = (TextView) view.findViewById(R.id.tv_ReceivedAmt_Mgnt);
        tvApproved = (TextView) view.findViewById(R.id.tv_Approved_Mgnt);
        tvApprovedAmt = (TextView) view.findViewById(R.id.tv_ApprovedAmt_Mgnt);
        tvPending = (TextView) view.findViewById(R.id.tv_Pending_Mgnt);
        tvPendingAmt = (TextView) view.findViewById(R.id.tv_PendingAmt_Mgnt);
        tvClaimsPending = (TextView) view.findViewById(R.id.tv_ClaimsPending_Mgnt);
        tvBeyondAmt = (TextView) view.findViewById(R.id.tv_BeyondAmt_Mgnt);
        tvClaimsPeningForeign = (TextView) view.findViewById(R.id.tv_ClaimsPendingForeign_Mgnt);
        tvBeyondForeignAmt = (TextView) view.findViewById(R.id.tv_BeyondForeignAmt_Mgnt);
        tvClaimsRejected = (TextView) view.findViewById(R.id.tv_ClaimsRejected_Mgnt);
        tvRejectedAmt = (TextView) view.findViewById(R.id.tv_RejectedAmt_Mgnt);
        tvClaimsPaid = (TextView) view.findViewById(R.id.tv_ClaimsPaid_Mgnt);
        tvPaidAmt = (TextView) view.findViewById(R.id.tv_PaidAmt_Mgnt);
        tvPendingFloatGeneration = (TextView) view.findViewById(R.id.tv_PendingFloatGeneration_Mgnt);
        tvPendingFloatGenAmt = (TextView) view.findViewById(R.id.tv_PendingFloatGenAmt_Mgnt);

       // getAllStateDocuments(loginStateCode);


        if (getArguments().getInt("temp") == 8) {
          //  Log.d(TAG, "inside 1");
            loginStateCode = 0;
            tvStateName.setText("ALL STATES");
            getAllStateDocuments(loginStateCode);
        } else if (getArguments().getInt("temp") == 9) {
          //  Log.d(TAG, "inside 2");
            loginStateCode = 29;
            tvStateName.setText("KARNATAKA");
            getAllStateDocuments(loginStateCode);
        } else if (getArguments().getInt("temp") == 10) {
          //  Log.d(TAG, "inside 3");
            loginStateCode = 22;
            tvStateName.setText("CHHATTISGARH");
            getAllStateDocuments(loginStateCode);
        } else if (getArguments().getInt("temp") == 11) {
          //  Log.d(TAG, "inside 4");
            loginStateCode = 33;
            tvStateName.setText("TAMIL NADU");
            getAllStateDocuments(loginStateCode);
        } else if (getArguments().getInt("temp") == 12) {
         //   Log.d(TAG, "inside 5");
            loginStateCode = 10;
            tvStateName.setText("BIHAR");
            getAllStateDocuments(loginStateCode);
        } else if (getArguments().getInt("temp") == 13) {
          //  Log.d(TAG, "inside 6");
            loginStateCode = 3;
            tvStateName.setText("PUNJAB");
            getAllStateDocuments(loginStateCode);
        } else if (getArguments().getInt("temp") == 14) {
           // Log.d(TAG, "inside 7");
            loginStateCode = 20;
            tvStateName.setText("JHARKHAND");
            getAllStateDocuments(loginStateCode);
        }

        // getAllStateClaims(loginStateCode);

      /*  btnRefresh.setColorSchemeResources(R.color.colorAccent);
        btnRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // getAllStateClaims(loginStateCode);

                btnRefresh.setRefreshing(false);
            }
        });
*/
        return view;
    }

    private void getAllStateDocuments(int loginStateCode) {

      //  Log.d(TAG, "inside getAllStateDocuments");
        progressDialog.setMessage("Please Wait....");
        progressDialog.show();
        progressDialog.setCancelable(false);

        NativeGraphInput nativeGraphInput = new NativeGraphInput();
        // nativeGraphInput.setUserName("Ajitha");
        nativeGraphInput.setStateCode(loginStateCode);
      //  Log.d(TAG, nativeGraphInput + "nativegraphinput");

        Call<List<AllStateDocResponse>> call = mApiService.getAllStateDoc(nativeGraphInput);
        call.enqueue(new Callback<List<AllStateDocResponse>>() {
            @Override
            public void onResponse(Call<List<AllStateDocResponse>> call, Response<List<AllStateDocResponse>> response) {
              //  Log.d(TAG, "Result : " + response.body());
                if (response.isSuccessful())
                {
                    progressDialog.dismiss();
                    allStateDocResponseList = response.body();
                  //  Log.d(TAG, "allStateDocResponseList : " + allStateDocResponseList);
                    if (allStateDocResponseList.size() >= 1) {
                     //   progressDialog.dismiss();
                        for (int i = 0; i < allStateDocResponseList.size(); i++) {
                            tvStateName.setText(allStateDocResponseList.get(i).getStateName());
                            tvReceived.setText(String.valueOf(allStateDocResponseList.get(i).getReceived()));
                            tvReceivedAmt.setText(String.valueOf(allStateDocResponseList.get(i).getReceivedAmt()));
                            tvApproved.setText(String.valueOf(allStateDocResponseList.get(i).getApproved()));
                            tvApprovedAmt.setText(String.valueOf(allStateDocResponseList.get(i).getApprovedAmt()));
                            tvPending.setText(String.valueOf(allStateDocResponseList.get(i).getPending()));
                            tvPendingAmt.setText(String.valueOf(allStateDocResponseList.get(i).getPendingAmt()));
                            tvClaimsPending.setText(String.valueOf(allStateDocResponseList.get(i).getClaimspendingbeyond25days_N_I()));
                            tvBeyondAmt.setText(String.valueOf(allStateDocResponseList.get(i).getBeyond25DaysAmt()));
                            tvClaimsPeningForeign.setText(String.valueOf(allStateDocResponseList.get(i).getClaimspendingbeyond25days_Foreign()));
                            tvBeyondForeignAmt.setText(String.valueOf(allStateDocResponseList.get(i).getBeyond25DaysForeignAmt()));
                            tvClaimsRejected.setText(String.valueOf(allStateDocResponseList.get(i).getClaimsRejectedNos()));
                            tvRejectedAmt.setText(String.valueOf(allStateDocResponseList.get(i).getRejectedAmt()));
                            tvClaimsPaid.setText(String.valueOf(allStateDocResponseList.get(i).getClaimspaidNos()));
                            tvPaidAmt.setText(String.valueOf(allStateDocResponseList.get(i).getPaidAmt()));
                            tvPendingFloatGeneration.setText(String.valueOf(allStateDocResponseList.get(i).getPendingFloatGeneration()));
                            tvPendingFloatGenAmt.setText(String.valueOf(allStateDocResponseList.get(i).getPendingFloatGenerationAmt()));

                            //gotoNextActivity();
                        }
                    }
                    else {
                      //  Log.d(TAG, "userResponse is null");
                        progressDialog.dismiss();
                        showNoDataFound();
                    }

                }
                else {
                  //  Log.d(TAG, "response is not successfull");
                    progressDialog.dismiss();
                    showNoDataFound();
                }

            }

            @Override
            public void onFailure(Call<List<AllStateDocResponse>> call, Throwable t) {
              //  Log.d(TAG, "Exception " + t.getMessage());
                progressDialog.dismiss();
                showErrorDialog();
            }
        });
    }

    private void showNoDataFound() {

        builder.setCancelable(false);
        builder.setTitle("Message ");
        builder.setMessage("No Data Found !");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                /*mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.dashboard_fragment_container, new ManagementHomeGraphFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();*/
                //finish();
            }
        });

        builder.show();
    }

    private void showErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Network Failure");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                mActivity.finish();
            }
        });

        builder.show();
    }

    public interface AllStateClaimsGraphService {

        @POST(UrlConfig.GET_DAILY_REPORT_LIST)
        Call<List<AllStateDocResponse>> getAllStateDoc(@Body NativeGraphInput nativeGraphInput);

    }
}
