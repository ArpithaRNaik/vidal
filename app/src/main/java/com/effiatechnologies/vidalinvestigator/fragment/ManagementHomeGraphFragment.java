package com.effiatechnologies.vidalinvestigator.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidplot.xy.XYPlot;
import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.helper.SqliteStatesHelper;
import com.effiatechnologies.vidalinvestigator.model.IndividualStatesResponse;
import com.effiatechnologies.vidalinvestigator.model.NativeGraphInput;
import com.effiatechnologies.vidalinvestigator.model.StatesModel;
import com.effiatechnologies.vidalinvestigator.model.fa;
import com.effiatechnologies.vidalinvestigator.model.fg;
import com.effiatechnologies.vidalinvestigator.model.fn;
import com.effiatechnologies.vidalinvestigator.model.inv;
import com.effiatechnologies.vidalinvestigator.model.io;
import com.effiatechnologies.vidalinvestigator.model.nat;
import com.effiatechnologies.vidalinvestigator.model.pcd;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Administrator on 25-11-2016.
 */

public class ManagementHomeGraphFragment extends Fragment {

  //  private static final String TAG = "managemenehomegraphfragment";

    private AllStateClaimsGraphService mApiService = null;
    private AlertDialog.Builder builder;
    private ProgressDialog progressDialog;

    private Activity mActivity = null;
    private Context mContext = null;

    private TextView tvCurrentDate = null;
    private TextView tvStateName = null;
    private BarChart verticalbarchart;
    private BarChart horizontalbarChart;
    private BarChart foreignclaimhorbarChart;
    private BarChart floatgenerationhorbarChart;
    private PieChart floatapprovalpieChart;
    private PieChart investigationcasespieChart;
    private PieChart paymentCountdetailspieChart;
    private PieChart pendingInvestigationPieChart;
    private PieChart pendingIndividualInvestigationPieChart;

    private BarData verticalbarData;
    private BarData horizontalbarData;
    private BarData foreignclaimshorizontalbarData;
    private BarData floatgenerationhorizontalbarData;

    private PieData floatapprovalpiedata;
    private PieData investigationcasespiedata;
    private PieData paymentcountdetailspiedata;


    private ArrayList<BarEntry> bargroup1;
    private ArrayList<BarEntry> bargroup2;
    private ArrayList<BarEntry> bargroup3;
    private ArrayList<BarEntry> bargroup4;
    private ArrayList<Entry> piechart1;
    private ArrayList<Entry> piechart2;
    private ArrayList<Entry> piechart3;


    private BarDataSet barDataSet1;
    private BarDataSet barDataSet2;
    private BarDataSet barDataSet3;
    private BarDataSet barDataSet4;

    private PieDataSet pieDataSet1;
    private PieDataSet pieDataSet2;
    private PieDataSet pieDataSet3;


    private ArrayList<BarDataSet> dataSets;
    private ArrayList<BarDataSet> dataSets2;
    private ArrayList<BarDataSet> dataSets3;
    private ArrayList<BarDataSet> dataSets4;

    private int nativeReceived;
    private int nativeApproved;
    private int nativePending;
    private int nativeRejected;

    private int ioReceived;
    private int ioApproved;
    private int ioPending;
    private int ioRejected;

    private int foreignReceived;
    private int foreignApproved;
    private int foreignPending;
    private int foreignRejected;

    private int floatGenGenerated;
    private int floatGenPending;

    private int floatApprovalApproved;
    private int floatApprovalPending;

    private int investigationCompleted;
    private int investigationPending;

    private int paymentMadeToHospital;
    private int paymentPendingToHospital;

    private SwipeRefreshLayout btnRefresh;

    private TextView tvNoDataFound;
    private TextView tvNoDataFoundFloatApproval;
    private TextView tvNoDataFoundPaymentCountDetails;
    private TextView tvNoDataFoundPendingInvestigationCases;
    private TextView tvNoDataFoundIndiPendingInvestigationCases;

    private LinearLayout paymentCountDetailsLayout;
    private LinearLayout investigationCasesLayout;
    private LinearLayout floatApprovalLayout;
    private LinearLayout floatGenerationLayout;
    private LinearLayout foreignReceivableClaimsLayout;
    private LinearLayout ioClaimsLayout;
    private LinearLayout nativeClaimsLayout;
    private LinearLayout pendingInvestigationCasesLayout;
    private LinearLayout pendingIndividualInvestigationCasesLayout;

    private XYPlot plot;
    private String UserName;

    private SqliteStatesHelper sqliteStatesHelper;
    private List<StatesModel> statesModelArrayList = new ArrayList<>();
    private String stateNames;
    private int stateCodes;
    private int roleCode;
    private int loginStateCode;
    private String loginStateName;
    private String formattedDate;


    public ManagementHomeGraphFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // Log.d(TAG, "inside onCreate");
        mActivity = getActivity();
        mContext = getContext();
        builder = new AlertDialog.Builder(mContext);
        progressDialog = new ProgressDialog(mContext);
        mApiService = MyApplication.getRetrofit().create(AllStateClaimsGraphService.class);
        UserName = SharedPreferenceUtil.getUserName(mContext);
        roleCode = SharedPreferenceUtil.getRoleCode(mContext);
       // loginStateCode = SharedPreferenceUtil.getLoginStateCode(mContext);
       // loginStateName = SharedPreferenceUtil.getLoginStateName(mContext);
      //  Log.d(TAG, "roleCode : " + roleCode);
     //   Log.d(TAG, "loginStateCode : " + loginStateCode);
     //   Log.d(TAG, "loginStateName : " + loginStateName);

        sqliteStatesHelper = new SqliteStatesHelper(mContext);
        statesModelArrayList = sqliteStatesHelper.getProducts();
        /*if (statesModelArrayList.size() >= 2) {

            //  Log.d(TAG, "inside if");
            for (int i = 0; i < statesModelArrayList.size(); i++) {
                stateNames = statesModelArrayList.get(i).getStateName();
                 Log.d(TAG, "stateNames : " + stateNames);
                stateCodes = statesModelArrayList.get(i).getStateCode();
                Log.d(TAG, "stateCodes : " + stateCodes);

            }
        }*/
        /*if (statesModelArrayList.size() >= 2) {

            Log.d(TAG, "inside if");
            for (int i = 0; i < statesModelArrayList.size(); i++)
            {
                stateNames = statesModelArrayList.get(i).getStateName();
                Log.d(TAG, "stateNames : " + stateNames);
                stateCodes = statesModelArrayList.get(i).getStateCode();
                Log.d(TAG, "stateCodes : " + stateCodes);

                if (stateCodes == 3)
                {
                    Log.d(TAG, "inside state name punjab");

                    nav_Menu.findItem(R.id.nav_punjabGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_punjabDoc).setVisible(true);

                }
                if (stateCodes == 10)
                {
                    Log.d(TAG, "inside state name bihar");

                    nav_Menu.findItem(R.id.nav_biharGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_biharDoc).setVisible(true);

                }
                if (stateCodes == 20)
                {
                    Log.d(TAG, "inside state name JHARKHAND");

                    nav_Menu.findItem(R.id.nav_jharkhandGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_jharkhandDoc).setVisible(true);

                }
                if (stateCodes == 22)
                {
                    Log.d(TAG, "inside state name CHHATTISGARH");

                    nav_Menu.findItem(R.id.nav_chhattisgarhGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_chhattisgarhDoc).setVisible(true);

                }
                if (stateCodes == 29)
                {
                    Log.d(TAG, "inside state name KARNATAKA");

                    nav_Menu.findItem(R.id.nav_karnatakaGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_karnatakaDoc).setVisible(true);

                }
                if (stateCodes == 33)
                {
                    Log.d(TAG, "inside state name tamil nadu");
                    nav_Menu.findItem(R.id.nav_tamilnaduGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_tamilnaduDoc).setVisible(true);

                }
            }
        } else {
            Log.d(TAG, "inside else");
            //showDataNotAvailbale();
        }*/
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        formattedDate = df.format(c.getTime());
       //  Log.d(TAG, "formattedDate : " + formattedDate);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_management_home_graph, container, false);

      //  Log.d(TAG, "inside onCreateView");

        tvCurrentDate = (TextView) view.findViewById(R.id.tv_CurrentDateManagement);
        tvStateName = (TextView) view.findViewById(R.id.tv_stateNameManagement);
        verticalbarchart = (BarChart) view.findViewById(R.id.verticalchartMgnt);
        horizontalbarChart = (BarChart) view.findViewById(R.id.barchartMgnt);
        foreignclaimhorbarChart = (BarChart) view.findViewById(R.id.foreignclaimshorizontalbarchartMgnt);
        floatgenerationhorbarChart = (BarChart) view.findViewById(R.id.floatgeneraionhorizontalbarchartMgnt);
        floatapprovalpieChart = (PieChart) view.findViewById(R.id.floatApprovalpiechartMgnt);
        investigationcasespieChart = (PieChart) view.findViewById(R.id.investigationcasespiechartMgnt);
        paymentCountdetailspieChart = (PieChart) view.findViewById(R.id.paymentcountdetailspiechartMgnt);
      //  btnRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefreshMgnt);
        tvNoDataFound = (TextView) view.findViewById(R.id.tv_NoDataFoundForInvCaseMgnt);
        tvNoDataFoundFloatApproval = (TextView) view.findViewById(R.id.tv_NoDataFoundForFloatApprovalMgnt);
        tvNoDataFoundPaymentCountDetails = (TextView) view.findViewById(R.id.tv_NoDataFoundForPaymentCountDetailsMgnt);

        paymentCountDetailsLayout = (LinearLayout) view.findViewById(R.id.layout_PaymentCountDetailsMgnt);
        investigationCasesLayout = (LinearLayout) view.findViewById(R.id.layout_InvestigationCasesMgnt);
        floatApprovalLayout = (LinearLayout) view.findViewById(R.id.layout_FloatApprovalMgnt);
        floatGenerationLayout = (LinearLayout) view.findViewById(R.id.layout_FloatGenerationMgnt);
        foreignReceivableClaimsLayout = (LinearLayout) view.findViewById(R.id.layout_ForeignReceivableClaimsMgnt);
        ioClaimsLayout = (LinearLayout) view.findViewById(R.id.layout_IOClaimsMgnt);
        nativeClaimsLayout = (LinearLayout) view.findViewById(R.id.layout_NativeClaimsMgnt);

        tvCurrentDate.setText(formattedDate);

        if (getArguments().getInt("temp") == 0)
        {
         //   Log.d(TAG, "inside 0");
            loginStateCode = 0;
            tvStateName.setText("ALL STATES");
            getAllStateClaims(loginStateCode);
        }
        if (getArguments().getInt("temp") == 1)
        {
          //  Log.d(TAG, "inside 1");
            loginStateCode = 0;
            tvStateName.setText("ALL STATES");
            getAllStateClaims(loginStateCode);
        }
        else if (getArguments().getInt("temp") == 2)
        {
          //  Log.d(TAG, "inside 2");
            loginStateCode = 29;
            tvStateName.setText("KARNATAKA");
            getAllStateClaims(loginStateCode);
        }
        else if (getArguments().getInt("temp") == 3)
        {
         //   Log.d(TAG, "inside 3");
            loginStateCode = 22;
            tvStateName.setText("CHHATTISGARH");
            getAllStateClaims(loginStateCode);
        }
        else if (getArguments().getInt("temp") == 4)
        {
           // Log.d(TAG, "inside 4");
            loginStateCode = 20;
            tvStateName.setText("JHARKHAND");
            getAllStateClaims(loginStateCode);
        }
        else if (getArguments().getInt("temp") == 5)
        {
          //  Log.d(TAG, "inside 5");
            loginStateCode = 33;
            tvStateName.setText("TAMIL NADU");
            getAllStateClaims(loginStateCode);
        }
        else if (getArguments().getInt("temp") == 6)
        {
          //  Log.d(TAG, "inside 6");
            loginStateCode = 10;
            tvStateName.setText("BIHAR");
            getAllStateClaims(loginStateCode);
        }
        else if (getArguments().getInt("temp") == 7)
        {
           // Log.d(TAG, "inside 7");
            loginStateCode = 3;
            tvStateName.setText("PUNJAB");
            getAllStateClaims(loginStateCode);
        }

       // getAllStateClaims(loginStateCode);

       /* btnRefresh.setColorSchemeResources(R.color.colorAccent);
        btnRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               // getAllStateClaims(loginStateCode);

                btnRefresh.setRefreshing(false);
            }
        });*/

        return view;
    }


    private void getPaymentCountDetailsPieChart() {

       // Log.d(TAG, "inside getPaymentCountDetailsPieChart");
        piechart3 = new ArrayList<>();
        piechart3.add(new Entry(paymentMadeToHospital, 0));
        piechart3.add(new Entry(paymentPendingToHospital, 1));


        pieDataSet3 = new PieDataSet(piechart3, "Payment Count Details");
      //  pieDataSet3.setColors(new int[]{R.color.bar_Received, R.color.bar_Approved}, mContext);
        pieDataSet3.setColors(new int[]{R.color.completed_color, R.color.pendingpiechart_color}, mContext);


        //pieDataSet3.setColors(ColorTemplate.JOYFUL_COLORS);

        paymentcountdetailspiedata = new PieData(getPaymentCountValues(), pieDataSet3); // initialize Piedata<br />
        paymentcountdetailspiedata.setValueTextSize(13f);

        paymentCountdetailspieChart.setData(paymentcountdetailspiedata);
        //paymentCountdetailspieChart.setDescription("");
        //paymentCountdetailspieChart.animateXY(3000, 3000);
        paymentCountdetailspieChart.animateY(3000, Easing.EasingOption.EaseInExpo);
        paymentCountdetailspieChart.invalidate();
        paymentCountdetailspieChart.setDescription("");
        paymentCountdetailspieChart.setClickable(false);
        paymentCountdetailspieChart.getLegend().setEnabled(false);
    }

    private void getInvestigationCasesPieChart() {

      //  Log.d(TAG, "inside getInvestigationCasesPieChart");
        piechart2 = new ArrayList<>();
        piechart2.add(new Entry(investigationCompleted, 0));
        piechart2.add(new Entry(investigationPending, 1));


        pieDataSet2 = new PieDataSet(piechart2, "inv Cases");
        pieDataSet2.setColors(new int[]{R.color.completed_color, R.color.pendingpiechart_color}, mContext);

        //pieDataSet2.setColors(ColorTemplate.JOYFUL_COLORS);

        investigationcasespiedata = new PieData(getInvestigationCaseValues(), pieDataSet2); // initialize Piedata<br />
        investigationcasespiedata.setValueTextSize(13f);

        investigationcasespieChart.setData(investigationcasespiedata);
        //investigationcasespieChart.setDescription("");
       // investigationcasespieChart.animateXY(3000, 3000);
        investigationcasespieChart.animateY(3000, Easing.EasingOption.EaseInOutBounce);
        investigationcasespieChart.invalidate();
        investigationcasespieChart.setDescription("");
        investigationcasespieChart.setClickable(false);
        investigationcasespieChart.getLegend().setEnabled(false);
    }

    private void getFloatApprovalPieChart() {

       // Log.d(TAG, "inside getFloatApprovalPieChart");

        piechart1 = new ArrayList<>();
        piechart1.add(new Entry(floatApprovalApproved, 0));
        piechart1.add(new Entry(floatApprovalPending, 1));


        pieDataSet1 = new PieDataSet(piechart1, "Float Approval");
      //  pieDataSet1.setColors(new int[]{R.color.bar_Received, R.color.bar_Approved}, mContext);
        pieDataSet1.setColors(new int[]{R.color.completed_color, R.color.pendingpiechart_color}, mContext);



        //pieDataSet1.setColors(ColorTemplate.PASTEL_COLORS);

        floatapprovalpiedata = new PieData(getFloatApprovalValues(), pieDataSet1); // initialize Piedata<br />
        floatapprovalpiedata.setValueTextSize(13f);

        floatapprovalpieChart.setData(floatapprovalpiedata);
        //floatapprovalpieChart.setDescription("");
      //  floatapprovalpieChart.animateXY(3000, 3000);
        floatapprovalpieChart.animateY(3000, Easing.EasingOption.EaseInExpo);

        floatapprovalpieChart.invalidate();
        floatapprovalpieChart.setDescription("");
        floatapprovalpieChart.setClickable(false);
        floatapprovalpieChart.getLegend().setEnabled(false);
    }

    private void getFloatGenerationBarGraph() {

       // Log.d(TAG, "inside getFloatGenerationBarGraph");

        bargroup4 = new ArrayList<>();
        bargroup4.add(new BarEntry(floatGenGenerated, 0));
        bargroup4.add(new BarEntry(floatGenPending, 1));

        barDataSet4 = new BarDataSet(bargroup4, "Float Generation");
       // barDataSet4.setColors(new int[]{R.color.bar_Received, R.color.bar_Approved}, mContext);
        barDataSet4.setColors(new int[]{R.color.completed_color, R.color.pendingpiechart_color}, mContext);


        //barDataSet4.setColors(ColorTemplate.VORDIPLOM_COLORS);
        barDataSet4.setBarSpacePercent(75f);
        barDataSet4.setValueTextSize(10f);

        dataSets4 = new ArrayList<>();  // combined all dataset into an arraylist
        dataSets4.add(barDataSet4);

        floatgenerationhorizontalbarData = new BarData(getFloatGenerationValues(), dataSets4);
        floatgenerationhorbarChart.setData(floatgenerationhorizontalbarData); // set the data and list of labels into chart
       // floatgenerationhorbarChart.animateY(2000);
       // floatgenerationhorbarChart.animateXY(3000, 3000);
        floatgenerationhorbarChart.animateY(3000, Easing.EasingOption.EaseOutQuad);
        floatgenerationhorbarChart.invalidate();
        floatgenerationhorbarChart.setDescription("");
        floatgenerationhorbarChart.setClickable(false);
        floatgenerationhorbarChart.getAxisLeft().setDrawLabels(false);
        floatgenerationhorbarChart.getXAxis().setDrawLabels(false);
        floatgenerationhorbarChart.getLegend().setEnabled(false);
    }

    private void getForeignClaimsBarGraph() {

      //  Log.d(TAG, "inside getForeignClaimsBarGraph");

        bargroup3 = new ArrayList<>();
        bargroup3.add(new BarEntry(foreignApproved, 0));
        bargroup3.add(new BarEntry(foreignPending, 1));
        bargroup3.add(new BarEntry(foreignRejected, 2));
        bargroup3.add(new BarEntry(foreignReceived, 3));

        barDataSet3 = new BarDataSet(bargroup3, "Foreign Claims");
      //  barDataSet3.setColors(new int[]{R.color.bar_Received, R.color.bar_Approved, R.color.bar_Pending, R.color.bar_Rejected}, mContext);
        barDataSet3.setColors(new int[]{R.color.received_color, R.color.approved_color, R.color.completed_color, R.color.pendingpiechart_color}, mContext);

        //barDataSet3.setColors(ColorTemplate.COLORFUL_COLORS);
        barDataSet3.setBarSpacePercent(55f);
        barDataSet3.setValueTextSize(10f);

        dataSets3 = new ArrayList<>();  // combined all dataset into an arraylist
        dataSets3.add(barDataSet3);

        foreignclaimshorizontalbarData = new BarData(getForeignClaimsValues(), dataSets3);
        foreignclaimhorbarChart.setData(foreignclaimshorizontalbarData); // set the data and list of labels into chart
        //foreignclaimhorbarChart.setDescription("Graph 3");  // set the description
        //foreignclaimhorbarChart.animateY(2000);
        //foreignclaimhorbarChart.animateXY(3000, 3000);
        foreignclaimhorbarChart.animateY(3000, Easing.EasingOption.EaseOutBounce);
        foreignclaimhorbarChart.invalidate();
        foreignclaimhorbarChart.setDescription("");
        foreignclaimhorbarChart.setClickable(false);
        foreignclaimhorbarChart.getAxisLeft().setDrawLabels(false);
        foreignclaimhorbarChart.getXAxis().setDrawLabels(false);
        foreignclaimhorbarChart.getLegend().setEnabled(false);

    }

    private void getIOClaimsBarGraph() {

       // Log.d(TAG, "inside getIOClaimsBarGraph");

        // create BarEntry for Bar Group 1
        bargroup1 = new ArrayList<>();
        bargroup1.add(new BarEntry(ioReceived, 0));
        bargroup1.add(new BarEntry(ioApproved, 1));
        bargroup1.add(new BarEntry(ioPending, 2));
        bargroup1.add(new BarEntry(ioRejected, 3));

        barDataSet1 = new BarDataSet(bargroup1, "IO Claims");
      //  barDataSet1.setColors(new int[]{R.color.bar_Received, R.color.bar_Approved, R.color.bar_Pending, R.color.bar_Rejected}, mContext);
        barDataSet1.setColors(new int[]{R.color.received_color, R.color.approved_color, R.color.completed_color, R.color.pendingpiechart_color}, mContext);

        //barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);
        barDataSet1.setBarSpacePercent(55f);
        barDataSet1.setValueTextSize(10f);

        dataSets = new ArrayList<>();  // combined all dataset into an arraylist
        dataSets.add(barDataSet1);

        horizontalbarData = new BarData(getForeignClaimsValues(), dataSets);
        horizontalbarChart.setData(horizontalbarData); // set the data and list of labels into chart
        //horizontalbarChart.setDescription("Graph 2");  // set the description
        // horizontalbarChart.animateY(2000);
        //horizontalbarChart.animateY(2000);
        horizontalbarChart.animateY(3000, Easing.EasingOption.EaseOutElastic);
        horizontalbarChart.invalidate();
        horizontalbarChart.setDescription("");
        horizontalbarChart.setClickable(false);
        horizontalbarChart.getAxisLeft().setDrawLabels(false);
        horizontalbarChart.getXAxis().setDrawLabels(false);
        horizontalbarChart.getLegend().setEnabled(false);

    }

    private void getNativeClaimsBarGraph() {

       // Log.d(TAG, "inside getNativeClaimsBarGraph");

        bargroup2 = new ArrayList<>();
        bargroup2.add(new BarEntry(nativeReceived, 0));
        bargroup2.add(new BarEntry(nativeApproved, 1));
        bargroup2.add(new BarEntry(nativePending, 2));
        bargroup2.add(new BarEntry(nativeRejected, 3));

        barDataSet2 = new BarDataSet(bargroup2, "Native Claims");
      //  barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);
       // barDataSet2.setColors(new int[]{R.color.bar_Received, R.color.bar_Approved, R.color.bar_Pending, R.color.bar_Rejected}, mContext);

        barDataSet2.setColors(new int[]{R.color.received_color, R.color.approved_color, R.color.completed_color, R.color.pendingpiechart_color}, mContext);
        //barDataSet2.setColors(ColorTemplate.COLORFUL_COLORS);
        barDataSet2.setBarSpacePercent(55f);
        barDataSet2.setValueTextSize(10f);

        dataSets2 = new ArrayList<>();  // combined all dataset into an arraylist
        dataSets2.add(barDataSet2);

        verticalbarData = new BarData(getForeignClaimsValues(), barDataSet2);
        verticalbarchart.setData(verticalbarData);

        //verticalbarchart.animateY(3000);
       // verticalbarchart.animateXY(3000, 3000);
        verticalbarchart.animateY(3000, Easing.EasingOption.EaseInOutElastic);
        verticalbarchart.invalidate();
        verticalbarchart.setDescription("");
        verticalbarchart.setClickable(false);
        verticalbarchart.getAxisLeft().setDrawLabels(false);
        verticalbarchart.getXAxis().setDrawLabels(false);
        verticalbarchart.getLegend().setEnabled(false);

    }

    private ArrayList<String> getForeignClaimsValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("Rec");
        xAxis.add("App");
        xAxis.add("Pen");
        xAxis.add("Rej");
        return xAxis;
    }

    private ArrayList<String> getPaymentCountValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("Payment Made To Hospital");
        xAxis.add("Payment Pending To Hospital");

        return xAxis;
    }

    private ArrayList<String> getInvestigationCaseValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("Completed");
        xAxis.add("Pending");

        return xAxis;
    }

    private ArrayList<String> getFloatApprovalValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("Approved");
        xAxis.add("Pending");

        return xAxis;
    }

    private ArrayList<String> getFloatGenerationValues() {
        ArrayList<String> xAxis = new ArrayList<>();
        xAxis.add("Generated");
        xAxis.add("Pending");

        return xAxis;
    }

    private void getAllStateClaims(int loginStateCode) {
      //  Log.d(TAG, "inside getAllStateClaims");

        progressDialog.setMessage("Please Wait, Loading Data");
        progressDialog.show();
        progressDialog.setCancelable(false);

        NativeGraphInput nativeGraphInput = new NativeGraphInput();
        nativeGraphInput.setUserName(UserName);
        nativeGraphInput.setStateCode(loginStateCode);
       // nativeGraphInput.setStateCode(22);
      //  Log.d(TAG, nativeGraphInput + "nativegraphinput");

        Call<IndividualStatesResponse> call = mApiService.getNativeGraphValues(nativeGraphInput);
        call.enqueue(new Callback<IndividualStatesResponse>() {
            @Override
            public void onResponse(Call<IndividualStatesResponse> call, Response<IndividualStatesResponse> response) {
                if (response.isSuccessful()) {
                    IndividualStatesResponse userType = response.body();
                   // Log.d(TAG, userType + "");
                    if (userType != null) {
                      //  Log.d(TAG, "Result : " + response.body().toString() + "result");
                        progressDialog.dismiss();

                        nat nat = userType.getNat();
                        nativeApproved = nat.getApproved();
                        nativePending = nat.getPending();
                        nativeReceived = nat.getReceived();
                        nativeRejected = nat.getRejected();
                        getNativeClaimsBarGraph();


                        io io = userType.getIo();
                        ioApproved = io.getApproved();
                        ioPending = io.getPending();
                        ioReceived = io.getReceived();
                        ioRejected = io.getRejected();
                        getIOClaimsBarGraph();


                        fn fn = userType.getFn();
                        foreignReceived = fn.getApproved();
                        foreignApproved = fn.getPending();
                        foreignPending = fn.getReceived();
                        foreignRejected = fn.getRejected();
                        getForeignClaimsBarGraph();

                        inv investigation = userType.getInvestigation();
                      //  Log.d(TAG, investigation + "");
                        investigationCompleted = investigation.getCompleted();
                        investigationPending = investigation.getPending();
                        if (investigationCompleted == 0 && investigationPending == 0) {
                            tvNoDataFound.setVisibility(View.VISIBLE);
                        } else {
                            getInvestigationCasesPieChart();
                        }

                        fg foreigngen = userType.getForeigngenclaims();
                      //  Log.d(TAG, foreigngen + "");
                        floatGenGenerated = foreigngen.getGenerated();
                        floatGenPending = foreigngen.getPending();
                        getFloatGenerationBarGraph();

                        fa floatapproval = userType.getFloatapproval();
                       // Log.d(TAG, floatapproval + "");
                        floatApprovalApproved = floatapproval.getApproved();
                        floatApprovalPending = floatapproval.getPending();

                        if (floatApprovalApproved == 0 && floatApprovalPending == 0) {
                            tvNoDataFoundFloatApproval.setVisibility(View.VISIBLE);
                        } else {
                            getFloatApprovalPieChart();

                        }

                        pcd paymentcount = userType.getPaymentcount();
                      //  Log.d(TAG, paymentcount + "");
                        paymentMadeToHospital = paymentcount.getPaymentMadeToHospital();
                        paymentPendingToHospital = paymentcount.getPaymentReceivedFromIC();
                        if (paymentMadeToHospital == 0 && paymentPendingToHospital == 0) {
                            tvNoDataFoundPaymentCountDetails.setVisibility(View.VISIBLE);
                        } else {
                            getPaymentCountDetailsPieChart();
                        }


                    } else {
                     //   Log.d(TAG, userType + "usertype is null");
                        progressDialog.dismiss();
                        showDataNotAvailbale();
                    }
                } else {
                 //   Log.d(TAG, "response is not successfull");
                    progressDialog.dismiss();
                    showDataNotAvailbale();
                }

            }

            @Override
            public void onFailure(Call<IndividualStatesResponse> call, Throwable t) {
               // Log.d(TAG, "Exception " + t.getMessage());
                progressDialog.dismiss();
                showErrorDialog();
            }
        });
      //  progressDialog.dismiss();
    }
    private void showDataNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Message ");
        builder.setMessage("Data Not Found!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }

    public interface AllStateClaimsGraphService {

        @POST(UrlConfig.GET_NATIVE_GRAPH_LIST)
        Call<IndividualStatesResponse> getNativeGraphValues(@Body NativeGraphInput nativeGraphInput);

    }

    private void showErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Network Failure");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

              //  mActivity.finish();
            }
        });

        builder.show();
    }
}
