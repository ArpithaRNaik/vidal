package com.effiatechnologies.vidalinvestigator.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;


/**
 * Created by Administrator on 05-01-2017.
 */

public class AdminHomeFragment extends Fragment {

   // private static final String TAG = "adminhomepage";

    //private AdminHomeFragment.AdminHomePageServices mApiService = null;
    private AlertDialog.Builder builder;
    private ProgressDialog progressDialog;

    private Activity mActivity = null;
    private Context mContext = null;

    private FragmentManager mFragmentManager = null;
    private FragmentTransaction mFragmentTransaction = null;
    private String userDisplayName;
    private String userRoleDescription;
    private TextView tvFacultyName;
    private Button btnAddQuestion;
    private Button btnUpdateQuestion;

    public AdminHomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       // Log.d(TAG, "inside onCreate");
        mActivity = getActivity();
        mContext = getContext();
        builder = new AlertDialog.Builder(mContext);
        progressDialog = new ProgressDialog(mContext);
        mFragmentManager = getFragmentManager();
        userDisplayName = SharedPreferenceUtil.getUserName(mContext);

        /*Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                Intent i = new Intent(mContext,  LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                mActivity.finish();
                return;
            }
        }, 300000);*/

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_home_page, container, false);
      //  Log.d(TAG, "inside onCreateView");
        tvFacultyName = (TextView) view.findViewById(R.id.tv_Heading);
        tvFacultyName.setText("WELCOME" + " " + userDisplayName);

        btnAddQuestion = (Button) view.findViewById(R.id.btn_AddQuestion);
        btnUpdateQuestion = (Button) view.findViewById(R.id.btn_UpdateQuestion);

        btnAddQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.admin_dashboard_fragment_container, new AdminAddQuestionFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
            }
        });

        btnUpdateQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.admin_dashboard_fragment_container, new AdminUpdateQuestionFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
            }
        });


        return view;
    }
}
