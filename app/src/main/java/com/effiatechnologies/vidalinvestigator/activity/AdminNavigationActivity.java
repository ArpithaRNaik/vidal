package com.effiatechnologies.vidalinvestigator.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.fragment.AdminAddQuestionFragment;
import com.effiatechnologies.vidalinvestigator.fragment.AdminHomeFragment;
import com.effiatechnologies.vidalinvestigator.fragment.AdminUpdateQuestionFragment;
import com.effiatechnologies.vidalinvestigator.helper.SqliteStatesHelper;
import com.effiatechnologies.vidalinvestigator.model.StatesModel;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;

import retrofit2.Call;
import retrofit2.http.GET;

public class AdminNavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

  //  private static final String TAG = "admindashboard";
    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;

    private Toolbar facToolbar;
    private DrawerLayout facDrawer;
    private ActionBarDrawerToggle facToggle;
    private NavigationView facNavigationView;

    private FragmentManager mFragmentManager = null;
    private FragmentTransaction mFragmentTransaction = null;

    private TextView tvFacultyName = null;
    private TextView tvFacultyDesignation = null;
    private ImageView imgFacultyImage = null;
    private Spinner spinnerFaculty = null;

    // private SQLiteHelper SQLITEHELPER = null;
    private SQLiteDatabase SQLITEDATABASE;


    private long lastActivity;
    private long now;
    private Date date;
    private int hours;
    private int minutes;
    private int seconds;
    private Timer timer;
    private int count = 0;

    private String userDisplayName;
    private String userRoleDescription;
    private Handler myHandler;
    private Runnable myRunnable;
    private SqliteStatesHelper db;
    private List<StatesModel> productSpinnerList = new ArrayList<>();
    private int departmentID;
    private String departmentName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_navigation);
        // Log.d(TAG, "inside oncreate");


        facToolbar = (Toolbar) findViewById(R.id.toolbar_admin);
        setSupportActionBar(facToolbar);

        facDrawer = (DrawerLayout) findViewById(R.id.drawer_layout_admin);
        facToggle = new ActionBarDrawerToggle(
                this, facDrawer, facToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        facDrawer.setDrawerListener(facToggle);
        facToggle.syncState();

        facNavigationView = (NavigationView) findViewById(R.id.nav_view_admin);
        facNavigationView.setNavigationItemSelectedListener(this);

        userDisplayName = SharedPreferenceUtil.getUserName(AdminNavigationActivity.this);

       // Log.d(TAG, "user display name : " + userDisplayName);
        // Log.d(TAG, "user role description : " + userRoleDescription);


        setTitle("eClaims");
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.admin_dashboard_fragment_container, new AdminHomeFragment());
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();
        // Log.d(TAG, "activity is replaced with fragment ");

        View view = LayoutInflater.from(this).inflate(R.layout.nav_header_admin_navigation, facNavigationView);

        tvFacultyName = (TextView) view.findViewById(R.id.tv_faculty);
        imgFacultyImage = (ImageView) view.findViewById(R.id.image_faculty);
      //  spinnerFaculty = (Spinner) view.findViewById(R.id.spinner_facDepType);

        tvFacultyName.setText(userDisplayName);

        db = new SqliteStatesHelper(this);
        productSpinnerList = db.getProducts();
        //   Log.d(TAG, "productList : " + productSpinnerList);


    }

    @Override
    public void onBackPressed() {

        if (facDrawer.isDrawerOpen(GravityCompat.START)) {
            facDrawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fm = getSupportFragmentManager(); // or 'getSupportFragmentManager();'
            int count = fm.getBackStackEntryCount();
            facToolbar.setTitle("eClaims");
            //  Log.d(TAG, count + "");
            if (count == 0) {
                // Log.d(TAG, "inside if");
                //super.onBackPressed();
                new AlertDialog.Builder(this)
                        .setTitle("Close App?")
                        .setMessage("Do you really want to close this app?")
                        .setPositiveButton("YES",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //  Log.d(TAG, "inside yes");
                                        finish();
                                        Intent intent = new Intent(Intent.ACTION_MAIN);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addCategory(Intent.CATEGORY_HOME);
                                        startActivity(intent);
                                    }
                                })
                        .setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }).show();
            } else if (count == 1) {
                // Log.d(TAG, "inside else if ");
                AppUtil.toastMessage(AdminNavigationActivity.this, "Press the back button once again to close the application...");
                for (int i = 0; i < count; ++i) {
                    //  Log.d(TAG, "inside for loop");
                    fm.popBackStack();
                }
            } else {
                // Log.d(TAG, "inside else");
                for (int i = 0; i < count; ++i) {
                    // Log.d(TAG, "inside for loop");
                    fm.popBackStack();
                }
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard, menu);

        MenuItem menuItem = menu.findItem(R.id.dashboard);
        // menuItem.setIcon(buildCounterDrawable(count, R.drawable.ic_notifications_off_white_18pt_3x));
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {


            case R.id.dashboard:
                facToolbar.setTitle("eClaims");
                mFragmentManager = getSupportFragmentManager();
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.admin_dashboard_fragment_container, new AdminHomeFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.logout:
                manageLogout();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {


            case R.id.nav_adminDashboard:
                facToolbar.setTitle("eClaims");
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.admin_dashboard_fragment_container, new AdminHomeFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_adminAddQuestion:
                facToolbar.setTitle("Add Questions");
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.admin_dashboard_fragment_container, new AdminAddQuestionFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_adminUpdateQuestion:
                facToolbar.setTitle("Update Questions");
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.admin_dashboard_fragment_container, new AdminUpdateQuestionFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;


        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_admin);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void manageLogout() {
        // Log.d(TAG, "in logout");

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(AdminNavigationActivity.this);
        builder.setTitle("Confirm");
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to Sign Out?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //SharedPreferenceUtil.removeUserName(FacultyNavigationActivity.this);
                RemoveDetails();
                Intent claimsLogoutIntent = new Intent(AdminNavigationActivity.this, LoginActivity.class);
                startActivity(claimsLogoutIntent);
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    /*@Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        RemoveDetails();
        Intent intent = new Intent(FacultyNavigationActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }*/
    private void RemoveDetails() {
        //Log.d(TAG, "inside removedetails");
        SharedPreferenceUtil.removeLoginId(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeIMIENum(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeFcmId(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeUserName(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeOfficeTypeCode(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeDefaultProcessTask(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeAdditionalProcessTask(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeRoleCode(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeLoginStatus(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeUserCode(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeTPACompCode(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeTPACompName(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeTPAStatus(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeLoginCode(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeLoginStateCode(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeLoginStateName(AdminNavigationActivity.this);
        SharedPreferenceUtil.removeDistrictCode(AdminNavigationActivity.this);


    }


    @Override
    protected void onStart() {
        super.onStart();
        // Log.d(TAG, "inside onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Log.d(TAG, "inside onResume");


    }

    @Override
    protected void onPause() {
        super.onPause();
        // Log.d(TAG, "inside onPause");

    }

    @Override
    protected void onStop() {
        super.onStop();
        // Log.d(TAG, "inside onStop");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // RemoveDetails();
        finish();
        // Log.d(TAG, "inside onDestroy");

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //  Log.d(TAG, "inside onRestart");

    }

}
