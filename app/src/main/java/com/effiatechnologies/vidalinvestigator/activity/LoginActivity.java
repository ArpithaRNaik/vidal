package com.effiatechnologies.vidalinvestigator.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.adapter.LoginStateSpinnerAdapter;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.helper.RoleTypesSqliteHelper;
import com.effiatechnologies.vidalinvestigator.helper.SqliteStatesHelper;
import com.effiatechnologies.vidalinvestigator.model.LoginResponse;
import com.effiatechnologies.vidalinvestigator.model.LoginStatesResponse;
import com.effiatechnologies.vidalinvestigator.model.ResponseResultMessage;
import com.effiatechnologies.vidalinvestigator.model.UserInput;
import com.effiatechnologies.vidalinvestigator.model.UserResponse;
import com.effiatechnologies.vidalinvestigator.model.UserValueResponse;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class LoginActivity extends AppCompatActivity {

  //  private static final String TAG = "loginPage";
    private EditText etUserName = null;
    private Spinner spinnerStateNames = null;
    private EditText etPassword = null;
    private CheckBox checkBoxShowPass;
    private EditText etPin = null;
    private Button btnSignIn = null;
    private LinearLayout passwordLayout = null;
    private LinearLayout statesLayout = null;
    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;
    private String userName = null;
    private String passWord = null;
    private String userPin;
    private UserLoginService mApiService = null;
    private UserInput userInput = null;
    private Intent intent = null;
    private String re_userName = null;
    private RoleTypesSqliteHelper roleTypesSqliteHelper = null;
    private List<UserValueResponse> userValueResponseList;
    private UserValueResponse userRoleType;

    private String LoginId;
    private List<LoginStatesResponse> loginStateResponseList;
    private int totalStateCount;
    private LoginStateSpinnerAdapter loginStateSpinnerAdapter;
    private LoginStatesResponse loginStateResponse;
    private int loginStateCode;
    private String loginStateName;

    private List<LoginResponse> loginResponseList;
    private String vi_LoginId = null;
    private String vi_UserName = null;
    private int vi_OfficeTypeCode = 0;
    private String vi_DefaultProcessTask = null;
    private String vi_AdditionalProcessTask = null;
    private int vi_RoleCode = 0;
    private String vi_LoginStatus = null;
    private int vi_UserCode = 0;
    private int vi_TPACompCode = 0;
    private String vi_TPACompName = null;
    private String vi_TPAStatus = null;
    private int vi_LoginCode = 0;
    private int vi_DistCode = 0;

    private SqliteStatesHelper sqliteStatesHelper;

    private String getSimSerialNumber = null;
    private static final int REQUEST_READ_PHONE_STATE = 1;
    private TelephonyManager telemamanger;
    private String imei = null;
    private String m_wlanMacAdd;
    private WifiManager wifiManager;
    private WifiInfo ipAddress = null;

    private List<ResponseResultMessage> responseResultMessageList;
    private String resultMessage;


    @SuppressLint({"WifiManagerLeak", "MissingPermission"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        setContentView(R.layout.activity_login);
        //  Log.d(TAG, "inside onCreate");

        if ((int) Build.VERSION.SDK_INT < 23) {
            //this is a check for build version below 23
            telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (telemamanger != null) {
                getSimSerialNumber = telemamanger.getSimSerialNumber();
                imei = telemamanger.getDeviceId();
                //  AppUtil.toastMessage(this, "sim serial no 1: " + getSimSerialNumber);
                //  AppUtil.toastMessage(this, "imie no 1: " + imei);

                //  Log.d(TAG, getSimSerialNumber + " sim serial number 1");
                //  Log.d(TAG, "imei 1 :" + imei);
            }
            if (imei == null || imei.length() == 0) {
                imei = Settings.Secure.getString(getApplication().getContentResolver(), Settings.Secure.ANDROID_ID);
                //   AppUtil.toastMessage(this, "imie no 11: " + imei);
                //  Log.d(TAG, "imei 2 : " + imei);
            }

        } else {
            //this is a check for build version above 23
            if (ContextCompat.checkSelfPermission(LoginActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(LoginActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE},
                        REQUEST_READ_PHONE_STATE);

                //  Log.d("If permission is not granted",", request for permission");


            } else {
                telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                Log.d("If Permission is granted", "");
                if (telemamanger != null) {
                    getSimSerialNumber = telemamanger.getSimSerialNumber();
                    imei = telemamanger.getDeviceId();
                    //   AppUtil.toastMessage(this, "sim serial no 2 : " + getSimSerialNumber);
                    //   AppUtil.toastMessage(this, "imie no 2: " + imei);
                    //   Log.d(TAG, getSimSerialNumber + " sim serial number 3");
                    //   Log.d(TAG, "imei 3 :" + imei);
                }
                if (imei == null || imei.length() == 0) {
                    imei = Settings.Secure.getString(getApplication().getContentResolver(), Settings.Secure.ANDROID_ID);
                    //  AppUtil.toastMessage(this, "imie no 22: " + imei);
                    //  Log.d(TAG, "imei 4 : " + imei);
                }
            }

        }

        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        ipAddress = wifiManager.getConnectionInfo();
        m_wlanMacAdd = ipAddress.getMacAddress();
        int ip = ipAddress.getIpAddress();
        //  Log.d(TAG, "ip : " + ip);
        //  Log.d(TAG, "ipadrresss : " + ipAddress);
        //  Log.d(TAG, "m_wlanMacAdd : " + m_wlanMacAdd);

        userValueResponseList = new ArrayList<>();
        //  roleTypesSqliteHelper = new RoleTypesSqliteHelper(this);
        // userValueResponseList = roleTypesSqliteHelper.getUserTypeList();
        //  Log.d(TAG, userValueResponseList.toString() + "role response list");

     /*   for (UserValueResponse userRoleType : userValueResponseList) {

            if (userRoleType.getRoleCode() == 6) {
                userRoleType.getRoleName();
                //  Log.d(TAG, userRoleType.getRoleName() + " roll name");
            }

        }*/

        builder = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);
        mApiService = MyApplication.getRetrofit().create(UserLoginService.class);
        sqliteStatesHelper = new SqliteStatesHelper(getApplicationContext());

        etUserName = (EditText) findViewById(R.id.et_UserName);
        spinnerStateNames = (Spinner) findViewById(R.id.spn_LoginStateName);
        etPassword = (EditText) findViewById(R.id.et_Password);
        etPassword.setTransformationMethod(new PasswordTransformationMethod());
        checkBoxShowPass = (CheckBox) findViewById(R.id.checkBox_ShowPass);
        checkBoxShowPass.setText(getString(R.string.label_show_password)); // Hide initially, but prompting "Show Password"
        checkBoxShowPass.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
                if (isChecked) {
                    etPassword.setTransformationMethod(null); // Show password when box checked
                    checkBoxShowPass.setText(getString(R.string.label_hide_password)); // Prompting "Hide Password"
                } else {
                    etPassword.setTransformationMethod(new PasswordTransformationMethod()); // Hide password when box not checked
                    checkBoxShowPass.setText(getString(R.string.label_show_password)); // Prompting "Show Password"
                }
            }
        });
        btnSignIn = (Button) findViewById(R.id.btn_SignIn);
        passwordLayout = (LinearLayout) findViewById(R.id.LayoutPassword);
        statesLayout = (LinearLayout) findViewById(R.id.spinnerLayoutStates);

        //FirebaseMessaging.getInstance().subscribeToTopic("test");
        //FirebaseInstanceId.getInstance().getToken();

        etUserName.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if (!hasFocus) {
                    LoginId = etUserName.getText().toString();
                    validateImeiDetails(LoginId);

                }
            }
        });

        spinnerStateNames.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                {

                    loginStateResponse = (LoginStatesResponse) parent.getItemAtPosition(position);
                    loginStateCode = loginStateResponse.getStateCode();
                    //   Log.d(TAG, "loginStateCode : " + loginStateCode);
                    loginStateName = loginStateResponse.getStateName();
                    //   Log.d(TAG, "loginStateName : " + loginStateName);

                    SharedPreferenceUtil.setLoginStateCode(getApplicationContext(), loginStateCode);
                    SharedPreferenceUtil.setLoginStateName(getApplicationContext(), loginStateName);
                    //  getStuDepartmentType();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

        etUserName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                //  Log.d(TAG, "inside beforeTextChanged");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //  Log.d(TAG, "inside onTextChanged");
                etUserName.setError(null);

            }

            @Override
            public void afterTextChanged(Editable s) {
                //  Log.d(TAG, "inside afterTextChanged");
                /*if (s.length() >= 3) {

                    LoginId = etUserName.getText().toString();
                    Log.d(TAG, "LoginId : " + LoginId);
                    getLoginStateDetails(LoginId);
                }*/
            }
        });


        etPassword.setClickable(true);
       /* etPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginId = etUserName.getText().toString();
                validateImeiDetails(LoginId);
                // getLoginStateDetails(LoginId);
            }

        });*/

       /* etPassword.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                        if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_DONE) {
                            //  Log.d(TAG, "inside hasFocus bnull");
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(etPassword.getWindowToken(), 0);
                            LoginId = etUserName.getText().toString();
                            //  Log.d(TAG, "LoginId : " + LoginId);
                            validateImeiDetails(LoginId);
                            return true;
                        }
                        // the user is done typing.


                        return false; // pass on to other listeners.
                    }
                });*/

        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //   validateImeiDetails(LoginId);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                etPassword.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateUserLogin();
                //gotoNextActivity();

            }
        });

    }

    @SuppressLint("MissingPermission")
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.e("grantResults.length", " " + grantResults.length + " " + grantResults[0]);

        switch (requestCode) {
            case REQUEST_READ_PHONE_STATE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.d("Permission request is accepted", "");
                    telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    if (telemamanger != null) {
                        getSimSerialNumber = telemamanger.getSimSerialNumber();
                        imei = telemamanger.getDeviceId();
                        //  AppUtil.toastMessage(this, "sim serial no 4: " + getSimSerialNumber);
                        //  AppUtil.toastMessage(this, "imie no 4: " + imei);
                        //  Log.d(TAG, getSimSerialNumber + " sim serial number");
                        //  Log.d(TAG, "imei 5 : " + imei );
                    }

                    if (imei == null || imei.length() == 0) {
                        imei = Settings.Secure.getString(getApplication().getContentResolver(), Settings.Secure.ANDROID_ID);
                        //   AppUtil.toastMessage(this, "imie no 44: " + imei);
                        //   Log.d(TAG, "imei 6 : " + imei);

                    }
                } else {

                    Log.d("Permission request is denied", "");

                    boolean should = ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, Manifest.permission.READ_PHONE_STATE);
                    if (should) {
                        new android.app.AlertDialog.Builder(LoginActivity.this)
                                .setTitle("Permission Denied")
                                .setMessage("Without this permission the app will not be able to get IMIE Details.Are you sure you want to deny this permission?")
                            /*.setPositiveButton("RE-TRY", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Log.d("Click of Retry, If permission request is denied",",ask request for permission again");

                                }
                            })*/
                                .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        Log.d("Click of I m sure", ", permission request is denied");

                                    }
                                })

                                .show();

                    } else {
                        Log.d("Click of Never ask again", ", permission request is denied");

                    }
                }
                break;
            }


        }
    }


    private void validateImeiDetails(String LoginId) {

        //  Log.d(TAG, "inside validateImeiDetails ");
        if (AppUtil.isNetworkAvailable(LoginActivity.this)) {

            progressDialog.setMessage("Please Wait....");
            progressDialog.show();
            progressDialog.setCancelable(false);

            SharedPreferenceUtil.setIMIENum(LoginActivity.this, imei);

            userInput = new UserInput();
            userInput.setLoginId(LoginId);
            if (getSimSerialNumber == null) {
                userInput.setSimSerialNumber("null");
            } else {
                userInput.setSimSerialNumber(getSimSerialNumber);
            }
            if (m_wlanMacAdd == null) {
                userInput.setIpAddress("0");

            } else {
                userInput.setIpAddress(m_wlanMacAdd);

            }
            if (imei == null) {
                userInput.setIMEINumber("null");

            } else {
                userInput.setIMEINumber(imei);
            }

            //  Log.d(TAG, "input validateImeiDetails : " + userInput);

            Call<List<ResponseResultMessage>> call = mApiService.validateIMEI(userInput);
            call.enqueue(new Callback<List<ResponseResultMessage>>() {
                @Override
                public void onResponse(Call<List<ResponseResultMessage>> call, Response<List<ResponseResultMessage>> response) {
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        //  Log.d(TAG, "inside successfull validateImeiDetails");
                        responseResultMessageList = response.body();
                        //  Log.d(TAG, "responseResultMessageList : " + responseResultMessageList);
                        int resultCount = responseResultMessageList.size();
                        //   Log.d(TAG, "resultCount : " + resultCount);
                        if (responseResultMessageList.size() >= 1) {

                            //  Log.d(TAG, "inside if 1");
                            // Log.d(TAG, "loginStateResponseList : " + response.body());
                            for (int i = 0; i < responseResultMessageList.size(); i++) {
                                resultMessage = responseResultMessageList.get(i).getResult();
                                progressDialog.dismiss();
                                checkIMEIDetails(resultMessage);
                            }

                        } else {
                            // Log.d(TAG, "inside else 2");
                            progressDialog.dismiss();
                            getLoginStateDetails();
                            showDataNotAvailbale();
                        }

                    } else {

                        // Log.d(TAG, "response is failure 3");
                        progressDialog.dismiss();
                        getLoginStateDetails();
                        showDataNotAvailbale();

                    }
                }

                @Override
                public void onFailure(Call<List<ResponseResultMessage>> call, Throwable t) {


                    // Log.d(TAG, "in failure 4 " + t.getMessage());
                    progressDialog.dismiss();
                    getLoginStateDetails();
                    showErrorDialog();

                }
            });
        } else {
            // AppUtil.toastMessage(mContext, "Check Internet connection");
            showInternetNotAvailbale();
        }
    }

    private void checkIMEIDetails(String resultMessage) {
        //   Log.d(TAG, "resultMessage : " + resultMessage);
        if (resultMessage.equalsIgnoreCase("IMEI Match")) {
            // Log.d(TAG, "inside case 1 ");
            getLoginStateDetails();
        } else if (resultMessage.equalsIgnoreCase("IMEI Mismatch")) {
            // Log.d(TAG, "inside case 2 ");
            // showIMEIMismatchDialog();
            getLoginStateDetails();
        } else if (resultMessage.equalsIgnoreCase("LoginId has been Registered to the Mobile Successfully")) {
            // Log.d(TAG, "inside case 3 ");
            getLoginStateDetails();
        } else {
            showDataNotAvailbale();
        }

    }

    private void getLoginStateDetails() {

        //   Log.d(TAG, "inside getLoginStateDetails ");
        if (AppUtil.isNetworkAvailable(LoginActivity.this)) {

            progressDialog.setMessage("Please Wait....");
            progressDialog.show();
            progressDialog.setCancelable(false);

            userInput = new UserInput();
            userInput.setLoginId(LoginId);

            // Log.d(TAG, "input getLoginStateDetails : " + userInput);

            Call<List<LoginStatesResponse>> call = mApiService.getLoginStateDetails(userInput);
            call.enqueue(new Callback<List<LoginStatesResponse>>() {
                @Override
                public void onResponse(Call<List<LoginStatesResponse>> call, Response<List<LoginStatesResponse>> response) {
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        //  Log.d(TAG, "inside successfull");
                        loginStateResponseList = response.body();
                        // Log.d(TAG, "loginStateResponseList : " + loginStateResponseList);
                        sqliteStatesHelper.addProducts(loginStateResponseList);
                        //  Log.d(TAG, "response from sqlite " + sqliteStatesHelper.getProducts().toString());
                        sqliteStatesHelper.close();

                        totalStateCount = loginStateResponseList.size();
                        // Log.d(TAG, "totalStateCount : " + totalStateCount);
                        if (loginStateResponseList.size() == 1) {
                            //Log.d(TAG, "inside equal to 5");
                            statesLayout.setVisibility(View.GONE);
                            for (int i = 0; i < loginStateResponseList.size(); i++) {
                                loginStateCode = loginStateResponseList.get(i).getStateCode();
                                loginStateName = loginStateResponseList.get(i).getStateName();
                                SharedPreferenceUtil.setLoginStateCode(getApplicationContext(), loginStateCode);
                                SharedPreferenceUtil.setLoginStateName(getApplicationContext(), loginStateName);
                            }

                        } else if (loginStateResponseList.size() > 1) {

                            //  Log.d(TAG, "inside greater than 1");

                            // Log.d(TAG, "inside if 6 ");
                            // Log.d(TAG, "loginStateResponseList : " + response.body());
                            statesLayout.setVisibility(View.VISIBLE);
                            loginStateResponseList.add(0, new LoginStatesResponse(0, " Select "));
                            setLoginStatesSpinner();

                        } else {
                            // Log.d(TAG, "inside else 7");
                            showDataNotAvailbale();
                        }

                    } else {

                        // Log.d(TAG, "response is failure 8 ");
                        progressDialog.dismiss();
                        showDataNotAvailbale();

                    }
                }

                @Override
                public void onFailure(Call<List<LoginStatesResponse>> call, Throwable t) {


                    //  Log.d(TAG, "in failure 9 " + t.getMessage());
                    progressDialog.dismiss();
                    showErrorDialog();

                }
            });
        } else {
            // AppUtil.toastMessage(mContext, "Check Internet connection");
            showInternetNotAvailbale();
        }
    }

    private void setLoginStatesSpinner() {

        if (spinnerStateNames != null) {
            //  Log.d(TAG, "leave type spinner not null :" + courseNameResponseForStuAttEntireDayList.size());
            loginStateSpinnerAdapter = new LoginStateSpinnerAdapter(this, loginStateResponseList);
            spinnerStateNames.setAdapter(loginStateSpinnerAdapter);
        } else {
            // Log.d(TAG, "leave type spinner  null");
        }

    }

    private void validateUserLogin() {

        boolean cancel = false;

        userName = etUserName.getText().toString();
        passWord = etPassword.getText().toString();
        //userPin = etPin.getText().toString();

        if (TextUtils.isEmpty(userName)) {
            etUserName.setError("Enter UserName");
            cancel = true;
        }
        if (TextUtils.isEmpty(passWord)) {
            etPassword.setError("Enter Password");
            cancel = true;
        }

        /*if (loginStateCode == 0) {
            AppUtil.toastMessage(LoginActivity.this, "Please Select State");
            cancel = true;
        } else {
            SharedPreferenceUtil.setLoginStateCode(getApplicationContext(), loginStateCode);
            SharedPreferenceUtil.setLoginStateName(getApplicationContext(), loginStateName);

        }*/
        if (!cancel) {
            userInput = new UserInput();
            userInput.setLoginId(userName);
            userInput.setPassword(passWord);

            //  Log.d(TAG, userInput + " userInput is ");
            if (AppUtil.isNetworkAvailable(LoginActivity.this)) {
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setMessage("Please Wait," + "\n" + "We Are Auto Verifying Your Sign In");
                progressDialog.show();
                progressDialog.setCancelable(false);

                Call<List<LoginResponse>> call = mApiService.validateUserLogin(userInput);
                call.enqueue(new Callback<List<LoginResponse>>() {
                                 @Override
                                 public void onResponse(Call<List<LoginResponse>> call, Response<List<LoginResponse>> response) {

                                     if (response.isSuccessful()) {
                                         //  Log.d(TAG, "inside successfull");
                                         progressDialog.dismiss();
                                         loginResponseList = response.body();
                                        // Log.d(TAG, "loginResponseList : " + loginResponseList);
                                         if (loginResponseList.size() >= 1) {
                                             for (int i = 0; i < loginResponseList.size(); i++) {
                                                 vi_LoginId = loginResponseList.get(i).getLoginID();
                                                 vi_UserName = loginResponseList.get(i).getUserName();
                                                 vi_OfficeTypeCode = loginResponseList.get(i).getOfficeTypeCode();
                                                 vi_DefaultProcessTask = loginResponseList.get(i).getDefaultProcessTask();
                                                 vi_AdditionalProcessTask = loginResponseList.get(i).getAdditional_ProcessTask();
                                                 vi_RoleCode = loginResponseList.get(i).getRoleCode();
                                                 vi_LoginStatus = loginResponseList.get(i).getLoginStatus();
                                                 vi_UserCode = loginResponseList.get(i).getUserCode();
                                                 vi_TPACompCode = loginResponseList.get(i).getTPACompCode();
                                                 vi_TPACompName = loginResponseList.get(i).getTPACompName();
                                                 vi_TPAStatus = loginResponseList.get(i).getTPAStatus();
                                                 vi_LoginCode = loginResponseList.get(i).getLoginCode();
                                                 vi_DistCode = loginResponseList.get(i).getDistCode();


                                              /*   Log.d(TAG, "vi_LoginId : " + vi_LoginId);
                                                 Log.d(TAG, "vi_UserName : " + vi_UserName);
                                                 Log.d(TAG, "vi_OfficeTypeCode : " + vi_OfficeTypeCode);
                                                 Log.d(TAG, "vi_DefaultProcessTask : " + vi_DefaultProcessTask);
                                                 Log.d(TAG, "vi_AdditionalProcessTask : " + vi_AdditionalProcessTask);
                                                 Log.d(TAG, "vi_RoleCode : " + vi_RoleCode);
                                                 Log.d(TAG, "vi_LoginStatus : " + vi_LoginStatus);
                                                 Log.d(TAG, "vi_UserCode : " + vi_UserCode);
                                                 Log.d(TAG, "vi_TPACompCode : " + vi_TPACompCode);
                                                 Log.d(TAG, "vi_TPACompName : " + vi_TPACompName);
                                                 Log.d(TAG, "vi_TPAStatus : " + vi_TPAStatus);
                                                 Log.d(TAG, "vi_LoginCode : " + vi_LoginCode);
                                                 Log.d(TAG, "vi_DistCode : " + vi_DistCode);*/


                                                 if (vi_LoginStatus.equalsIgnoreCase("Y")) {
                                                     SharedPreferenceUtil.storeLoginId(getApplicationContext(), vi_LoginId);
                                                     SharedPreferenceUtil.setUserName(getApplicationContext(), vi_UserName);
                                                     SharedPreferenceUtil.setOfficeTypeCode(getApplicationContext(), vi_OfficeTypeCode);
                                                     SharedPreferenceUtil.setDefaultProcessTask(getApplicationContext(), vi_DefaultProcessTask);
                                                     SharedPreferenceUtil.setAdditionalProcessTask(getApplicationContext(), vi_AdditionalProcessTask);
                                                     SharedPreferenceUtil.setRoleCode(getApplicationContext(), vi_RoleCode);
                                                     SharedPreferenceUtil.setLoginStatus(getApplicationContext(), vi_LoginStatus);
                                                     SharedPreferenceUtil.setUserCode(getApplicationContext(), vi_UserCode);
                                                     SharedPreferenceUtil.setTPACompCode(getApplicationContext(), vi_TPACompCode);
                                                     SharedPreferenceUtil.setTPACompName(getApplicationContext(), vi_TPACompName);
                                                     SharedPreferenceUtil.setTPAStatus(getApplicationContext(), vi_TPAStatus);
                                                     SharedPreferenceUtil.setLoginCode(getApplicationContext(), vi_LoginCode);
                                                     SharedPreferenceUtil.setDistrictCode(getApplicationContext(), vi_DistCode);

                                                     gotoNextActivity();
                                                 } else {
                                                     showLoginDisabledDialog();
                                                 }

                                             }
                                         } else {
                                             //  Log.d(TAG, "userResponse is null 10");
                                             progressDialog.dismiss();
                                             showInvalidCredentialError();

                                         }
                                     } else {
                                         // Log.d(TAG, "response is failure 11 ");
                                         progressDialog.dismiss();
                                         showInvalidCredentialError();
                                     }
                                 }

                                 @Override
                                 public void onFailure(Call<List<LoginResponse>> call, Throwable t) {
                                     //  Log.d(TAG, "in failure 12 " + t.getMessage());
                                     progressDialog.dismiss();
                                     showErrorDialog();
                                 }
                             }

                );
            } else {
                // Log.d(TAG, "inside network not available");
                // AppUtil.toastMessage(LoginActivity.this, "Check Internet connection");
                showInternetNotAvailbale();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
        //finish();
        // ActivityCompat.finishAffinity(this);
    }

    private void showInvalidCredentialError() {

        builder.setCancelable(false);
        builder.setTitle("Invalid Credentials");
        builder.setMessage("Please Try Again With Valid Credential!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //finish();
            }
        });

        builder.show();
    }

    private void gotoNextActivity() {

        //  Log.d(TAG, "inside goToNextActivity");

        /*----ROLECODE 102 for local data entry login----*/
                /*----ROLECODE 11 for live data entry login----*/

        //  Log.d(TAG, "vi rolecode inside gotonext : " + vi_RoleCode);
        // Log.d(TAG, "vi_LoginStatus : " + vi_LoginStatus);
        if (vi_RoleCode == 98 || vi_RoleCode == 100) {
            // Log.d(TAG, "inside admin intent ");
            intent = new Intent(LoginActivity.this, AdminNavigationActivity.class);
            startActivity(intent);
            // Log.d(TAG, "moving to admin activity");
            finish();
        } else if (vi_RoleCode == 11) {
            intent = new Intent(LoginActivity.this, AadharSeedingNavigationActivity.class);
            startActivity(intent);
            // Log.d(TAG, "moving to admin activity");
            finish();
        } else {
            //  Log.d(TAG, "inside home page intent ");
            //  intent = new Intent(LoginActivity.this, InvestigatorLoginActivity.class);
            intent = new Intent(LoginActivity.this, HomePageNavigationActivity.class);
            startActivity(intent);
            // Log.d(TAG, "moving to home page activity");
            finish();

        }


        /*else if (vi_UserName.equalsIgnoreCase("Sanjay")){
            intent = new Intent(LoginActivity.this, DashboardActivity.class);
            startActivity(intent);
            finish();
        }
        else if(vi_RoleCode == 5 ){
           // Log.d(TAG, "inside management intent ");
            //  intent = new Intent(LoginActivity.this, InvestigatorLoginActivity.class);
            intent = new Intent(LoginActivity.this, DashboardActivity.class);
            startActivity(intent);
          //  Log.d(TAG, "moving to management activity");
            finish();
        }*/

    }

    private void showErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Network Failure");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                finish();
            }
        });

        builder.show();
    }

    private void showInternetNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Network Not Available");
        builder.setMessage("Please check your Internet Connection!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }

    private void showDataNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Data Not Found!");
        builder.setMessage("Please Try Again With Valid Credential");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }

    private void showLoginDisabledDialog() {

        builder.setCancelable(false);
        builder.setTitle("Message ");
        builder.setMessage("Your Login has been disabled !!!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
                etUserName.requestFocus();
                etUserName.getText().clear();
                etPassword.getText().clear();
                loginStateResponseList.clear();
                spinnerStateNames.setAdapter(null);
                statesLayout.setVisibility(View.GONE);
                //getLoginStateDetails();
            }
        });

        builder.show();
    }

    private void showIMEIMismatchDialog() {

        builder.setCancelable(false);
        builder.setTitle("Message ");
        builder.setMessage("IMEI Mismatch !!!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }

    public interface UserLoginService {

        @POST(UrlConfig.USER_LOGIN_IMEI_CHECK)
        Call<List<ResponseResultMessage>> validateIMEI(@Body UserInput userInput);

        @POST(UrlConfig.USER_LOGIN_STATE_DETAILS)
        Call<List<LoginStatesResponse>> getLoginStateDetails(@Body UserInput userInput);

        @POST(UrlConfig.USER_LOGIN_SESSION_DETAILS)
        Call<List<LoginResponse>> validateUserLogin(@Body UserInput userInput);

    }
   /* public void checkUserType() {
        progressDialog.dismiss();

        SharedPreferenceUtil.storeRollId(LoginActivity.this, userRoleType.getRoleCode());
        SharedPreferenceUtil.storeRollName(LoginActivity.this, userRoleType.getRoleName());
        Log.d(TAG, userRoleType.getRoleCode() + "in check rollcode");
        Log.d(TAG, userRoleType.getRoleName() + "in check rollname");

        if(userRoleType.getRoleCode() == 6)
        {
            intent = new Intent(LoginActivity.this, InvestigatorLoginActivity.class);
            startActivity(intent);
            finish();
        }
    }*/
}
