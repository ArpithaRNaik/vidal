package com.effiatechnologies.vidalinvestigator.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.fragment.AadharSeedingHomeFragment;
import com.effiatechnologies.vidalinvestigator.helper.UrnDescriptionSqliteHelper;
import com.effiatechnologies.vidalinvestigator.helper.UrnDetailsHelperTable;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import java.io.File;
import java.util.List;

public class AadharSeedingNavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


   // private static final String TAG = "AadharSeedingNavigationActivity";
    private FragmentManager mFragmentManager = null;
    private FragmentTransaction mFragmentTransaction = null;
    private Toolbar toolbar_aadhar_seeding;
    private DrawerLayout drawer_aadhar_seeding;
    private NavigationView navigationView_aadhar_seeding;
    private int roleCode;
    private int loginStateCode;
    private String loginStateName;
    private String loginUserName;
    private UrnDetailsHelperTable urnDetailsHelperTable;
    private static final String DB_NAME_URL_DETAILS = "URNDetails.db";
    private static final String DB_NAME_SAVED_URL = "URNDescription.db";

    private UrnDescriptionSqliteHelper SQLITEHELPER = null;
    private SQLiteDatabase SQLITEDATABASE;
    String GetSQliteQuery;
    Cursor cursor;
    String DeleteQuery;
    private TextView tvUserName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aadhar_seeding_navigation);

        roleCode = SharedPreferenceUtil.getRoleCode(AadharSeedingNavigationActivity.this);
        loginStateCode = SharedPreferenceUtil.getLoginStateCode(AadharSeedingNavigationActivity.this);
        loginStateName = SharedPreferenceUtil.getLoginStateName(AadharSeedingNavigationActivity.this);
        loginUserName = SharedPreferenceUtil.getLoginId(AadharSeedingNavigationActivity.this);
        urnDetailsHelperTable = new UrnDetailsHelperTable(this);
        SQLITEHELPER = new UrnDescriptionSqliteHelper(this);


        // Log.d(TAG, "roleCode : " + roleCode);
        //  Log.d(TAG, "loginStateCode : " + loginStateCode);
        //  Log.d(TAG, "loginStateName : " + loginStateName);
        toolbar_aadhar_seeding = (Toolbar) findViewById(R.id.toolbar_aadhar_seeding);
        setSupportActionBar(toolbar_aadhar_seeding);

        drawer_aadhar_seeding = (DrawerLayout) findViewById(R.id.drawer_layout_aadhar_navi);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer_aadhar_seeding, toolbar_aadhar_seeding, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_aadhar_seeding.setDrawerListener(toggle);
        toggle.syncState();

        navigationView_aadhar_seeding = (NavigationView) findViewById(R.id.nav_view_aadhar_seeding);
        navigationView_aadhar_seeding.setNavigationItemSelectedListener(this);


        toolbar_aadhar_seeding.setTitle("Aadhar Seeding");
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.aadhar_seeding_fragment_container, new AadharSeedingHomeFragment());
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();


        View view = LayoutInflater.from(this).inflate(R.layout.nav_header_aadhar_seeding_navigation, navigationView_aadhar_seeding);

        tvUserName = (TextView) view.findViewById(R.id.tv_LoginName_aadhar_seeding);
        //  spinnerFaculty = (Spinner) view.findViewById(R.id.spinner_facDepType);

        tvUserName.setText(loginUserName);

        //  tv_AadharSeeding = (TextView) findViewById(R.id.nav_Aadhar_seeding);

        //  tvLoginName = (TextView) findViewById(R.id.tv_LoginName_aadhar_seeding);
        //  tvLoginName.setText(loginUserName);
/*
        tv_AadharSeeding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invIntent = new Intent(AadharSeedingNavigationActivity.this, AadharSeedingActivity.class);
                startActivity(invIntent);
                Log.d(TAG, "moving to AadharSeedingActivity");
                finish();
            }
        });*/

    }

    @Override
    public void onBackPressed() {

        if (drawer_aadhar_seeding.isDrawerOpen(GravityCompat.START)) {
            drawer_aadhar_seeding.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fm = getSupportFragmentManager(); // or 'getSupportFragmentManager();'
            int count = fm.getBackStackEntryCount();
            toolbar_aadhar_seeding.setTitle("Aadhar Seeding");
            //  Log.d(TAG, count + "");
            if (count == 0) {
                // Log.d(TAG, "inside if");
                //super.onBackPressed();
                new AlertDialog.Builder(this)
                        .setTitle("Close App?")
                        .setMessage("Do you really want to close this app?")
                        .setPositiveButton("YES",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //  Log.d(TAG, "inside yes");
                                        doesDatabaseExist(getApplicationContext(), DB_NAME_SAVED_URL);
                                        doesURLDatabaseExist(getApplicationContext(), DB_NAME_URL_DETAILS);
                                        finish();
                                        Intent intent = new Intent(Intent.ACTION_MAIN);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addCategory(Intent.CATEGORY_HOME);
                                        startActivity(intent);
                                    }
                                })
                        .setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }).show();
            } else if (count == 1) {
                // Log.d(TAG, "inside else if ");
                AppUtil.toastMessage(AadharSeedingNavigationActivity.this, "Press the back button once again to close the application...");
                for (int i = 0; i < count; ++i) {
                    //  Log.d(TAG, "inside for loop");
                    fm.popBackStack();
                }
            } else {
                // Log.d(TAG, "inside else");
                for (int i = 0; i < count; ++i) {
                    // Log.d(TAG, "inside for loop");
                    fm.popBackStack();
                }
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_aadhar_seeding, menu);

        MenuItem menuItem = menu.findItem(R.id.dashboard_aadhar_seeding);
        // menuItem.setIcon(buildCounterDrawable(count, R.drawable.ic_notifications_off_white_18pt_3x));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {


            case R.id.dashboard_aadhar_seeding:
                toolbar_aadhar_seeding.setTitle("Aadhar Seeding");
                mFragmentManager = getSupportFragmentManager();
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.aadhar_seeding_fragment_container, new AadharSeedingHomeFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.logout_aadhar_seeding:
                manageLogout();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {


            /*case R.id.nav_aAdhar_seeding:
                toolbar_aadhar_seeding.setTitle("Aadhar Seeding");
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.aadhar_seeding_fragment_container, new AadharSeedingHomeFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;*/

            case R.id.nav_scan_qr_code:

                toolbar_aadhar_seeding.setTitle("Aadhar Seeding");
                Intent intent = new Intent(this, AadharSeedingActivity.class);
                startActivity(intent);

                break;

            case R.id.nav_aadhar_report:
                if (loginUserName.contains("DE Operator"))
                {
                    AppUtil.toastMessage(this, "You Dont Have Access");
                }
                else {
                    toolbar_aadhar_seeding.setTitle("Reports");
                    Intent intentRe = new Intent(this, AadharSeedingReportActivity.class);
                    startActivity(intentRe);
                }

                break;

            /*case R.id.nav_add_new_member:

                toolbar_aadhar_seeding.setTitle("Add New Member");
                Intent intentAdd = new Intent(this, AddNewAadharSeedingMemberActivity.class);
                startActivity(intentAdd);

                break;*/
        }
        // drawer_aadhar_seeding = (DrawerLayout) findViewById(R.id.drawer_layout_admin);
        drawer_aadhar_seeding.closeDrawer(GravityCompat.START);
        return true;
    }

    private void manageLogout() {
        // Log.d(TAG, "in logout");

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(AadharSeedingNavigationActivity.this);
        builder.setTitle("Confirm");
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to Sign Out?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //SharedPreferenceUtil.removeUserName(FacultyNavigationActivity.this);
                doesDatabaseExist(getApplicationContext(), DB_NAME_SAVED_URL);
                doesURLDatabaseExist(getApplicationContext(), DB_NAME_URL_DETAILS);
                RemoveDetails();
                Intent claimsLogoutIntent = new Intent(AadharSeedingNavigationActivity.this, LoginActivity.class);
                startActivity(claimsLogoutIntent);
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    /*@Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        RemoveDetails();
        Intent intent = new Intent(FacultyNavigationActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }*/
    private void RemoveDetails() {
        //Log.d(TAG, "inside removedetails");
        SharedPreferenceUtil.removeLoginId(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeIMIENum(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeFcmId(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeUserName(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeOfficeTypeCode(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeDefaultProcessTask(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeAdditionalProcessTask(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeRoleCode(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeLoginStatus(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeUserCode(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeTPACompCode(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeTPACompName(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeTPAStatus(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeLoginCode(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeLoginStateCode(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeLoginStateName(AadharSeedingNavigationActivity.this);
        SharedPreferenceUtil.removeDistrictCode(AadharSeedingNavigationActivity.this);


        // urnDetailsHelperTable.removeProducts();

       /* SQLITEASSIGNCLSRESPONSE = urnDescriptionSqliteHelper.getWritableDatabase();
        SQLITEASSIGNCLSRESPONSE.execSQL("DELETE FROM URNDescriptionTable"); //delete all rows in a table
        SQLITEASSIGNCLSRESPONSE.close();*/

        /*SQLITEASSIGNCLSRESPONSE = urnDescriptionSqliteHelper.getWritableDatabase();
        SQLITEASSIGNCLSRESPONSE.execSQL("DELETE FROM URNDescriptionTable"); //delete all rows in a table
        SQLITEASSIGNCLSRESPONSE.close();*/
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
       // RemoveDetails();

    }

    private boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        if (dbFile.exists())
        {
            // Log.d(TAG, "inside if");
            deleteSqliteList();
            deleteSqliteData();
        }
        else
        {
            // Log.d(TAG, "inside else");
        }
        return dbFile.exists();
    }
    private boolean doesURLDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        if (dbFile.exists())
        {
            // Log.d(TAG, "inside if");
            deleteURNSqliteData();
            deleteURNSqliteList();
        }
        else
        {
            // Log.d(TAG, "inside else");
        }
        return dbFile.exists();
    }
    private void deleteSqliteData() {
        //  Log.d(TAG, "insied deleteSqliteData");
        SQLITEDATABASE = this.openOrCreateDatabase("URNDescription.db", Context.MODE_PRIVATE, null);
        //  Log.d(TAG, "SQLITEDATABASE : " + SQLITEDATABASE);
        // SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();
        GetSQliteQuery = "SELECT * FROM URNDescriptionTable";
        //  Log.d(TAG, "GetSQliteQuery : " + GetSQliteQuery);
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        //  Log.d(TAG, "cursor : " + cursor);
        cursor.moveToFirst();
        DeleteQuery = "DELETE FROM URNDescriptionTable";
        SQLITEDATABASE.execSQL(DeleteQuery);
        //  Toast.makeText(this, "Record Deleted", Toast.LENGTH_LONG).show();
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        /*SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();

        SQLITEDATABASE.execSQL("DELETE FROM QuestionsAnswerTable"); //delete all rows in a table*/
        SQLITEDATABASE.close();
    }

    private void deleteSqliteList() {

        SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();
        String countAssCls = "SELECT count(*) FROM URNDescriptionTable";
        //  Log.d(TAG, "countAssCls : " + countAssCls);
        Cursor mcursorAss = SQLITEDATABASE.rawQuery(countAssCls, null);
        mcursorAss.moveToFirst();
        int icountAss = mcursorAss.getInt(0);
        if (icountAss > 0) {
            //  Log.d(TAG, "icountAss is greater than 0 : " + icountAss);
            SQLITEDATABASE.execSQL("DELETE FROM URNDescriptionTable"); //delete all rows in a table
            SQLITEDATABASE.close();
        } else {
            //  Log.d(TAG, "inside icountAss less than 0 ");
            //  finish();

        }
    }

    private void deleteURNSqliteData() {
        //  Log.d(TAG, "insied deleteSqliteData");
        SQLITEDATABASE = this.openOrCreateDatabase("URNDetails.db", Context.MODE_PRIVATE, null);
        //  Log.d(TAG, "SQLITEDATABASE : " + SQLITEDATABASE);
        // SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();
        GetSQliteQuery = "SELECT * FROM URNDetailsTable";
        //  Log.d(TAG, "GetSQliteQuery : " + GetSQliteQuery);
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        //  Log.d(TAG, "cursor : " + cursor);
        cursor.moveToFirst();
        DeleteQuery = "DELETE FROM URNDetailsTable";
        SQLITEDATABASE.execSQL(DeleteQuery);
        //  Toast.makeText(this, "Record Deleted", Toast.LENGTH_LONG).show();
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        /*SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();

        SQLITEDATABASE.execSQL("DELETE FROM QuestionsAnswerTable"); //delete all rows in a table*/
        SQLITEDATABASE.close();
    }

    private void deleteURNSqliteList() {

        SQLITEDATABASE = urnDetailsHelperTable.getWritableDatabase();
        String countAssCls = "SELECT count(*) FROM URNDetailsTable";
        //  Log.d(TAG, "countAssCls : " + countAssCls);
        Cursor mcursorAss = SQLITEDATABASE.rawQuery(countAssCls, null);
        mcursorAss.moveToFirst();
        int icountAss = mcursorAss.getInt(0);
        if (icountAss > 0) {
            //  Log.d(TAG, "icountAss is greater than 0 : " + icountAss);
            SQLITEDATABASE.execSQL("DELETE FROM URNDetailsTable"); //delete all rows in a table
            SQLITEDATABASE.close();
        } else {
            //  Log.d(TAG, "inside icountAss less than 0 ");
            //  finish();

        }
    }
}
