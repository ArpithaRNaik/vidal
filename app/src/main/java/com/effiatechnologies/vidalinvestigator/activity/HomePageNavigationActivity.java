package com.effiatechnologies.vidalinvestigator.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.adapter.GraphsStateListViewAdapter;
import com.effiatechnologies.vidalinvestigator.adapter.LoginStateSpinnerAdapter;
import com.effiatechnologies.vidalinvestigator.fragment.AllStateDocuments;
import com.effiatechnologies.vidalinvestigator.fragment.BarGraphFragment;
import com.effiatechnologies.vidalinvestigator.fragment.InvestigateSelectClaimsFragment;
import com.effiatechnologies.vidalinvestigator.fragment.ManagementHomeGraphFragment;
import com.effiatechnologies.vidalinvestigator.fragment.ManagementHomeReportsFragment;
import com.effiatechnologies.vidalinvestigator.helper.SqliteStatesHelper;
import com.effiatechnologies.vidalinvestigator.model.StatesModel;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import java.util.ArrayList;
import java.util.List;

public class HomePageNavigationActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

   // private static final String TAG = "HomePageNavigationActivity";

    private static final int DE_ROLECODE = 102;

    private FragmentManager mFragmentManager = null;
    private FragmentTransaction mFragmentTransaction = null;
    private Toolbar toolbar_homePage;
    private DrawerLayout drawer_homePage;
    private NavigationView navigationView_homePage;
    private Intent invIntent;
    private int roleCode;
    private SqliteStatesHelper sqliteStatesHelper;
    private List<StatesModel> statesModelArrayList = new ArrayList<>();
    private String stateNames;
    private int stateCodes;
    private int loginStateCode;
    private String loginStateName;
    private String loginUserName;
    private Bundle bundle;
    private Menu nav_Menu;

    private RecyclerView graphsNavView;
    private GraphsStateListViewAdapter loginStateSpinnerAdapter;
    private StatesModel statesModel;
    private String stateNameSelected;
    private int stateCodeSelected;

    private RecyclerView reportsNavView;
    private TextView tv_Investigate;
    private TextView tv_AadharSeeding;
    private LinearLayout investigator_layout;

    private TextView tvLoginName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page_navigation);
        bundle = new Bundle();

        roleCode = SharedPreferenceUtil.getRoleCode(HomePageNavigationActivity.this);
        loginStateCode = SharedPreferenceUtil.getLoginStateCode(HomePageNavigationActivity.this);
        loginStateName = SharedPreferenceUtil.getLoginStateName(HomePageNavigationActivity.this);
        loginUserName = SharedPreferenceUtil.getLoginId(HomePageNavigationActivity.this);

      //  Log.d(TAG, "roleCode : " + roleCode);
        //  Log.d(TAG, "loginStateCode : " + loginStateCode);
        //  Log.d(TAG, "loginStateName : " + loginStateName);
        toolbar_homePage = (Toolbar) findViewById(R.id.toolbar_home_page);
        setSupportActionBar(toolbar_homePage);

        drawer_homePage = (DrawerLayout) findViewById(R.id.drawer_layout_home_page);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer_homePage, toolbar_homePage, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_homePage.setDrawerListener(toggle);
        toggle.syncState();

        navigationView_homePage = (NavigationView) findViewById(R.id.nav_view_home_page);
        navigationView_homePage.setNavigationItemSelectedListener(this);

        tvLoginName = (TextView) findViewById(R.id.tv_LoginName);
        tvLoginName.setText(loginUserName);

        sqliteStatesHelper = new SqliteStatesHelper(this);
        statesModelArrayList = sqliteStatesHelper.getProducts();
        //  Log.d(TAG, "statesModelArrayList : " + statesModelArrayList);
       /* if (statesModelArrayList.size() >= 2) {

            //  Log.d(TAG, "inside if");
            for (int i = 0; i < statesModelArrayList.size(); i++) {
                stateNames = statesModelArrayList.get(i).getStateName();
                // Log.d(TAG, "stateNames : " + stateNames);
                stateCodes = statesModelArrayList.get(i).getStateCode();
            }
        }*/
        tv_Investigate = (TextView) findViewById(R.id.tv_navInvestigate);


        tv_Investigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar_homePage.setTitle("Investigate");
                if (roleCode == 6) {
                    //  Log.d(TAG, "inside rolecode 6");
                    invIntent = new Intent(HomePageNavigationActivity.this, PendingClaimsActivity.class);
                    startActivity(invIntent);
                    //  Log.d(TAG, "moving to pendingclaims");
                    finish();

                } else if (roleCode == 3) {
                    //  Log.d(TAG, "inside rolecode 3");
                    invIntent = new Intent(HomePageNavigationActivity.this, PendingClaimsActivity.class);
                    startActivity(invIntent);
                    //  Log.d(TAG, "moving to pendingclaims");
                    finish();
                } else {
                    //  Log.d(TAG, "inside rolecode not equal to 6");
                    AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");
                }
            }
        });

        graphsNavView = (RecyclerView) findViewById(R.id.list_view_graphs_nav);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        graphsNavView.setHasFixedSize(true);
        graphsNavView.setLayoutManager(mLayoutManager);
        graphsNavView.setItemAnimator(new DefaultItemAnimator());
        loginStateSpinnerAdapter = new GraphsStateListViewAdapter(this, statesModelArrayList);
        graphsNavView.setAdapter(loginStateSpinnerAdapter);

        graphsNavView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, graphsNavView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        // statesModel = (StatesModel) parent.getItemAtPosition(position);
                        stateNameSelected = statesModelArrayList.get(position).getStateName();
                        stateCodeSelected = statesModelArrayList.get(position).getStateCode();

                        // Log.d(TAG, "stateNameSelected : " + stateNameSelected);
                        // Log.d(TAG, "stateCodeSelected : " + stateCodeSelected);

                        if (stateNameSelected.contains("Select State")) {
                            toolbar_homePage.setTitle("ALL STATES");
                        } else {
                            toolbar_homePage.setTitle(stateNameSelected);
                        }
                        //  toolbar_homePage.setTitle(stateNameSelected);
                        mFragmentTransaction = mFragmentManager.beginTransaction();

                        bundle = new Bundle();
                        bundle.putInt("stateCode", stateCodeSelected);
                        bundle.putString("stateName", stateNameSelected);

                        BarGraphFragment managementHomeGraphFragmentAllStates = new BarGraphFragment();
                        managementHomeGraphFragmentAllStates.setArguments(bundle);

                        mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeGraphFragmentAllStates);
                        mFragmentTransaction.addToBackStack(null);
                        mFragmentTransaction.commit();

                        drawer_homePage.closeDrawer(GravityCompat.START);
                    }
                }));


        reportsNavView = (RecyclerView) findViewById(R.id.list_view_reports_nav);
        RecyclerView.LayoutManager mLayoutManagerReports = new LinearLayoutManager(getApplicationContext());
        reportsNavView.setHasFixedSize(true);
        reportsNavView.setLayoutManager(mLayoutManagerReports);
        reportsNavView.setItemAnimator(new DefaultItemAnimator());
        loginStateSpinnerAdapter = new GraphsStateListViewAdapter(this, statesModelArrayList);
        reportsNavView.setAdapter(loginStateSpinnerAdapter);

        reportsNavView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, reportsNavView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        // statesModel = (StatesModel) parent.getItemAtPosition(position);
                        stateNameSelected = statesModelArrayList.get(position).getStateName();
                        stateCodeSelected = statesModelArrayList.get(position).getStateCode();

                        //  Log.d(TAG, "stateNameSelected : " + stateNameSelected);
                        //  Log.d(TAG, "stateCodeSelected : " + stateCodeSelected);
                        if (stateNameSelected.contains("Select State")) {
                            toolbar_homePage.setTitle("ALL STATES");
                        } else {
                            toolbar_homePage.setTitle(stateNameSelected);
                        }
                        //  toolbar_homePage.setTitle(stateNameSelected);
                        if (roleCode == 3) {
                            mFragmentTransaction = mFragmentManager.beginTransaction();

                            bundle = new Bundle();
                            bundle.putInt("stateCode", stateCodeSelected);
                            bundle.putString("stateName", stateNameSelected);
                            AllStateDocuments managementHomeReportsFragmentKarnataka = new AllStateDocuments();
                            managementHomeReportsFragmentKarnataka.setArguments(bundle);

                            mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeReportsFragmentKarnataka);
                            mFragmentTransaction.addToBackStack(null);
                            mFragmentTransaction.commit();
                        } else {
                            AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");

                        }

                        drawer_homePage.closeDrawer(GravityCompat.START);
                    }
                }));



        /*nav_Menu = navigationView_homePage.getMenu();
        if (statesModelArrayList.size() >= 2) {

           // Log.d(TAG, "inside if");
            for (int i = 0; i < statesModelArrayList.size(); i++) {
                stateNames = statesModelArrayList.get(i).getStateName();
              //  Log.d(TAG, "stateNames : " + stateNames);
                stateCodes = statesModelArrayList.get(i).getStateCode();
              //  Log.d(TAG, "stateCodes : " + stateCodes);

                if (stateCodes == 3) {
                   // Log.d(TAG, "inside state name punjab");

                    nav_Menu.findItem(R.id.nav_punjabGraphss).setVisible(true);
                    nav_Menu.findItem(R.id.nav_punjabDocss).setVisible(true);

                }
                if (stateCodes == 10) {
                   // Log.d(TAG, "inside state name bihar");

                    nav_Menu.findItem(R.id.nav_biharGraphss).setVisible(true);
                    nav_Menu.findItem(R.id.nav_biharDocss).setVisible(true);

                }
                if (stateCodes == 20) {
                   // Log.d(TAG, "inside state name JHARKHAND");

                    nav_Menu.findItem(R.id.nav_jharkhandGraphss).setVisible(true);
                    nav_Menu.findItem(R.id.nav_jharkhandDocss).setVisible(true);

                }
                if (stateCodes == 22) {
                  //  Log.d(TAG, "inside state name CHHATTISGARH");

                    nav_Menu.findItem(R.id.nav_chhattisgarhGraphss).setVisible(true);
                    nav_Menu.findItem(R.id.nav_chhattisgarhDocss).setVisible(true);

                }
                if (stateCodes == 29) {
                   // Log.d(TAG, "inside state name KARNATAKA");

                    nav_Menu.findItem(R.id.nav_karnatakaGraphss).setVisible(true);
                    nav_Menu.findItem(R.id.nav_karnatakaDocss).setVisible(true);

                }
                if (stateCodes == 33) {
                   // Log.d(TAG, "inside state name tamil nadu");
                    nav_Menu.findItem(R.id.nav_tamilnaduGraphss).setVisible(true);
                    nav_Menu.findItem(R.id.nav_tamilnaduDocss).setVisible(true);

                }
                if (stateCodes == 24) {
                    // Log.d(TAG, "inside state name tamil nadu");
                    nav_Menu.findItem(R.id.nav_gujarathGraphss).setVisible(true);
                    nav_Menu.findItem(R.id.nav_gujarathDocss).setVisible(true);

                }
            }
        }*/


        // setTitle(stateNames);

        bundle = new Bundle();
        bundle.putInt("stateCode", loginStateCode);
        bundle.putString("stateName", loginStateName);

        BarGraphFragment managementHomeGraphFragmentAllStateHomess = new BarGraphFragment();
        managementHomeGraphFragmentAllStateHomess.setArguments(bundle);
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeGraphFragmentAllStateHomess);
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
       /* DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_home_page);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/

        if (drawer_homePage.isDrawerOpen(GravityCompat.START)) {
            drawer_homePage.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fm = getSupportFragmentManager(); // or 'getSupportFragmentManager();'
            int count = fm.getBackStackEntryCount();
            toolbar_homePage.setTitle("eClaims");
            //  Log.d(TAG, count + "");
            if (count == 0) {
                // Log.d(TAG, "inside if");
                //super.onBackPressed();
                new AlertDialog.Builder(this)
                        .setTitle("Close App?")
                        .setMessage("Do you really want to close this app?")
                        .setPositiveButton("YES",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // Log.d(TAG, "inside yes");
                                        finish();
                                        Intent intent = new Intent(Intent.ACTION_MAIN);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addCategory(Intent.CATEGORY_HOME);
                                        startActivity(intent);
                                    }
                                })
                        .setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }).show();
            } else if (count == 1) {
                //  Log.d(TAG, "inside else if ");
                AppUtil.toastMessage(HomePageNavigationActivity.this, "Press the back button once again to close the application...");
                for (int i = 0; i < count; ++i) {
                    //  Log.d(TAG, "inside for loop");
                    fm.popBackStack();
                }
            } else {
                // Log.d(TAG, "inside else");
                for (int i = 0; i < count; ++i) {
                    //  Log.d(TAG, "inside for loop");
                    //  Log.d(TAG, "inside for loop count : " + i);
                    fm.popBackStack();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.home_page_navigation, menu);

        MenuItem menuItem = menu.findItem(R.id.logout_homePage);
        // menuItem.setIcon(buildCounterDrawable(count, R.drawable.ic_notifications_off_white_18pt_3x));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.logout_homePage:
                manageLogout();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_Investigate:
                toolbar_homePage.setTitle("Investigate");
                if (roleCode == 6) {
                    //  Log.d(TAG, "inside rolecode 6");
                    invIntent = new Intent(HomePageNavigationActivity.this, PendingClaimsActivity.class);
                    startActivity(invIntent);
                    //  Log.d(TAG, "moving to pendingclaims");
                    finish();

                } else if (roleCode == 3) {
                    //  Log.d(TAG, "inside rolecode 3");
                    invIntent = new Intent(HomePageNavigationActivity.this, PendingClaimsActivity.class);
                    startActivity(invIntent);
                    //  Log.d(TAG, "moving to pendingclaims");
                    finish();
                } else {
                    //  Log.d(TAG, "inside rolecode not equal to 6");
                    AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");
                }
                break;

            case R.id.nav_capturePhoto:
                //  toolbar_homePage.setTitle("Capture Photo");
                AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");
                break;

            case R.id.nav_InvestigateSearch:
                // toolbar_homePage.setTitle("Search");
                if (roleCode == 6) {
                   /* mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.home_page_fragment_container, new InvestigateSelectClaimsFragment());
                    mFragmentTransaction.addToBackStack(null);
                    mFragmentTransaction.commit();*/

                    AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");

                } else if (roleCode == 3) {
                   /* mFragmentTransaction = mFragmentManager.beginTransaction();
                    mFragmentTransaction.replace(R.id.home_page_fragment_container, new InvestigateSelectClaimsFragment());
                    mFragmentTransaction.addToBackStack(null);
                    mFragmentTransaction.commit();*/

                    AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");

                } else {
                    //  Log.d(TAG, "inside rolecode not equal to 6");
                    AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");
                }
                break;

            /*case R.id.nav_ReportsGraph:
             //   toolbar_homePage.setTitle(loginStateName);
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.home_page_fragment_container, new BarGraphFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_ReportsDocument:
               // toolbar_homePage.setTitle("Reports");
                mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.home_page_fragment_container, new AllStateDocuments());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;*/

            /*case R.id.nav_allStateGraphss:
                toolbar_homePage.setTitle("All States");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",1);
                BarGraphFragment managementHomeGraphFragmentAllStates =new BarGraphFragment();
                managementHomeGraphFragmentAllStates.setArguments(bundle);

                mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeGraphFragmentAllStates);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_karnatakaGraphss:
                toolbar_homePage.setTitle("Karnataka");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",2);
                BarGraphFragment managementHomeGraphFragmentKarnataka =new BarGraphFragment();
                managementHomeGraphFragmentKarnataka.setArguments(bundle);

                mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeGraphFragmentKarnataka);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_chhattisgarhGraphss:
                toolbar_homePage.setTitle("Chhattisgarh");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",3);
                BarGraphFragment managementHomeGraphFragmentChhattisgarh =new BarGraphFragment();
                managementHomeGraphFragmentChhattisgarh.setArguments(bundle);

                mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeGraphFragmentChhattisgarh);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_jharkhandGraphss:
                toolbar_homePage.setTitle("Jharkhand");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",4);
                BarGraphFragment managementHomeGraphFragmentJharkhand =new BarGraphFragment();
                managementHomeGraphFragmentJharkhand.setArguments(bundle);

                mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeGraphFragmentJharkhand);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_tamilnaduGraphss:
                toolbar_homePage.setTitle("Tamil Nadu");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",5);
                BarGraphFragment managementHomeGraphFragmentTamilNadu =new BarGraphFragment();
                managementHomeGraphFragmentTamilNadu.setArguments(bundle);

                mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeGraphFragmentTamilNadu);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_biharGraphss:
                toolbar_homePage.setTitle("Bihar");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",6);
                BarGraphFragment managementHomeGraphFragmentBihar =new BarGraphFragment();
                managementHomeGraphFragmentBihar.setArguments(bundle);

                mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeGraphFragmentBihar);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;
            case R.id.nav_punjabGraphss:
                toolbar_homePage.setTitle("Punjab");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",7);
                BarGraphFragment managementHomeGraphFragmentPunjab =new BarGraphFragment();
                managementHomeGraphFragmentPunjab.setArguments(bundle);

                mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeGraphFragmentPunjab);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_allStateDocss:
                toolbar_homePage.setTitle("All States");

                if (roleCode == 3){
                    mFragmentTransaction = mFragmentManager.beginTransaction();

                    bundle = new Bundle();
                    bundle.putInt("temp",8);
                    AllStateDocuments managementHomeReportsFragmentAllStates = new AllStateDocuments();
                    managementHomeReportsFragmentAllStates.setArguments(bundle);

                    mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeReportsFragmentAllStates);
                    mFragmentTransaction.addToBackStack(null);
                    mFragmentTransaction.commit();
                }
                else
                {
                    AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");

                }

                break;

            case R.id.nav_karnatakaDocss:
                toolbar_homePage.setTitle("Karnataka");
                if (roleCode == 3) {
                    mFragmentTransaction = mFragmentManager.beginTransaction();

                    bundle = new Bundle();
                    bundle.putInt("temp", 9);
                    AllStateDocuments managementHomeReportsFragmentKarnataka = new AllStateDocuments();
                    managementHomeReportsFragmentKarnataka.setArguments(bundle);

                    mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeReportsFragmentKarnataka);
                    mFragmentTransaction.addToBackStack(null);
                    mFragmentTransaction.commit();
                }
                else {
                    AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");

                }
                break;

            case R.id.nav_chhattisgarhDocss:
                toolbar_homePage.setTitle("Chhattisgarh");
                if (roleCode == 3) {
                    mFragmentTransaction = mFragmentManager.beginTransaction();

                    bundle = new Bundle();
                    bundle.putInt("temp", 10);
                    AllStateDocuments managementHomeReportsFragmentChattisgarh = new AllStateDocuments();
                    managementHomeReportsFragmentChattisgarh.setArguments(bundle);

                    mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeReportsFragmentChattisgarh);
                    mFragmentTransaction.addToBackStack(null);
                    mFragmentTransaction.commit();
                }
                else
                {
                    AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");

                }
                break;
            case R.id.nav_tamilnaduDocss:
                toolbar_homePage.setTitle("Tamil Nadu");
                if (roleCode == 3) {

                    mFragmentTransaction = mFragmentManager.beginTransaction();

                    bundle = new Bundle();
                    bundle.putInt("temp", 11);
                    AllStateDocuments managementHomeReportsFragmentTamilNadu = new AllStateDocuments();
                    managementHomeReportsFragmentTamilNadu.setArguments(bundle);

                    mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeReportsFragmentTamilNadu);
                    mFragmentTransaction.addToBackStack(null);
                    mFragmentTransaction.commit();
                }
                else
                {
                    AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");

                }
                break;
            case R.id.nav_biharDocss:
                toolbar_homePage.setTitle("Bihar");
                if (roleCode == 3) {

                    mFragmentTransaction = mFragmentManager.beginTransaction();

                    bundle = new Bundle();
                    bundle.putInt("temp", 12);
                    AllStateDocuments managementHomeReportsFragmentBihar = new AllStateDocuments();
                    managementHomeReportsFragmentBihar.setArguments(bundle);

                    mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeReportsFragmentBihar);
                    mFragmentTransaction.addToBackStack(null);
                    mFragmentTransaction.commit();
                }
                else
                {
                    AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");

                }
                break;
            case R.id.nav_punjabDocss:
                toolbar_homePage.setTitle("Punjab");
                if (roleCode == 3) {

                    mFragmentTransaction = mFragmentManager.beginTransaction();

                    bundle = new Bundle();
                    bundle.putInt("temp", 13);
                    AllStateDocuments managementHomeReportsFragmentPunjab = new AllStateDocuments();
                    managementHomeReportsFragmentPunjab.setArguments(bundle);

                    mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeReportsFragmentPunjab);
                    mFragmentTransaction.addToBackStack(null);
                    mFragmentTransaction.commit();
                }
                else {
                    AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");

                }
                break;
            case R.id.nav_jharkhandDocss:
                toolbar_homePage.setTitle("Jharkhand");
                if (roleCode == 3) {

                    mFragmentTransaction = mFragmentManager.beginTransaction();

                    bundle = new Bundle();
                    bundle.putInt("temp", 14);
                    AllStateDocuments managementHomeReportsFragmentJharkhand = new AllStateDocuments();
                    managementHomeReportsFragmentJharkhand.setArguments(bundle);

                    mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeReportsFragmentJharkhand);
                    mFragmentTransaction.addToBackStack(null);
                    mFragmentTransaction.commit();
                }
                else {
                    AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");

                }
                break;

            case R.id.nav_gujarathGraphss:
                toolbar_homePage.setTitle("Gujarat");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",15);
                BarGraphFragment managementHomeGraphFragmentGujarath = new BarGraphFragment();
                managementHomeGraphFragmentGujarath.setArguments(bundle);

                mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeGraphFragmentGujarath);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_gujarathDocss:
                toolbar_homePage.setTitle("Gujarat");
                if (roleCode == 3) {

                    mFragmentTransaction = mFragmentManager.beginTransaction();

                    bundle = new Bundle();
                    bundle.putInt("temp", 16);
                    AllStateDocuments managementHomeReportsFragmentGujarat = new AllStateDocuments();
                    managementHomeReportsFragmentGujarat.setArguments(bundle);

                    mFragmentTransaction.replace(R.id.home_page_fragment_container, managementHomeReportsFragmentGujarat);
                    mFragmentTransaction.addToBackStack(null);
                    mFragmentTransaction.commit();
                }
                else {
                    AppUtil.toastMessage(HomePageNavigationActivity.this, "You Don't Have Access");

                }
                break;*/

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_home_page);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // RemoveDetails();

    }
    private void RemoveDetails() {
        //Log.d(TAG, "inside removedetails");
        SharedPreferenceUtil.removeLoginId(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeIMIENum(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeFcmId(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeUserName(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeOfficeTypeCode(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeDefaultProcessTask(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeAdditionalProcessTask(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeRoleCode(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeLoginStatus(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeUserCode(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeTPACompCode(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeTPACompName(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeTPAStatus(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeLoginCode(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeLoginStateCode(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeLoginStateName(HomePageNavigationActivity.this);
        SharedPreferenceUtil.removeDistrictCode(HomePageNavigationActivity.this);


        // urnDetailsHelperTable.removeProducts();

    }
    private void manageLogout() {
        //  Log.d(TAG, "in logout");

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(HomePageNavigationActivity.this);
        builder.setTitle("Confirm");
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to Sign Out?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                RemoveDetails();
                Intent claimsLogoutIntent = new Intent(HomePageNavigationActivity.this, LoginActivity.class);
                startActivity(claimsLogoutIntent);
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static class RecyclerItemClickListener implements RecyclerView.OnItemTouchListener {
        private OnItemClickListener mListener;

        public interface OnItemClickListener {
            public void onItemClick(View view, int position);

            // public void onLongItemClick(View view, int position);
        }

        GestureDetector mGestureDetector;

        public RecyclerItemClickListener(Context context, final RecyclerView recyclerView, OnItemClickListener listener) {
            mListener = listener;
            mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                /*@Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && mListener != null) {
                        mListener.onLongItemClick(child, recyclerView.getChildAdapterPosition(child));
                    }
                }*/
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
            View childView = view.findChildViewUnder(e.getX(), e.getY());
            if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
                mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
                return true;
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }
    }
}
