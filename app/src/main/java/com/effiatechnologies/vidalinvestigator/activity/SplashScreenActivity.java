package com.effiatechnologies.vidalinvestigator.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.configs.Config;
import com.effiatechnologies.vidalinvestigator.utils.NotificationUtils;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;
import com.google.firebase.messaging.FirebaseMessaging;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.concurrent.ExecutionException;


public class SplashScreenActivity extends AppCompatActivity {

   // private static final String TAG = "splash";
    private ProgressBar mProgressBar = null;
    private AlertDialog.Builder builder = null;
    private Intent intent = null;
    private String userName = null;
    private int vi_RoleCode = 0;
    private ImageView imgSplash = null;
    private String currentVersion;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private String regId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
      //  Log.d(TAG, "inside oncreate");

        imgSplash = (ImageView) findViewById(R.id.imgSplash);
        AnimationSet animation = new AnimationSet(true);
        animation.addAnimation(new AlphaAnimation(0.0F, 1.0F));
        animation.addAnimation(new ScaleAnimation(0.0f, 1, 0.0f, 1, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f)); // Change args as desired
        animation.setDuration(1500);
        imgSplash.startAnimation(animation);

        getCurrentVersion();
       // registerBroadCast();

        builder = new AlertDialog.Builder(this);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);

        userName = SharedPreferenceUtil.getUserName(SplashScreenActivity.this);
        vi_RoleCode = SharedPreferenceUtil.getRoleCode(SplashScreenActivity.this);
        //  roleTypesSqliteHelper = new RoleTypesSqliteHelper(this);

      //  Log.d(TAG, userName + "userName");
      //  Log.d(TAG, vi_RoleCode + "vi_RoleCode");

    }

    private void displayFirebaseRegId() {
        regId = SharedPreferenceUtil.getFcmId(SplashScreenActivity.this);
       // Log.e(TAG, "Firebase reg id: " + regId);
        new Thread() {
            public void run() {

                try {
                    sleep(2500);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {

                    //getValueslist();
                    gotoNextActivity();
                }
            }

        }.start();

    }

    private void getCurrentVersion() {
      //  Log.d(TAG, "inside getCurrentVersion ");
        PackageManager pm = this.getPackageManager();
        PackageInfo pInfo = null;

        try {
            pInfo = pm.getPackageInfo(this.getPackageName(), 0);
          //  Log.d(TAG, "inside pInfo : " + pInfo);

        } catch (PackageManager.NameNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        currentVersion = pInfo.versionName;
      //  Log.d(TAG, "currentVersion : " + currentVersion);

        VersionChecker versionChecker = new VersionChecker();
        try {
            String latestVersion = versionChecker.execute().get();
           // Log.d(TAG, "latest version : " + latestVersion);
            if (currentVersion.equals(latestVersion)) {
               // Log.d(TAG, "inside if");
                registerBroadCast();
            } else if (!currentVersion.equals(latestVersion)) {
               // Log.d(TAG, "inside else if ");
                showUpdateDialog();
            }
            /*else
            {
                Log.d(TAG, "inside else");
                showUpdateDialog();
            }*/
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    public class VersionChecker extends AsyncTask<String, String, String> {
        String newVersion;

        @Override
        protected String doInBackground(String... params) {
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + "com.effiatechnologies.vidalinvestigator" + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div[itemprop=softwareVersion]")
                        .first()
                        .ownText();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;
        }

    }

    private void registerBroadCast() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);
                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                    //tvText.setText(message);
                }
            }
        };
        displayFirebaseRegId();
    }

    private void gotoNextActivity() {

      //  Log.d(TAG, "inside goToNextActivity");
      //  Log.d(TAG, userName + "userName");
      //  Log.d(TAG, vi_RoleCode + "vi_RoleCode");
        if (userName == null) {
           // Log.d(TAG, "inside login intent ");

            intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        } else if (vi_RoleCode == 98 || vi_RoleCode == 100) {
             // Log.d(TAG, "inside admin intent ");
            intent = new Intent(this, AdminNavigationActivity.class);
            startActivity(intent);
            //  Log.d(TAG, "moving to admin activity");
            finish();
        } else if (vi_RoleCode == 11) {
           // Log.d(TAG, "inside aadhar intent ");

            intent = new Intent(this, AadharSeedingNavigationActivity.class);
            startActivity(intent);
            // Log.d(TAG, "moving to admin activity");
            finish();
        }
        else {
           // Log.d(TAG, "inside investigator intent ");

            //  intent = new Intent(this, InvestigatorLoginActivity.class);
            intent = new Intent(this, HomePageNavigationActivity.class);
            startActivity(intent);
            //   Log.d(TAG, "moving to admin activity");
            finish();
        }
        /*else if (userName.equalsIgnoreCase("Sanjay")){
            intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);
            finish();
        }
        else if(vi_RoleCode == 5 ){
          //  Log.d(TAG, "inside management intent ");
            //  intent = new Intent(LoginActivity.this, InvestigatorLoginActivity.class);
            intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);
           // Log.d(TAG, "moving to management activity");
            finish();
        }*/

    }

    private void showUpdateDialog() {
      //  Log.d(TAG, "inside showUpdateDialog ");

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(SplashScreenActivity.this);
        builder.setTitle("A New Update is Available !");
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to Update App?");
        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.effiatechnologies.vidalinvestigator"));
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                registerBroadCast();
            }
        });
        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();

    }

    @Override
    protected void onResume() {
       // Log.d(TAG, "inside onresume");
        super.onResume();

        if (mRegistrationBroadcastReceiver != null) {
            //   Log.d(TAG, "Register Broadcast");
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(Config.REGISTRATION_COMPLETE));
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(Config.PUSH_NOTIFICATION));
            NotificationUtils.clearNotifications(getApplicationContext());
        }
    }

    @Override
    protected void onPause() {
      //  Log.d(TAG, "inside onPause");
        if (mRegistrationBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    private void RemoveDetails() {
        //Log.d(TAG, "inside removedetails");
        SharedPreferenceUtil.removeLoginId(SplashScreenActivity.this);
        SharedPreferenceUtil.removeIMIENum(SplashScreenActivity.this);
        SharedPreferenceUtil.removeFcmId(SplashScreenActivity.this);
        SharedPreferenceUtil.removeUserName(SplashScreenActivity.this);
        SharedPreferenceUtil.removeOfficeTypeCode(SplashScreenActivity.this);
        SharedPreferenceUtil.removeDefaultProcessTask(SplashScreenActivity.this);
        SharedPreferenceUtil.removeAdditionalProcessTask(SplashScreenActivity.this);
        SharedPreferenceUtil.removeRoleCode(SplashScreenActivity.this);
        SharedPreferenceUtil.removeLoginStatus(SplashScreenActivity.this);
        SharedPreferenceUtil.removeUserCode(SplashScreenActivity.this);
        SharedPreferenceUtil.removeTPACompCode(SplashScreenActivity.this);
        SharedPreferenceUtil.removeTPACompName(SplashScreenActivity.this);
        SharedPreferenceUtil.removeTPAStatus(SplashScreenActivity.this);
        SharedPreferenceUtil.removeLoginCode(SplashScreenActivity.this);
        SharedPreferenceUtil.removeLoginStateCode(SplashScreenActivity.this);
        SharedPreferenceUtil.removeLoginStateName(SplashScreenActivity.this);
        SharedPreferenceUtil.removeDistrictCode(SplashScreenActivity.this);

    }
}
