package com.effiatechnologies.vidalinvestigator.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.model.ClaimsPersonDetails;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;

public class ClaimsDetailsActivity extends AppCompatActivity {

   // private static final String TAG = "claimsdetails";

    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;
    private GetClaimsDetailsService mApiService = null;

    private Button tvUpdateDetails = null;
    private Intent updateIntent = null;
    private TextView tvClaimId = null;
    private TextView tvPatientName = null;
    private TextView tvPatientAge = null;
    private TextView tvPatientGender = null;
    private TextView tvPatientDOJ = null;
    private TextView tvPatientDOD = null;
    private TextView tvPatientHospital = null;
    private TextView tvPatientTreatmentAvailed = null;
    private TextView tvPatientCategory = null;
    private TextView tvPatientAmountCharged = null;
    private String claim_id;
    private String claim_date;
    private String claim_patientName;
    private String claim_dateOfAdmission;
    private String claim_dateOfDischarge;
    private String claim_hospitalName;
    private String claim_procedureName;
    private String claim_amountnegotiated;
    private String claim_patientAge;
    private String claim_patientGender;
    private String claim_packageName;
    String[] dateOfAdmission;
    String dateJoing, timeJoining;

    String[] dateOfDischarge;
    String dateDischarge, timeDischarge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_claims_details);
      //  Log.d(TAG, "inside oncreate");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        builder = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);
        mApiService = MyApplication.getRetrofit().create(GetClaimsDetailsService.class);

        //getPersonClaimsDetails();

        tvUpdateDetails = (Button) findViewById(R.id.tv_clickToUpdateDetails);
        tvClaimId = (TextView) findViewById(R.id.tv_recClaimId);
        tvPatientName = (TextView) findViewById(R.id.tv_PatientName);
        tvPatientAge = (TextView) findViewById(R.id.tv_PatientAge);
        tvPatientGender = (TextView) findViewById(R.id.tv_PatientGender);
        tvPatientDOJ = (TextView) findViewById(R.id.tv_PatientDateOfJoining);
        tvPatientDOD = (TextView) findViewById(R.id.tv_PatientDateOfDischarge);
        tvPatientHospital = (TextView) findViewById(R.id.tv_PatientHospital);
        tvPatientTreatmentAvailed = (TextView) findViewById(R.id.tv_PatientTreatmentAvailed);
        tvPatientCategory = (TextView) findViewById(R.id.tv_PatientCategory);
        tvPatientAmountCharged = (TextView) findViewById(R.id.tv_PatientAmountCharged);

        Intent intent = getIntent();

        if (intent != null) {
           // Log.d(TAG, "inside getIntent");

            claim_id = intent.getStringExtra("claimId");
            claim_date = intent.getStringExtra("claimDate");
            claim_patientName = intent.getStringExtra("claimPatientName");
            claim_dateOfAdmission = intent.getStringExtra("claimDateOfAdmission");
            claim_dateOfDischarge = intent.getStringExtra("claimDateOfDischarge");
            claim_hospitalName = intent.getStringExtra("claimHospitalName");
            claim_procedureName = intent.getStringExtra("claimsProcedureName");
            claim_amountnegotiated = intent.getStringExtra("claimsAmountNegotiated");
            claim_patientAge = intent.getStringExtra("claimsPatientAge");
            claim_patientGender = intent.getStringExtra("claimsPatientGender");
            claim_packageName = intent.getStringExtra("claimsPackageName");

          /*  Log.d(TAG, claim_id + " : claim_id");
            Log.d(TAG, claim_date + " : claim_date");
            Log.d(TAG, claim_patientName + " : claim_patientName");
            Log.d(TAG, claim_dateOfAdmission + " : claim_dateOfAdmission");
            Log.d(TAG, claim_dateOfDischarge + " : claim_dateOfDischarge");
            Log.d(TAG, claim_hospitalName + " : claim_hospitalName");
            Log.d(TAG, claim_procedureName + " : claim_procedureName");
            Log.d(TAG, claim_amountnegotiated + " : claim_amountnegotiated");
            Log.d(TAG, claim_patientAge + " : claim_patientAge");
            Log.d(TAG, claim_patientGender + " : claim_patientGender");
            Log.d(TAG, claim_packageName + " : claim_packageName");*/



        }
        dateOfAdmission = claim_dateOfAdmission.split("T");
        dateJoing = dateOfAdmission[0].toString();
        timeJoining = dateOfAdmission[1].toString();

        dateOfDischarge = claim_dateOfDischarge.split("T");
        dateDischarge = dateOfDischarge[0].toString();
        timeDischarge = dateOfDischarge[1].toString();

        tvClaimId.setText(claim_id);
        tvPatientName.setText(claim_patientName);
        tvPatientAge.setText(claim_patientAge);
        tvPatientGender.setText(claim_patientGender);
        tvPatientDOJ.setText(dateJoing + " " + timeJoining);
        tvPatientDOD.setText(dateDischarge + " " + timeDischarge);
        tvPatientHospital.setText(claim_hospitalName);
        tvPatientCategory.setText(claim_packageName);
        tvPatientAmountCharged.setText(claim_amountnegotiated);
        tvPatientTreatmentAvailed.setText(claim_procedureName);

        tvUpdateDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateIntent = new Intent(ClaimsDetailsActivity.this, UpdateClaimsDetailsActivity.class);
                updateIntent.putExtra("claimId", claim_id);
                updateIntent.putExtra("claimDate", claim_date);
                updateIntent.putExtra("claimPatientName", claim_patientName);
                updateIntent.putExtra("claim_dateOfAdmission", claim_dateOfAdmission);
                updateIntent.putExtra("claim_dateOfDischarge", claim_dateOfDischarge);
                updateIntent.putExtra("claim_hospitalName", claim_hospitalName);

                startActivity(updateIntent);
              //  Log.d(TAG, "moving to updateclaimsdetails");
                //finish();

            }
        });
    }

    private void getPersonClaimsDetails() {

      //  Log.d(TAG, "inside getPendingClaims");

        progressDialog.setMessage("Please Wait....");
        progressDialog.show();
        progressDialog.setCancelable(false);

        Call<ClaimsPersonDetails> call = mApiService.getClaimsDetails();
        call.enqueue(new Callback<ClaimsPersonDetails>() {
            @Override
            public void onResponse(Call<ClaimsPersonDetails> call, Response<ClaimsPersonDetails> response) {
               // Log.d(TAG, "Result : " + response.body().toString());
                if (response.isSuccessful()) {
                    ClaimsPersonDetails claimsDetails = response.body();
                   // Log.d(TAG, claimsDetails + "");
                    if (claimsDetails != null) {
                        progressDialog.dismiss();
                       // Log.d(TAG, "Result : " + response.body().toString() + "result");

                        tvPatientName.setText(claimsDetails.getPatientName());
                        tvPatientAge.setText(claimsDetails.getPatientAge());
                        tvPatientGender.setText(claimsDetails.getPatientGender());
                        tvPatientDOJ.setText(claimsDetails.getPatientDateOfJoining());
                        tvPatientDOD.setText(claimsDetails.getPatientDateOfDischarge());
                        tvPatientHospital.setText(claimsDetails.getPatientHospital());
                        tvPatientTreatmentAvailed.setText(claimsDetails.getPatientTreatmentAvailed());
                        tvPatientCategory.setText(claimsDetails.getPatientCategory());
                        tvPatientAmountCharged.setText(claimsDetails.getPatientAmountCharged());

                    } else {
                       // Log.d(TAG, claimsDetails + "usertype is null");
                        progressDialog.dismiss();
                        showErrorDialog();
                    }
                } else {
                   // Log.d(TAG, "response is not successfull");
                    progressDialog.dismiss();
                    showErrorDialog();
                }

            }

            @Override
            public void onFailure(Call<ClaimsPersonDetails> call, Throwable t) {
               // Log.d(TAG, "Exception " + t.getMessage());
                progressDialog.dismiss();
                showErrorDialog();
            }
        });
    }

    private void showErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Network Failure");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                finish();
            }
        });

        builder.show();
    }

    public interface GetClaimsDetailsService {

        @GET(UrlConfig.GET_USER_TYPE_LIST)
        Call<ClaimsPersonDetails> getClaimsDetails();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.dashboard:
               // Intent homeIntent = new Intent(ClaimsDetailsActivity.this, InvestigatorLoginActivity.class);
                Intent homeIntent = new Intent(ClaimsDetailsActivity.this, HomePageNavigationActivity.class);
                startActivity(homeIntent);
                finish();
                break;

            case R.id.logout:
                manageLogout();
                break;

            case android.R.id.home:

                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // RemoveDetails();
    }

    private void manageLogout() {
       // Log.d(TAG, "in logout");

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(ClaimsDetailsActivity.this);
        builder.setTitle("Confirm");
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to Sign Out?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                RemoveDetails();
                Intent claimsLogoutIntent = new Intent(ClaimsDetailsActivity.this, LoginActivity.class);
                claimsLogoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(claimsLogoutIntent);
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void  RemoveDetails()
    {
        SharedPreferenceUtil.removeLoginId(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeIMIENum(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeFcmId(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeUserName(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeOfficeTypeCode(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeDefaultProcessTask(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeAdditionalProcessTask(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeRoleCode(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeLoginStatus(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeUserCode(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeTPACompCode(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeTPACompName(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeTPAStatus(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeLoginCode(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeLoginStateCode(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeLoginStateName(ClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeDistrictCode(ClaimsDetailsActivity.this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
