package com.effiatechnologies.vidalinvestigator.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.Image;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.adapter.InvestigatorQuestionDetailsAdapter;
import com.effiatechnologies.vidalinvestigator.adapter.QuestionDetailsAdapter;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.helper.DocumentsContract;
import com.effiatechnologies.vidalinvestigator.helper.GPSTracker;
import com.effiatechnologies.vidalinvestigator.helper.InvQuestionsAnswerSqliteHelper;
import com.effiatechnologies.vidalinvestigator.helper.RoleTypesSqliteHelper;
import com.effiatechnologies.vidalinvestigator.model.GetQuestionsResponse;
import com.effiatechnologies.vidalinvestigator.model.InputToSaveQuestionAnswer;
import com.effiatechnologies.vidalinvestigator.model.QuestionAnswerTableList;
import com.effiatechnologies.vidalinvestigator.model.ResponseResultMessage;
import com.effiatechnologies.vidalinvestigator.model.SaveClaimsDetails;
import com.effiatechnologies.vidalinvestigator.model.StatesModel;
import com.effiatechnologies.vidalinvestigator.model.UserData;
import com.effiatechnologies.vidalinvestigator.model.UserResponse;
import com.effiatechnologies.vidalinvestigator.model.UserValueResponse;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;
import com.effiatechnologies.vidalinvestigator.utils.Utility;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public class UpdateClaimsDetailsActivity extends AppCompatActivity {
   // private static final String TAG = "updateclaimsdetails";
    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;
    private SaveClaimsDetailsService mApiService = null;

    private static final int GALEERY_REQUEST = 1;
    private static final int CAMERA_REQUEST = 2;
    private static final int REQUEST_CODE_DOC = 101;
    private static final int REQUEST_CODE_AUDIO = 3;


    private TextView tvYourLocation = null;
    private TextView tvYourLong = null;
    private GPSTracker gps = null;
    private int count;
    private Location location;
    private double latitude;
    private double langitude;

    private double latitudeFirstImage;
    private double longitudeFirstImage;
    private double latitudeSecondImage;
    private double longitudeSecondImage;
    private double latitudeThirdImage;
    private double longitudeThirdImage;

    private String currentDateTimeString = null;
    private EditText etImageFileName1 = null;
    private EditText etImageFileName2 = null;
    private EditText etImageFileName3 = null;
    private EditText etDocumentName1 = null;
    private EditText etDocumentName2 = null;
    private EditText etAuduioName1 = null;
    private TextView tvYourDate = null;
    private TextView tvClaimId = null;
    private RecyclerView recyclerQuestions = null;
    private EditText etDescription = null;
    private EditText etRemarks = null;
    private ImageView imgSelect1 = null;
    private ImageView imgSelect2 = null;
    private ImageView imgSelect3 = null;
    private ImageView documentSelect1 = null;
    private ImageView documentSelect2 = null;
    private ImageView audioSelect1 = null;
    private Button btnSave = null;
    private LinearLayout documentLayout1 = null;
    private LinearLayout documentLayout2 = null;
    private TextView documentName1 = null;
    private TextView documentName2 = null;
    private ImageView cancelDocument1 = null;
    private ImageView cancelDocument2 = null;
    private Button btnSaveAnswers = null;

    private static final int FRONTIMAGE = 1;
    private static final int SECONDIMAGE = 2;
    private static final int THIRDIMAGE = 3;
    private static final int FIRSTDOCUMENT = 4;
    private static final int SECONDDOCUMENT = 5;
    private static final int FIRSTAUDIO = 6;


    private int whichImage;
    private AlertDialog.Builder mImageAlertDialog = null;

    private Intent saveIntent = null;
    private String claim_id;
    private String claim_date;
    private String claim_patientName;
    private String claim_dateOfAdmission;
    private String claim_dateOfDischarge;
    private String claim_hospitalName;

    private String et_Description = null;
    private String et_Remark = null;

    private SaveClaimsDetails saveClaimsDetails = null;
    private InputToSaveQuestionAnswer inputToSaveQuseAns = null;

    private String firstImage;
    private String secondImage;
    private String formattedDate;

    private byte[] bbytesGalleryImage;
    private String userChoosenTask;
    MultipartBody.Part imagenPerfil = null;

    private String encodedGallery;
    private String ResultMsg;
    private String ResultMsgQuestion;

    private int selectedStateCode;
    private String loginId;

    private StatesModel statesModelInput;
    private List<GetQuestionsResponse> getQuestionResponseList;
    private int statusCode;
    private InvestigatorQuestionDetailsAdapter investigatorQuestionDetailsAdapter;


    private InvQuestionsAnswerSqliteHelper SQLITEHELPER = null;
    private SQLiteDatabase SQLITEDATABASE;
    private JSONArray resultSetForQueAns;

    String GetSQliteQuery;
    Cursor cursor;
    String DeleteQuery;

    private JSONArray resultSetForAssCls;
    private List<ResponseResultMessage> responseResultMessageList;
    private String resultMessageRe;

    private List<QuestionAnswerTableList> questionAnswerTableListList;
    List<QuestionAnswerTableList> productList;

    private byte[] inputDataDocument;
    private String DocumentString;
    private Uri pdfDocument1;
    private Uri pdfDocument2;
    private Uri FirstImage;
    private Uri SecondImage;
    private Uri ThirdImage;
    private Uri AudioFile1;

    private String filePath1;
    private String filePath2;
    private String filePath3;
    private String filePath4;
    private String filePath5;
    private String filePath6;

    private String fileNameDocument1;

    private File fileDocSize1;
    private double bytesDoc1;
    private double kilobytesDoc1;
    private double megabytesDoc1;

    private File fileDocSize2;
    private double bytesDoc2;
    private double kilobytesDoc2;
    private double megabytesDoc2;

    private File fileDocSize3;
    private double bytesDoc3;
    private double kilobytesDoc3;
    private double megabytesDoc3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_claims_details);
        isStoragePermissionGranted();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 1);
            }
        }
        //deleteSqliteData();

        builder = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);
        mApiService = MyApplication.getRetrofit().create(SaveClaimsDetailsService.class);
        selectedStateCode = SharedPreferenceUtil.getLoginStateCode(this);
        loginId = SharedPreferenceUtil.getLoginId(this);
        SQLITEHELPER = new InvQuestionsAnswerSqliteHelper(this);

        getStateQuestionDetails(selectedStateCode);
        tvClaimId = (TextView) findViewById(R.id.tv_recClaimId);
        recyclerQuestions = (RecyclerView) findViewById(R.id.rec_investigatorQuestions);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerQuestions.setHasFixedSize(true);
        recyclerQuestions.setLayoutManager(mLayoutManager);
        recyclerQuestions.setItemAnimator(new DefaultItemAnimator());

        etDescription = (EditText) findViewById(R.id.et_Details);
        etRemarks = (EditText) findViewById(R.id.et_Remarks);

        imgSelect1 = (ImageView) findViewById(R.id.imgtoselect1);
        imgSelect2 = (ImageView) findViewById(R.id.imgtoselect2);
        imgSelect3 = (ImageView) findViewById(R.id.imgtoselect3);
        documentSelect1 = (ImageView) findViewById(R.id.documentSelect1);
        documentSelect2 = (ImageView) findViewById(R.id.documentSelect2);
        audioSelect1 = (ImageView) findViewById(R.id.imgtoselectAudio1);

        btnSave = (Button) findViewById(R.id.btn_SaveImages);
        //   btnSaveAnswers = (Button) findViewById(R.id.btn_SaveQuestionAnswers);
        //  tvYourLocation = (TextView) findViewById(R.id.tv_yourlocation);
        //  tvYourLong = (TextView) findViewById(R.id.tv_yourlong);
        //  tvYourDate = (TextView) findViewById(R.id.tv_yourdate);

        // etImageFileName1 = (EditText) findViewById(R.id.et_ImageFileName1);
        // etImageFileName2 = (EditText) findViewById(R.id.et_ImageFileName2);
        // etImageFileName3 = (EditText) findViewById(R.id.et_ImageFileName3);
        // etDocumentName1 = (EditText) findViewById(R.id.et_FileDocument1);
        // etDocumentName2 = (EditText) findViewById(R.id.et_FileDocument2);
        //  etAuduioName1 = (EditText) findViewById(R.id.et_AudioFileName1);

        documentLayout1 = (LinearLayout) findViewById(R.id.linearLayoutUploadDocument1);
        documentName1 = (TextView) findViewById(R.id.tv_documentName1);
        cancelDocument1 = (ImageView) findViewById(R.id.img_cancelDocument1);

        documentLayout2 = (LinearLayout) findViewById(R.id.linearLayoutUploadDocument2);
        documentName2 = (TextView) findViewById(R.id.tv_documentName2);
        cancelDocument2 = (ImageView) findViewById(R.id.img_cancelDocument2);

        Intent intent = getIntent();

        if (intent != null) {
            // Log.d(TAG, "inside getIntent");

            claim_id = intent.getStringExtra("claimId");
            claim_date = intent.getStringExtra("claimDate");
            claim_patientName = intent.getStringExtra("claimPatientName");
            claim_dateOfAdmission = intent.getStringExtra("claim_dateOfAdmission");
            claim_dateOfDischarge = intent.getStringExtra("claim_dateOfDischarge");
            claim_hospitalName = intent.getStringExtra("claim_hospitalName");

           /* Log.d(TAG, claim_id + " : claim_id");
            Log.d(TAG, claim_date + " : claim_date");
            Log.d(TAG, claim_patientName + " : claim_patientName");
            Log.d(TAG, claim_dateOfAdmission + " : claim_dateOfAdmission");
            Log.d(TAG, claim_dateOfDischarge + " : claim_dateOfDischarge");
            Log.d(TAG, claim_hospitalName + " : claim_hospitalName");*/
        }
        tvClaimId.setText(claim_id);

        buildImageAlertDialog();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
          //  Toast.makeText(this, "You need have granted permission", Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(this, this);

            // Check if GPS enabled
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                langitude = gps.getLongitude();

               // Log.d(TAG, latitude + "latitude is ");
              //  Log.d(TAG, langitude + "longitutude is ");

                // \n is for new line
              //  Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + langitude, Toast.LENGTH_LONG).show();
            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                gps.showSettingsAlert();
            }
        }
        //gps = new GPSTracker(UpdateClaimsDetailsActivity.this);

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        formattedDate = df.format(c.getTime());
        //  Log.d(TAG, "formattedDate : " + formattedDate);
        //Toast.makeText(this, formattedDate, Toast.LENGTH_SHORT).show();
        //  tvYourDate.setText("Current Date and Time : " + formattedDate);

        /*if (gps.canGetLocation()) {

          //  Log.d(TAG, "inside can get location");
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            Log.d(TAG, latitude + "latitude is ");
            Log.d(TAG, longitude + "longitutude is ");

            //   tvYourLocation.setText("Lat : " + latitude);
            //   tvYourLong.setText("Long : " + longitude);
            //Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
        } else {
            showSettingsAlert();
        }*/

        imgSelect1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Log.d(TAG, "inside imgSelect1");
                whichImage = FRONTIMAGE;
                mImageAlertDialog.show();
                getImagesLocation();

            }
        });
        imgSelect2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   Log.d(TAG, "inside imgSelect2");
                whichImage = SECONDIMAGE;
                mImageAlertDialog.show();
                getImagesLocation();

            }
        });
        imgSelect3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   Log.d(TAG, "inside imgSelect3");
                whichImage = THIRDIMAGE;
                mImageAlertDialog.show();
                getImagesLocation();

            }
        });


        documentSelect1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Log.d(TAG, "inside documentSelect1");

                whichImage = FIRSTDOCUMENT;
                //  mImageAlertDialog.show();
                getDocument();
            }
        });
        documentSelect2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Log.d(TAG, "inside documentSelect2");

                whichImage = SECONDDOCUMENT;
                // mImageAlertDialog.show();
                getDocument();
            }
        });
        audioSelect1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   Log.d(TAG, "inside audioSelect1");

                whichImage = FIRSTAUDIO;
                // mImageAlertDialog.show();
                getAudioFile();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // getAllQuestionsAnswers();
                //validateSavedData();
                //gotoNextActivity();
                uploadFile();

            }
        });
      /*  btnSaveAnswers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadFile();
            }
        });*/

        etDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                etDescription.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                etRemarks.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    public boolean isStoragePermissionGranted() {

       // Log.d(TAG, "inside isStoragePermissionGranted");
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
               // Log.d(TAG,"Permission is granted");
                return true;
            }
            if (checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
               // Log.d(TAG,"Permission is granted");
                return true;
            }else {

               // Log.d(TAG,"Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
           // Log.d(TAG,"Permission is granted");
            return true;
        }
    }


    private void getImagesLocation() {
      //  Log.d(TAG, "inside getImagesLocation");
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
          //  Toast.makeText(this, "You need have granted permission", Toast.LENGTH_SHORT).show();
            gps = new GPSTracker(this, this);

            // Check if GPS enabled
            if (gps.canGetLocation()) {

                switch (whichImage) {

                    case FRONTIMAGE:
                      //  Log.d(TAG, "inside front image location ");

                        latitudeFirstImage = gps.getLatitude();
                        longitudeFirstImage = gps.getLongitude();

                      //  Log.d(TAG, "latitudeFirstImage : " + latitudeFirstImage);
                      //  Log.d(TAG, "longitudeFirstImage : " + longitudeFirstImage);

                      //  Toast.makeText(getApplicationContext(), "Your First Image Location is - \nLat: " + latitudeFirstImage + "\nLong: " + longitudeFirstImage, Toast.LENGTH_LONG).show();

                        break;

                    case SECONDIMAGE:
                      //  Log.d(TAG, " inside second image location ");

                        latitudeSecondImage = gps.getLatitude();
                        longitudeSecondImage = gps.getLongitude();

                     //   Log.d(TAG, "latitudeSecondImage : " + latitudeSecondImage);
                     //   Log.d(TAG, "longitudeSecondImage : " + longitudeSecondImage);

                      //  Toast.makeText(getApplicationContext(), "Your Second Image Location is - \nLat: " + latitudeSecondImage + "\nLong: " + longitudeSecondImage, Toast.LENGTH_LONG).show();

                        break;

                    case THIRDIMAGE:
                      //  Log.d(TAG, " inside third image location");

                        latitudeThirdImage = gps.getLatitude();
                        longitudeThirdImage = gps.getLongitude();

                      //  Log.d(TAG, "latitudeThirdImage : " + latitudeThirdImage);
                      //  Log.d(TAG, "longitudeThirdImage : " + longitudeThirdImage);

                      //  Toast.makeText(getApplicationContext(), "Your Third Image Location is - \nLat: " + latitudeThirdImage + "\nLong: " + longitudeThirdImage, Toast.LENGTH_LONG).show();

                        break;
                }

            } else {
                // Can't get location.
                // GPS or network is not enabled.
                // Ask user to enable GPS/network in settings.
                gps.showSettingsAlert();
            }
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    gps = new GPSTracker(this, this);
                    // Check if GPS enabled
                    if (gps.canGetLocation()) {
                        latitude = gps.getLatitude();
                        langitude = gps.getLongitude();

                     //   Log.d(TAG, "latitude : " + latitude);
                     //   Log.d(TAG, "langitude : " + langitude);

                      //  Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + langitude, Toast.LENGTH_LONG).show();
                        switch (whichImage) {

                            case FRONTIMAGE:
                              //  Log.d(TAG, "inside front image location 2");

                                latitudeFirstImage = gps.getLatitude();
                                longitudeFirstImage = gps.getLongitude();

                              //  Log.d(TAG, "latitudeFirstImage : " + latitudeFirstImage);
                              //  Log.d(TAG, "longitudeFirstImage : " + longitudeFirstImage);

                               // Toast.makeText(getApplicationContext(), "Your First Image Location is - \nLat: " + latitudeFirstImage + "\nLong: " + longitudeFirstImage, Toast.LENGTH_LONG).show();

                                break;

                            case SECONDIMAGE:
                              //  Log.d(TAG, " inside second image location 2");

                                latitudeSecondImage = gps.getLatitude();
                                longitudeSecondImage = gps.getLongitude();

                             //  Log.d(TAG, "latitudeSecondImage : " + latitudeSecondImage);
                             //   Log.d(TAG, "longitudeSecondImage : " + longitudeSecondImage);

                              //  Toast.makeText(getApplicationContext(), "Your Second Image Location is - \nLat: " + latitudeSecondImage + "\nLong: " + longitudeSecondImage, Toast.LENGTH_LONG).show();

                                break;

                            case THIRDIMAGE:
                             //   Log.d(TAG, " inside third image location 3");

                                latitudeThirdImage = gps.getLatitude();
                                longitudeThirdImage = gps.getLongitude();

                              //  Log.d(TAG, "latitudeThirdImage : " + latitudeThirdImage);
                             //   Log.d(TAG, "longitudeThirdImage : " + longitudeThirdImage);

                               // Toast.makeText(getApplicationContext(), "Your Third Image Location is - \nLat: " + latitudeThirdImage + "\nLong: " + longitudeThirdImage, Toast.LENGTH_LONG).show();

                                break;
                        }
                    } else {
                        // Can't get location.
                        // GPS or network is not enabled.
                        // Ask user to enable GPS/network in settings.
                        gps.showSettingsAlert();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "You need to grant permission to get location", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                  /*else if (userChoosenTask.equals("Choose from Library"))
                    galleryIntent();*/
                } else {
                    //code for deny
                }
                break;
        }
    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case Utility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    *//*else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();*//*
                } else {
                    //code for deny
                }
                break;
        }
    }*/

    /*@Override
    public void onLocationUdate(Location location) {

        if (location != null)
            count++;
        this.location = location;
      //  Log.d(TAG, location + "");
        latitude = location.getLatitude();
        langitude = location.getLongitude();
      //  Log.d(TAG, count + "");
        if (count == 1) {

            findAdressusingLatLang(latitude, langitude);

        }
    }*/

    public void findAdressusingLatLang(double lat, double lang) {

        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = null;
            addresses = geocoder.getFromLocation(lat, lang, 1);
            String cityName = addresses.get(0).getAddressLine(0);
            String stateName = addresses.get(0).getAddressLine(1);
            String countryName = addresses.get(0).getAddressLine(2);
            //  Log.d(TAG, cityName + "city name");
            //  Log.d(TAG, stateName + "state name");
            //  Log.d(TAG, countryName + "country name");

        } catch (IOException e) {
            e.printStackTrace();

        }
    }

    private void buildImageAlertDialog() {

        mImageAlertDialog = new AlertDialog.Builder(this);
        mImageAlertDialog.setTitle("Select Photo!");
        mImageAlertDialog.setItems(R.array.image_select_options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                switch (item) {
                    case 0:
                        userChoosenTask = "Take Photo";
                        //callCamera();
                        cameraIntent();
                        break;
                    /*case 1:
                        userChoosenTask = "Choose from Library";
                        //callGallery();
                        galleryIntent();
                        break;*/
                    case 3:
                        dialog.dismiss();
                        break;
                }
            }
        });
    }

    private void galleryIntent() {
        //  Log.d(TAG, "inside galleryIntent");
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), GALEERY_REQUEST);
    }

    private void cameraIntent() {
       // Log.d(TAG, "inside cameraIntent");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

       // Log.d(TAG, "data inside onActivityResult : " + data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == GALEERY_REQUEST) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == CAMERA_REQUEST) {
                onCaptureImageResult(data);
            } else if (requestCode == REQUEST_CODE_DOC) {
                onGetDocumentResult(data);
            } else if (requestCode == REQUEST_CODE_AUDIO) {
                onGetAudioResult(data);
            }

        }
    }

    private void getAudioFile() {

        //  Log.d(TAG, "inside getDocument");
        Intent intentAudio = new Intent();
        intentAudio.setType("audio/*");
        intentAudio.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intentAudio, REQUEST_CODE_AUDIO);

    }

    private void getDocument() {

        //  Log.d(TAG, "inside getDocument");
        Intent intent = new Intent(android.content.Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        // intent.setPackage("com.adobe.reader");
        intent.setType("file/*");
        startActivityForResult(intent, REQUEST_CODE_DOC);


    }

    private void onGetAudioResult(Intent data) {

        //  Log.d(TAG, "inside onGetAudioResult : " + data.getData());

        switch (whichImage) {
            case FIRSTAUDIO:
                //  Log.d(TAG, "inside front document ");
                AudioFile1 = data.getData();
                //  Log.d(TAG, "AudioFile1 : " + AudioFile1);

                fileDocSize3 = new File(AudioFile1.getPath());
                long size3 = fileDocSize3.length();
                //  Log.d(TAG, "size3 : " + size3);

                bytesDoc3 = fileDocSize3.length();
                kilobytesDoc3 = (bytesDoc3 / 1024);
                megabytesDoc3 = (kilobytesDoc3 / 1024);
                audioSelect1.setImageResource(R.drawable.uploaded);

                //  Log.d(TAG, "bytesDoc3 : " + bytesDoc3);
                //   Log.d(TAG, "kilobytesDoc3 : " + kilobytesDoc3);
                //   Log.d(TAG, "megabytesDoc3 : " + megabytesDoc3);

                break;

        }
    }

    private void onGetDocumentResult(Intent data) {

        // Log.d(TAG, "inside onGetDocumentResult : " + data.getData());

        switch (whichImage) {
            case FIRSTDOCUMENT:
                // Log.d(TAG, "inside front document ");
                pdfDocument1 = data.getData();
                //  Log.d(TAG, "pdfDocument2 : " + pdfDocument2);

                fileDocSize1 = new File(pdfDocument1.getPath());
                long size = fileDocSize1.length();
                //  Log.d(TAG, "size : " + size);

                bytesDoc1 = fileDocSize1.length();
                kilobytesDoc1 = (bytesDoc1 / 1024);
                megabytesDoc1 = (kilobytesDoc1 / 1024);

                documentSelect1.setImageResource(R.drawable.uploaded);

                // Log.d(TAG, "bytesDoc1 : " + bytesDoc1);
                //  Log.d(TAG, "kilobytesDoc1 : " + kilobytesDoc1);
                //  Log.d(TAG, "megabytesDoc1 : " + megabytesDoc1);

                break;

            case SECONDDOCUMENT:
                //  Log.d(TAG, "inside second document ");
                pdfDocument2 = data.getData();
                //  Log.d(TAG, "pdfDocument2 : " + pdfDocument2);

                fileDocSize2 = new File(pdfDocument2.getPath());
                long size2 = fileDocSize2.length();
                //  Log.d(TAG, "size2 : " + size2);

                bytesDoc2 = fileDocSize2.length();
                kilobytesDoc2 = (bytesDoc2 / 1024);
                megabytesDoc2 = (kilobytesDoc2 / 1024);
                documentSelect2.setImageResource(R.drawable.uploaded);


                //  Log.d(TAG, "bytesDoc2 : " + bytesDoc2);
                //  Log.d(TAG, "kilobytesDoc2 : " + kilobytesDoc2);
                //  Log.d(TAG, "megabytesDoc2 : " + megabytesDoc2);
                break;

        }
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void onCaptureImageResult(Intent data) {
       // Log.d(TAG, "data: " + data);
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
      //  Log.d(TAG, "thumbnail : " + thumbnail);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        Uri tempUri = getImageUri(getApplicationContext(), thumbnail);

      //  Log.d(TAG, "tempUri : " + tempUri);

        //Cursor cursor = getContentResolver().query(tempUri, null, null, null, null);
        //cursor.moveToFirst();
        //int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);

        //String FinalPath=cursor.getString(idx);
        byte[] byteArrayCamera = bytes.toByteArray();
        //  Log.d(TAG, "byteArrayCamera : " + byteArrayCamera);
        encodedGallery = Base64.encodeToString(byteArrayCamera, Base64.NO_WRAP);
        //  Log.d(TAG, "camimageBase64String : " + encodedGallery);

        //  Log.d(TAG, "camera bitmap : " + thumbnail);
//        imgSelect1.setImageBitmap(thumbnail);
        switch (whichImage) {

            case FRONTIMAGE:
                // Log.d(TAG, "inside front image ");
                imgSelect1.setImageBitmap(thumbnail);
                FirstImage = getImageUri(getApplicationContext(), thumbnail);
                //Uri tempUri = getImageUri(getApplicationContext(), thumbnail);

                break;

            case SECONDIMAGE:
                //  Log.d(TAG, " inside second image ");
                imgSelect2.setImageBitmap(thumbnail);
                SecondImage = getImageUri(getApplicationContext(), thumbnail);
                break;

            case THIRDIMAGE:
                //  Log.d(TAG, " inside third image ");
                imgSelect3.setImageBitmap(thumbnail);
                ThirdImage = getImageUri(getApplicationContext(), thumbnail);
                break;
        }

    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
                //  Log.d(TAG, "gallery image : " + bm);

                ByteArrayOutputStream bytesGa = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 90, bytesGa);
                bbytesGalleryImage = bytesGa.toByteArray();
                //  Log.d(TAG, "bbytesGalleryImage : " + bbytesGalleryImage);
                encodedGallery = Base64.encodeToString(bbytesGalleryImage, Base64.NO_WRAP);
                // Log.d(TAG, "galimageBase64String : " + encodedGallery);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        // imgSelect1.setImageBitmap(bm);
        switch (whichImage) {

            case FRONTIMAGE:
                // Log.d(TAG, "inside gallery front image ");
                imgSelect1.setImageBitmap(bm);
                FirstImage = getImageUri(getApplicationContext(), bm);
                break;

            case SECONDIMAGE:
                //  Log.d(TAG, " inside gallery second image ");
                imgSelect2.setImageBitmap(bm);
                SecondImage = getImageUri(getApplicationContext(), bm);
                break;

            case THIRDIMAGE:
                // Log.d(TAG, " inside gallery third image ");
                imgSelect3.setImageBitmap(bm);
                ThirdImage = getImageUri(getApplicationContext(), bm);
                break;

        }

    }

    private void uploadFile() {

      //  Log.d(TAG, "inside uploadfile");
        boolean cancelFile = false;
        if (AppUtil.isNetworkAvailable(this)) {


            if (pdfDocument1 == null) {
                AppUtil.toastMessage(this, "Please Select First Document");
                cancelFile = true;
            } else if (megabytesDoc1 > 2.0) {
                AppUtil.toastMessage(this, "Document File Size Should Be Less Than 2.0 MB");
                cancelFile = true;
            }
            if (pdfDocument2 == null) {
                AppUtil.toastMessage(this, "Please Select Second Document");
                cancelFile = true;
            } else if (megabytesDoc2 > 2.0) {
                AppUtil.toastMessage(this, "Document File Size Should Be Less Than 2.0 MB");
                cancelFile = true;
            }
            if (FirstImage == null) {
                AppUtil.toastMessage(this, "Please Select First Image");
                cancelFile = true;
            }
            if (SecondImage == null) {
                AppUtil.toastMessage(this, "Please Select Second Image");
                cancelFile = true;
            }
            if (ThirdImage == null) {
                AppUtil.toastMessage(this, "Please Select Third Image");
                cancelFile = true;
            }
            if (AudioFile1 == null) {
                AppUtil.toastMessage(this, "Please Select Audio File");
                cancelFile = true;
            } else if (kilobytesDoc3 > 500.0) {
                AppUtil.toastMessage(this, "Audio File Size Should Be Less Than 500.0 KB");
                cancelFile = true;
            }
            if (!cancelFile) {
                progressDialog.setMessage("Please Wait....");
                progressDialog.show();
                progressDialog.setCancelable(false);
                filePath1 = getRealPathFromUri(pdfDocument1);
                filePath2 = getRealPathFromUri(pdfDocument2);
                filePath3 = getRealPathFromUri(FirstImage);
                filePath4 = getRealPathFromUri(SecondImage);
                filePath5 = getRealPathFromUri(ThirdImage);
                filePath6 = getRealPathFromUri(AudioFile1);
                // String filePath = getRealPathFromUri(pdfDocument);
                //  Log.d(TAG, "filePath1 : " + filePath1);
                //   Log.d(TAG, "filePath2 : " + filePath2);
                //   Log.d(TAG, "filePath3 : " + filePath3);
                //   Log.d(TAG, "filePath4 : " + filePath4);
                //   Log.d(TAG, "filePath5 : " + filePath5);
                //   Log.d(TAG, "filePath6 : " + filePath6);

        /*if (filePath1 != null && !filePath1.isEmpty() && filePath2 != null && !filePath2.isEmpty() &&
                filePath3 != null && !filePath3.isEmpty() && filePath4 != null && !filePath4.isEmpty() &&
                filePath5 != null && !filePath5.isEmpty() && filePath6 != null && !filePath6.isEmpty()) {*/
                File fileDoc1 = new File(filePath1);
                File fileDoc2 = new File(filePath2);
                File fileDoc3 = new File(filePath3);
                File fileDoc4 = new File(filePath4);
                File fileDoc5 = new File(filePath5);
                File fileDoc6 = new File(filePath6);

                //  Log.d(TAG, "fileDoc1: " + fileDoc1);
                //  Log.d(TAG, "fileDoc2: " + fileDoc2);
                //   Log.d(TAG, "fileDoc3: " + fileDoc3);
                //   Log.d(TAG, "fileDoc4: " + fileDoc4);
                //   Log.d(TAG, "fileDoc5: " + fileDoc5);
                //   Log.d(TAG, "fileDoc6: " + fileDoc6);

            /*if (fileDoc1.exists() && fileDoc2.exists() && fileDoc3.exists() && fileDoc4.exists() && fileDoc5.exists() &&
                    fileDoc6.exists()) {*/
                //  Log.d(TAG, "inside if");


               /* RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), fileDoc1);
                Log.d(TAG, "requestFile : " + requestFile);

                RequestBody requestFile2 = RequestBody.create(MediaType.parse("multipart/form-data"), fileDoc2);
                Log.d(TAG, "requestFile2 : " + requestFile2);

                MultipartBody.Part body = MultipartBody.Part.createFormData("file", fileDoc1.getName(), requestFile);
                Log.d(TAG, "body : " + body);

                MultipartBody.Part body2 = MultipartBody.Part.createFormData("file2", fileDoc2.getName(), requestFile2);
                Log.d(TAG, "body2 : " + body2);*/

                MultipartBody.Builder builder = new MultipartBody.Builder();
                builder.setType(MultipartBody.FORM);
                builder.addFormDataPart("files", fileDoc1.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), fileDoc1));
                builder.addFormDataPart("files", fileDoc2.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), fileDoc2));
                builder.addFormDataPart("files", fileDoc3.getName(), RequestBody.create(MediaType.parse("image/*"), fileDoc3));
                builder.addFormDataPart("files", fileDoc4.getName(), RequestBody.create(MediaType.parse("image/*"), fileDoc4));
                builder.addFormDataPart("files", fileDoc5.getName(), RequestBody.create(MediaType.parse("image/*"), fileDoc5));
                builder.addFormDataPart("files", fileDoc6.getName(), RequestBody.create(MediaType.parse("audio/*"), fileDoc6));
                builder.addFormDataPart("TransId", claim_id);
                // builder.addFormDataPart("TransId", "TransId", RequestBody.create(MediaType.parse("text/*"), claim_id));


                MultipartBody requestBody = builder.build();
                Call<ResponseBody> call = mApiService.postPdfFile(requestBody);

                //  Call<ResponseBody> call = mApiService.postPdfFile(body, body2);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           Response<ResponseBody> response) {
                        //  Log.d(TAG, "response : " + response);
                        //  Log.d(TAG, "response code : " + response.code());
                       //   Log.d(TAG, "response body : " + response.body());
                       //   Log.i(TAG, "success");
                        progressDialog.dismiss();
                        if (response.code() == 200) {
                            progressDialog.dismiss();
                            getProducts();

                            // showSuccessDialog();
                        } else {
                            progressDialog.dismiss();
                            showErrorDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        progressDialog.dismiss();
                        showErrorDialog();
                       // Log.e(TAG, t.getMessage());
                    }
                });
            }
        } else {
            progressDialog.dismiss();
            showInternetNotAvailbale();
        }
    }

    public List<QuestionAnswerTableList> getProducts() {
        productList = new ArrayList<>();
        //  productList.add(new QuestionAnswerTableList(0,"Select Department",false,0));
        //  String selectQuery = "SELECT * FROM " + TABLE_PRODUCTS;
        String selectQueryForAssCls = "Select * from QuestionsAnswerTable ";
        SQLITEDATABASE = SQLITEHELPER.getReadableDatabase();
        Cursor cursorAssCls = SQLITEDATABASE.rawQuery(selectQueryForAssCls, null);

        if (cursorAssCls.moveToFirst()) {
            do {
                QuestionAnswerTableList product = new QuestionAnswerTableList();
                product.setId(cursorAssCls.getString(0));
                product.setQuestionId(cursorAssCls.getString(1));
                product.setQuestion(cursorAssCls.getString(2));
                product.setStateCode(cursorAssCls.getString(3));
                product.setIsActive(cursorAssCls.getString(4));
                product.setQuestionAnswer(cursorAssCls.getString(5));
                //  Log.d(TAG, "product : " + product);
                productList.add(product);
            } while (cursorAssCls.moveToNext());
        }
        cursorAssCls.close();
        SQLITEDATABASE.close();
        //  Log.d(TAG, "product list : " + productList);
        validateSavedData(productList);
        return productList;
    }

    private void validateSavedData(List<QuestionAnswerTableList> productList) {

        // Log.d(TAG, "inside validateSavedData");
        // getAllQuestionsAnswers();

        if (AppUtil.isNetworkAvailable(this)) {

            et_Description = etDescription.getText().toString();
            et_Remark = etRemarks.getText().toString();

            boolean cancel = false;
            if (productList.size() == 0 || productList.isEmpty()) {
                AppUtil.toastMessage(this, "Please Answer For Any One Question");
                cancel = true;
            }
            if (TextUtils.isEmpty(et_Description)) {
                etDescription.setError("Enter Description");
                cancel = true;
            }
            if (TextUtils.isEmpty(et_Remark)) {
                etRemarks.setError("Enter Remarks");
                cancel = true;
            }
            //  Log.d(TAG, "product list size : " + productList.size());

            if (!cancel) {

                saveClaimsDetails = new SaveClaimsDetails();
                saveClaimsDetails.setLoginId(loginId);
                saveClaimsDetails.setTransID(claim_id);
                saveClaimsDetails.setQuestionAnswersList(productList);
                //   saveClaimsDetails.setFileName(claim_id + "_" + etImageFileName1.getText().toString());
                saveClaimsDetails.setDescription(et_Description);
                saveClaimsDetails.setRemarks(et_Remark);
                saveClaimsDetails.setLatitude(String.valueOf(latitude));
                saveClaimsDetails.setLongitude(String.valueOf(langitude));
                //  saveClaimsDetails.setFileData(encodedGallery);
                //   saveClaimsDetails.setFileData(DocumentString);

                // saveClaimsDetails.setDocument(DocumentString);

               // Log.d(TAG, " saveInput is : " + saveClaimsDetails);

                progressDialog.setMessage("Please Wait....");
                progressDialog.show();
                progressDialog.setCancelable(false);

                Call<List<UserResponse>> call = mApiService.validateClaimsDetails(saveClaimsDetails);
                call.enqueue(new Callback<List<UserResponse>>() {
                    @Override
                    public void onResponse(Call<List<UserResponse>> call, Response<List<UserResponse>> response) {
                        //  Log.d(TAG, "response.body : " + response.body());
                        //  Log.d(TAG, "userResponse " + response.code());

                        if (response.isSuccessful()) {
                            // Log.d(TAG, "inside successfull");
                            List<UserResponse> userResponse = response.body();
                            //  Log.d(TAG, "response.body : " + response.body());
                            //  Log.d(TAG, "userResponse " + userResponse);

                            for (int i = 0; i < userResponse.size(); i++) {
                                ResultMsg = userResponse.get(i).getResult();
                                if (ResultMsg.equals("Details Has Been Saved Succesfully")) {
                                    // Log.d(TAG, "inside if details saved successfully");
                                    progressDialog.dismiss();
                                    deleteSqliteData();
                                    gotoNextActivity();

                                } else

                                {
                                    //  Log.d(TAG, "inside else no data found");
                                    deleteSqliteData();
                                    progressDialog.dismiss();
                                    showErrorDialog();

                                }
                            }
                        } else {
                            //  Log.d(TAG, "response is not successful" + response.errorBody());
                            deleteSqliteData();
                            progressDialog.dismiss();
                            showErrorDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<UserResponse>> call, Throwable t) {
                        // Log.d(TAG, "in failure" + t.getMessage());
                        deleteSqliteData();
                        progressDialog.dismiss();
                        showErrorDialog();
                    }
                });
            }
        } else {
            showInternetNotAvailbale();
        }

    }

    private void getStateQuestionDetails(int selectedStateCode) {

       // Log.d(TAG, "inside getStateQuestionDetails ");
        if (AppUtil.isNetworkAvailable(this)) {

            progressDialog.setMessage("Please Wait....");
            progressDialog.show();
            progressDialog.setCancelable(false);

            statesModelInput = new StatesModel();
            statesModelInput.setStateCode(selectedStateCode);

          //  Log.d(TAG, "input to statesModelInput : " + statesModelInput);

            Call<List<GetQuestionsResponse>> call = mApiService.getStatesQuestionDetails(statesModelInput);
            call.enqueue(new Callback<List<GetQuestionsResponse>>() {
                @Override
                public void onResponse(Call<List<GetQuestionsResponse>> call, Response<List<GetQuestionsResponse>> response) {
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        getQuestionResponseList = response.body();
                        statusCode = response.code();
                        // Log.d(TAG, "statuscode : " + statusCode);
                       // Log.d(TAG, "statesModelList : " + getQuestionResponseList);
                        // Log.d(TAG, "statesModelList size : " + getQuestionResponseList.size());

                        if (getQuestionResponseList.size() >= 1) {
                            //  Log.d(TAG, "getQuestionResponseList : " + getQuestionResponseList);
                            //layout_QuestionDetails.setVisibility(View.VISIBLE);
                            investigatorQuestionDetailsAdapter = new InvestigatorQuestionDetailsAdapter(getApplicationContext(), UpdateClaimsDetailsActivity.this, getQuestionResponseList);
                            recyclerQuestions.setAdapter(investigatorQuestionDetailsAdapter);
                            investigatorQuestionDetailsAdapter.notifyDataSetChanged();
                        } else {
                            progressDialog.dismiss();
                            //  layout_QuestionDetails.setVisibility(View.GONE);
                            showDataNotAvailbale();
                        }

                    } else {

                        //  layout_QuestionDetails.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        statusCode = response.code();
                       //  Log.d(TAG, statusCode + "failed");
                        showDataNotAvailbale();
                        //  ResponseBody errorBody = response.errorBody();

                    }
                }

                @Override
                public void onFailure(Call<List<GetQuestionsResponse>> call, Throwable t) {

                    progressDialog.dismiss();
                  //  Log.d(TAG, t.getMessage() + "on failure");
                    showErrorDialog();

                }
            });
        } else {
            // AppUtil.toastMessage(mContext, "Check Internet connection");
            showInternetNotAvailbale();
        }
    }

    private void showInternetNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Network Not Available");
        builder.setMessage("Please check your Internet Connection!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }

    private void showDataNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Please Try Again!!");
        builder.setMessage("Data Not Found");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }

    public interface SaveClaimsDetailsService {

        @POST(UrlConfig.SAVE_CLAIMS_DETAILS)
        Call<List<UserResponse>> validateClaimsDetails(@Body SaveClaimsDetails saveClaimsDetails);

       /* @POST(UrlConfig.SAVE_INVESTIGATOR_QUESTION_ANSWERS)
        Call<List<ResponseResultMessage>> validateQuestionsAnswers(@Body InputToSaveQuestionAnswer inputToSavEQueAns);*/

        @POST(UrlConfig.GET_INVESTIGATOR_STATE_QUESTIONS)
        Call<List<GetQuestionsResponse>> getStatesQuestionDetails(@Body StatesModel statesModel);

        /*@Multipart
        @POST(UrlConfig.UPLOAD_PDF_DOCUMENT)
        Call<ResponseBody> postPdfFile(@Part MultipartBody.Part file1, @Part MultipartBody.Part file2);*/


        @POST(UrlConfig.UPLOAD_PDF_DOCUMENT)
        Call<ResponseBody> postPdfFile(@Body RequestBody files);

    }

    private void showErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Network Failure");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //  finish();
            }
        });

        builder.show();
    }

    private void showSuccessDialog() {

        builder.setCancelable(false);
        builder.setTitle("Message");
        builder.setMessage("Documents Uploaded Successfully");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //  finish();
            }
        });

        builder.show();
    }

    private void showSomethingErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Something Wrong....");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //  finish();
            }
        });

        builder.show();
    }

    private void showDataNotSavedDialog() {

        builder.setCancelable(false);
        builder.setTitle("Data Not Saved...");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //  finish();
            }
        });

        builder.show();
    }

    private void gotoNextActivity() {

        builder.setCancelable(false);
        builder.setTitle("Message");
        builder.setMessage("Details Has Been Saved Succesfully");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //  Log.d(TAG, "inside goToNextActivity");
                // saveIntent = new Intent(UpdateClaimsDetailsActivity.this, InvestigatorLoginActivity.class);
                saveIntent = new Intent(UpdateClaimsDetailsActivity.this, HomePageNavigationActivity.class);
                startActivity(saveIntent);
                UpdateClaimsDetailsActivity.this.finish();
            }
        });

        builder.show();


    }

    private void DialogQuestionAnswersUploaded(String ResultMsgQuestion) {

        builder.setCancelable(false);
        builder.setTitle("Message");
        builder.setMessage(ResultMsgQuestion);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //  Log.d(TAG, "inside goToNextActivity");
                //   saveIntent = new Intent(UpdateClaimsDetailsActivity.this, InvestigatorLoginActivity.class);
                saveIntent = new Intent(UpdateClaimsDetailsActivity.this, HomePageNavigationActivity.class);
                startActivity(saveIntent);
                UpdateClaimsDetailsActivity.this.finish();
            }
        });

        builder.show();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.dashboard:
                // Intent homeIntent = new Intent(UpdateClaimsDetailsActivity.this, InvestigatorLoginActivity.class);
                Intent homeIntent = new Intent(UpdateClaimsDetailsActivity.this, HomePageNavigationActivity.class);
                startActivity(homeIntent);
                finish();
                break;

            case R.id.logout:
                manageLogout();
                break;

            case android.R.id.home:

                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void deleteSqliteData() {
        // Log.d(TAG, "insied deleteSqliteData");

        GetSQliteQuery = "SELECT * FROM QuestionsAnswerTable";
        SQLITEDATABASE = this.openOrCreateDatabase("QuestionsAnswer.db", Context.MODE_PRIVATE, null);
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        cursor.moveToFirst();
        DeleteQuery = "DELETE FROM QuestionsAnswerTable;";
        SQLITEDATABASE.execSQL(DeleteQuery);
        //  Toast.makeText(this, "Record Deleted", Toast.LENGTH_LONG).show();
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        /*SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();

        SQLITEDATABASE.execSQL("DELETE FROM QuestionsAnswerTable"); //delete all rows in a table*/
        SQLITEDATABASE.close();
    }
    private void removeDetails()
    {
        SharedPreferenceUtil.removeUserName(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeIMIENum(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeFcmId(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeLoginId(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeOfficeTypeCode(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeDefaultProcessTask(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeAdditionalProcessTask(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeRoleCode(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeLoginStatus(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeUserCode(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeTPACompCode(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeTPACompName(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeTPAStatus(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeLoginCode(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeLoginStateCode(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeLoginStateName(UpdateClaimsDetailsActivity.this);
        SharedPreferenceUtil.removeDistrictCode(UpdateClaimsDetailsActivity.this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // removeDetails();

    }

    private void manageLogout() {
        //  Log.d(TAG, "in logout");

        final AlertDialog.Builder builder = new AlertDialog.Builder(UpdateClaimsDetailsActivity.this);
        builder.setTitle("Confirm");
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to Sign Out?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                removeDetails();
                Intent intent = new Intent(UpdateClaimsDetailsActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public String getRealPathFromUri(Uri pdfDocument) {
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(pdfDocument)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(pdfDocument)) {
                final String docId = DocumentsContract.getDocumentId(pdfDocument);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(pdfDocument)) {

                final String id = DocumentsContract.getDocumentId(pdfDocument);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(this, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(pdfDocument)) {
                final String docId = DocumentsContract.getDocumentId(pdfDocument);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(this, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(pdfDocument.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(pdfDocument))
                return pdfDocument.getLastPathSegment();

            return getDataColumn(this, pdfDocument, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(pdfDocument.getScheme())) {
            return pdfDocument.getPath();
        }

        return null;
    }

    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
