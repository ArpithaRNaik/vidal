package com.effiatechnologies.vidalinvestigator.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.multidex.MultiDex;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.adapter.UrnDetailsAdapterScan;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.helper.UrnDescriptionSqliteHelper;
import com.effiatechnologies.vidalinvestigator.helper.UrnDetailsHelperTable;
import com.effiatechnologies.vidalinvestigator.model.UpdateUrnDetailsInput;
import com.effiatechnologies.vidalinvestigator.model.UpdatedUrnDetailsResponse;
import com.effiatechnologies.vidalinvestigator.model.UrnDetailsInput;
import com.effiatechnologies.vidalinvestigator.model.UrnDetailsResponse;
import com.effiatechnologies.vidalinvestigator.model.UserData;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

public class AadharSeedingActivity extends AppCompatActivity {

   // private static final String TAG = "AadharSeedingActivity";

    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;
    private GetUrnDetailsService mApiService = null;
    private EditText etURN = null;
    private Button btnShowURNDetails = null;
    private RecyclerView rc_UrnDetails = null;
    private Button btn_saveUrnDetails = null;
    private CheckBox agree_checkbox = null;
    private LinearLayout agreeLayout = null;
    private String urnValue = null;
    private UrnDetailsInput urnDetailsInput = null;
    private List<UrnDetailsResponse> urnDetailsResponseList = null;
    private List<UrnDetailsResponse> urnSqliteResponseList = null;
    private UrnDetailsHelperTable urnDetailsHelperTable = null;
    private UrnDetailsAdapterScan urnDetailsAdapter = null;
    private static final String DB_NAME = "URNDetails.db";
    private static final String DB_NAME_URNDES = "URNDescription.db";
    private static final String FAMILY_ID = "FamilyId";
    private static final String URN_VALUE = "UrnId";

    private SQLiteDatabase SQLITEDATABASE;
    String GetSQliteQuery;
    Cursor cursor;
    String DeleteQuery;
    private SQLiteDatabase SQLITEDATABASEFORURNUPDATION;
    private static final int REQUEST_CODE_DOC = 101;
    private String returnString;
    private String returnMemberId;
    private String scan_UID;
    private String scan_NAME;
    private String scan_GENDER;
    private String scan_YOB;
    public static final String TABLE_URN_DE = "URNDetailsTable";
    public static final String MEMBER_ID = "MemberID";
    public static final String AADHARNO = "AadhaarNo";
    public static final String AADHAR_NUMBER = "AadhaarNumber";
    public static final String NAME_AS_AADHAR = "NameAsAadhaar";

    UrnDetailsHelperTable URNHELPER;
    private List<UrnDetailsResponse> mDataItemsListUpdated;
    private List<UrnDetailsResponse> UrlDetailsList = null;
    private UrnDescriptionSqliteHelper SQLITEHELPER = null;
    private UpdateUrnDetailsInput updateUrnDetailsInput;
    private String resultMessageRe = null;

    private String LoginId;
    private int DistId;
    private int re_FlagValue;
    private String re_FamilyId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aadhar_seeding);
      //  Log.d(TAG, "inside onCreate");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        LoginId = SharedPreferenceUtil.getLoginId(AadharSeedingActivity.this);
        DistId = SharedPreferenceUtil.getDistrictCode(AadharSeedingActivity.this);


        builder = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);
        mApiService = MyApplication.getRetrofit().create(GetUrnDetailsService.class);
        urnDetailsHelperTable = new UrnDetailsHelperTable(this);
        SQLITEHELPER = new UrnDescriptionSqliteHelper(this);

        etURN = (EditText) findViewById(R.id.et_URNScan);
        btnShowURNDetails = (Button) findViewById(R.id.btn_ShowURNDetailsScan);
        rc_UrnDetails = (RecyclerView) findViewById(R.id.recyclerViewURNDetailsForDEScan);
        btn_saveUrnDetails = (Button) findViewById(R.id.btn_UrnDetailsSaveScan);
        agree_checkbox = (CheckBox) findViewById(R.id.save_checkboxScan);
        agreeLayout = (LinearLayout) findViewById(R.id.aggrement_layoutScan);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rc_UrnDetails.setHasFixedSize(true);
        rc_UrnDetails.setLayoutManager(mLayoutManager);
        rc_UrnDetails.setItemAnimator(new DefaultItemAnimator());

        etURN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                etURN.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        btnShowURNDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getUrnDetails();
            }
        });
        btn_saveUrnDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getSavedUrnDetails();

            }
        });

    }

    public List<UrnDetailsResponse> getSavedUrnDetails() {
        //  Log.d(TAG, "inside getproducts");
        UrlDetailsList = new ArrayList<>();
        //  productList.add(new QuestionAnswerTableList(0,"Select Department",false,0));
        //  String selectQuery = "SELECT * FROM " + TABLE_PRODUCTS;
        String selectQueryForAssCls = "Select * from URNDescriptionTable ";
        SQLITEDATABASE = SQLITEHELPER.getReadableDatabase();
        Cursor cursorAssCls = SQLITEDATABASE.rawQuery(selectQueryForAssCls, null);

        if (cursorAssCls.moveToFirst()) {
            do {
                UrnDetailsResponse product = new UrnDetailsResponse();
                product.setMemberID(cursorAssCls.getInt(1));
                product.setMemberName(cursorAssCls.getString(2));
                product.setURN(cursorAssCls.getString(3));
                product.setSINo(cursorAssCls.getString(4));
                product.setAge(cursorAssCls.getString(5));
                product.setRelation(cursorAssCls.getString(6));
                product.setIsEnrolled(cursorAssCls.getString(7));
                product.setAadhaarNo(cursorAssCls.getString(8));
                product.setAadhaarNumber(cursorAssCls.getString(8));
                product.setNameAsAadhaar(cursorAssCls.getString(9));
                product.setMobileNO(cursorAssCls.getString(10));
                product.setIMIENumber(cursorAssCls.getString(11));
                product.setDateTime(cursorAssCls.getString(12));
                product.setRemarksID(cursorAssCls.getInt(13));
                product.setRemarksName(cursorAssCls.getString(14));
                product.setLoginId(cursorAssCls.getString(15));
                product.setUserCode(cursorAssCls.getString(16));
                product.setRelationCode(cursorAssCls.getString(17));
                product.setGenderName(cursorAssCls.getString(18));
                product.setGenderCode(cursorAssCls.getString(19));
                product.setFamilyId(cursorAssCls.getString(20));
                product.setFlag(cursorAssCls.getInt(21));

               // Log.d(TAG, "product : " + product);
                UrlDetailsList.add(product);
            } while (cursorAssCls.moveToNext());
        }

        cursorAssCls.close();
        SQLITEDATABASE.close();

        // Log.d(TAG, "UrlDetailsList : " + UrlDetailsList);
        //  Log.d(TAG, "periodDescriptionList size : " + periodDescriptionList.size());
        validateSavedDescription(UrlDetailsList);
        return UrlDetailsList;
    }

    private void validateSavedDescription(List<UrnDetailsResponse> UrlDetailsList) {


        // Log.d(TAG, "inside validateSavedDescription");
        boolean cancel = false;

        //  Log.d(TAG, "periodDescriptionList size : " + periodDescriptionList.size());
        // Log.d(TAG, "facStudentAttendanceModelList size : " + facStudentAttendanceModelList.size());
        if (agree_checkbox.isChecked() == false) {
            AppUtil.toastMessage(this, "Please Check The Checkbox To Confirm");
            cancel = true;
        }
        /*if (!(UrlDetailsList.size() > urnDetailsResponseList.size())) {

            AppUtil.toastMessage(mContext, "Please Check All The Details To Confirm");
            cancel = true;
        }*/

        if (!cancel) {

            if (AppUtil.isNetworkAvailable(this)) {
                progressDialog.setMessage("Please Wait....");
                progressDialog.show();
                progressDialog.setCancelable(false);

                updateUrnDetailsInput = new UpdateUrnDetailsInput();

                updateUrnDetailsInput.setUrlDetailsList(UrlDetailsList);
                updateUrnDetailsInput.setFlagValue(0);


              // Log.d(TAG, "updateUrnDetailsInput : " + updateUrnDetailsInput);

                Call<UpdatedUrnDetailsResponse> call = mApiService.submitUrnDescription(updateUrnDetailsInput);
                call.enqueue(new Callback<UpdatedUrnDetailsResponse>() {
                    @Override
                    public void onResponse(Call<UpdatedUrnDetailsResponse> call, Response<UpdatedUrnDetailsResponse> response) {

                        if (response.isSuccessful()) {
                          //  Log.d(TAG, response.body() + "inside successfull response");
                            if (response.body() != null) {
                                progressDialog.dismiss();
                                UpdatedUrnDetailsResponse userResponse = response.body();
                               // Log.d(TAG, userResponse + "");

                                List<UserData> userData = userResponse.getTable1();
                               // Log.d(TAG, "userData : " + userData);

                                for (int i = 0; i < userData.size(); i++) {
                                    resultMessageRe = userData.get(i).getResult();
                                   // Log.d(TAG, "resultMessageRe : " + resultMessageRe);
                                    if (resultMessageRe.contains("Member Details Updated Successfully")) {
                                        //  Log.d(TAG, "resultMessageRe inside if : " + resultMessageRe);
                                        deleteSqliteList();
                                        deleteSqliteData();
                                        deleteURNDescriptionList();
                                        deleteURNDescriptionData();
                                        showSMSApprovedDialog(resultMessageRe);

                                        //  showSuccessDialog();
                                    } else {
                                        // Log.d(TAG, "resultMessageRe inside else: " + resultMessageRe);
                                        deleteSqliteList();
                                        deleteSqliteData();
                                        deleteURNDescriptionList();
                                        deleteURNDescriptionData();
                                        showSMSNotCanceledDialog(resultMessageRe);

                                    }
                                }

                            } else if (response.body() == null) {
                                // Log.d(TAG, "response is null");
                                //response.body();
                                deleteSqliteList();
                                deleteSqliteData();
                                deleteURNDescriptionList();
                                deleteURNDescriptionData();
                                showNoRecordDialog();
                                progressDialog.dismiss();

                            }

                        } else {
                            // Log.d(TAG, "response is not successfull");
                            //deleteSqliteList();
                            //deleteSqliteData();
                            showNoRecordDialog();
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdatedUrnDetailsResponse> call, Throwable t) {
                        // Log.d(TAG, "in failure" + t.getMessage());
                        showErrorDialog();
                        progressDialog.dismiss();
                    }
                });
            } else {
                showInternetNotAvailbale();
                // AppUtil.toastMessage(AddStudentAttendanceActivity.this, "Check Internet connection");
            }
        }
    }


    private boolean doesDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        if (dbFile.exists())
        {
           // Log.d(TAG, "inside if");
            deleteSqliteList();
            deleteSqliteData();
        }
        else
        {
           // Log.d(TAG, "inside else");
        }
        return dbFile.exists();
    }

    private boolean doesURNDescDatabaseExist(Context context, String dbName) {
        File dbFile = context.getDatabasePath(dbName);
        if (dbFile.exists())
        {
            // Log.d(TAG, "inside if");
            deleteURNDescriptionData();
            deleteURNDescriptionList();
        }
        else
        {
            // Log.d(TAG, "inside else");
        }
        return dbFile.exists();
    }

    private void getUrnDetails() {

        // Log.d(TAG, "inside validateDate");
        boolean cancel = false;

        urnValue = etURN.getText().toString();

        if (TextUtils.isEmpty(urnValue)) {
            etURN.setError("Enter URN");
            cancel = true;
        }
        if (!cancel) {
            if (AppUtil.isNetworkAvailable(this)) {
                progressDialog.setMessage("Please Wait....");
                progressDialog.show();
                progressDialog.setCancelable(false);

                urnDetailsInput = new UrnDetailsInput();

                urnDetailsInput.setURN(urnValue);
                urnDetailsInput.setDistrictCode(String.valueOf(DistId));

                /*if (LoginId.contains("DE Rajnandgaon"))
                {
                    urnDetailsInput.setDistrictCode("9");
                }
                else if (LoginId.contains("DE Kawardha"))
                {
                    urnDetailsInput.setDistrictCode("8");
                }
                else{
                    urnDetailsInput.setDistrictCode("0");

                }*/

              //  Log.d(TAG, "urnDetailsInput : " + urnDetailsInput);

                Call<List<UrnDetailsResponse>> call = mApiService.getUrnDetails(urnDetailsInput);
                call.enqueue(new Callback<List<UrnDetailsResponse>>() {
                    @Override
                    public void onResponse(Call<List<UrnDetailsResponse>> call, Response<List<UrnDetailsResponse>> response) {

                        if (response.isSuccessful()) {
                          //  Log.d(TAG, response.body() + "inside successfull response" + response.body());
                          //  Log.d(TAG, response.body() + "inside successfull response" + response.message());

                            if (response.body() != null) {
                                progressDialog.dismiss();
                                //  layoutRecyclerHeading.setVisibility(View.VISIBLE);

                                urnDetailsResponseList = response.body();
                              //  Log.d(TAG, "urnDetailsResponseList size : " + urnDetailsResponseList.size());
                              //  Log.d(TAG, "urnDetailsResponseList : " + urnDetailsResponseList);

                                if (urnDetailsResponseList.size() >= 1) {
                                   // Log.d(TAG, "inside if ");
                                    for (int i = 0; i < urnDetailsResponseList.size(); i++)
                                    {
                                        re_FlagValue = urnDetailsResponseList.get(0).getFlag();
                                       // Log.d(TAG, "re_FlagValue inside for : " + re_FlagValue);
                                        re_FamilyId = urnDetailsResponseList.get(0).getFamilyId();
                                       // Log.d(TAG, "re_FamilyId inside for : " + re_FamilyId);

                                    }
                                  //  Log.d(TAG, "re_FlagValue outside for : " + re_FlagValue);
                                  //  Log.d(TAG, "re_FamilyId outside for : " + re_FamilyId);

                                    if (re_FlagValue == 1)
                                    {
                                        rc_UrnDetails.setVisibility(View.GONE);
                                        btn_saveUrnDetails.setVisibility(View.GONE);
                                        agreeLayout.setVisibility(View.GONE);
                                        progressDialog.dismiss();
                                        showAddDataDialog(re_FamilyId, urnValue);
                                    }
                                    else {
                                        File dbFilename = getApplicationContext().getDatabasePath(DB_NAME);
                                        if (dbFilename.exists()) {
                                            // Log.d(TAG, "inside if");
                                            deleteSqliteList();
                                            deleteSqliteData();
                                            urnDetailsHelperTable.addURNDetails(urnDetailsResponseList);
                                            //Log.d(TAG, "response from sqlite 1" + urnDetailsHelperTable.getURNDetails().toString());

                                            urnSqliteResponseList = urnDetailsHelperTable.getURNDetails();
                                            // Log.d(TAG, "urnSqliteResponseList 2" + urnSqliteResponseList);
                                        } else {
                                            // Log.d(TAG, "inside else");
                                            urnDetailsHelperTable.addURNDetails(urnDetailsResponseList);
                                            // Log.d(TAG, "response from sqlite 3" + urnDetailsHelperTable.getURNDetails().toString());

                                            urnSqliteResponseList = urnDetailsHelperTable.getURNDetails();
                                            // Log.d(TAG, "urnSqliteResponseList 4" + urnSqliteResponseList);
                                        }

                                        urnDetailsHelperTable.close();
                                        if (urnSqliteResponseList.size() >= 1) {
                                            rc_UrnDetails.setVisibility(View.VISIBLE);
                                            btn_saveUrnDetails.setVisibility(View.VISIBLE);
                                            agreeLayout.setVisibility(View.VISIBLE);
                                            urnDetailsAdapter = new UrnDetailsAdapterScan(AadharSeedingActivity.this, getApplicationContext(), urnSqliteResponseList);
                                            rc_UrnDetails.setAdapter(urnDetailsAdapter);
                                            urnDetailsAdapter.notifyDataSetChanged();
                                        } else {
                                            showNoRecordDialog();
                                            rc_UrnDetails.setVisibility(View.GONE);
                                            btn_saveUrnDetails.setVisibility(View.GONE);
                                            agreeLayout.setVisibility(View.GONE);
                                        }
                                    }
                                }
                                else
                                {
                                    rc_UrnDetails.setVisibility(View.GONE);
                                    btn_saveUrnDetails.setVisibility(View.GONE);
                                    agreeLayout.setVisibility(View.GONE);
                                    progressDialog.dismiss();
                                    showNoRecordDialog();
                                }

                            } else if (response.body() == null) {
                               // Log.d(TAG, "response is null" + response.body());
                                //response.body();
                                rc_UrnDetails.setVisibility(View.GONE);
                                btn_saveUrnDetails.setVisibility(View.GONE);
                                agreeLayout.setVisibility(View.GONE);
                                progressDialog.dismiss();
                                showNoRecordDialog();

                            }

                        } else {
                          //  Log.d(TAG, "response is not successfull" + response.message());
                            rc_UrnDetails.setVisibility(View.GONE);
                            btn_saveUrnDetails.setVisibility(View.GONE);
                            agreeLayout.setVisibility(View.GONE);
                            progressDialog.dismiss();
                            showNoRecordDialog();
                           // showNoRecordDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<UrnDetailsResponse>> call, Throwable t) {
                       // Log.d(TAG, "in failure" + t.getMessage());
                        rc_UrnDetails.setVisibility(View.GONE);
                        btn_saveUrnDetails.setVisibility(View.GONE);
                        agreeLayout.setVisibility(View.GONE);
                        progressDialog.dismiss();
                        showErrorDialog();
                    }
                });
            } else {
                showInternetNotAvailbale();
                // AppUtil.toastMessage(AddStudentAttendanceActivity.this, "Check Internet connection");
            }
        }

    }

    public interface GetUrnDetailsService {

        @POST(UrlConfig.GET_URN_DETAILS)
        Call<List<UrnDetailsResponse>> getUrnDetails(@Body UrnDetailsInput urnDetailsInput);

        @POST(UrlConfig.UPDATE_URN_DETAILS)
        Call<UpdatedUrnDetailsResponse> submitUrnDescription(@Body UpdateUrnDetailsInput updateUrnDetailsInput);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.dashboard:
                // Intent homeIntent = new Intent(ClaimsDetailsActivity.this, InvestigatorLoginActivity.class);
                doesDatabaseExist(getApplicationContext(), DB_NAME);
                doesURNDescDatabaseExist(getApplicationContext(), DB_NAME_URNDES);

                Intent homeIntent = new Intent(AadharSeedingActivity.this, AadharSeedingNavigationActivity.class);
                startActivity(homeIntent);
                finish();
                break;

            case R.id.logout:
                manageLogout();
                break;

            case android.R.id.home:

                doesDatabaseExist(getApplicationContext(), DB_NAME);
                doesURNDescDatabaseExist(getApplicationContext(), DB_NAME_URNDES);

                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
    private void manageLogout() {
        // Log.d(TAG, "in logout");

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(AadharSeedingActivity.this);
        builder.setTitle("Confirm");
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to Sign Out?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                doesDatabaseExist(getApplicationContext(), DB_NAME);
                doesURNDescDatabaseExist(getApplicationContext(), DB_NAME_URNDES);

                RemoveDetails();
                Intent claimsLogoutIntent = new Intent(AadharSeedingActivity.this, LoginActivity.class);
                claimsLogoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(claimsLogoutIntent);
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void  RemoveDetails()
    {
        SharedPreferenceUtil.removeLoginId(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeIMIENum(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeFcmId(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeUserName(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeOfficeTypeCode(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeDefaultProcessTask(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeAdditionalProcessTask(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeRoleCode(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeLoginStatus(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeUserCode(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeTPACompCode(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeTPACompName(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeTPAStatus(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeLoginCode(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeLoginStateCode(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeLoginStateName(AadharSeedingActivity.this);
        SharedPreferenceUtil.removeDistrictCode(AadharSeedingActivity.this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        doesDatabaseExist(getApplicationContext(), DB_NAME);
        doesURNDescDatabaseExist(getApplicationContext(), DB_NAME_URNDES);


    }
    private void showAddDataDialog(String re_familyId, final String urnValue)
    {
        final android.support.v7.app.AlertDialog.Builder builder1 = new android.support.v7.app.AlertDialog.Builder(AadharSeedingActivity.this);
        builder1.setTitle("Information");
        builder1.setCancelable(false);
        builder1.setMessage("Data Not Found, " +
                "Are you sure you want to Add New Member?");
        builder1.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                doesDatabaseExist(getApplicationContext(), DB_NAME);
                doesURNDescDatabaseExist(getApplicationContext(), DB_NAME_URNDES);

                Intent addIntent = new Intent(AadharSeedingActivity.this, AddNewAadharSeedingMemberActivity.class);
                addIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                addIntent.putExtra(FAMILY_ID , re_FamilyId);
                addIntent.putExtra(URN_VALUE , urnValue);

                startActivity(addIntent);
                finish();
            }
        });
        builder1.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        android.support.v7.app.AlertDialog dialog1 = builder1.create();
        dialog1.show();
    }

    private void showNoRecordDialog() {

        builder.setCancelable(false);
        builder.setTitle("Information");
        builder.setMessage("No Records Found!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //finish();
            }
        });

        builder.show();
    }
    private void showErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Network Failure");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //finish();
            }
        });

        builder.show();
    }
    private void showInternetNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Network Not Available");
        builder.setMessage("Please check your Internet Connection!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }
    private void showSMSApprovedDialog(String resultMessageRe) {

        builder.setCancelable(false);
        builder.setTitle("Message");
        builder.setMessage(resultMessageRe);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                // mActivity.finish();
                deleteSqliteList();
                deleteSqliteData();
                Intent finishIntent = new Intent(AadharSeedingActivity.this, AadharSeedingNavigationActivity.class);
                startActivity(finishIntent);
                finish();

            }
        });

        builder.show();
    }

    private void showSMSNotCanceledDialog(String resultMessageRe) {

        builder.setCancelable(false);
        builder.setTitle("Message");
        builder.setMessage(resultMessageRe);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //  finish();
                agree_checkbox.setChecked(false);
                getUrnDetails();

            }
        });

        builder.show();
    }

    private void deleteSqliteData() {
        //  Log.d(TAG, "insied deleteSqliteData");
        SQLITEDATABASE = this.openOrCreateDatabase("URNDetails.db", Context.MODE_PRIVATE, null);
        //  Log.d(TAG, "SQLITEDATABASE : " + SQLITEDATABASE);
        // SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();
        GetSQliteQuery = "SELECT * FROM URNDetailsTable";
        //  Log.d(TAG, "GetSQliteQuery : " + GetSQliteQuery);
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        //  Log.d(TAG, "cursor : " + cursor);
        cursor.moveToFirst();
        DeleteQuery = "DELETE FROM URNDetailsTable";
        SQLITEDATABASE.execSQL(DeleteQuery);
        //  Toast.makeText(this, "Record Deleted", Toast.LENGTH_LONG).show();
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        /*SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();

        SQLITEDATABASE.execSQL("DELETE FROM QuestionsAnswerTable"); //delete all rows in a table*/
        SQLITEDATABASE.close();
    }

    private void deleteSqliteList() {

        SQLITEDATABASE = urnDetailsHelperTable.getWritableDatabase();
        String countAssCls = "SELECT count(*) FROM URNDetailsTable";
        //  Log.d(TAG, "countAssCls : " + countAssCls);
        Cursor mcursorAss = SQLITEDATABASE.rawQuery(countAssCls, null);
        mcursorAss.moveToFirst();
        int icountAss = mcursorAss.getInt(0);
        if (icountAss > 0) {
            //  Log.d(TAG, "icountAss is greater than 0 : " + icountAss);
            SQLITEDATABASE.execSQL("DELETE FROM URNDetailsTable"); //delete all rows in a table
            SQLITEDATABASE.close();
        } else {
            //  Log.d(TAG, "inside icountAss less than 0 ");
            //  finish();

        }
    }

    private void deleteURNDescriptionData() {
        //  Log.d(TAG, "insied deleteSqliteData");
        SQLITEDATABASE = this.openOrCreateDatabase("URNDescription.db", Context.MODE_PRIVATE, null);
        //  Log.d(TAG, "SQLITEDATABASE : " + SQLITEDATABASE);
        // SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();
        GetSQliteQuery = "SELECT * FROM URNDescriptionTable";
        //  Log.d(TAG, "GetSQliteQuery : " + GetSQliteQuery);
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        //  Log.d(TAG, "cursor : " + cursor);
        cursor.moveToFirst();
        DeleteQuery = "DELETE FROM URNDescriptionTable";
        SQLITEDATABASE.execSQL(DeleteQuery);
        //  Toast.makeText(this, "Record Deleted", Toast.LENGTH_LONG).show();
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        /*SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();

        SQLITEDATABASE.execSQL("DELETE FROM QuestionsAnswerTable"); //delete all rows in a table*/
        SQLITEDATABASE.close();
    }

    private void deleteURNDescriptionList() {

        SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();
        String countAssCls = "SELECT count(*) FROM URNDescriptionTable";
        //  Log.d(TAG, "countAssCls : " + countAssCls);
        Cursor mcursorAss = SQLITEDATABASE.rawQuery(countAssCls, null);
        mcursorAss.moveToFirst();
        int icountAss = mcursorAss.getInt(0);
        if (icountAss > 0) {
            //  Log.d(TAG, "icountAss is greater than 0 : " + icountAss);
            SQLITEDATABASE.execSQL("DELETE FROM URNDescriptionTable"); //delete all rows in a table
            SQLITEDATABASE.close();
        } else {
            //  Log.d(TAG, "inside icountAss less than 0 ");
            //  finish();

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

       // Log.d(TAG, "inside onActivity result");
        //  Log.d(TAG, "request code : " + requestCode);
        //  Log.d(TAG, "result code : " + resultCode);
        // Log.d(TAG, "data : " + data);
        SQLITEDATABASEFORURNUPDATION = this.openOrCreateDatabase("URNDetails.db", Context.MODE_PRIVATE, null);

        // Log.d(TAG, "inside onActivity result");
        if (resultCode == Activity.RESULT_OK) {
            //  Log.d(TAG, "inside RESULT_OK");
            if (requestCode == REQUEST_CODE_DOC) {
                //  Log.d(TAG, "request code : " + REQUEST_CODE_DOC);
                returnString = data.getStringExtra("result");
               // Log.d(TAG, "returnString : " + returnString);
                // readXML(returnString);
                returnMemberId = data.getStringExtra("memberId");
               // Log.d(TAG, "returnMemberId : " + returnMemberId);

                DocumentBuilder builder = null;
                try {
                    builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    InputSource src = new InputSource();
                    src.setCharacterStream(new StringReader(returnString));

                    Document doc = builder.parse(src);

                    doc.getDocumentElement().normalize();
                    NodeList nList = doc.getElementsByTagName("PrintLetterBarcodeData");
                    for (int temp = 0; temp < nList.getLength(); temp++) {

                        Node nNode = nList.item(temp);

                       // Log.d(TAG, "Current Element :" + nNode.getNodeName());

                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                            Element eElement = (Element) nNode;

                            scan_UID = eElement.getAttribute("uid");
                            scan_NAME = eElement.getAttribute("name");
                            scan_GENDER = eElement.getAttribute("gender");
                            scan_YOB = eElement.getAttribute("yob");

                             // Log.d(TAG, "scan_UID : " + scan_UID);
                             // Log.d(TAG, "scan_NAME : " + scan_NAME);
                             // Log.d(TAG, "scan_GENDER : " + scan_GENDER);
                             // Log.d(TAG, "scan_YOB : " + scan_YOB);

                            // mDataItemsList.add(7,scan_UID);
                            // updateData(mDataItemsList);
                            //  Log.d(TAG, "scan_memberId : " + returnMemberId);
                            updateData(returnMemberId, scan_UID, scan_NAME);
                        }
                    }
                } catch (ParserConfigurationException e) {
                    //  Log.d(TAG, " ecxp1 " + e);
                    e.printStackTrace();
                } catch (SAXException e) {
                    //  Log.d(TAG, " ecxp2 " + e);

                    e.printStackTrace();
                } catch (IOException e) {
                    // Log.d(TAG, " ecxp3 " + e);

                    e.printStackTrace();
                }
            }
        }
    }

    public void updateData(String returnMemberId, String scan_UID, String scan_NAME) {
        // Log.d(TAG, "inside updateData");
       //  Log.d(TAG, "scan_uid : " + scan_UID);
       //  Log.d(TAG, "memberId_scan : " + returnMemberId);
       // Log.d(TAG, "scan_NAME : " + scan_NAME);


        GetSQliteQuery = "SELECT * FROM URNDetailsTable";
        SQLITEDATABASEFORURNUPDATION = this.openOrCreateDatabase("URNDetails.db", Context.MODE_PRIVATE, null);
        cursor = SQLITEDATABASEFORURNUPDATION.rawQuery(GetSQliteQuery, null);
        cursor.moveToFirst();
        SQLITEDATABASEFORURNUPDATION.execSQL("UPDATE " + TABLE_URN_DE + " SET " + AADHARNO + "= '" + scan_UID + "', " + AADHAR_NUMBER + "= '" + scan_UID + "', " + NAME_AS_AADHAR + "= '" + scan_NAME + "' WHERE " + MEMBER_ID + "= '" + returnMemberId + "'");

        Toast.makeText(this, "Record Updated", Toast.LENGTH_LONG).show();

        SQLITEDATABASEFORURNUPDATION.close();
        URNHELPER = new UrnDetailsHelperTable(this);
        //mDataItemsList.clear();
        mDataItemsListUpdated = URNHELPER.getURNDetails();
        URNHELPER.close();
       // Log.d(TAG, "updated mDataItemsList : " + mDataItemsListUpdated);

        if (mDataItemsListUpdated.size() >= 1) {
            rc_UrnDetails.setVisibility(View.VISIBLE);
            btn_saveUrnDetails.setVisibility(View.VISIBLE);
            agreeLayout.setVisibility(View.VISIBLE);
            urnDetailsAdapter = new UrnDetailsAdapterScan(AadharSeedingActivity.this, getApplicationContext(), mDataItemsListUpdated);
            rc_UrnDetails.setAdapter(urnDetailsAdapter);
            urnDetailsAdapter.notifyDataSetChanged();
        } else {
            showNoRecordDialog();
            rc_UrnDetails.setVisibility(View.GONE);
            btn_saveUrnDetails.setVisibility(View.GONE);
            agreeLayout.setVisibility(View.GONE);
        }


    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(getBaseContext());
    }
}
