package com.effiatechnologies.vidalinvestigator.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.helper.UrnDetailsHelperTable;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class LocalAadharSeedingActivity extends AppCompatActivity {


   // private static final String TAG = "localAadharSeedingActivity";

    private static final int REQUEST_CODE_SCAN = 100;

    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;
    // private UrnDetailsAddService mApiService = null;

    private EditText etAddNewMemberURN = null;
    private EditText etAddNewMemberId = null;
    private EditText etAddNewMemberName = null;
    private EditText etAddNewMemAge = null;
    private EditText etAddNewMemGender = null;
    private EditText etAddNewMemRelation = null;
    private EditText etAddNewMemNameInAadhar = null;
    private EditText et_AddNewMemMobileNum = null;
    private EditText et_AddNewMemAadharNum = null;
    private Button btn_ScanQRCOde = null;
    private CheckBox checkbox_new = null;
    private Button btn_Save = null;
    private Button btn_Finish = null;

    private String urn_string;
    private String memberId_string;
    private String memberName_string;
    private String memberAge_string;
    private String nameInAadhar_string;
    private String mobileNum_string;
    private String aadharNum_string;
    private String formattedDate;

    private UrnDetailsHelperTable urnDetailsHelperTable = null;
    private SQLiteDatabase SQLITEDATABASE;
    private String SQLiteQuery = null;
    private String IMIE_Number;
    private String LoginId;
    private int UserCode;
    private int saveMemberCount = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_aadhar_seeding);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        builder = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);
        IMIE_Number = SharedPreferenceUtil.getIMIENum(LocalAadharSeedingActivity.this);
        LoginId = SharedPreferenceUtil.getLoginId(LocalAadharSeedingActivity.this);
        // Log.d(TAG, "LoginId : " + LoginId);

        UserCode = SharedPreferenceUtil.getLoginCode(LocalAadharSeedingActivity.this);
        //  Log.d(TAG, "UserCode : " + UserCode);
        etAddNewMemberURN = (EditText) findViewById(R.id.et_LocalAddMemberURN);
        etAddNewMemberId = (EditText) findViewById(R.id.et_LocalAddMemberID);
        etAddNewMemberName = (EditText) findViewById(R.id.et_LocalAddMemberName);
        etAddNewMemAge = (EditText) findViewById(R.id.et_LocalAddMemberAge);
        etAddNewMemGender = (EditText) findViewById(R.id.et_LocalAddMemberGender);
        etAddNewMemRelation = (EditText) findViewById(R.id.et_LocalAddMemberRelation);
        etAddNewMemNameInAadhar = (EditText) findViewById(R.id.et_LocalAddMemberNameInAadhar);
        et_AddNewMemMobileNum = (EditText) findViewById(R.id.et_LocalAddMemberMobileNum);
        et_AddNewMemAadharNum = (EditText) findViewById(R.id.et_LocalAddMemberAadharNum);
        btn_ScanQRCOde = (Button) findViewById(R.id.btn_LocalAddMemberScanQRCode);
        checkbox_new = (CheckBox) findViewById(R.id.checkbox_LocalAddMember);
        btn_Save = (Button) findViewById(R.id.btn_LocalAddMemberSave);
        btn_Finish = (Button) findViewById(R.id.btn_LocalAddMemberFinish);

        urnDetailsHelperTable = new UrnDetailsHelperTable(this);


        btn_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  Log.d(TAG, "saveMemberCount : " + saveMemberCount);
                if (saveMemberCount >= 5)
                {
                    AlertBox();
                }
                else
                {
                    saveMemberDetails();
                }
            }
        });

        btn_Finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent finishIntent = new Intent(LocalAadharSeedingActivity.this, AadharSeedingNavigationActivity.class);
                startActivity(finishIntent);
                finish();

            }
        });
    }

    private void saveMemberDetails() {

        boolean cancel = false;

        urn_string = etAddNewMemberURN.getText().toString();
        memberId_string = etAddNewMemberId.getText().toString();
        memberName_string = etAddNewMemberName.getText().toString();
        memberAge_string = etAddNewMemAge.getText().toString();
        nameInAadhar_string = etAddNewMemNameInAadhar.getText().toString();
        mobileNum_string = et_AddNewMemMobileNum.getText().toString();
        aadharNum_string = et_AddNewMemAadharNum.getText().toString();

        if (TextUtils.isEmpty(urn_string)) {
            etAddNewMemberURN.setError("Enter URN");
            cancel = true;
        }
        if (TextUtils.isEmpty(memberId_string)) {
            etAddNewMemberId.setError("Enter Member Id");
            cancel = true;
        }
        if (TextUtils.isEmpty(memberName_string)) {
            etAddNewMemberName.setError("Enter Member Name");
            cancel = true;
        }
        if (TextUtils.isEmpty(memberAge_string)) {
            etAddNewMemAge.setError("Enter Member Age");
            cancel = true;
        }
        if (TextUtils.isEmpty(nameInAadhar_string)) {
            etAddNewMemNameInAadhar.setError("Enter Name As In Aadhar");
            cancel = true;
        }
        if (TextUtils.isEmpty(mobileNum_string)) {
            et_AddNewMemMobileNum.setError("Enter Mobile Number");
            cancel = true;
        } else if (!(AppUtil.isMobileNumbervalid(mobileNum_string))) {
            et_AddNewMemMobileNum.setError("Enter Valid Mobile Number");
            cancel = true;
        }
        if (TextUtils.isEmpty(aadharNum_string)) {
            et_AddNewMemAadharNum.setError("Enter Aadhar Number");
            cancel = true;
        }
        if (checkbox_new.isChecked() == false) {
            AppUtil.toastMessage(this, "Please Check The Checkbox To Confirm");
            cancel = true;
        }
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        formattedDate = df.format(c.getTime());
        // Log.d(TAG, "formattedDate : " + formattedDate);
        if (!cancel) {

            /*urnDetailsHelperTable.addURNDetails(urnDetailsResponseList);
            //Log.d(TAG, "response from sqlite 1" + urnDetailsHelperTable.getURNDetails().toString());

            urnSqliteResponseList = urnDetailsHelperTable.getURNDetails();
            // Log.d(TAG, "urnSqliteResponseList 2" + urnSqliteResponseList);*/

                SQLITEDATABASE = this.openOrCreateDatabase("LocalURNMemberDetails.db", Context.MODE_PRIVATE, null);

                SQLITEDATABASE.execSQL("CREATE TABLE IF NOT EXISTS URNMemberDetailsTable(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,URN VARCHAR,MemberID INTEGER,MemberName VARCHAR,Age VARCHAR,AadhaarNo VARCHAR,NameAsAadhaar VARCHAR,MobileNO VARCHAR,IMIENumber VARCHAR,DATETIME VARCHAR,LoginId VARCHAR,UserCode VARCHAR);");

                SQLiteQuery = "INSERT INTO URNMemberDetailsTable(URN,MemberID,MemberName,Age,AadhaarNo,NameAsAadhaar,MobileNO,IMIENumber,DATETIME,LoginId,UserCode) " +
                        "VALUES('" + urn_string + "','" + memberId_string + "','" + memberName_string + "','" + memberAge_string + "','" + aadharNum_string + "','" + nameInAadhar_string + "','" + mobileNum_string + "','" + IMIE_Number + "','" + formattedDate + "','" + LoginId + "','" + UserCode + "');";

                SQLITEDATABASE.execSQL(SQLiteQuery);
                SQLITEDATABASE.close();
                saveMemberCount = saveMemberCount + 1;
                Toast.makeText(this, "Data Added Successfully", Toast.LENGTH_LONG).show();
                //remarksUrnResponseList.clear();
                // selectedRemarksId = 0;
                // selectedRemarks = null;

            clearAllFields();



        }

    }

    private void clearAllFields() {

        etAddNewMemberURN.getText().clear();
        etAddNewMemberId.getText().clear();
        etAddNewMemberName.getText().clear();
        etAddNewMemAge.getText().clear();
        etAddNewMemNameInAadhar.getText().clear();
        et_AddNewMemMobileNum.getText().clear();
        et_AddNewMemAadharNum.getText().clear();
        checkbox_new.setChecked(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.dashboard:
                // Intent homeIntent = new Intent(ClaimsDetailsActivity.this, InvestigatorLoginActivity.class);

                Intent homeIntent = new Intent(LocalAadharSeedingActivity.this, AadharSeedingNavigationActivity.class);
                startActivity(homeIntent);
                finish();
                break;

            case R.id.logout:
                manageLogout();
                break;

            case android.R.id.home:

                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
    private void manageLogout() {
        // Log.d(TAG, "in logout");

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(LocalAadharSeedingActivity.this);
        builder.setTitle("Confirm");
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to Sign Out?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                RemoveDetails();
                Intent claimsLogoutIntent = new Intent(LocalAadharSeedingActivity.this, LoginActivity.class);
                claimsLogoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(claimsLogoutIntent);
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void  RemoveDetails()
    {
        SharedPreferenceUtil.removeLoginId(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeIMIENum(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeFcmId(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeUserName(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeOfficeTypeCode(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeDefaultProcessTask(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeAdditionalProcessTask(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeRoleCode(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeLoginStatus(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeUserCode(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeTPACompCode(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeTPACompName(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeTPAStatus(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeLoginCode(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeLoginStateCode(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeLoginStateName(LocalAadharSeedingActivity.this);
        SharedPreferenceUtil.removeDistrictCode(LocalAadharSeedingActivity.this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();


    }

    private void AlertBox() {

        builder.setCancelable(false);
        builder.setTitle("Information");
        builder.setMessage("You Can Not Add More Than 5 Members!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //finish();
            }
        });

        builder.show();
    }
}
