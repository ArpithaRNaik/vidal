
package com.effiatechnologies.vidalinvestigator.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.adapter.AddedMemberDetailsAdapterScan;
import com.effiatechnologies.vidalinvestigator.adapter.UrnDetailsAdapterScan;
import com.effiatechnologies.vidalinvestigator.helper.LocalMemberDetailsHelperTable;
import com.effiatechnologies.vidalinvestigator.helper.UrnDetailsHelperTable;
import com.effiatechnologies.vidalinvestigator.model.LocalMemDetailsResponse;
import com.effiatechnologies.vidalinvestigator.model.UrnDetailsResponse;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import java.util.List;

public class LocallyAddedMemberDetailsActivity extends AppCompatActivity {

   // private static final String TAG = "addedmemberDetailsActivity";
    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;

    private RecyclerView recyclerMemberDetails;
    private CheckBox checkBoxSelect;
    private LinearLayout checkboxLayout;
    private Button BtnSync = null;
    private Button BtnClear = null;
    private LocalMemberDetailsHelperTable localMemberDetailsHelperTable = null;
    private List<LocalMemDetailsResponse> localMemDetailsResponseList = null;
    private AddedMemberDetailsAdapterScan addedMemberDetailsAdapterScan = null;
    private TextView tvRecordCount = null;
    private LinearLayout layoutRecordCount = null;
    private int recordCount;
    private SQLiteDatabase SQLITEDATABASE;
    private String GetSQliteQuery;
    private Cursor cursor;
    private String DeleteQuery;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locally_added_member_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        builder = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);

        recyclerMemberDetails = (RecyclerView) findViewById(R.id.recyclerViewMemDetails);
        tvRecordCount = (TextView) findViewById(R.id.tv_recordCount);
        layoutRecordCount = (LinearLayout) findViewById(R.id.layout_recordCount);
        checkBoxSelect = (CheckBox) findViewById(R.id.save_LAcheckbox);
        checkboxLayout = (LinearLayout) findViewById(R.id.aggrement_LocalLAyout);
        BtnSync = (Button) findViewById(R.id.btn_Sync);
        BtnClear = (Button) findViewById(R.id.btn_Clear);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerMemberDetails.setHasFixedSize(true);
        recyclerMemberDetails.setLayoutManager(mLayoutManager);
        recyclerMemberDetails.setItemAnimator(new DefaultItemAnimator());
        localMemberDetailsHelperTable = new LocalMemberDetailsHelperTable(this);

        getMemberDetails();

        BtnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deleteSqliteData();
                deleteSqliteList();
            }
        });

    }

    private void getMemberDetails() {

        localMemDetailsResponseList = localMemberDetailsHelperTable.getURNDetails();
      //  Log.d(TAG, "localMemDetailsResponseList : " + localMemDetailsResponseList);
        recordCount = localMemDetailsResponseList.size();
        tvRecordCount.setText(String.valueOf(recordCount));
        tvRecordCount.requestFocus();

        localMemberDetailsHelperTable.close();
        if (localMemDetailsResponseList.size() >= 1) {
            recyclerMemberDetails.setVisibility(View.VISIBLE);
            BtnSync.setVisibility(View.VISIBLE);
            checkboxLayout.setVisibility(View.VISIBLE);
            addedMemberDetailsAdapterScan = new AddedMemberDetailsAdapterScan(this, getApplicationContext(), localMemDetailsResponseList);
            recyclerMemberDetails.setAdapter(addedMemberDetailsAdapterScan);
            addedMemberDetailsAdapterScan.notifyDataSetChanged();
        } else {
            showNoRecordDialog();
            recyclerMemberDetails.setVisibility(View.GONE);
            //BtnSync.setVisibility(View.GONE);
            checkboxLayout.setVisibility(View.GONE);
            BtnSync.setEnabled(false);
            BtnClear.setEnabled(false);
            //addedMemberDetailsAdapterScan.notifyDataSetChanged();
           // notify();
        }
    }

    private void deleteSqliteData() {
        //  Log.d(TAG, "insied deleteSqliteData");
        SQLITEDATABASE = this.openOrCreateDatabase("LocalURNMemberDetails.db", Context.MODE_PRIVATE, null);
        //  Log.d(TAG, "SQLITEDATABASE : " + SQLITEDATABASE);
        // SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();
        GetSQliteQuery = "SELECT * FROM URNMemberDetailsTable";
        //  Log.d(TAG, "GetSQliteQuery : " + GetSQliteQuery);
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        //  Log.d(TAG, "cursor : " + cursor);
        cursor.moveToFirst();
        DeleteQuery = "DELETE FROM URNMemberDetailsTable";
        SQLITEDATABASE.execSQL(DeleteQuery);
        //  Toast.makeText(this, "Record Deleted", Toast.LENGTH_LONG).show();
        cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
        /*SQLITEDATABASE = SQLITEHELPER.getWritableDatabase();

        SQLITEDATABASE.execSQL("DELETE FROM QuestionsAnswerTable"); //delete all rows in a table*/
        SQLITEDATABASE.close();
        Toast.makeText(this, "Data Cleared Successfully", Toast.LENGTH_LONG).show();

        getMemberDetails();
    }

    private void deleteSqliteList() {

        SQLITEDATABASE = localMemberDetailsHelperTable.getWritableDatabase();
        String countAssCls = "SELECT count(*) FROM URNMemberDetailsTable";
        //  Log.d(TAG, "countAssCls : " + countAssCls);
        Cursor mcursorAss = SQLITEDATABASE.rawQuery(countAssCls, null);
        mcursorAss.moveToFirst();
        int icountAss = mcursorAss.getInt(0);
        if (icountAss > 0) {
            //  Log.d(TAG, "icountAss is greater than 0 : " + icountAss);
            SQLITEDATABASE.execSQL("DELETE FROM URNMemberDetailsTable"); //delete all rows in a table
            SQLITEDATABASE.close();
            Toast.makeText(this, "Data Cleared Successfully", Toast.LENGTH_LONG).show();
            getMemberDetails();
        } else {
            //  Log.d(TAG, "inside icountAss less than 0 ");
            //  finish();

        }
    }

    private void showNoRecordDialog() {

        builder.setCancelable(false);
        builder.setTitle("Information");
        builder.setMessage("No Records Found!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //finish();
            }
        });

        builder.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.dashboard:
                // Intent homeIntent = new Intent(ClaimsDetailsActivity.this, InvestigatorLoginActivity.class);

                Intent homeIntent = new Intent(LocallyAddedMemberDetailsActivity.this, AadharSeedingNavigationActivity.class);
                startActivity(homeIntent);
                finish();
                break;

            case R.id.logout:
                manageLogout();
                break;

            case android.R.id.home:

                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
    private void manageLogout() {
        // Log.d(TAG, "in logout");

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(LocallyAddedMemberDetailsActivity.this);
        builder.setTitle("Confirm");
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to Sign Out?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                RemoveDetails();
                Intent claimsLogoutIntent = new Intent(LocallyAddedMemberDetailsActivity.this, LoginActivity.class);
                claimsLogoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(claimsLogoutIntent);
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
    private void  RemoveDetails()
    {
        SharedPreferenceUtil.removeLoginId(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeIMIENum(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeFcmId(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeUserName(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeOfficeTypeCode(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeDefaultProcessTask(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeAdditionalProcessTask(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeRoleCode(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeLoginStatus(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeUserCode(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeTPACompCode(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeTPACompName(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeTPAStatus(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeLoginCode(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeLoginStateCode(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeLoginStateName(LocallyAddedMemberDetailsActivity.this);
        SharedPreferenceUtil.removeDistrictCode(LocallyAddedMemberDetailsActivity.this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
