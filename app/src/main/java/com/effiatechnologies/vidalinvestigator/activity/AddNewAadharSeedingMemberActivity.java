package com.effiatechnologies.vidalinvestigator.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.adapter.DistrictSpinnerAdapter;
import com.effiatechnologies.vidalinvestigator.adapter.GenderNameSpinnerAdapter;
import com.effiatechnologies.vidalinvestigator.adapter.RelationsNameSpinnerAdapter;
import com.effiatechnologies.vidalinvestigator.adapter.UrnDetailsRemarksSpinnerAdapter;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.model.AddNewMemberInput;
import com.effiatechnologies.vidalinvestigator.model.DistrictModel;
import com.effiatechnologies.vidalinvestigator.model.GenderResponse;
import com.effiatechnologies.vidalinvestigator.model.RelationsResponse;
import com.effiatechnologies.vidalinvestigator.model.RemarksUrnResponse;
import com.effiatechnologies.vidalinvestigator.model.UpdateUrnDetailsInput;
import com.effiatechnologies.vidalinvestigator.model.UpdatedUrnDetailsResponse;
import com.effiatechnologies.vidalinvestigator.model.UrnDetailsInput;
import com.effiatechnologies.vidalinvestigator.model.UrnDetailsResponse;
import com.effiatechnologies.vidalinvestigator.model.UserData;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public class AddNewAadharSeedingMemberActivity extends AppCompatActivity {

    private static final String TAG = "AddNewMemberAadhar";

    private static final int REQUEST_CODE_SCAN = 100;

    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;
    private UrnDetailsAddService mApiService = null;

    private EditText et_AM_Urn = null;
    private EditText et_AM_MenmberID = null;
    private EditText et_AM_MemberName = null;
    private EditText et_AM_MemberAge = null;
    private EditText et_AM_NameInAadhar = null;
    private EditText et_AM_MobileNumber = null;
    private EditText et_AM_AadharNum = null;
    private Spinner spn_AM_MemberRelation = null;
    private Spinner spn_AM_MemberGender = null;
    private Spinner Spn_AM_Remarks = null;
    private Button btn_AM_ScanQRCode = null;
    private Button btn_AM_Save = null;
    private Button btn_AM_Finish = null;
    private CheckBox CheckBox_AM = null;

    private String urn_string;
    private String memberId_string;
    private String memberName_string;
    private String memberAge_string;
    private String nameInAadhar_string;
    private String mobileNum_string;
    private String aadharNum_string;

    private List<GenderResponse> genderResponseList = null;
    private GenderNameSpinnerAdapter genderNameSpinnerAdapter = null;
    private GenderResponse genderResponse = null;
    private int genderCode;
    private String genderName;

    private List<RelationsResponse> relationsResponseList = null;
    private RelationsNameSpinnerAdapter relationsNameSpinnerAdapter = null;
    private RelationsResponse relationsResponse = null;
    private int relationsCode;
    private String relationName;

    private List<RemarksUrnResponse> remarksUrnResponseList = null;
    private UrnDetailsRemarksSpinnerAdapter urnDetailsRemarksSpinnerAdapter = null;
    private int remarksId;
    private String remarksName;

    private AddNewMemberInput urnDetailsResponse = null;
    private String resultMessageRe = null;
    private String IMIENumber;
    private String LoginId;
    private int UserCode;
    private String formattedDate;

    private String returnString;
    private String scan_UID;
    private String scan_NAME;
    private String scan_GENDER;
    private String scan_YOB;
    private static final String FAMILY_ID = "FamilyId";
    private static final String URN_VALUE = "UrnId";
    private String FamilyId;
    private String UrnId;
    private int saveMemberCount = 0;
    private int DistCode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_aadhar_seeding_member);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        builder = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);
        mApiService = MyApplication.getRetrofit().create(UrnDetailsAddService.class);

        Intent faIntent = getIntent();
        if (faIntent != null) {
            FamilyId = faIntent.getStringExtra(FAMILY_ID);
            UrnId = faIntent.getStringExtra(URN_VALUE);
           // Log.d(TAG, "FamilyId : " + FamilyId);
        }

        IMIENumber = SharedPreferenceUtil.getIMIENum(AddNewAadharSeedingMemberActivity.this);
        // Log.d(TAG, "IMIENumber : " + IMIENumber);
        LoginId = SharedPreferenceUtil.getLoginId(AddNewAadharSeedingMemberActivity.this);
        // Log.d(TAG, "LoginId : " + LoginId);
        UserCode = SharedPreferenceUtil.getLoginCode(AddNewAadharSeedingMemberActivity.this);
        //  Log.d(TAG, "UserCode : " + UserCode);
        DistCode = SharedPreferenceUtil.getDistrictCode(AddNewAadharSeedingMemberActivity.this);
        Log.d(TAG, "DistCode : " + DistCode);

        et_AM_Urn = (EditText) findViewById(R.id.et_AddMemberURN);
        et_AM_Urn.setText(UrnId);
        et_AM_Urn.setEnabled(false);
        et_AM_MenmberID = (EditText) findViewById(R.id.et_AddMemberID);
        et_AM_MemberName = (EditText) findViewById(R.id.et_AddMemberName);
        spn_AM_MemberRelation = (Spinner) findViewById(R.id.spinner_AddMemberRelation);
        et_AM_MemberAge = (EditText) findViewById(R.id.et_AddMemberAge);
        spn_AM_MemberGender = (Spinner) findViewById(R.id.spinner_AddMemberGender);
        et_AM_NameInAadhar = (EditText) findViewById(R.id.et_AddMemberNameInAadhar);
        et_AM_MobileNumber = (EditText) findViewById(R.id.et_AddMemberMobileNum);
        et_AM_AadharNum = (EditText) findViewById(R.id.et_AddMemberAadharNum);
        btn_AM_ScanQRCode = (Button) findViewById(R.id.btn_AddMemberScanQRCode);
        Spn_AM_Remarks = (Spinner) findViewById(R.id.spinner_AddMemberRemarks);
        CheckBox_AM = (CheckBox) findViewById(R.id.checkbox_AddMember);
        btn_AM_Save = (Button) findViewById(R.id.btn_AddMemberSave);
        btn_AM_Finish = (Button) findViewById(R.id.btn_AddMemberFinish);

        getGenders();
        getRelations();

        remarksUrnResponseList = new ArrayList<>();
        remarksUrnResponseList.add(0, new RemarksUrnResponse(0, "Select"));
        remarksUrnResponseList.add(1, new RemarksUrnResponse(1, "Death"));
        remarksUrnResponseList.add(2, new RemarksUrnResponse(2, "Aadhar Not Available"));
        remarksUrnResponseList.add(3, new RemarksUrnResponse(3, "Split Of Family"));
        remarksUrnResponseList.add(4, new RemarksUrnResponse(4, "Member Not available"));
        urnDetailsRemarksSpinnerAdapter = new UrnDetailsRemarksSpinnerAdapter(AddNewAadharSeedingMemberActivity.this, remarksUrnResponseList);
        Spn_AM_Remarks.setAdapter(urnDetailsRemarksSpinnerAdapter);
        urnDetailsRemarksSpinnerAdapter.notifyDataSetChanged();

        spn_AM_MemberRelation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                {

                    relationsResponse = (RelationsResponse) parent.getItemAtPosition(position);
                    relationsCode = relationsResponse.getRelationCode();
                   // Log.d(TAG, "relationsCode : " + relationsCode);

                    relationName = relationsResponse.getRelationDescription();
                   // Log.d(TAG, "relationName : " + relationName);
                    //requiredDetails.setCarrierType(subjectsResponse.getSubjectID());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
        spn_AM_MemberGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                {

                    genderResponse = (GenderResponse) parent.getItemAtPosition(position);
                    genderCode = genderResponse.getGenderCode();
                  //  Log.d(TAG, "genderCode : " + genderCode);

                    genderName = genderResponse.getGenderName();
                  //  Log.d(TAG, "genderName : " + genderName);
                    //requiredDetails.setCarrierType(subjectsResponse.getSubjectID());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
        Spn_AM_Remarks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {

                    remarksId = remarksUrnResponseList.get(position).getRemarksId();
                    remarksName = remarksUrnResponseList.get(position).getRemarks();
                  //  Log.d(TAG, "remarksId 1: " + remarksId);
                  //  Log.d(TAG, "remarksName 1: " + remarksName);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_AM_Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveNewMemberDetails();
            }
        });

        btn_AM_ScanQRCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddNewAadharSeedingMemberActivity.this, ScanQRCodeForNewMemberActivity.class);
                //Activity origin = (Activity) mContext;
                startActivityForResult(intent, REQUEST_CODE_SCAN);
            }
        });
        btn_AM_Finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent finishIntent = new Intent(AddNewAadharSeedingMemberActivity.this, AadharSeedingNavigationActivity.class);
                startActivity(finishIntent);
                finish();
            }
        });
    }

    private void getGenders() {

        //  Log.d(TAG, "inside getPeriodFaculyNames");
        if (AppUtil.isNetworkAvailable(this)) {
            progressDialog.setMessage("Please Wait....");
            progressDialog.show();
            progressDialog.setCancelable(false);

            Call<List<GenderResponse>> call = mApiService.getGenders();
            call.enqueue(new Callback<List<GenderResponse>>() {
                @Override
                public void onResponse(Call<List<GenderResponse>> call, Response<List<GenderResponse>> response) {

                    if (response.isSuccessful()) {
                        //  Log.d(TAG, "response is successfull");
                        progressDialog.dismiss();
                        genderResponseList = response.body();
                      //  Log.d(TAG, "genderResponseList : " + genderResponseList);
                        genderResponseList.add(0, new GenderResponse(0, " Select "));
                      //  Log.d(TAG, "genderResponseList : " + genderResponseList);
                        //genderNameSpinnerAdapter = new GenderNameSpinnerAdapter(AddNewAadharSeedingMemberActivity.this, genderResponseList);
                        setGendersSpinner();

                    } else {

                        progressDialog.dismiss();
                        int statusCode = response.code();
                        //  Log.d(TAG, statusCode + "failed");
                        //  ResponseBody errorBody = response.errorBody();

                    }
                }

                @Override
                public void onFailure(Call<List<GenderResponse>> call, Throwable t) {
                    //  Log.d(TAG, "in failure" + t.getMessage());
                    showErrorDialog();
                    progressDialog.dismiss();
                }
            });
        } else {
            showInternetNotAvailbale();
            // AppUtil.toastMessage(AddStudentAttendanceActivity.this, "Check Internet connection");
        }
    }

    private void getRelations() {

       // Log.d(TAG, "inside getRelations");
        if (AppUtil.isNetworkAvailable(this)) {
            progressDialog.setMessage("Please Wait....");
            progressDialog.show();
            progressDialog.setCancelable(false);

            Call<List<RelationsResponse>> call = mApiService.getRelations();
            call.enqueue(new Callback<List<RelationsResponse>>() {
                @Override
                public void onResponse(Call<List<RelationsResponse>> call, Response<List<RelationsResponse>> response) {

                    if (response.isSuccessful()) {
                        //  Log.d(TAG, "response is successfull");
                        progressDialog.dismiss();
                        relationsResponseList = response.body();
                      //  Log.d(TAG, "relationsResponseList : " + relationsResponseList);
                        relationsResponseList.add(0, new RelationsResponse(0, " Select "));
                      //  Log.d(TAG, "relationsResponseList : " + relationsResponseList);
                        //relationsNameSpinnerAdapter = new RelationsNameSpinnerAdapter(AddNewAadharSeedingMemberActivity.this, relationsResponseList);
                        setRelationsSpinner();
                    } else {

                        progressDialog.dismiss();
                        int statusCode = response.code();
                        //  Log.d(TAG, statusCode + "failed");
                        //  ResponseBody errorBody = response.errorBody();

                    }
                }

                @Override
                public void onFailure(Call<List<RelationsResponse>> call, Throwable t) {
                    //  Log.d(TAG, "in failure" + t.getMessage());
                    showErrorDialog();
                    progressDialog.dismiss();
                }
            });
        } else {
            showInternetNotAvailbale();
            // AppUtil.toastMessage(AddStudentAttendanceActivity.this, "Check Internet connection");
        }
    }


    private void setRelationsSpinner() {

        if (spn_AM_MemberRelation != null) {
            //  Log.d(TAG, "spinner not null :" + subjectsResponseList.size());
            relationsNameSpinnerAdapter = new RelationsNameSpinnerAdapter(this, relationsResponseList);
            spn_AM_MemberRelation.setAdapter(relationsNameSpinnerAdapter);
        } else {
            //  Log.d(TAG, "spinner  null");
        }

    }

    private void setGendersSpinner() {

        if (spn_AM_MemberGender != null) {
            //  Log.d(TAG, "spinner not null :" + subjectsResponseList.size());
            genderNameSpinnerAdapter = new GenderNameSpinnerAdapter(this, genderResponseList);
            spn_AM_MemberGender.setAdapter(genderNameSpinnerAdapter);
        } else {
            //  Log.d(TAG, "spinner  null");
        }

    }


    private void SaveNewMemberDetails() {


        //Log.d(TAG, "inside validateSavedDescription");
        boolean cancel = false;

        urn_string = et_AM_Urn.getText().toString();
        memberId_string = et_AM_MenmberID.getText().toString();
        memberName_string = et_AM_MemberName.getText().toString();
        memberAge_string = et_AM_MemberAge.getText().toString();
        nameInAadhar_string = et_AM_NameInAadhar.getText().toString();
        mobileNum_string = et_AM_MobileNumber.getText().toString();
        aadharNum_string = et_AM_AadharNum.getText().toString();

        if (TextUtils.isEmpty(urn_string)) {
            et_AM_Urn.setError("Enter URN");
            cancel = true;
        }
        if (TextUtils.isEmpty(memberId_string)) {
            et_AM_MenmberID.setError("Enter Member Id");
            cancel = true;
        }
        if (TextUtils.isEmpty(memberName_string)) {
            et_AM_MemberName.setError("Enter Member Name");
            cancel = true;
        }
        if (TextUtils.isEmpty(memberAge_string)) {
            et_AM_MemberAge.setError("Enter Member Age");
            cancel = true;
        }
        if (TextUtils.isEmpty(nameInAadhar_string)) {
            et_AM_NameInAadhar.setError("Enter Name As In Aadhar");
            cancel = true;
        }
        if (TextUtils.isEmpty(mobileNum_string)) {
            et_AM_MobileNumber.setError("Enter Mobile Number");
            cancel = true;
        }
        else if (!(AppUtil.isMobileNumbervalid(mobileNum_string))) {
            et_AM_MobileNumber.setError("Enter Valid Mobile Number");
            cancel = true;
        }
        if (TextUtils.isEmpty(aadharNum_string)) {
            et_AM_AadharNum.setError("Enter Aadhar Number");
            cancel = true;
        }
        if (relationsCode == 0) {
            AppUtil.toastMessage(this, "Please Select Relation");
            cancel = true;
        }
        if (genderCode == 0) {
            AppUtil.toastMessage(this, "Please Select Gender");
            cancel = true;
        }
        if (remarksId == 0) {
            AppUtil.toastMessage(this, "Please Select Remarks");
            cancel = true;
        }
        if (CheckBox_AM.isChecked() == false) {
            AppUtil.toastMessage(this, "Please Check The Checkbox To Confirm");
            cancel = true;
        }
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        formattedDate = df.format(c.getTime());
        // Log.d(TAG, "formattedDate : " + formattedDate);
        if (!cancel) {

            if (AppUtil.isNetworkAvailable(this)) {
                progressDialog.setMessage("Please Wait....");
                progressDialog.show();
                progressDialog.setCancelable(false);

                urnDetailsResponse = new AddNewMemberInput();

                urnDetailsResponse.setURN(urn_string);
                urnDetailsResponse.setMemberID(Integer.parseInt(memberId_string));
                urnDetailsResponse.setMemberName(memberName_string);
                urnDetailsResponse.setRelationCode(String.valueOf(relationsCode));
                urnDetailsResponse.setRelation(relationName);
                urnDetailsResponse.setAge(memberAge_string);
                urnDetailsResponse.setGenderCode(String.valueOf(genderCode));
                urnDetailsResponse.setGenderName(genderName);
                urnDetailsResponse.setNameAsAadhaar(nameInAadhar_string);
                urnDetailsResponse.setMobileNO(mobileNum_string);
                urnDetailsResponse.setAadhaarNumber(aadharNum_string);
                urnDetailsResponse.setAadhaarNo(aadharNum_string);
                urnDetailsResponse.setRemarksID(remarksId);
                urnDetailsResponse.setRemarksName(remarksName);
                urnDetailsResponse.setIMIENumber(IMIENumber);
                urnDetailsResponse.setLoginId(LoginId);
                urnDetailsResponse.setUserCode(String.valueOf(UserCode));
                urnDetailsResponse.setDateTime(formattedDate);
                urnDetailsResponse.setFamilyId(FamilyId);
                urnDetailsResponse.setFlagValue(1);

               // Log.d(TAG, "addNewMemberInput : " + urnDetailsResponse);

                Call<UpdatedUrnDetailsResponse> call = mApiService.submitNewMemUrnDescription(urnDetailsResponse);
                call.enqueue(new Callback<UpdatedUrnDetailsResponse>() {
                    @Override
                    public void onResponse(Call<UpdatedUrnDetailsResponse> call, Response<UpdatedUrnDetailsResponse> response) {

                        if (response.isSuccessful()) {
                          //  Log.d(TAG, response.body() + "inside successfull response");
                            if (response.body() != null) {
                                progressDialog.dismiss();
                                UpdatedUrnDetailsResponse userResponse = response.body();
                                // Log.d(TAG, userResponse + "");

                                List<UserData> userData = userResponse.getTable1();
                                // Log.d(TAG, "userData : " + userData);

                                for (int i = 0; i < userData.size(); i++) {
                                    resultMessageRe = userData.get(i).getResult();
                                    // Log.d(TAG, "resultMessageRe : " + resultMessageRe);
                                    if (resultMessageRe.contains("Member Details Inserted Successfully")) {
                                        //  Log.d(TAG, "resultMessageRe inside if : " + resultMessageRe);
                                        saveMemberCount = saveMemberCount + 1;
                                        showSMSApprovedDialog(resultMessageRe);

                                        //  showSuccessDialog();
                                    }
                                    else if (resultMessageRe.contains("AADHAR No. Already Exists"))
                                    {
                                        showAadharNumExistDialog(resultMessageRe);

                                    }
                                    else
                                     {
                                        // Log.d(TAG, "resultMessageRe inside else: " + resultMessageRe);
                                        showSMSNotCanceledDialog(resultMessageRe);

                                    }
                                }

                            } else if (response.body() == null) {
                              //  Log.d(TAG, "response is null" + response.body());
                                //response.body();
                                showNoRecordDialog();
                                progressDialog.dismiss();

                            }

                        } else {
                          //  Log.d(TAG, "response is not successfull" + response.body());
                            //deleteSqliteList();
                            //deleteSqliteData();
                            showNoRecordDialog();
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<UpdatedUrnDetailsResponse> call, Throwable t) {
                       // Log.d(TAG, "in failure" + t.getMessage());
                        showErrorDialog();
                        progressDialog.dismiss();
                    }
                });
            } else {
                showInternetNotAvailbale();
                // AppUtil.toastMessage(AddStudentAttendanceActivity.this, "Check Internet connection");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //  Log.d(TAG, "inside RESULT_OK");
            if (requestCode == REQUEST_CODE_SCAN) {
                //  Log.d(TAG, "request code : " + REQUEST_CODE_DOC);
                returnString = data.getStringExtra("result");
               // Log.d(TAG, "returnString : " + returnString);
                // readXML(returnString);

                DocumentBuilder builder = null;
                try {
                    builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    InputSource src = new InputSource();
                    src.setCharacterStream(new StringReader(returnString));

                    Document doc = builder.parse(src);

                    doc.getDocumentElement().normalize();
                    NodeList nList = doc.getElementsByTagName("PrintLetterBarcodeData");
                    for (int temp = 0; temp < nList.getLength(); temp++) {

                        Node nNode = nList.item(temp);

                        // Log.d(TAG, "Current Element :" + nNode.getNodeName());

                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {

                            Element eElement = (Element) nNode;

                            scan_UID = eElement.getAttribute("uid");
                            scan_NAME = eElement.getAttribute("name");
                            scan_GENDER = eElement.getAttribute("gender");
                            scan_YOB = eElement.getAttribute("yob");

                           // Log.d(TAG, "scan_UID : " + scan_UID);
                          //  Log.d(TAG, "scan_NAME : " + scan_NAME);
                          //  Log.d(TAG, "scan_GENDER : " + scan_GENDER);
                          //  Log.d(TAG, "scan_YOB : " + scan_YOB);

                            et_AM_AadharNum.setText(scan_UID);
                            et_AM_NameInAadhar.setText(scan_NAME);

                        }
                    }
                } catch (ParserConfigurationException e) {
                    //  Log.d(TAG, " ecxp1 " + e);
                    e.printStackTrace();
                } catch (SAXException e) {
                    //  Log.d(TAG, " ecxp2 " + e);

                    e.printStackTrace();
                } catch (IOException e) {
                    // Log.d(TAG, " ecxp3 " + e);

                    e.printStackTrace();
                }
            }
        }
    }

    private void showSMSNotCanceledDialog(String resultMessageRe) {

        builder.setCancelable(false);
        builder.setTitle("Message");
        builder.setMessage(resultMessageRe);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //  finish();
            }
        });

        builder.show();
    }
    private void showAadharNumExistDialog(String resultMessageRe) {

        builder.setCancelable(false);
        builder.setTitle("Message");
        builder.setMessage(resultMessageRe);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //  finish();
                CheckBox_AM.setChecked(false);
                et_AM_AadharNum.getText().clear();
                et_AM_AadharNum.setError("Enter Aadhar Number");
            }
        });

        builder.show();
    }

    private void showSMSApprovedDialog(String resultMessageRe) {

        if (saveMemberCount >= 5)
        {
            builder.setCancelable(false);
            builder.setTitle("Message");
            builder.setMessage("You Can Not Add More Than 5 Members");
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    // mActivity.finish();
               /* Intent finishIntent = new Intent(AddNewAadharSeedingMemberActivity.this, AadharSeedingNavigationActivity.class);
                startActivity(finishIntent);
                finish();*/

                }
            });
            builder.show();
        }
        else
        {
            builder.setCancelable(false);
            builder.setTitle("Message");
            builder.setMessage(resultMessageRe);
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    // mActivity.finish();
               /* Intent finishIntent = new Intent(AddNewAadharSeedingMemberActivity.this, AadharSeedingNavigationActivity.class);
                startActivity(finishIntent);
                finish();*/

                }
            });
            builder.show();
        }
    }

    private void showNoRecordDialog() {

        builder.setCancelable(false);
        builder.setTitle("Information");
        builder.setMessage("No Records Found!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //finish();
            }
        });

        builder.show();
    }

    private void showErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Network Failure");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //finish();
            }
        });

        builder.show();
    }

    private void showInternetNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Network Not Available");
        builder.setMessage("Please check your Internet Connection!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.dashboard:
                // Intent homeIntent = new Intent(ClaimsDetailsActivity.this, InvestigatorLoginActivity.class);
                //doesDatabaseExist(getApplicationContext(), DB_NAME);
                //doesURNDescDatabaseExist(getApplicationContext(), DB_NAME_URNDES);

                Intent homeIntent = new Intent(this, AadharSeedingNavigationActivity.class);
                startActivity(homeIntent);
                finish();
                break;

            case R.id.logout:
                manageLogout();
                break;

            case android.R.id.home:

                //doesDatabaseExist(getApplicationContext(), DB_NAME);
                //doesURNDescDatabaseExist(getApplicationContext(), DB_NAME_URNDES);

                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void manageLogout() {
        // Log.d(TAG, "in logout");

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(AddNewAadharSeedingMemberActivity.this);
        builder.setTitle("Confirm");
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to Sign Out?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //doesDatabaseExist(getApplicationContext(), DB_NAME);
                //doesURNDescDatabaseExist(getApplicationContext(), DB_NAME_URNDES);

                RemoveDetails();
                Intent claimsLogoutIntent = new Intent(AddNewAadharSeedingMemberActivity.this, LoginActivity.class);
                claimsLogoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(claimsLogoutIntent);
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void RemoveDetails() {
        SharedPreferenceUtil.removeLoginId(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeIMIENum(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeFcmId(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeUserName(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeOfficeTypeCode(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeDefaultProcessTask(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeAdditionalProcessTask(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeRoleCode(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeLoginStatus(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeUserCode(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeTPACompCode(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeTPACompName(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeTPAStatus(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeLoginCode(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeLoginStateCode(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeLoginStateName(AddNewAadharSeedingMemberActivity.this);
        SharedPreferenceUtil.removeDistrictCode(AddNewAadharSeedingMemberActivity.this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //doesDatabaseExist(getApplicationContext(), DB_NAME);
        //doesURNDescDatabaseExist(getApplicationContext(), DB_NAME_URNDES);


    }

    public interface UrnDetailsAddService {


        @POST(UrlConfig.UPDATE_NEW_MEMBER_DETAILS)
        Call<UpdatedUrnDetailsResponse> submitNewMemUrnDescription(@Body AddNewMemberInput urnDetailsResponse);

        @GET(UrlConfig.GET_GENDERS)
        Call<List<GenderResponse>> getGenders();

        @GET(UrlConfig.GET_RELATIONS)
        Call<List<RelationsResponse>> getRelations();

    }
}
