package com.effiatechnologies.vidalinvestigator.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.fragment.ManagementHomeGraphFragment;
import com.effiatechnologies.vidalinvestigator.fragment.ManagementHomeReportsFragment;
import com.effiatechnologies.vidalinvestigator.helper.SqliteStatesHelper;
import com.effiatechnologies.vidalinvestigator.model.StatesModel;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

   // private static final String TAG = "dashboard";
    private FragmentManager mFragmentManager = null;
    private FragmentTransaction mFragmentTransaction = null;
    private Toolbar mToolbar = null;
    private DrawerLayout mDrawer = null;
    private NavigationView navigationView = null;
    private SqliteStatesHelper sqliteStatesHelper;
    private List<StatesModel> statesModelArrayList = new ArrayList<>();
    private String stateNames;
    private int stateCodes;

    private View navKarnatakaGraph;
    private View navAllStateGraph;
    private Menu nav_Menu;
    private Bundle bundle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sqliteStatesHelper = new SqliteStatesHelper(this);
        statesModelArrayList = sqliteStatesHelper.getProducts();
      //  Log.d(TAG, "statesModelArrayList : " + statesModelArrayList);
      //  Log.d(TAG, "statesCount : " + statesModelArrayList.size());

        bundle = new Bundle();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.homeicon);
        setSupportActionBar(mToolbar);

        navAllStateGraph = findViewById(R.id.nav_allStateGraph);
        navKarnatakaGraph = findViewById(R.id.nav_karnatakaGraph);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setTitle("eClaims");
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();

        bundle = new Bundle();
        bundle.putInt("temp",0);
        ManagementHomeGraphFragment managementHomeGraphFragmentAllStateHome =new ManagementHomeGraphFragment();
        managementHomeGraphFragmentAllStateHome.setArguments(bundle);

        mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeGraphFragmentAllStateHome);
        mFragmentTransaction.addToBackStack(null);
        mFragmentTransaction.commit();
       // Log.d(TAG, "activity is replaced with fragment ");
        nav_Menu = navigationView.getMenu();

        /*if (statesModelArrayList.size() >= 2) {

            Log.d(TAG, "inside if");
            for (int i = 0; i < statesModelArrayList.size(); i++)
            {
                stateNames = statesModelArrayList.get(i).getStateName();
                Log.d(TAG, "stateNames : " + stateNames);
                stateCodes = statesModelArrayList.get(i).getStateCode();
                Log.d(TAG, "stateCodes : " + stateCodes);

                if (stateCodes == 3)
                {
                    Log.d(TAG, "inside state name punjab");

                    nav_Menu.findItem(R.id.nav_punjabGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_punjabDoc).setVisible(true);

                }
                if (stateCodes == 10)
                {
                    Log.d(TAG, "inside state name bihar");

                    nav_Menu.findItem(R.id.nav_biharGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_biharDoc).setVisible(true);

                }
                if (stateCodes == 20)
                {
                    Log.d(TAG, "inside state name JHARKHAND");

                    nav_Menu.findItem(R.id.nav_jharkhandGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_jharkhandDoc).setVisible(true);

                }
                if (stateCodes == 22)
                {
                    Log.d(TAG, "inside state name CHHATTISGARH");

                    nav_Menu.findItem(R.id.nav_chhattisgarhGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_chhattisgarhDoc).setVisible(true);

                }
                if (stateCodes == 29)
                {
                    Log.d(TAG, "inside state name KARNATAKA");

                    nav_Menu.findItem(R.id.nav_karnatakaGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_karnatakaDoc).setVisible(true);

                }
                if (stateCodes == 33)
                {
                    Log.d(TAG, "inside state name tamil nadu");
                    nav_Menu.findItem(R.id.nav_tamilnaduGraph).setVisible(true);
                    nav_Menu.findItem(R.id.nav_tamilnaduDoc).setVisible(true);

                }
            }
        } else {
            Log.d(TAG, "inside else");
            //showDataNotAvailbale();
        }*/

    }



    public void setTitle(String str) {
        mToolbar.setTitle(str);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fm = getSupportFragmentManager(); // or 'getSupportFragmentManager();'
            int count = fm.getBackStackEntryCount();
            mToolbar.setTitle("eClaims");
            //  Log.d(TAG, count + "");
            if (count == 0) {
                // Log.d(TAG, "inside if");
                //super.onBackPressed();
                new AlertDialog.Builder(this)
                        .setTitle("Close App?")
                        .setMessage("Do you really want to close this app?")
                        .setPositiveButton("YES",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //  Log.d(TAG, "inside yes");
                                        finish();
                                        Intent intent = new Intent(Intent.ACTION_MAIN);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.addCategory(Intent.CATEGORY_HOME);
                                        startActivity(intent);
                                    }
                                })
                        .setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                    }
                                }).show();
            } else if (count == 1) {
                // Log.d(TAG, "inside else if ");
                AppUtil.toastMessage(this, "Press the back button once again to close the application...");
                for (int i = 0; i < count; ++i) {
                    //  Log.d(TAG, "inside for loop");
                    fm.popBackStack();
                }
            } else {
                // Log.d(TAG, "inside else");
                for (int i = 0; i < count; ++i) {
                    // Log.d(TAG, "inside for loop");
                    fm.popBackStack();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.dashboard:
                mToolbar.setTitle("eClaims");
                //Intent homeIntent = new Intent(DashboardActivity.this, InvestigatorLoginActivity.class);
                Intent homeIntent = new Intent(DashboardActivity.this, DashboardActivity.class);
                startActivity(homeIntent);
                finish();
                /*mFragmentTransaction = mFragmentManager.beginTransaction();
                mFragmentTransaction.replace(R.id.dashboard_fragment_container, new BarGraphFragment());
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();*/
                break;

            case R.id.logout:
                manageLogout();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_allStateGraph:
                mToolbar.setTitle("All States");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",1);
                ManagementHomeGraphFragment managementHomeGraphFragmentAllStates =new ManagementHomeGraphFragment();
                managementHomeGraphFragmentAllStates.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeGraphFragmentAllStates);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_karnatakaGraph:
                mToolbar.setTitle("Karnataka");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",2);
                ManagementHomeGraphFragment managementHomeGraphFragmentKarnataka =new ManagementHomeGraphFragment();
                managementHomeGraphFragmentKarnataka.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeGraphFragmentKarnataka);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_chhattisgarhGraph:
                mToolbar.setTitle("Chhattisgarh");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",3);
                ManagementHomeGraphFragment managementHomeGraphFragmentChhattisgarh =new ManagementHomeGraphFragment();
                managementHomeGraphFragmentChhattisgarh.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeGraphFragmentChhattisgarh);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_jharkhandGraph:
                mToolbar.setTitle("Jharkhand");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",4);
                ManagementHomeGraphFragment managementHomeGraphFragmentJharkhand =new ManagementHomeGraphFragment();
                managementHomeGraphFragmentJharkhand.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeGraphFragmentJharkhand);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_tamilnaduGraph:
                mToolbar.setTitle("Tamil Nadu");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",5);
                ManagementHomeGraphFragment managementHomeGraphFragmentTamilNadu =new ManagementHomeGraphFragment();
                managementHomeGraphFragmentTamilNadu.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeGraphFragmentTamilNadu);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_biharGraph:
                mToolbar.setTitle("Bihar");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",6);
                ManagementHomeGraphFragment managementHomeGraphFragmentBihar =new ManagementHomeGraphFragment();
                managementHomeGraphFragmentBihar.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeGraphFragmentBihar);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;
            case R.id.nav_punjabGraph:
                mToolbar.setTitle("Punjab");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",7);
                ManagementHomeGraphFragment managementHomeGraphFragmentPunjab =new ManagementHomeGraphFragment();
                managementHomeGraphFragmentPunjab.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeGraphFragmentPunjab);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_allStateDoc:
                mToolbar.setTitle("All States");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",8);
                ManagementHomeReportsFragment managementHomeReportsFragmentAllStates = new ManagementHomeReportsFragment();
                managementHomeReportsFragmentAllStates.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeReportsFragmentAllStates);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_karnatakaDoc:
                mToolbar.setTitle("Karnataka");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",9);
                ManagementHomeReportsFragment managementHomeReportsFragmentKarnataka = new ManagementHomeReportsFragment();
                managementHomeReportsFragmentKarnataka.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeReportsFragmentKarnataka);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

            case R.id.nav_chhattisgarhDoc:
                mToolbar.setTitle("Chhattisgarh");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",10);
                ManagementHomeReportsFragment managementHomeReportsFragmentChattisgarh = new ManagementHomeReportsFragment();
                managementHomeReportsFragmentChattisgarh.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeReportsFragmentChattisgarh);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;
            case R.id.nav_tamilnaduDoc:
                mToolbar.setTitle("Tamil Nadu");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",11);
                ManagementHomeReportsFragment managementHomeReportsFragmentTamilNadu = new ManagementHomeReportsFragment();
                managementHomeReportsFragmentTamilNadu.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeReportsFragmentTamilNadu);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;
            case R.id.nav_biharDoc:
                mToolbar.setTitle("Bihar");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",12);
                ManagementHomeReportsFragment managementHomeReportsFragmentBihar = new ManagementHomeReportsFragment();
                managementHomeReportsFragmentBihar.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeReportsFragmentBihar);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;
            case R.id.nav_punjabDoc:
                mToolbar.setTitle("Punjab");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",13);
                ManagementHomeReportsFragment managementHomeReportsFragmentPunjab = new ManagementHomeReportsFragment();
                managementHomeReportsFragmentPunjab.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeReportsFragmentPunjab);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;
            case R.id.nav_jharkhandDoc:
                mToolbar.setTitle("Jharkhand");
                mFragmentTransaction = mFragmentManager.beginTransaction();

                bundle = new Bundle();
                bundle.putInt("temp",14);
                ManagementHomeReportsFragment managementHomeReportsFragmentJharkhand = new ManagementHomeReportsFragment();
                managementHomeReportsFragmentJharkhand.setArguments(bundle);

                mFragmentTransaction.replace(R.id.dashboard_fragment_container, managementHomeReportsFragmentJharkhand);
                mFragmentTransaction.addToBackStack(null);
                mFragmentTransaction.commit();
                break;

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // removeDetails();
    }

    private void removeDetails()
    {
        SharedPreferenceUtil.removeLoginId(DashboardActivity.this);
        SharedPreferenceUtil.removeIMIENum(DashboardActivity.this);
        SharedPreferenceUtil.removeFcmId(DashboardActivity.this);
        SharedPreferenceUtil.removeUserName(DashboardActivity.this);
        SharedPreferenceUtil.removeOfficeTypeCode(DashboardActivity.this);
        SharedPreferenceUtil.removeDefaultProcessTask(DashboardActivity.this);
        SharedPreferenceUtil.removeAdditionalProcessTask(DashboardActivity.this);
        SharedPreferenceUtil.removeRoleCode(DashboardActivity.this);
        SharedPreferenceUtil.removeLoginStatus(DashboardActivity.this);
        SharedPreferenceUtil.removeUserCode(DashboardActivity.this);
        SharedPreferenceUtil.removeTPACompCode(DashboardActivity.this);
        SharedPreferenceUtil.removeTPACompName(DashboardActivity.this);
        SharedPreferenceUtil.removeTPAStatus(DashboardActivity.this);
        SharedPreferenceUtil.removeLoginCode(DashboardActivity.this);
        SharedPreferenceUtil.removeLoginStateCode(DashboardActivity.this);
        SharedPreferenceUtil.removeLoginStateName(DashboardActivity.this);
        SharedPreferenceUtil.removeDistrictCode(DashboardActivity.this);

    }

    private void manageLogout() {
      //  Log.d(TAG, "in logout");

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(DashboardActivity.this);
        builder.setTitle("Confirm");
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to Sign Out?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                removeDetails();
                Intent claimsLogoutIntent = new Intent(DashboardActivity.this, LoginActivity.class);
                startActivity(claimsLogoutIntent);
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }
}
