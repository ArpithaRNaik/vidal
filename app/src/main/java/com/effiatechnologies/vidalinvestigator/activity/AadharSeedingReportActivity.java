package com.effiatechnologies.vidalinvestigator.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.adapter.DistrictSpinnerAdapter;
import com.effiatechnologies.vidalinvestigator.adapter.ReportCountDetailsAdapter;
import com.effiatechnologies.vidalinvestigator.adapter.ReportsDetailsAdapter;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.model.DistrictModel;
import com.effiatechnologies.vidalinvestigator.model.ReportResponseModel;
import com.effiatechnologies.vidalinvestigator.model.UrnReportCountResponse;
import com.effiatechnologies.vidalinvestigator.model.UrnReportsInput;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public class AadharSeedingReportActivity extends AppCompatActivity
        implements DatePickerDialog.OnDateSetListener {

  //  private static final String TAG = "AadharSeedingReportActivity";
    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;
    private GetUrnReportsService mApiService = null;


    private EditText et_fromDate = null;
    private EditText et_toDate = null;
    private RecyclerView rv_ReportCount = null;
    private LinearLayout layout_District = null;
    private Spinner spinnerDist = null;
    // private LinearLayout layoutCountDetails = null;
    private static boolean isFromDateSeleted = false;
    private static int whichDate = 0;
    private static final int FROM_DATE = 1;
    private static final int TO_DATE = 2;
    private DecimalFormat df;
    private String fromDateFormat;
    private String toDateFormat;
    private UrnReportsInput urnReportsInput = null;
    private int TPACompCode;
    private int LoginStateCode;
    private int LoginCode;
    private int UserCode;
    private String LoginId;
    private String fromDate;
    private String toDate;
    private List<ReportResponseModel> urnReportCountResponseList = null;
    private ReportsDetailsAdapter reportCountDetailsAdapter = null;
    private int DistId;
    private List<DistrictModel> districtModelList = null;
    private DistrictSpinnerAdapter districtSpinnerAdapter = null;
    private DistrictModel districtModel = null;
    private int districtCode;
    private String districtName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aadhar_seeding_report);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        df = new DecimalFormat("00");
        mApiService = MyApplication.getRetrofit().create(GetUrnReportsService.class);

        builder = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);
        getDistricts();

        et_fromDate = (EditText) findViewById(R.id.et_reportFromDate);
        et_toDate = (EditText) findViewById(R.id.et_ReportToDate);
        rv_ReportCount = (RecyclerView) findViewById(R.id.rv_ReportCountDetails);
        layout_District = (LinearLayout) findViewById(R.id.layout_districtSelect);
        spinnerDist = (Spinner) findViewById(R.id.spinner_districtSelect);

        TPACompCode = SharedPreferenceUtil.getTPACompCode(AadharSeedingReportActivity.this);
        LoginStateCode = SharedPreferenceUtil.getLoginStateCode(AadharSeedingReportActivity.this);
        LoginCode = SharedPreferenceUtil.getLoginCode(AadharSeedingReportActivity.this);
        UserCode = SharedPreferenceUtil.getUserCode(AadharSeedingReportActivity.this);
        LoginId = SharedPreferenceUtil.getLoginId(AadharSeedingReportActivity.this);
        DistId = SharedPreferenceUtil.getDistrictCode(AadharSeedingReportActivity.this);

        // Log.d(TAG, "TPACompCode : " + TPACompCode);
        // Log.d(TAG, "LoginStateCode : " + LoginStateCode);
        // Log.d(TAG, "LoginCode : " + LoginCode);
        // Log.d(TAG, "UserCode : " + UserCode);
       // Log.d(TAG, "DistId : " + DistId);


        //layoutCountDetails = (LinearLayout) findViewById(R.id.CountDetails_HomePage);


        if (DistId == 0) {
            layout_District.setVisibility(View.VISIBLE);
        } else {
            layout_District.setVisibility(View.GONE);
        }

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rv_ReportCount.setHasFixedSize(true);
        rv_ReportCount.setLayoutManager(mLayoutManager);
        rv_ReportCount.setItemAnimator(new DefaultItemAnimator());

        et_fromDate.setInputType(InputType.TYPE_NULL);
        et_fromDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                et_fromDate.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_fromDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    final Calendar c = Calendar.getInstance();

                    DatePickerDialog datePickerDialog =
                            new DatePickerDialog(AadharSeedingReportActivity.this, AadharSeedingReportActivity.this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

                    if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1) {
                        // Log.d(TAG, "inside equal to lollipop 5.1.1");
                        // c.add(c.DATE, -10);
                        //datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                        datePickerDialog.getDatePicker();
                        whichDate = FROM_DATE;
                        isFromDateSeleted = true;
                        et_toDate.setText("");

                        //  datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis() + (1000 * 60 * 60 * 24 * 10));
                    } else /*if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP)*/ {
                        //  Log.d(TAG, "inside equal to lollipop");
                        // c.add(c.DATE, -10);
                        //datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
                        datePickerDialog.getDatePicker();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            datePickerDialog.getDatePicker();
                        }
                        whichDate = FROM_DATE;
                        isFromDateSeleted = true;
                        et_toDate.setText("");

                        //datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis() + (1000 * 60 * 60 * 24 * 10));
                    }
                    datePickerDialog.show();
                }

                return false;
            }
        });

        et_toDate.setInputType(InputType.TYPE_NULL);
        et_toDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                et_toDate.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        et_toDate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (isFromDateSeleted) {

                        final Calendar c = Calendar.getInstance();

                        DatePickerDialog datePickerDialog =
                                new DatePickerDialog(AadharSeedingReportActivity.this, AadharSeedingReportActivity.this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

                        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP_MR1) {
                            //  Log.d(TAG, "inside equal to lollipop 5.1.1");
                            // c.add(c.DATE, -10);
                            //datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());

                            datePickerDialog.getDatePicker();
                            whichDate = TO_DATE;

                            //  datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis() + (1000 * 60 * 60 * 24 * 10));
                        } else /*if (Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP)*/ {
                            //  Log.d(TAG, "inside equal to lollipop");
                            // c.add(c.DATE, -10);
                            //datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());

                            datePickerDialog.getDatePicker();
                            whichDate = TO_DATE;

                            //datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis() + (1000 * 60 * 60 * 24 * 10));
                        }
                        datePickerDialog.show();
                    } else {
                        AppUtil.toastMessage(AadharSeedingReportActivity.this, "Please Select From Date First");
                    }
                }

                return false;
            }
        });

        spinnerDist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                {

                    districtModel = (DistrictModel) parent.getItemAtPosition(position);
                    districtCode = districtModel.getDistCode();
                   // Log.d(TAG, "districtCode : " + districtCode);

                    districtName = districtModel.getDistName();
                  //  Log.d(TAG, "districtName : " + districtName);
                    //requiredDetails.setCarrierType(subjectsResponse.getSubjectID());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
    }

    private void getDistricts() {

       // Log.d(TAG, "inside getDistricts");
        if (AppUtil.isNetworkAvailable(this)) {

            progressDialog.setMessage("Please Wait....");
            progressDialog.show();
            progressDialog.setCancelable(false);
            Call<List<DistrictModel>> call = mApiService.getDistricts();
            call.enqueue(new Callback<List<DistrictModel>>() {
                @Override
                public void onResponse(Call<List<DistrictModel>> call, Response<List<DistrictModel>> response) {
                    if (response.isSuccessful()) {
                        progressDialog.dismiss();
                        districtModelList = response.body();

                       // Log.d(TAG, districtModelList.toString());
                        //    carrierTypeList = carrierInfo.ge();
                        districtModelList.add(0, new DistrictModel(0, "Select "));
                        //   Log.d(TAG, carrierTypeList.toString());

                        setDistrictSpinner();

                    } else {

                        progressDialog.dismiss();
                        int statusCode = response.code();
                        //  Log.d(TAG, statusCode + "failed");
                        //  ResponseBody errorBody = response.errorBody();

                    }
                }

                @Override
                public void onFailure(Call<List<DistrictModel>> call, Throwable t) {

                    progressDialog.dismiss();
                    //  Log.d(TAG, t.getMessage() + "on failure");

                }
            });
        } else {
            // AppUtil.toastMessage(mContext, "Check Internet connection");
            showInternetNotAvailbale();
        }
    }

    private void setDistrictSpinner() {

        if (spinnerDist != null) {
            //  Log.d(TAG, "spinner not null :" + subjectsResponseList.size());
            districtSpinnerAdapter = new DistrictSpinnerAdapter(this, districtModelList);
            spinnerDist.setAdapter(districtSpinnerAdapter);
        } else {
            //  Log.d(TAG, "spinner  null");
        }

    }

    private void getReports(String fromDateFormat, String toDateFormat) {

        // Log.d(TAG, "inside validateDate");
        boolean cancel = false;

        fromDate = et_fromDate.getText().toString();
        toDate = et_toDate.getText().toString();

        /*if (TextUtils.isEmpty(fromDate)) {
            et_fromDate.setError("Enter From Date");
            cancel = true;
        }
        if (TextUtils.isEmpty(toDate)) {
            et_toDate.setError("Enter To Date");
            cancel = true;
        }*/

        if (TextUtils.isEmpty(fromDateFormat)) {
            et_fromDate.setError("Enter From Date");
            cancel = true;
        }
        if (TextUtils.isEmpty(toDateFormat)) {
            et_toDate.setError("Enter To Date");
            cancel = true;
        }
        if (!cancel) {
            if (AppUtil.isNetworkAvailable(this)) {
                progressDialog.setMessage("Please Wait....");
                progressDialog.show();
                progressDialog.setCancelable(false);

                urnReportsInput = new UrnReportsInput();

                urnReportsInput.setFromDate(fromDateFormat);
                urnReportsInput.setToDate(toDateFormat);
                urnReportsInput.setTPACode(String.valueOf(TPACompCode));
                urnReportsInput.setStateCode(String.valueOf(LoginStateCode));
                urnReportsInput.setLoginID(String.valueOf(LoginId));
                urnReportsInput.setUserCode(String.valueOf(LoginCode));

                if (DistId == 0) {
                    urnReportsInput.setDistrictCode(String.valueOf(districtCode));

                } else {
                    urnReportsInput.setDistrictCode(String.valueOf(DistId));

                }

                /*if (LoginId.contains("DE Rajnandgaon"))
                {
                    urnReportsInput.setDistrictCode("9");
                }
                else if (LoginId.contains("DE Kawardha"))
                {
                    urnReportsInput.setDistrictCode("8");
                }*/

              //  Log.d(TAG, "urnReportsInput : " + urnReportsInput);

                Call<List<ReportResponseModel>> call = mApiService.getUrnReportCountDetails(urnReportsInput);
                call.enqueue(new Callback<List<ReportResponseModel>>() {
                    @Override
                    public void onResponse(Call<List<ReportResponseModel>> call, Response<List<ReportResponseModel>> response) {

                        if (response.isSuccessful()) {
                             // Log.d(TAG, response.body() + "inside successfull response");
                            if (response.body() != null) {
                                progressDialog.dismiss();
                                urnReportCountResponseList = response.body();
                               //  Log.d(TAG, "urnReportCountResponseList : " + urnReportCountResponseList);
                                if (urnReportCountResponseList.size() >= 1) {
                                    //  layoutCountDetails.setVisibility(View.VISIBLE);
                                    rv_ReportCount.setVisibility(View.VISIBLE);
                                   // reportCountDetailsAdapter = new ReportCountDetailsAdapter(getApplicationContext(), urnReportCountResponseList);
                                    reportCountDetailsAdapter = new ReportsDetailsAdapter(getApplicationContext(), urnReportCountResponseList);

                                    rv_ReportCount.setAdapter(reportCountDetailsAdapter);
                                    reportCountDetailsAdapter.notifyDataSetChanged();
                                } else {
                                    showNoRecordDialog();
                                    // layoutCountDetails.setVisibility(View.GONE);
                                    rv_ReportCount.setVisibility(View.GONE);

                                }


                            } else if (response.body() == null) {
                               // Log.d(TAG, "response is null" + response.body());
                                //response.body();
                                rv_ReportCount.setVisibility(View.GONE);
                                progressDialog.dismiss();
                                showNoRecordDialog();

                            }

                        } else {
                           //  Log.d(TAG, "response is not successfull" + response.body());
                           // Log.d(TAG, "response is not successfull" + response.errorBody());

                            rv_ReportCount.setVisibility(View.GONE);
                            progressDialog.dismiss();
                            showNoRecordDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<ReportResponseModel>> call, Throwable t) {
                       //  Log.d(TAG, "in failure" + t.getMessage());
                        progressDialog.dismiss();
                        showErrorDialog();
                    }
                });
            } else {
                showInternetNotAvailbale();
                // AppUtil.toastMessage(AddStudentAttendanceActivity.this, "Check Internet connection");
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.dashboard:
                // Intent homeIntent = new Intent(ClaimsDetailsActivity.this, InvestigatorLoginActivity.class);
                // doesDatabaseExist(getApplicationContext(), DB_NAME);
                Intent homeIntent = new Intent(AadharSeedingReportActivity.this, AadharSeedingNavigationActivity.class);
                startActivity(homeIntent);
                finish();
                break;

            case R.id.logout:
                manageLogout();
                break;

            case android.R.id.home:

                //  doesDatabaseExist(getApplicationContext(), DB_NAME);
                finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void manageLogout() {
        // Log.d(TAG, "in logout");

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(AadharSeedingReportActivity.this);
        builder.setTitle("Confirm");
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to Sign Out?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                // doesDatabaseExist(getApplicationContext(), DB_NAME);
                RemoveDetails();
                Intent claimsLogoutIntent = new Intent(AadharSeedingReportActivity.this, LoginActivity.class);
                claimsLogoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(claimsLogoutIntent);
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void RemoveDetails() {
        SharedPreferenceUtil.removeLoginId(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeIMIENum(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeFcmId(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeUserName(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeOfficeTypeCode(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeDefaultProcessTask(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeAdditionalProcessTask(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeRoleCode(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeLoginStatus(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeUserCode(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeTPACompCode(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeTPACompName(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeTPAStatus(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeLoginCode(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeLoginStateCode(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeLoginStateName(AadharSeedingReportActivity.this);
        SharedPreferenceUtil.removeDistrictCode(AadharSeedingReportActivity.this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //  doesDatabaseExist(getApplicationContext(), DB_NAME);

    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        String date = df.format(dayOfMonth) + "/" + df.format(monthOfYear + 1) + "/" + year;
        // Log.d(TAG, "date : "+ date);

        switch (whichDate) {
            case FROM_DATE:
                et_fromDate.setText(date);
                fromDateFormat = year + "/" + df.format(monthOfYear + 1) + "/" + df.format(dayOfMonth);

                break;

            case TO_DATE:

                et_toDate.setText(date);
                toDateFormat = year + "/" + df.format(monthOfYear + 1) + "/" + df.format(dayOfMonth);

                getReports(fromDateFormat, toDateFormat);
                /*if (isFromDateSeleted) {

                    String fromdateuser = et_fromDate.getText().toString().trim();
                    if (AppUtil.isMinum(fromdateuser, date) >= 0) {


                    } else {
                        AppUtil.toastMessage(this, "Please Enter Valid Date");
                    }

                } else {
                    AppUtil.toastMessage(this, "Please Select From Date");
                }*/

                break;
        }
    }

    public interface GetUrnReportsService {

        @POST(UrlConfig.GET_URN_REPORT_COUNT)
        Call<List<ReportResponseModel>> getUrnReportCountDetails(@Body UrnReportsInput urnDetailsInput);

        @GET(UrlConfig.GET_DISTRICTS)
        Call<List<DistrictModel>> getDistricts();

    }

    private void showNoRecordDialog() {

        builder.setCancelable(false);
        builder.setTitle("Information");
        builder.setMessage("No Records Found!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //finish();
            }
        });

        builder.show();
    }

    private void showErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Network Failure");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //finish();
            }
        });

        builder.show();
    }

    private void showInternetNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Network Not Available");
        builder.setMessage("Please check your Internet Connection!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }
}
