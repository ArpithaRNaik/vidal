package com.effiatechnologies.vidalinvestigator.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.adapter.PendingClaimsAdapter;
import com.effiatechnologies.vidalinvestigator.adapter.SearchPendingClaimsAdapter;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.model.GetClaimsInput;
import com.effiatechnologies.vidalinvestigator.model.GetClaimsResponse;
import com.effiatechnologies.vidalinvestigator.model.LoginStatesResponse;
import com.effiatechnologies.vidalinvestigator.model.PendingClaimsModel;
import com.effiatechnologies.vidalinvestigator.model.UserInput;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public class PendingClaimsActivity extends AppCompatActivity {

   // private static final String TAG = "pendingclaims";

    private getClaimsValueService mApiService = null;
    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;

    private RecyclerView claimsRecyclerView = null;
    private AutoCompleteTextView etSearch = null;
    private PendingClaimsAdapter pendingClaimsAdapter = null;
    private SearchPendingClaimsAdapter searchPendingClaimsAdapter = null;
    //private List<PendingClaimsModel> pendingClaimsModelList = null;
    private List<GetClaimsResponse> pendingClaimsModelList;
    private static final String[] CLAIMS = new String[]{
            "VD1234", "VD5678", "VD9012", "VD3456", "VD7890"
    };
    private Intent pendingclaimsIntent = null;
    private LinearLayoutManager linearLayoutManager;

    private String vi_LoginId;
    private String vi_UserName;
    private int vi_OfficeTypeCode;
    private String vi_DefaultProcessTask;
    private String vi_AdditionalProcessTask;
    private int vi_RoleCode;
    private String vi_LoginStatus;
    private int vi_UserCode;
    private int vi_TPACompCode;
    private String vi_TPACompName;
    private String vi_TPAStatus;
    private int vi_LoginCode;
    private int loginStateCode;
    private String loginStateName;

    private GetClaimsInput getClaimsInput;
    private int pendingClaimsCount;
    private GetClaimsResponse getClaimsResponse;
    private String transClaimId;
    private String transClaimDate;
    private String transClaimPatientName;
    private String transClaimDateOfAdmission;
    private String transClaimDateOfDischarge;
    private String transClaimHospitalName;
    private String transClaimProcedureName;
    private String transClaimAmountNegotiated;
    private String transClaimPatientAge;
    private String transClaimPatientGender;
    private String transClaimPackageName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_claims);
     //   Log.d(TAG, "inside oncreate");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        builder = new AlertDialog.Builder(this);
        progressDialog = new ProgressDialog(this);
        mApiService = MyApplication.getRetrofit().create(getClaimsValueService.class);

        vi_LoginId = SharedPreferenceUtil.getLoginId(getApplicationContext());
        vi_UserName = SharedPreferenceUtil.getUserName(getApplicationContext());
        vi_OfficeTypeCode = SharedPreferenceUtil.getOfficeTypeCode(getApplicationContext());
        vi_DefaultProcessTask = SharedPreferenceUtil.getDefaultProcessTask(getApplicationContext());
        vi_AdditionalProcessTask = SharedPreferenceUtil.getAdditionalProcessTask(getApplicationContext());
        vi_RoleCode = SharedPreferenceUtil.getRoleCode(getApplicationContext());
        vi_LoginStatus = SharedPreferenceUtil.getLoginStatus(getApplicationContext());
        vi_UserCode = SharedPreferenceUtil.getUserCode(getApplicationContext());
        vi_TPACompCode = SharedPreferenceUtil.getTPACompCode(getApplicationContext());
        vi_TPACompName = SharedPreferenceUtil.getTPACompName(getApplicationContext());
        vi_TPAStatus = SharedPreferenceUtil.getTPAStatus(getApplicationContext());
        vi_LoginCode = SharedPreferenceUtil.getLoginCode(getApplicationContext());
        loginStateCode = SharedPreferenceUtil.getLoginStateCode(getApplicationContext());
        loginStateName = SharedPreferenceUtil.getLoginStateName(getApplicationContext());

       /* Log.d(TAG, "vi_LoginId : " + vi_LoginId);
        Log.d(TAG, "vi_UserName : " + vi_UserName);
        Log.d(TAG, "vi_OfficeTypeCode : " + vi_OfficeTypeCode);
        Log.d(TAG, "vi_DefaultProcessTask : " + vi_DefaultProcessTask);
        Log.d(TAG, "vi_AdditionalProcessTask : " + vi_AdditionalProcessTask);
        Log.d(TAG, "vi_RoleCode : " + vi_RoleCode);
        Log.d(TAG, "vi_LoginStatus : " + vi_LoginStatus);
        Log.d(TAG, "vi_UserCode : " + vi_UserCode);
        Log.d(TAG, "vi_TPACompCode : " + vi_TPACompCode);
        Log.d(TAG, "vi_TPACompName : " + vi_TPACompName);
        Log.d(TAG, "vi_TPAStatus : " + vi_TPAStatus);
        Log.d(TAG, "vi_LoginCode : " + vi_LoginCode);
        Log.d(TAG, "loginStateCode : " + loginStateCode);
        Log.d(TAG, "loginStateName : " + loginStateName);*/



        claimsRecyclerView = (RecyclerView) findViewById(R.id.claims_recyclerView);
        etSearch = (AutoCompleteTextView) findViewById(R.id.et_Search);

        getPendingClaims();
        //prepareMovieData();

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        claimsRecyclerView.setHasFixedSize(true);
        claimsRecyclerView.setLayoutManager(mLayoutManager);
        claimsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        etSearch.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                getClaimsResponse = (GetClaimsResponse) parent.getItemAtPosition(position);
                transClaimId = getClaimsResponse.getTransID();
               // Log.d(TAG, "transClaimId : " + transClaimId);
                transClaimDate = getClaimsResponse.getHospitalTimeStamp();
              // Log.d(TAG, "transClaimDate : " + transClaimDate);
                transClaimPatientName = getClaimsResponse.getPatientName();
              //  Log.d(TAG, "transClaimPatientName : " + transClaimPatientName);
                transClaimDateOfAdmission = getClaimsResponse.getDateofAdmission();
              //  Log.d(TAG, "transClaimDateOfAdmission : " + transClaimDateOfAdmission);
                transClaimDateOfDischarge = getClaimsResponse.getDateofDischarge();
              //  Log.d(TAG, "transClaimDateOfDischarge : " + transClaimDateOfDischarge);
                transClaimHospitalName = getClaimsResponse.getHospitalName();
              //  Log.d(TAG, "transClaimHospitalName : " + transClaimHospitalName);
                transClaimProcedureName = getClaimsResponse.getProcedureName();
              //  Log.d(TAG, "transClaimProcedureName : " + transClaimProcedureName);
                transClaimAmountNegotiated = String.valueOf(getClaimsResponse.getAmountNegotiated());
              //  Log.d(TAG, "transClaimAmountNegotiated : " + transClaimAmountNegotiated);
                transClaimPatientAge = String.valueOf(getClaimsResponse.getPatientAge());
             //   Log.d(TAG, "transClaimPatientAge : " + transClaimPatientAge);
                transClaimPatientGender = getClaimsResponse.getPatientGender();
             //   Log.d(TAG, "transClaimPatientGender : " + transClaimPatientGender);
                transClaimPackageName = getClaimsResponse.getPackageName();
             //   Log.d(TAG, "transClaimPackageName : " + transClaimPackageName);

                pendingclaimsIntent = new Intent(PendingClaimsActivity.this, ClaimsDetailsActivity.class);
                pendingclaimsIntent.putExtra("claimId", transClaimId);
                pendingclaimsIntent.putExtra("claimDate", transClaimDate);
                pendingclaimsIntent.putExtra("claimPatientName", transClaimPatientName);
                pendingclaimsIntent.putExtra("claimDateOfAdmission", transClaimDateOfAdmission);
                pendingclaimsIntent.putExtra("claimDateOfDischarge", transClaimDateOfDischarge);
                pendingclaimsIntent.putExtra("claimHospitalName", transClaimHospitalName);
                pendingclaimsIntent.putExtra("claimsProcedureName", transClaimProcedureName);
                pendingclaimsIntent.putExtra("claimsAmountNegotiated", transClaimAmountNegotiated);
                pendingclaimsIntent.putExtra("claimsPatientAge", transClaimPatientAge);
                pendingclaimsIntent.putExtra("claimsPatientGender", transClaimPatientGender);
                pendingclaimsIntent.putExtra("claimsPackageName", transClaimPackageName);

                startActivity(pendingclaimsIntent);

            }
        });

    }

    private void getPendingClaims() {

     //   Log.d(TAG, "inside getPendingClaims ");
        if (AppUtil.isNetworkAvailable(PendingClaimsActivity.this)) {
            progressDialog.setMessage("Please Wait....");
            progressDialog.show();
            progressDialog.setCancelable(false);

            getClaimsInput = new GetClaimsInput();
            getClaimsInput.setLoginID(vi_LoginId);
            getClaimsInput.setDefaultProcessTask(vi_DefaultProcessTask);
            getClaimsInput.setTPACompCode(vi_TPACompCode);
            getClaimsInput.setHospitalStateCode(String.valueOf(loginStateCode));

          //  Log.d(TAG, "input getPendingClaims : " + getClaimsInput);

            Call<List<GetClaimsResponse>> call = mApiService.getPendingClaims(getClaimsInput);
            call.enqueue(new Callback<List<GetClaimsResponse>>() {
                @Override
                public void onResponse(Call<List<GetClaimsResponse>> call, Response<List<GetClaimsResponse>> response) {
                   // Log.d(TAG, "Result : " + response.body().toString());
                    if (response.isSuccessful()) {
                        //PendingClaimsModel userType = response.body();
                        progressDialog.dismiss();
                        pendingClaimsModelList = response.body();
                        pendingClaimsCount = pendingClaimsModelList.size();
                      //  Log.d(TAG, "pendingClaimsCount : " +  pendingClaimsCount);
                        if (pendingClaimsCount <= 0)
                        {
                            showRecordsNotExistDialog();
                        }
                        else
                        {
                            pendingClaimsAdapter = new PendingClaimsAdapter(getApplicationContext(), pendingClaimsModelList);
                            claimsRecyclerView.setAdapter(pendingClaimsAdapter);
                            pendingClaimsAdapter.notifyDataSetChanged();

                            searchPendingClaimsAdapter = new SearchPendingClaimsAdapter(getApplicationContext(),R.layout.row_custom_spinner, pendingClaimsModelList);
                            etSearch.setAdapter(searchPendingClaimsAdapter);
                            searchPendingClaimsAdapter.notifyDataSetChanged();

                        }

                    } else {
                      //  Log.d(TAG, "response is not successfull");
                        progressDialog.dismiss();
                        //showErrorDialog();
                        showRecordsNotExistDialog();

                    }
                }

                @Override
                public void onFailure(Call<List<GetClaimsResponse>> call, Throwable t) {
                  //  Log.d(TAG, "Exception " + t.getMessage());
                    progressDialog.dismiss();
                    showErrorDialog();
                }
            });
        }
        else {
            // AppUtil.toastMessage(mContext, "Check Internet connection");
            showInternetNotAvailbale();
        }
    }
    private void showInternetNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Network Not Available");
        builder.setMessage("Please check your Internet Connection!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }
    private void showRecordsNotExistDialog() {

        builder.setCancelable(false);
        builder.setTitle("Message");
        builder.setMessage("No Records Found!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

              //  finish();
            }
        });

        builder.show();
    }

    private void showErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Network Failure");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                finish();
            }
        });

        builder.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.dashboard:
               // Intent homeIntent = new Intent(PendingClaimsActivity.this, InvestigatorLoginActivity.class);
                Intent homeIntent = new Intent(PendingClaimsActivity.this, HomePageNavigationActivity.class);
                startActivity(homeIntent);
                finish();
                break;

            case R.id.logout:
                manageLogout();
                break;
            case android.R.id.home:

                Intent backIntent = new Intent(PendingClaimsActivity.this, HomePageNavigationActivity.class);
                startActivity(backIntent);
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void removeDetails(){
        SharedPreferenceUtil.removeLoginId(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeIMIENum(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeFcmId(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeUserName(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeOfficeTypeCode(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeDefaultProcessTask(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeAdditionalProcessTask(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeRoleCode(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeLoginStatus(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeUserCode(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeTPACompCode(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeTPACompName(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeTPAStatus(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeLoginCode(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeLoginStateCode(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeLoginStateName(PendingClaimsActivity.this);
        SharedPreferenceUtil.removeDistrictCode(PendingClaimsActivity.this);

    }
    private void manageLogout() {
      //  Log.d(TAG, "in logout");

        final android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(PendingClaimsActivity.this);
        builder.setTitle("Confirm");
        builder.setCancelable(false);
        builder.setMessage("Are you sure you want to Sign Out?");
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                removeDetails();
                Intent intent = new Intent(PendingClaimsActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });

        android.support.v7.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    public interface getClaimsValueService {

        @POST(UrlConfig.GET_PENDING_CLAIMS)
        Call<List<GetClaimsResponse>> getPendingClaims(@Body GetClaimsInput getClaimsInput);

    }

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to go back?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                       // Intent backIntent = new Intent(PendingClaimsActivity.this, InvestigatorLoginActivity.class);
                        Intent backIntent = new Intent(PendingClaimsActivity.this, HomePageNavigationActivity.class);
                        startActivity(backIntent);
                        finish();
                    }
                }).create().show();
    }

    /*@Override
    public void onBackPressed() {
        super.onBackPressed();
    }*/

}
