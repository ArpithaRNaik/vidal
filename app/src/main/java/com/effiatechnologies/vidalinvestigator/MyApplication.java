package com.effiatechnologies.vidalinvestigator;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;


import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by User1 on 5/2/2016.
 */

public class MyApplication extends MultiDexApplication {

    private static final String BASE_URL = UrlConfig.BASE_URL;

    private static OkHttpClient mOkHttpClient;

    private static Retrofit mRetrofit;
    private static MyApplication mainApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        mainApplication = this;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }


    public static synchronized MyApplication getInstance() {
        return mainApplication;
    }

    public MyApplication() {

        if (mRetrofit == null) {
            mRetrofit = MyApplication.initialize();
        }

    }

    public static OkHttpClient getokhttp()
    {
        return mOkHttpClient;
    }

    private static synchronized Retrofit initialize() {

        mOkHttpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
             /*   .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder()
                                .addHeader("Content-Type", "application/json").build();
                        return chain.proceed(request);
                    }
                })*/
                .build();

        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(mOkHttpClient).build();
    }


    public static synchronized Retrofit getRetrofit() {

        if (mRetrofit == null) {

            if (mOkHttpClient == null) {

                mOkHttpClient = new OkHttpClient.Builder()
                        .connectTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .build();
            }

            return new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(mOkHttpClient).build();

        } else {
            return mRetrofit;
        }
    }

}
