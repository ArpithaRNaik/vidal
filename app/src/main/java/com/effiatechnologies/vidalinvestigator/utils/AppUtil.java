package com.effiatechnologies.vidalinvestigator.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AppUtil {

    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z]{2,})$";
    public static final String EMAIL_PATTERN_MORE = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z]{2,}\\.[A-Za-z]{2,})$";
    static final String PHONE_PATTERN = "^[0789]\\d{9}$";
    private static int cyear, cmonth, cday;
    private static Date date;
    private static final SimpleDateFormat dateForamt = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault());
    private static final SimpleDateFormat databaseFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
  //  private static final String TAG = "apputill";

    public static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }


    public static boolean isEmailValid2(String email) {
        Pattern pattern;
        Matcher matcher;

        pattern = Pattern.compile(EMAIL_PATTERN_MORE);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isMobileNumbervalid(String phone) {
        Pattern pattern;
        Matcher matcher;
        pattern = Pattern.compile(PHONE_PATTERN);
        matcher = pattern.matcher(phone);
        return matcher.matches();
    }


    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {

            return false;
        }
    }


    public static String timeTo12Hour(String time) {
        SimpleDateFormat fromDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        SimpleDateFormat toDateFormat = new SimpleDateFormat("hh:mm a", Locale.getDefault());
        try {
            return toDateFormat.format(fromDateFormat.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static boolean isTimeLess(String fromTime, String toTime) {
        SimpleDateFormat fromDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

        return false;
    }

    public static long isGreaterThan(String startDate, String endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault());

        try {
            Date d1 = sdf.parse(startDate);
            Date d2 = sdf.parse(endDate);

            return (d2.getTime() - d1.getTime());
        } catch (ParseException e) {
            e.printStackTrace();

            return -1;
        }

    }


    public static long isMinum(String startDate, String endDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());

        try {
            Date d1 = sdf.parse(startDate);
            Date d2 = sdf.parse(endDate);
            return (d2.getTime() - d1.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return -1;
        }

    }

    public static long startTimeMilliseconds(String startDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a", Locale.getDefault());

        try {
            Date d1 = sdf.parse(startDate);

            return d1.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return -1;
        }

    }

    public static String todaysDate() {
        final Calendar c = Calendar.getInstance();
        cyear = c.get(Calendar.YEAR);
        cmonth = c.get(Calendar.MONTH);
        cday = c.get(Calendar.DAY_OF_MONTH);

        return (cyear + "-" + (cmonth + 1) + "-" + cday);
    }


    public static String currentTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
    }

    public static String covertToDatabase() {
        Date date = new Date();
        String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        return modifiedDate;
    }


    public static String convertToDataBase(String dateStr) {
        try {
            date = dateForamt.parse(dateStr);
          //  Log.d(TAG, databaseFormat.format(date));
            return databaseFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
         //   Log.d(TAG, e.getMessage());
            return null;
        }
    }


    public static String conertFromDatabaseFormat(String dateStr) {

        //  Log.d(TAG,dateStr);
        try {
            //   date = databaseFormat.parse(dateStr);
            return dateForamt.format(databaseFormat.parse(dateStr));
        } catch (ParseException e) {
            e.printStackTrace();
           // Log.d(TAG, e.getMessage());
            return null;
        }
    }

    public static boolean compareDates(String startDate, String endDate) {


        try {
            Date start = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
                    .parse(startDate);
            Date end = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
                    .parse(endDate);

           // Log.d(TAG, start.compareTo(end) + "");
            if (start.compareTo(end) > 0) {
                return false;
            } else if (start.compareTo(end) < 0) {
                return true;
            } else if (start.compareTo(end) == 0) {
                return true;
            } else {
                return false;
            }

        } catch (ParseException e) {
          //  Log.d(TAG, e.getMessage());
            e.printStackTrace();
            return false;

        }
    }

    public static void toastMessage(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

}
