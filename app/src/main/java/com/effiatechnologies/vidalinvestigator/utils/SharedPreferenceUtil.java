package com.effiatechnologies.vidalinvestigator.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by User2 on 2/1/2016.
 */
public class SharedPreferenceUtil {

    public static final String APP_PREF = "Logistics";
    public static final String LOGIN_ID = "loginID";
    public static final String USER_NAME = "userName";
    public static final String IMIE_NUMBER = "imieNum";
    private static final String OFFICE_TYPE_CODE = "officeTypeCode";
    private static final String DEFAULT_PROCESS_TASK = "defaultProcessTask";
    private static final String ADDITIONAL_PROCESS_TASK = "additional_ProcessTask";
    private static final String ROLE_CODE = "roleCode";
    private static final String LOGIN_STATUS = "LoginStatus";
    private static final String USER_CODE = "userCode";
    private static final String TPA_COMP_CODE = "TPACompCode";
    private static final String TPA_COMP_NAME = "TPACompName";
    private static final String TPA_STATUS = "TPAStatus";
    private static final String LOGIN_CODE = "loginCode";
    private static final String DIST_CODE = "distCode";
    private static final String LOGIN_STATE_CODE = "stateCode";
    private static final String LOGIN_STATE_NAME = "stateName";
    public static final String FCM_ID = "regId";



    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);

    }

    //To store IMIE
    public static void setIMIENum(Context context, String imieNum) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(IMIE_NUMBER, imieNum);
        editor.apply();
    }

    public static String getIMIENum(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getString(IMIE_NUMBER, null);
    }


    public static void removeIMIENum(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(IMIE_NUMBER).apply();
    }

    //To store FCM ID
    public static void setFcmId(Context context, String user_name) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(FCM_ID, user_name);
        editor.apply();
    }

    public static String getFcmId(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getString(FCM_ID, null);
    }


    public static void removeFcmId(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(FCM_ID).apply();
    }
    //To store userId
    public static void storeLoginId(Context context, String vi_LoginId) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(LOGIN_ID, vi_LoginId);
        editor.apply();
    }

    public static String getLoginId(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getString(LOGIN_ID, null);

    }

    public static void removeLoginId(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(LOGIN_ID).apply();
    }

    //To store UserName
    public static void setUserName(Context context, String userName) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(USER_NAME, userName);
        editor.apply();
    }

    public static String getUserName(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getString(USER_NAME, null);
    }


    public static void removeUserName(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(USER_NAME).apply();
    }

    //To store officeTypeCode
    public static void setOfficeTypeCode(Context context, int officeTypeCode) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(OFFICE_TYPE_CODE, officeTypeCode).apply();
    }

    public static int getOfficeTypeCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getInt(OFFICE_TYPE_CODE, 0);
    }

    public static void removeOfficeTypeCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(OFFICE_TYPE_CODE).apply();
    }

    //To store DEFAULT_PROCESS_TASK
    public static void setDefaultProcessTask(Context context, String defaultProcessTask) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(DEFAULT_PROCESS_TASK, defaultProcessTask).apply();
    }

    public static String getDefaultProcessTask(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getString(DEFAULT_PROCESS_TASK, null);
    }

    public static void removeDefaultProcessTask(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(DEFAULT_PROCESS_TASK).apply();
    }

    //To Store additional_ProcessTask
    public static void setAdditionalProcessTask(Context context, String additional_ProcessTask) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(ADDITIONAL_PROCESS_TASK, additional_ProcessTask).apply();
    }

    public static String getAdditionalProcessTask(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getString(ADDITIONAL_PROCESS_TASK, null);
    }

    public static void removeAdditionalProcessTask(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(ADDITIONAL_PROCESS_TASK).apply();
    }

    //To Store roleCode
    public static void setRoleCode(Context context, int roleCode) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(ROLE_CODE, roleCode).apply();
    }

    public static int getRoleCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getInt(ROLE_CODE, 0);
    }

    public static void removeRoleCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(ROLE_CODE).apply();
    }

    //To Store LoginStatus
    public static void setLoginStatus(Context context, String LoginStatus) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(LOGIN_STATUS, LoginStatus).apply();
    }

    public static String getLoginStatus(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getString(LOGIN_STATUS, null);
    }

    public static void removeLoginStatus(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(LOGIN_STATUS).apply();
    }

    //To Store userCode
    public static void setUserCode(Context context, int userCode) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(USER_CODE, userCode).apply();
    }

    public static int getUserCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getInt(USER_CODE, 0);
    }

    public static void removeUserCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(USER_CODE).apply();
    }

    //To Store TPACompCode
    public static void setTPACompCode(Context context, int TPACompCode) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(TPA_COMP_CODE, TPACompCode).apply();
    }

    public static int getTPACompCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getInt(TPA_COMP_CODE, 0);
    }

    public static void removeTPACompCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(TPA_COMP_CODE).apply();
    }

    //To Store TPACompName
    public static void setTPACompName(Context context, String TPACompName) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(TPA_COMP_NAME, TPACompName).apply();
    }

    public static String getTPACompName(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getString(TPA_COMP_NAME, null);
    }

    public static void removeTPACompName(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(TPA_COMP_NAME).apply();
    }

    //To Store TPAStatus
    public static void setTPAStatus(Context context, String TPAStatus) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(TPA_STATUS, TPAStatus).apply();
    }

    public static String getTPAStatus(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getString(TPA_STATUS, null);
    }

    public static void removeTPAStatus(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(TPA_STATUS).apply();
    }

    //To Store Login Code
    public static void setLoginCode(Context context, int loginCode) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(LOGIN_CODE, loginCode).apply();
    }

    public static int getLoginCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getInt(LOGIN_CODE, 0);
    }

    public static void removeLoginCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(LOGIN_CODE).apply();
    }

    //To Store Dist Code
    public static void setDistrictCode(Context context, int distCode) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(DIST_CODE, distCode).apply();
    }

    public static int getDistrictCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getInt(DIST_CODE, 0);
    }

    public static void removeDistrictCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(DIST_CODE).apply();
    }

    //To Store Login State Code
    public static void setLoginStateCode(Context context, int stateCode) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(LOGIN_STATE_CODE, stateCode).apply();
    }

    public static int getLoginStateCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getInt(LOGIN_STATE_CODE, 0);
    }

    public static void removeLoginStateCode(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(LOGIN_STATE_CODE).apply();
    }

    //To Store Login State Name
    public static void setLoginStateName(Context context, String stateName) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(LOGIN_STATE_NAME, stateName).apply();
    }

    public static String getLoginStateName(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        return prefs.getString(LOGIN_STATE_NAME, null);
    }

    public static void removeLoginStateName(Context context) {
        final SharedPreferences prefs = getPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(LOGIN_STATE_NAME).apply();
    }
}
