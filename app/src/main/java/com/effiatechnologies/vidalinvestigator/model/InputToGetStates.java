package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 16-06-2017.
 */

public class InputToGetStates {

    private int UserCode;

    public InputToGetStates()
    {

    }

    public InputToGetStates(int userCode) {
        UserCode = userCode;
    }

    public int getUserCode() {
        return UserCode;
    }

    public void setUserCode(int userCode) {
        UserCode = userCode;
    }

    @Override
    public String toString() {
        return "InputToGetStates{" +
                "UserCode=" + UserCode +
                '}';
    }
}
