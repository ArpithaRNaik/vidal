package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Administrator on 03-12-2016.
 */

public class IndividualStatesResponse {

    private nat nat;
    private io io;
    private fn fn;
    private inv inv;
   private fg fg;
    private fa fa;
    private pcd pcd;



    public IndividualStatesResponse()
    {

    }

    public IndividualStatesResponse(nat nat, io io, fn fn,inv inv,
                                    fg fg, fa fa,
                                    pcd pcd)
    {
        nat = nat;
        io = io;
        fn = fn;
        inv = inv;
        fg = fg;
        fa = fa;
        pcd = pcd;

    }

    public com.effiatechnologies.vidalinvestigator.model.nat getNat() {
        return nat;
    }

    public void setNat(com.effiatechnologies.vidalinvestigator.model.nat nat) {
        this.nat = nat;
    }

    public com.effiatechnologies.vidalinvestigator.model.io getIo() {
        return io;
    }

    public void setIo(com.effiatechnologies.vidalinvestigator.model.io io) {
        this.io = io;
    }

    public com.effiatechnologies.vidalinvestigator.model.fn getFn() {
        return fn;
    }

    public void setFn(com.effiatechnologies.vidalinvestigator.model.fn fn) {
        this.fn = fn;
    }

    public com.effiatechnologies.vidalinvestigator.model.inv getInvestigation() {
        return inv;
    }

    public void setInvestigation(com.effiatechnologies.vidalinvestigator.model.inv investigation) {
        this.inv = inv;
    }

    public com.effiatechnologies.vidalinvestigator.model.fg getForeigngenclaims() {
        return fg;
    }

    public void setForeigngenclaims(com.effiatechnologies.vidalinvestigator.model.fg foreigngenclaims) {
        this.fg = fg;
    }

    public com.effiatechnologies.vidalinvestigator.model.fa getFloatapproval() {
        return fa;
    }

    public void setFloatapproval(com.effiatechnologies.vidalinvestigator.model.fa floatapproval) {
        this.fa = fa;
    }

    public com.effiatechnologies.vidalinvestigator.model.pcd getPaymentcount() {
        return pcd;
    }

    public void setPaymentcount(com.effiatechnologies.vidalinvestigator.model.pcd paymentcount) {
        this.pcd = pcd;
    }

    @Override
    public String toString() {
        return "IndividualStatesResponse{" +
                "nat=" + nat +
                ", io=" + io +
                ", fn=" + fn +
                ", inv=" + inv +
                ", fg=" + fg +
                ", fa=" + fa +
                ", pcd=" + pcd +
                '}';
    }
}
