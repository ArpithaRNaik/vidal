package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 06-02-2018.
 */

public class DistrictModel {


    private int distCode;
    private String distName;

    public DistrictModel() {
    }

    public DistrictModel(int distCode, String distName) {
        this.distCode = distCode;
        this.distName = distName;
    }

    public int getDistCode() {
        return distCode;
    }

    public void setDistCode(int distCode) {
        this.distCode = distCode;
    }

    public String getDistName() {
        return distName;
    }

    public void setDistName(String distName) {
        this.distName = distName;
    }

    @Override
    public String toString() {
        return "DistrictModel{" +
                "distCode=" + distCode +
                ", distName='" + distName + '\'' +
                '}';
    }
}
