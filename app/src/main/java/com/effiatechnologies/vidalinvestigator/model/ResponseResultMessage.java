package com.effiatechnologies.vidalinvestigator.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Arpitha on 19-06-2017.
 */

public class ResponseResultMessage implements Serializable{

  //  @SerializedName("Result")
    private String Result;


   // private List<UserData> Answer;
    public ResponseResultMessage()
    {

    }

    public ResponseResultMessage(String result) {
        Result = result;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    @Override
    public String toString() {
        return "ResponseResultMessage{" +
                "Result='" + Result + '\'' +
                '}';
    }
}
