package com.effiatechnologies.vidalinvestigator.model;

import org.json.JSONArray;

import java.util.List;

/**
 * Created by Arpitha on 19-06-2017.
 */

public class InputToSaveQuestionAnswer {

    private String TransID;
    private List<QuestionAnswerTableList> questionAnswersList;
  //  private int QuestionId;
  //  private String Question;

    public InputToSaveQuestionAnswer()
    {

    }

    public InputToSaveQuestionAnswer(String TransID, List<QuestionAnswerTableList> questionAnswersList) {
        this.TransID = TransID;
        this.questionAnswersList = questionAnswersList;
     //   this.QuestionId = QuestionId;
     //   this.Question = Question;
    }

    public String getTransID() {
        return TransID;
    }

    public void setTransID(String transID) {
        TransID = transID;
    }

    public List<QuestionAnswerTableList> getQuestionAnswersList() {
        return questionAnswersList;
    }

    public void setQuestionAnswersList(List<QuestionAnswerTableList> questionAnswersList) {
        this.questionAnswersList = questionAnswersList;
    }

    @Override
    public String toString() {
        return "InputToSaveQuestionAnswer{" +
                "TransID='" + TransID + '\'' +
                ", questionAnswersList=" + questionAnswersList +
                '}';
    }
}
