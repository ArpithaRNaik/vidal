package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 09-06-2017.
 */

public class LoginResponse {

    private String loginID;
    private String userName;
    private int officeTypeCode;
    private String defaultProcessTask;
    private String additional_ProcessTask;
    private int roleCode;
    private String LoginStatus;
    private int userCode;
    private int TPACompCode;
    private String TPACompName;
    private String TPAStatus;
    private int loginCode;
    private int distCode;

    public LoginResponse(String loginID, String userName, int officeTypeCode, String defaultProcessTask,
                         String additional_ProcessTask, int roleCode, String loginStatus, int userCode, int TPACompCode,
                         String TPACompName, String TPAStatus, int loginCode, int distCode) {
        this.loginID = loginID;
        this.userName = userName;
        this.officeTypeCode = officeTypeCode;
        this.defaultProcessTask = defaultProcessTask;
        this.additional_ProcessTask = additional_ProcessTask;
        this.roleCode = roleCode;
        LoginStatus = loginStatus;
        this.userCode = userCode;
        this.TPACompCode = TPACompCode;
        this.TPACompName = TPACompName;
        this.TPAStatus = TPAStatus;
        this.loginCode = loginCode;
        this.distCode = distCode;
    }

    public String getLoginID() {
        return loginID;
    }

    public void setLoginID(String loginID) {
        this.loginID = loginID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getOfficeTypeCode() {
        return officeTypeCode;
    }

    public void setOfficeTypeCode(int officeTypeCode) {
        this.officeTypeCode = officeTypeCode;
    }

    public String getDefaultProcessTask() {
        return defaultProcessTask;
    }

    public void setDefaultProcessTask(String defaultProcessTask) {
        this.defaultProcessTask = defaultProcessTask;
    }

    public String getAdditional_ProcessTask() {
        return additional_ProcessTask;
    }

    public void setAdditional_ProcessTask(String additional_ProcessTask) {
        this.additional_ProcessTask = additional_ProcessTask;
    }

    public int getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(int roleCode) {
        this.roleCode = roleCode;
    }

    public String getLoginStatus() {
        return LoginStatus;
    }

    public void setLoginStatus(String loginStatus) {
        LoginStatus = loginStatus;
    }

    public int getUserCode() {
        return userCode;
    }

    public void setUserCode(int userCode) {
        this.userCode = userCode;
    }

    public int getTPACompCode() {
        return TPACompCode;
    }

    public void setTPACompCode(int TPACompCode) {
        this.TPACompCode = TPACompCode;
    }

    public String getTPACompName() {
        return TPACompName;
    }

    public void setTPACompName(String TPACompName) {
        this.TPACompName = TPACompName;
    }

    public String getTPAStatus() {
        return TPAStatus;
    }

    public void setTPAStatus(String TPAStatus) {
        this.TPAStatus = TPAStatus;
    }

    public int getLoginCode() {
        return loginCode;
    }

    public void setLoginCode(int loginCode) {
        this.loginCode = loginCode;
    }

    public int getDistCode() {
        return distCode;
    }

    public void setDistCode(int distCode) {
        this.distCode = distCode;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "loginID='" + loginID + '\'' +
                ", userName='" + userName + '\'' +
                ", officeTypeCode=" + officeTypeCode +
                ", defaultProcessTask='" + defaultProcessTask + '\'' +
                ", additional_ProcessTask='" + additional_ProcessTask + '\'' +
                ", roleCode=" + roleCode +
                ", LoginStatus='" + LoginStatus + '\'' +
                ", userCode=" + userCode +
                ", TPACompCode=" + TPACompCode +
                ", TPACompName='" + TPACompName + '\'' +
                ", TPAStatus='" + TPAStatus + '\'' +
                ", loginCode=" + loginCode +
                ", distCode=" + distCode +
                '}';
    }
}
