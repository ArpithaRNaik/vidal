package com.effiatechnologies.vidalinvestigator.model;

import java.util.List;

/**
 * Created by Arpitha on 11-12-2017.
 */

public class UpdateUrnDetailsInput {

    private List<UrnDetailsResponse> UrlDetailsList;
    private int FlagValue;

    public UpdateUrnDetailsInput()
    {

    }

    public UpdateUrnDetailsInput(List<UrnDetailsResponse> urlDetailsList, int FlagValue) {
        UrlDetailsList = urlDetailsList;
        this.FlagValue = FlagValue;
    }

    public List<UrnDetailsResponse> getUrlDetailsList() {
        return UrlDetailsList;
    }

    public void setUrlDetailsList(List<UrnDetailsResponse> urlDetailsList) {
        UrlDetailsList = urlDetailsList;
    }

    public int getFlagValue() {
        return FlagValue;
    }

    public void setFlagValue(int flagValue) {
        FlagValue = flagValue;
    }

    @Override
    public String toString() {
        return "UpdateUrnDetailsInput{" +
                "UrlDetailsList=" + UrlDetailsList +
                ", FlagValue=" + FlagValue +
                '}';
    }
}




