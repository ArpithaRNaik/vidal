package com.effiatechnologies.vidalinvestigator.model;

import android.content.Context;
import android.view.View;

import com.github.mikephil.charting.data.ChartData;

/**
 * Created by Administrator on 29-11-2016.
 */

public abstract class ChartItem {

    protected static final int TYPE_BARCHART = 0;
   // protected static final int TYPE_LINECHART = 1;
    protected static final int TYPE_PIECHART = 1;

    protected ChartData<?> mChartData;

    public ChartItem(ChartData<?> cd) {
        this.mChartData = cd;
    }

    public abstract int getItemType();

    public abstract View getView(int position, View convertView, Context c);
}
