package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 06-02-2018.
 */

public class ReportResponseModel {

    private String DistrictName;
    private String BlockName;
    private String PanchayatName;
    private String VillageName;
    private int TotalAadhaarSeeding;

    public ReportResponseModel()
    {

    }

    public ReportResponseModel(String districtName, String blockName, String panchayatName, String villageName, int totalAadhaarSeeding) {
        DistrictName = districtName;
        BlockName = blockName;
        PanchayatName = panchayatName;
        VillageName = villageName;
        TotalAadhaarSeeding = totalAadhaarSeeding;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public String getBlockName() {
        return BlockName;
    }

    public void setBlockName(String blockName) {
        BlockName = blockName;
    }

    public String getPanchayatName() {
        return PanchayatName;
    }

    public void setPanchayatName(String panchayatName) {
        PanchayatName = panchayatName;
    }

    public String getVillageName() {
        return VillageName;
    }

    public void setVillageName(String villageName) {
        VillageName = villageName;
    }

    public int getTotalAadhaarSeeding() {
        return TotalAadhaarSeeding;
    }

    public void setTotalAadhaarSeeding(int totalAadhaarSeeding) {
        TotalAadhaarSeeding = totalAadhaarSeeding;
    }

    @Override
    public String toString() {
        return "ReportResponseModel{" +
                "DistrictName='" + DistrictName + '\'' +
                ", BlockName='" + BlockName + '\'' +
                ", PanchayatName='" + PanchayatName + '\'' +
                ", VillageName='" + VillageName + '\'' +
                ", TotalAadhaarSeeding=" + TotalAadhaarSeeding +
                '}';
    }
}
