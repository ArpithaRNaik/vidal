package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 20-06-2017.
 */

public class QuestionAnswerTableList {


    private String id;
    private String Question;
    private String QuestionId;
    private String StateCode;
    private String IsActive;
    private String QuestionAnswer;
    public QuestionAnswerTableList()
    {

    }

    public QuestionAnswerTableList(String id, String question, String questionId, String stateCode, String isActive, String questionAnswer) {
        this.id = id;
        Question = question;
        QuestionId = questionId;
        StateCode = stateCode;
        IsActive = isActive;
        QuestionAnswer = questionAnswer;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

    public String getStateCode() {
        return StateCode;
    }

    public void setStateCode(String stateCode) {
        StateCode = stateCode;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getQuestionAnswer() {
        return QuestionAnswer;
    }

    public void setQuestionAnswer(String questionAnswer) {
        QuestionAnswer = questionAnswer;
    }

    @Override
    public String toString() {
        return "QuestionAnswerTableList{" +
                "id='" + id + '\'' +
                ", Question='" + Question + '\'' +
                ", QuestionId='" + QuestionId + '\'' +
                ", StateCode='" + StateCode + '\'' +
                ", IsActive='" + IsActive + '\'' +
                ", QuestionAnswer='" + QuestionAnswer + '\'' +
                '}';
    }
}
