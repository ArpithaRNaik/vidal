package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 19-12-2017.
 */

public class UrnReportsInput {

    private String TPACode;
    private String FromDate;
    private String ToDate;
    private String DistrictCode;
    private String StateCode;
    private String LoginID;
    private String UserCode;

    public UrnReportsInput()
    {

    }

    public UrnReportsInput(String TPACode, String fromDate, String toDate, String districtCode, String stateCode, String loginID, String userCode) {
        this.TPACode = TPACode;
        FromDate = fromDate;
        ToDate = toDate;
        DistrictCode = districtCode;
        StateCode = stateCode;
        LoginID = loginID;
        UserCode = userCode;
    }

    public String getTPACode() {
        return TPACode;
    }

    public void setTPACode(String TPACode) {
        this.TPACode = TPACode;
    }

    public String getFromDate() {
        return FromDate;
    }

    public void setFromDate(String fromDate) {
        FromDate = fromDate;
    }

    public String getToDate() {
        return ToDate;
    }

    public void setToDate(String toDate) {
        ToDate = toDate;
    }

    public String getDistrictCode() {
        return DistrictCode;
    }

    public void setDistrictCode(String districtCode) {
        DistrictCode = districtCode;
    }

    public String getStateCode() {
        return StateCode;
    }

    public void setStateCode(String stateCode) {
        StateCode = stateCode;
    }

    public String getLoginID() {
        return LoginID;
    }

    public void setLoginID(String loginID) {
        LoginID = loginID;
    }

    public String getUserCode() {
        return UserCode;
    }

    public void setUserCode(String userCode) {
        UserCode = userCode;
    }

    @Override
    public String toString() {
        return "UrnReportsInput{" +
                "TPACode='" + TPACode + '\'' +
                ", FromDate='" + FromDate + '\'' +
                ", ToDate='" + ToDate + '\'' +
                ", DistrictCode='" + DistrictCode + '\'' +
                ", StateCode='" + StateCode + '\'' +
                ", LoginID='" + LoginID + '\'' +
                ", UserCode='" + UserCode + '\'' +
                '}';
    }
}
