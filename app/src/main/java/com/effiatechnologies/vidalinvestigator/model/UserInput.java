package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Administrator on 17-11-2016.
 */

public class UserInput {

    private String LoginId;
    private String Password;
    //private String PIN;

    private String SimSerialNumber;
    private String ipAddress;
    private String IMEINumber;
    private String Latitude;
    private String Longitude;

    public UserInput()
    {

    }

    public UserInput(String LoginId, String Password, String SimSerialNumber, String ipAddress, String IMEINumber,
                     String Latitude, String Longitude) {
        this.LoginId = LoginId;
        this.Password = Password;
        this.SimSerialNumber = SimSerialNumber;
        this.ipAddress = ipAddress;
        this.IMEINumber = IMEINumber;
        this.Latitude = Latitude;
        this.Longitude = Longitude;
      //  PIN = PIN;
    }

    public String getLoginId() {
        return LoginId;
    }

    public void setLoginId(String loginId) {
        LoginId = loginId;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getSimSerialNumber() {
        return SimSerialNumber;
    }

    public void setSimSerialNumber(String simSerialNumber) {
        SimSerialNumber = simSerialNumber;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIMEINumber() {
        return IMEINumber;
    }

    public void setIMEINumber(String IMEINumber) {
        this.IMEINumber = IMEINumber;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    @Override
    public String toString() {
        return "UserInput{" +
                "LoginId='" + LoginId + '\'' +
                ", Password='" + Password + '\'' +
                ", SimSerialNumber='" + SimSerialNumber + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", IMEINumber='" + IMEINumber + '\'' +
                ", Latitude='" + Latitude + '\'' +
                ", Longitude='" + Longitude + '\'' +
                '}';
    }
}
