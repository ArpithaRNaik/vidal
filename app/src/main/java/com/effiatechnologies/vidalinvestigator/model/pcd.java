package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Administrator on 03-12-2016.
 */

public class pcd {

    private String Trans;
    private int stateCode;
    private int PaymentMadeToHospital;
    private int PaymentReceivedFromIC;

    public pcd()
    {

    }

    public pcd(String Trans, int stateCode, int PaymentMadeToHospital, int PaymentReceivedFromIC) {
        Trans = Trans;
        stateCode = stateCode;
        PaymentMadeToHospital = PaymentMadeToHospital;
        PaymentReceivedFromIC = PaymentReceivedFromIC;
    }

    public String getTrans() {
        return Trans;
    }

    public void setTrans(String trans) {
        Trans = trans;
    }

    public int getStateCode() {
        return stateCode;
    }

    public void setStateCode(int stateCode) {
        this.stateCode = stateCode;
    }

    public int getPaymentMadeToHospital() {
        return PaymentMadeToHospital;
    }

    public void setPaymentMadeToHospital(int paymentMadeToHospital) {
        PaymentMadeToHospital = paymentMadeToHospital;
    }

    public int getPaymentReceivedFromIC() {
        return PaymentReceivedFromIC;
    }

    public void setPaymentReceivedFromIC(int paymentReceivedFromIC) {
        PaymentReceivedFromIC = paymentReceivedFromIC;
    }

    @Override
    public String toString() {
        return "pcd{" +
                "Trans='" + Trans + '\'' +
                ", stateCode=" + stateCode +
                ", PaymentMadeToHospital=" + PaymentMadeToHospital +
                ", PaymentReceivedFromIC=" + PaymentReceivedFromIC +
                '}';
    }
}
