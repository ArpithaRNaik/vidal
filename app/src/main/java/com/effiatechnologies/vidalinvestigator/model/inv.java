package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Administrator on 03-12-2016.
 */

public class inv {

    private String Trans;
    private int stateCode;
    private int Completed;
    private int Pending;


    public inv()
    {

    }

    public inv(String Trans, int stateCode, int Completed, int Pending)
    {
        Trans = Trans;
        stateCode= stateCode;
        Completed = Completed;
        Pending = Pending;

    }

    public String getTrans() {
        return Trans;
    }

    public void setTrans(String trans) {
        Trans = trans;
    }

    public int getStateCode() {
        return stateCode;
    }

    public void setStateCode(int stateCode) {
        this.stateCode = stateCode;
    }

    public int getCompleted() {
        return Completed;
    }

    public void setCompleted(int completed) {
        Completed = completed;
    }

    public int getPending() {
        return Pending;
    }

    public void setPending(int pending) {
        Pending = pending;
    }

    @Override
    public String toString() {
        return "inv{" +
                "Trans='" + Trans + '\'' +
                ", stateCode=" + stateCode +
                ", Completed=" + Completed +
                ", Pending=" + Pending +
                '}';
    }
}
