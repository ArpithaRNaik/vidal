package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Administrator on 03-12-2016.
 */

public class io {

    private String Trans;
    private int stateCode;
    private int Received;
    private int Approved;
    private int Rejected;
    private int Pending;

    public io()
    {

    }

    public io(String Trans, int stateCode, int Received, int Approved, int Rejected, int Pending)
    {
        Trans = Trans;
        stateCode= stateCode;
        Received = Received;
        Approved = Approved;
        Rejected = Rejected;
        Pending = Pending;

    }

    public String getTrans() {
        return Trans;
    }

    public void setTrans(String trans) {
        Trans = trans;
    }

    public int getStateCode() {
        return stateCode;
    }

    public void setStateCode(int stateCode) {
        this.stateCode = stateCode;
    }

    public int getReceived() {
        return Received;
    }

    public void setReceived(int received) {
        Received = received;
    }

    public int getApproved() {
        return Approved;
    }

    public void setApproved(int approved) {
        Approved = approved;
    }

    public int getRejected() {
        return Rejected;
    }

    public void setRejected(int rejected) {
        Rejected = rejected;
    }

    public int getPending() {
        return Pending;
    }

    public void setPending(int pending) {
        Pending = pending;
    }

    @Override
    public String toString() {
        return "io{" +
                "Trans='" + Trans + '\'' +
                ", stateCode=" + stateCode +
                ", Received=" + Received +
                ", Approved=" + Approved +
                ", Rejected=" + Rejected +
                ", Pending=" + Pending +
                '}';
    }
}
