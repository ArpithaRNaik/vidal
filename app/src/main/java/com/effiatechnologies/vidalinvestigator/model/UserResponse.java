package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by User3 on 5/27/2016.
 */
public class UserResponse {

    private String Result;
    //private String UserName;
    //private String Password;


    public UserResponse()
    {

    }

    public UserResponse(String Result)
    {
        this.Result = Result;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "Result='" + Result + '\'' +
                '}';
    }
}
