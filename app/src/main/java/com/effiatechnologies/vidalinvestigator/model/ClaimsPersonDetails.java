package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Administrator on 14-12-2016.
 */

public class ClaimsPersonDetails {

    private String PatientName;
    private String PatientAge;
    private String PatientGender;
    private String PatientDateOfJoining;
    private String PatientDateOfDischarge;
    private String PatientHospital;
    private String PatientTreatmentAvailed;
    private String PatientCategory;
    private String PatientAmountCharged;

    public ClaimsPersonDetails()
    {

    }
    public ClaimsPersonDetails(String PatientName, String PatientAge, String PatientGender, String PatientDateOfJoining,
                               String PatientDateOfDischarge, String PatientHospital, String PatientTreatmentAvailed,
                               String PatientCategory, String PatientAmountCharged)
    {
        this.PatientName = PatientName;
        this.PatientAge = PatientAge;
        this.PatientGender = PatientGender;
        this.PatientDateOfJoining = PatientDateOfJoining;
        this.PatientDateOfDischarge = PatientDateOfDischarge;
        this.PatientHospital = PatientHospital;
        this.PatientTreatmentAvailed = PatientTreatmentAvailed;
        this.PatientCategory = PatientCategory;
        this.PatientAmountCharged = PatientAmountCharged;
    }

    public String getPatientName() {
        return PatientName;
    }

    public void setPatientName(String patientName) {
        PatientName = patientName;
    }

    public String getPatientAge() {
        return PatientAge;
    }

    public void setPatientAge(String patientAge) {
        PatientAge = patientAge;
    }

    public String getPatientGender() {
        return PatientGender;
    }

    public void setPatientGender(String patientGender) {
        PatientGender = patientGender;
    }

    public String getPatientDateOfJoining() {
        return PatientDateOfJoining;
    }

    public void setPatientDateOfJoining(String patientDateOfJoining) {
        PatientDateOfJoining = patientDateOfJoining;
    }

    public String getPatientDateOfDischarge() {
        return PatientDateOfDischarge;
    }

    public void setPatientDateOfDischarge(String patientDateOfDischarge) {
        PatientDateOfDischarge = patientDateOfDischarge;
    }

    public String getPatientHospital() {
        return PatientHospital;
    }

    public void setPatientHospital(String patientHospital) {
        PatientHospital = patientHospital;
    }

    public String getPatientTreatmentAvailed() {
        return PatientTreatmentAvailed;
    }

    public void setPatientTreatmentAvailed(String patientTreatmentAvailed) {
        PatientTreatmentAvailed = patientTreatmentAvailed;
    }

    public String getPatientCategory() {
        return PatientCategory;
    }

    public void setPatientCategory(String patientCategory) {
        PatientCategory = patientCategory;
    }

    public String getPatientAmountCharged() {
        return PatientAmountCharged;
    }

    public void setPatientAmountCharged(String patientAmountCharged) {
        PatientAmountCharged = patientAmountCharged;
    }

    @Override
    public String toString() {
        return "ClaimsPersonDetails{" +
                "PatientName='" + PatientName + '\'' +
                ", PatientAge='" + PatientAge + '\'' +
                ", PatientGender='" + PatientGender + '\'' +
                ", PatientDateOfJoining='" + PatientDateOfJoining + '\'' +
                ", PatientDateOfDischarge='" + PatientDateOfDischarge + '\'' +
                ", PatientHospital='" + PatientHospital + '\'' +
                ", PatientTreatmentAvailed='" + PatientTreatmentAvailed + '\'' +
                ", PatientCategory='" + PatientCategory + '\'' +
                ", PatientAmountCharged='" + PatientAmountCharged + '\'' +
                '}';
    }
}
