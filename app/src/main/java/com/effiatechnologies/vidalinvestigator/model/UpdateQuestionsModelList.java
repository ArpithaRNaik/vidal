package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 20-06-2017.
 */

public class UpdateQuestionsModelList {

    private String ids;
    private String Question;
    private String QuestionId;
    private String StateCode;
    private String IsActive;

    public UpdateQuestionsModelList()
    {

    }

    public UpdateQuestionsModelList(String ids, String question, String questionId, String stateCode, String isActive) {
        this.ids = ids;
        Question = question;
        QuestionId = questionId;
        StateCode = stateCode;
        IsActive = isActive;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public String getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(String questionId) {
        QuestionId = questionId;
    }

    public String getStateCode() {
        return StateCode;
    }

    public void setStateCode(String stateCode) {
        StateCode = stateCode;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    @Override
    public String toString() {
        return "UpdateQuestionsModelList{" +
                "ids='" + ids + '\'' +
                ", Question='" + Question + '\'' +
                ", QuestionId='" + QuestionId + '\'' +
                ", StateCode='" + StateCode + '\'' +
                ", IsActive='" + IsActive + '\'' +
                '}';
    }
}
