package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 09-06-2017.
 */

public class RelationsResponse {

    private int RelationCode;
    private String RelationDescription;



    public RelationsResponse()
    {

    }

    public RelationsResponse(int relationCode, String relationDescription) {
        RelationCode = relationCode;
        RelationDescription = relationDescription;
    }

    public int getRelationCode() {
        return RelationCode;
    }

    public void setRelationCode(int relationCode) {
        RelationCode = relationCode;
    }

    public String getRelationDescription() {
        return RelationDescription;
    }

    public void setRelationDescription(String relationDescription) {
        RelationDescription = relationDescription;
    }

    @Override
    public String toString() {
        return "RelationsResponse{" +
                "RelationCode=" + RelationCode +
                ", RelationDescription='" + RelationDescription + '\'' +
                '}';
    }
}
