package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 16-03-2018.
 */

public class LocalMemDetailsResponse {

    private String URN;
    private int MemberID;
    private String MemberName;
    private String MemberAge;
    private String AadhaarNo;
    private String NameAsAadhaar;
    private String MobileNO;
    private String IMIENumber;
    private String DateTime;
    private String LoginId;
    private String UserCode;

    public LocalMemDetailsResponse()
    {

    }
    public LocalMemDetailsResponse(String URN, int memberID, String memberName, String memberAge, String aadhaarNo,
                                   String nameAsAadhaar, String mobileNO, String IMIENumber, String dateTime,
                                   String loginId, String userCode) {
        this.URN = URN;
        MemberID = memberID;
        MemberName = memberName;
        MemberAge = memberAge;
        AadhaarNo = aadhaarNo;
        NameAsAadhaar = nameAsAadhaar;
        MobileNO = mobileNO;
        this.IMIENumber = IMIENumber;
        DateTime = dateTime;
        LoginId = loginId;
        UserCode = userCode;
    }

    public String getURN() {
        return URN;
    }

    public void setURN(String URN) {
        this.URN = URN;
    }

    public int getMemberID() {
        return MemberID;
    }

    public void setMemberID(int memberID) {
        MemberID = memberID;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }

    public String getAadhaarNo() {
        return AadhaarNo;
    }

    public void setAadhaarNo(String aadhaarNo) {
        AadhaarNo = aadhaarNo;
    }

    public String getNameAsAadhaar() {
        return NameAsAadhaar;
    }

    public void setNameAsAadhaar(String nameAsAadhaar) {
        NameAsAadhaar = nameAsAadhaar;
    }

    public String getMobileNO() {
        return MobileNO;
    }

    public void setMobileNO(String mobileNO) {
        MobileNO = mobileNO;
    }

    public String getIMIENumber() {
        return IMIENumber;
    }

    public void setIMIENumber(String IMIENumber) {
        this.IMIENumber = IMIENumber;
    }

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public String getLoginId() {
        return LoginId;
    }

    public void setLoginId(String loginId) {
        LoginId = loginId;
    }

    public String getUserCode() {
        return UserCode;
    }

    public void setUserCode(String userCode) {
        UserCode = userCode;
    }

    public String getMemberAge() {
        return MemberAge;
    }

    public void setMemberAge(String memberAge) {
        MemberAge = memberAge;
    }

    @Override
    public String toString() {
        return "LocalMemDetailsResponse{" +
                "URN='" + URN + '\'' +
                ", MemberID=" + MemberID +
                ", MemberName='" + MemberName + '\'' +
                ", AadhaarNo='" + AadhaarNo + '\'' +
                ", NameAsAadhaar='" + NameAsAadhaar + '\'' +
                ", MobileNO='" + MobileNO + '\'' +
                ", IMIENumber='" + IMIENumber + '\'' +
                ", DateTime='" + DateTime + '\'' +
                ", LoginId='" + LoginId + '\'' +
                ", UserCode='" + UserCode + '\'' +
                '}';
    }
}
