package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Administrator on 02-12-2016.
 */

public class NativeGraphInput {

    private String UserName;
    private int StateCode;

    public NativeGraphInput()
    {

    }

    public NativeGraphInput(String UserName, int StateCode) {
        UserName = UserName;
        StateCode = StateCode;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public int getStateCode() {
        return StateCode;
    }

    public void setStateCode(int stateCode) {
        StateCode = stateCode;
    }

    @Override
    public String toString() {
        return "NativeGraphInput{" +
                "UserName='" + UserName + '\'' +
                ", StateCode=" + StateCode +
                '}';
    }
}
