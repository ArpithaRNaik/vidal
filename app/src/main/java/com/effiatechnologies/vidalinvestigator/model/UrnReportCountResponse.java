package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 19-12-2017.
 */

public class UrnReportCountResponse {

    private String UserName;
    private int DistrictCode;
    private String DistrictName;
    private int FamilySeeding;
    private int BeneficiarySeeding;

    public UrnReportCountResponse()
    {

    }

    public UrnReportCountResponse(String userName, int districtCode, String districtName, int familySeeding, int beneficiarySeeding) {
        UserName = userName;
        DistrictCode = districtCode;
        DistrictName = districtName;
        FamilySeeding = familySeeding;
        BeneficiarySeeding = beneficiarySeeding;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public int getDistrictCode() {
        return DistrictCode;
    }

    public void setDistrictCode(int districtCode) {
        DistrictCode = districtCode;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public int getFamilySeeding() {
        return FamilySeeding;
    }

    public void setFamilySeeding(int familySeeding) {
        FamilySeeding = familySeeding;
    }

    public int getBeneficiarySeeding() {
        return BeneficiarySeeding;
    }

    public void setBeneficiarySeeding(int beneficiarySeeding) {
        BeneficiarySeeding = beneficiarySeeding;
    }

    @Override
    public String toString() {
        return "UrnReportCountResponse{" +
                "UserName='" + UserName + '\'' +
                ", DistrictCode=" + DistrictCode +
                ", DistrictName='" + DistrictName + '\'' +
                ", FamilySeeding=" + FamilySeeding +
                ", BeneficiarySeeding=" + BeneficiarySeeding +
                '}';
    }
}
