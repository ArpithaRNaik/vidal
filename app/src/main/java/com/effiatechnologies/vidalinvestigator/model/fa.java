package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Administrator on 03-12-2016.
 */

public class fa {

    private String Trans;
    private int stateCode;
    private int Approved;
    private int Pending;


    public fa()
    {

    }

    public fa(String Trans, int stateCode, int Approved, int Pending)
    {
        Trans = Trans;
        stateCode= stateCode;
        Approved = Approved;
        Pending = Pending;

    }

    public String getTrans() {
        return Trans;
    }

    public void setTrans(String trans) {
        Trans = trans;
    }

    public int getStateCode() {
        return stateCode;
    }

    public void setStateCode(int stateCode) {
        this.stateCode = stateCode;
    }

    public int getApproved() {
        return Approved;
    }

    public void setApproved(int approved) {
        Approved = approved;
    }

    public int getPending() {
        return Pending;
    }

    public void setPending(int pending) {
        Pending = pending;
    }

    @Override
    public String toString() {
        return "fa{" +
                "Trans='" + Trans + '\'' +
                ", stateCode=" + stateCode +
                ", Approved=" + Approved +
                ", Pending=" + Pending +
                '}';
    }
}
