package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 09-06-2017.
 */

public class GenderResponse {

    private int GenderCode;
    private String GenderName;



    public GenderResponse()
    {

    }

    public GenderResponse(int genderCode, String genderName) {
        GenderCode = genderCode;
        GenderName = genderName;
    }

    public int getGenderCode() {
        return GenderCode;
    }

    public void setGenderCode(int genderCode) {
        GenderCode = genderCode;
    }

    public String getGenderName() {
        return GenderName;
    }

    public void setGenderName(String genderName) {
        GenderName = genderName;
    }

    @Override
    public String toString() {
        return "GenderResponse{" +
                "GenderCode=" + GenderCode +
                ", GenderName='" + GenderName + '\'' +
                '}';
    }
}
