package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 11-12-2017.
 */

public class UrnDetailsResponse {

    private String URN;
    private String SINo;
    private String MemberName;
    private int MemberID;
    private String Age;
    private String Relation;
    private String IsEnrolled;
    private String AadhaarNo;
    private String NameAsAadhaar;
    private String MobileNO;
    private String AadhaarNumber;
    private String IMIENumber;
    private String DateTime;
    private int RemarksID;
    private String RemarksName;
    private String LoginId;
    private String UserCode;
    private String RelationCode;
    private String GenderName;
    private String GenderCode;
    private String FamilyId;
    private int Flag;
    private boolean selected;


    public UrnDetailsResponse()
    {

    }

    public UrnDetailsResponse(String URN, String SINo, String memberName, int memberID, String age, String genderCode,
                              String genderName, String relationCode, String relation, String isEnrolled, String aadhaarNo,
                              String nameAsAadhaar, String mobileNO, String aadhaarNumber, String IMIENumber, String dateTime,
                              int remarksID, String remarksName, String loginId, String userCode, String FamilyId, int Flag, boolean selected) {
        this.URN = URN;
        this.SINo = SINo;
        MemberName = memberName;
        MemberID = memberID;
        Age = age;
        GenderCode = genderCode;
        GenderName = genderName;
        RelationCode = relationCode;
        Relation = relation;
        IsEnrolled = isEnrolled;
        AadhaarNo = aadhaarNo;
        NameAsAadhaar = nameAsAadhaar;
        MobileNO = mobileNO;
        AadhaarNumber = aadhaarNumber;
        this.IMIENumber = IMIENumber;
        DateTime = dateTime;
        RemarksID = remarksID;
        RemarksName = remarksName;
        LoginId = loginId;
        UserCode = userCode;
        this.FamilyId = FamilyId;
        this.Flag = Flag;
        this.selected = selected;
    }

    public String getURN() {
        return URN;
    }

    public void setURN(String URN) {
        this.URN = URN;
    }

    public String getSINo() {
        return SINo;
    }

    public void setSINo(String SINo) {
        this.SINo = SINo;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }

    public int getMemberID() {
        return MemberID;
    }

    public void setMemberID(int memberID) {
        MemberID = memberID;
    }

    public String getRelation() {
        return Relation;
    }

    public void setRelation(String relation) {
        Relation = relation;
    }

    public String getIsEnrolled() {
        return IsEnrolled;
    }

    public void setIsEnrolled(String isEnrolled) {
        IsEnrolled = isEnrolled;
    }

    public String getAadhaarNo() {
        return AadhaarNo;
    }

    public void setAadhaarNo(String aadhaarNo) {
        AadhaarNo = aadhaarNo;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getNameAsAadhaar() {
        return NameAsAadhaar;
    }

    public void setNameAsAadhaar(String nameAsAadhaar) {
        NameAsAadhaar = nameAsAadhaar;
    }

    public String getMobileNO() {
        return MobileNO;
    }

    public void setMobileNO(String mobileNO) {
        MobileNO = mobileNO;
    }

    public String getAadhaarNumber() {
        return AadhaarNumber;
    }

    public void setAadhaarNumber(String aadhaarNumber) {
        AadhaarNumber = aadhaarNumber;
    }

    public String getIMIENumber() {
        return IMIENumber;
    }

    public void setIMIENumber(String IMIENumber) {
        this.IMIENumber = IMIENumber;
    }

    public String getDateTime() {
        return DateTime;
    }

    public void setDateTime(String dateTime) {
        DateTime = dateTime;
    }

    public int getRemarksID() {
        return RemarksID;
    }

    public void setRemarksID(int remarksID) {
        RemarksID = remarksID;
    }

    public String getRemarksName() {
        return RemarksName;
    }

    public void setRemarksName(String remarksName) {
        RemarksName = remarksName;
    }

    public String getLoginId() {
        return LoginId;
    }

    public void setLoginId(String loginId) {
        LoginId = loginId;
    }

    public String getUserCode() {
        return UserCode;
    }

    public void setUserCode(String userCode) {
        UserCode = userCode;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getGenderCode() {
        return GenderCode;
    }

    public void setGenderCode(String genderCode) {
        GenderCode = genderCode;
    }

    public String getGenderName() {
        return GenderName;
    }

    public void setGenderName(String genderName) {
        GenderName = genderName;
    }

    public String getRelationCode() {
        return RelationCode;
    }

    public void setRelationCode(String relationCode) {
        RelationCode = relationCode;
    }

    public String getFamilyId() {
        return FamilyId;
    }

    public void setFamilyId(String familyId) {
        FamilyId = familyId;
    }

    public int getFlag() {
        return Flag;
    }

    public void setFlag(int flag) {
        Flag = flag;
    }

    @Override
    public String toString() {
        return "UrnDetailsResponse{" +
                "URN='" + URN + '\'' +
                ", SINo='" + SINo + '\'' +
                ", MemberName='" + MemberName + '\'' +
                ", MemberID=" + MemberID +
                ", Age='" + Age + '\'' +
                ", Relation='" + Relation + '\'' +
                ", IsEnrolled='" + IsEnrolled + '\'' +
                ", AadhaarNo='" + AadhaarNo + '\'' +
                ", NameAsAadhaar='" + NameAsAadhaar + '\'' +
                ", MobileNO='" + MobileNO + '\'' +
                ", AadhaarNumber='" + AadhaarNumber + '\'' +
                ", IMIENumber='" + IMIENumber + '\'' +
                ", DateTime='" + DateTime + '\'' +
                ", RemarksID=" + RemarksID +
                ", RemarksName='" + RemarksName + '\'' +
                ", LoginId='" + LoginId + '\'' +
                ", UserCode='" + UserCode + '\'' +
                ", RelationCode='" + RelationCode + '\'' +
                ", GenderName='" + GenderName + '\'' +
                ", GenderCode='" + GenderCode + '\'' +
                ", FamilyId='" + FamilyId + '\'' +
                ", Flag=" + Flag +
                ", selected=" + selected +
                '}';
    }
}




