package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Administrator on 22-12-2016.
 */

public class AllStateDocResponse {

    private int rowNo;
    private int StateCode;
    private String StateName;
    private double Received;
    private double ReceivedAmt;
    private double Approved;
    private double ApprovedAmt;
    private double Pending;
    private double PendingAmt;
    private double Claimspendingbeyond25days_N_I;
    private double Beyond25DaysAmt;
    private double Claimspendingbeyond25days_Foreign;
    private double Beyond25DaysForeignAmt;
    private double ClaimsRejectedNos;
    private double RejectedAmt;
    private double ClaimspaidNos;
    private double PaidAmt;
    private double PendingFloatGeneration;
    private double PendingFloatGenerationAmt;

    public AllStateDocResponse()
    {

    }

    public AllStateDocResponse(int rowNo, int stateCode, String stateName, double received, double receivedAmt, double approved, double approvedAmt, double pending, double pendingAmt, double claimspendingbeyond25days_N_I, double beyond25DaysAmt, double claimspendingbeyond25days_Foreign, double beyond25DaysForeignAmt, double claimsRejectedNos, double rejectedAmt, double claimspaidNos, double paidAmt, double pendingFloatGeneration, double pendingFloatGenerationAmt) {
        this.rowNo = rowNo;
        StateCode = stateCode;
        StateName = stateName;
        Received = received;
        ReceivedAmt = receivedAmt;
        Approved = approved;
        ApprovedAmt = approvedAmt;
        Pending = pending;
        PendingAmt = pendingAmt;
        Claimspendingbeyond25days_N_I = claimspendingbeyond25days_N_I;
        Beyond25DaysAmt = beyond25DaysAmt;
        Claimspendingbeyond25days_Foreign = claimspendingbeyond25days_Foreign;
        Beyond25DaysForeignAmt = beyond25DaysForeignAmt;
        ClaimsRejectedNos = claimsRejectedNos;
        RejectedAmt = rejectedAmt;
        ClaimspaidNos = claimspaidNos;
        PaidAmt = paidAmt;
        PendingFloatGeneration = pendingFloatGeneration;
        PendingFloatGenerationAmt = pendingFloatGenerationAmt;
    }

    public int getRowNo() {
        return rowNo;
    }

    public void setRowNo(int rowNo) {
        this.rowNo = rowNo;
    }

    public int getStateCode() {
        return StateCode;
    }

    public void setStateCode(int stateCode) {
        StateCode = stateCode;
    }

    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public double getReceived() {
        return Received;
    }

    public void setReceived(double received) {
        Received = received;
    }

    public double getReceivedAmt() {
        return ReceivedAmt;
    }

    public void setReceivedAmt(double receivedAmt) {
        ReceivedAmt = receivedAmt;
    }

    public double getApproved() {
        return Approved;
    }

    public void setApproved(double approved) {
        Approved = approved;
    }

    public double getApprovedAmt() {
        return ApprovedAmt;
    }

    public void setApprovedAmt(double approvedAmt) {
        ApprovedAmt = approvedAmt;
    }

    public double getPending() {
        return Pending;
    }

    public void setPending(double pending) {
        Pending = pending;
    }

    public double getPendingAmt() {
        return PendingAmt;
    }

    public void setPendingAmt(double pendingAmt) {
        PendingAmt = pendingAmt;
    }

    public double getClaimspendingbeyond25days_N_I() {
        return Claimspendingbeyond25days_N_I;
    }

    public void setClaimspendingbeyond25days_N_I(double claimspendingbeyond25days_N_I) {
        Claimspendingbeyond25days_N_I = claimspendingbeyond25days_N_I;
    }

    public double getBeyond25DaysAmt() {
        return Beyond25DaysAmt;
    }

    public void setBeyond25DaysAmt(double beyond25DaysAmt) {
        Beyond25DaysAmt = beyond25DaysAmt;
    }

    public double getClaimspendingbeyond25days_Foreign() {
        return Claimspendingbeyond25days_Foreign;
    }

    public void setClaimspendingbeyond25days_Foreign(double claimspendingbeyond25days_Foreign) {
        Claimspendingbeyond25days_Foreign = claimspendingbeyond25days_Foreign;
    }

    public double getBeyond25DaysForeignAmt() {
        return Beyond25DaysForeignAmt;
    }

    public void setBeyond25DaysForeignAmt(double beyond25DaysForeignAmt) {
        Beyond25DaysForeignAmt = beyond25DaysForeignAmt;
    }

    public double getClaimsRejectedNos() {
        return ClaimsRejectedNos;
    }

    public void setClaimsRejectedNos(double claimsRejectedNos) {
        ClaimsRejectedNos = claimsRejectedNos;
    }

    public double getRejectedAmt() {
        return RejectedAmt;
    }

    public void setRejectedAmt(double rejectedAmt) {
        RejectedAmt = rejectedAmt;
    }

    public double getClaimspaidNos() {
        return ClaimspaidNos;
    }

    public void setClaimspaidNos(double claimspaidNos) {
        ClaimspaidNos = claimspaidNos;
    }

    public double getPaidAmt() {
        return PaidAmt;
    }

    public void setPaidAmt(double paidAmt) {
        PaidAmt = paidAmt;
    }

    public double getPendingFloatGeneration() {
        return PendingFloatGeneration;
    }

    public void setPendingFloatGeneration(double pendingFloatGeneration) {
        PendingFloatGeneration = pendingFloatGeneration;
    }

    public double getPendingFloatGenerationAmt() {
        return PendingFloatGenerationAmt;
    }

    public void setPendingFloatGenerationAmt(double pendingFloatGenerationAmt) {
        PendingFloatGenerationAmt = pendingFloatGenerationAmt;
    }

    @Override
    public String toString() {
        return "AllStateDocResponse{" +
                "rowNo=" + rowNo +
                ", StateCode=" + StateCode +
                ", StateName='" + StateName + '\'' +
                ", Received=" + Received +
                ", ReceivedAmt=" + ReceivedAmt +
                ", Approved=" + Approved +
                ", ApprovedAmt=" + ApprovedAmt +
                ", Pending=" + Pending +
                ", PendingAmt=" + PendingAmt +
                ", Claimspendingbeyond25days_N_I=" + Claimspendingbeyond25days_N_I +
                ", Beyond25DaysAmt=" + Beyond25DaysAmt +
                ", Claimspendingbeyond25days_Foreign=" + Claimspendingbeyond25days_Foreign +
                ", Beyond25DaysForeignAmt=" + Beyond25DaysForeignAmt +
                ", ClaimsRejectedNos=" + ClaimsRejectedNos +
                ", RejectedAmt=" + RejectedAmt +
                ", ClaimspaidNos=" + ClaimspaidNos +
                ", PaidAmt=" + PaidAmt +
                ", PendingFloatGeneration=" + PendingFloatGeneration +
                ", PendingFloatGenerationAmt=" + PendingFloatGenerationAmt +
                '}';
    }
}
