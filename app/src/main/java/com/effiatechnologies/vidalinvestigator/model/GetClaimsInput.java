package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 09-06-2017.
 */

public class GetClaimsInput {

    private String loginID;
    private String defaultProcessTask;
    private int TPACompCode;
    private String HospitalStateCode;

    public GetClaimsInput()
    {

    }

    public GetClaimsInput(String loginID, String defaultProcessTask, int TPACompCode, String HospitalStateCode) {
        this.loginID = loginID;
        this.defaultProcessTask = defaultProcessTask;
        this.TPACompCode = TPACompCode;
        this.HospitalStateCode = HospitalStateCode;
    }

    public String getLoginID() {
        return loginID;
    }

    public void setLoginID(String loginID) {
        this.loginID = loginID;
    }

    public String getDefaultProcessTask() {
        return defaultProcessTask;
    }

    public void setDefaultProcessTask(String defaultProcessTask) {
        this.defaultProcessTask = defaultProcessTask;
    }

    public int getTPACompCode() {
        return TPACompCode;
    }

    public void setTPACompCode(int TPACompCode) {
        this.TPACompCode = TPACompCode;
    }

    public String getHospitalStateCode() {
        return HospitalStateCode;
    }

    public void setHospitalStateCode(String hospitalStateCode) {
        HospitalStateCode = hospitalStateCode;
    }

    @Override
    public String toString() {
        return "GetClaimsInput{" +
                "loginID='" + loginID + '\'' +
                ", defaultProcessTask='" + defaultProcessTask + '\'' +
                ", TPACompCode=" + TPACompCode +
                ", HospitalStateCode='" + HospitalStateCode + '\'' +
                '}';
    }
}
