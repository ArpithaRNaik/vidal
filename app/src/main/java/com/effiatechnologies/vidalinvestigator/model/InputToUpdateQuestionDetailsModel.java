package com.effiatechnologies.vidalinvestigator.model;

import org.json.JSONArray;

import java.util.List;

/**
 * Created by Arpitha on 17-06-2017.
 */

public class InputToUpdateQuestionDetailsModel {

    private List<UpdateQuestionsModelList> questionDetailsList;
    private String StateCode;


    public InputToUpdateQuestionDetailsModel()
    {

    }

    public InputToUpdateQuestionDetailsModel(List<UpdateQuestionsModelList> questionDetailsList, String StateCode) {
        this.questionDetailsList = questionDetailsList;
        this.StateCode = StateCode;
    }

    public List<UpdateQuestionsModelList> getQuestionDetailsList() {
        return questionDetailsList;
    }

    public void setQuestionDetailsList(List<UpdateQuestionsModelList> questionDetailsList) {
        this.questionDetailsList = questionDetailsList;
    }

    public String getStateCode() {
        return StateCode;
    }

    public void setStateCode(String stateCode) {
        StateCode = stateCode;
    }

    @Override
    public String toString() {
        return "InputToUpdateQuestionDetailsModel{" +
                "questionDetailsList=" + questionDetailsList +
                ", StateCode='" + StateCode + '\'' +
                '}';
    }
}
