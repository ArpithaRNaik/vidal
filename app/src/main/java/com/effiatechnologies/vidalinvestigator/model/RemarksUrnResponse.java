package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 09-06-2017.
 */

public class RemarksUrnResponse {

    private int remarksId;
    private String remarks;



    public RemarksUrnResponse()
    {

    }

    public RemarksUrnResponse(int remarksId, String remarks) {
        this.remarksId = remarksId;
        this.remarks = remarks;
    }

    public int getRemarksId() {
        return remarksId;
    }

    public void setRemarksId(int remarksId) {
        this.remarksId = remarksId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "RemarksUrnResponse{" +
                "remarksId=" + remarksId +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
