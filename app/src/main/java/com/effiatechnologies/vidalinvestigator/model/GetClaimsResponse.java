package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 09-06-2017.
 */

public class GetClaimsResponse {

    private String transID;
    private String HospitalTimeStamp;
    private String patientName;
    private String DateofAdmission;
    private String DateofDischarge;
    private String hospitalName;
    private String procedureName;
    private int amountNegotiated;
    private int patientAge;
    private String patientGender;
    private String PackageName;

    public GetClaimsResponse()
    {

    }

    public GetClaimsResponse(String transID, String hospitalTimeStamp, String patientName, String DateofAdmission,
                             String DateofDischarge, String hospitalName, String procedureName, int amountNegotiated, int patientAge,
                             String patientGender, String PackageName) {
        this.transID = transID;
        HospitalTimeStamp = hospitalTimeStamp;
        this.patientName = patientName;
        this.DateofAdmission = DateofAdmission;
        this.DateofDischarge = DateofDischarge;
        this.hospitalName = hospitalName;
        this.procedureName = procedureName;
        this.amountNegotiated = amountNegotiated;
        this.patientAge = patientAge;
        this.patientGender = patientGender;
        this.PackageName = PackageName;
    }

    public String getTransID() {
        return transID;
    }

    public void setTransID(String transID) {
        this.transID = transID;
    }

    public String getHospitalTimeStamp() {
        return HospitalTimeStamp;
    }

    public void setHospitalTimeStamp(String hospitalTimeStamp) {
        HospitalTimeStamp = hospitalTimeStamp;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getDateofAdmission() {
        return DateofAdmission;
    }

    public void setDateofAdmission(String dateofAdmission) {
        DateofAdmission = dateofAdmission;
    }

    public String getDateofDischarge() {
        return DateofDischarge;
    }

    public void setDateofDischarge(String dateofDischarge) {
        DateofDischarge = dateofDischarge;
    }

    public String getHospitalName() {
        return hospitalName;
    }

    public void setHospitalName(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    public int getAmountNegotiated() {
        return amountNegotiated;
    }

    public void setAmountNegotiated(int amountNegotiated) {
        this.amountNegotiated = amountNegotiated;
    }

    public int getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(int patientAge) {
        this.patientAge = patientAge;
    }

    public String getPatientGender() {
        return patientGender;
    }

    public void setPatientGender(String patientGender) {
        this.patientGender = patientGender;
    }

    public String getPackageName() {
        return PackageName;
    }

    public void setPackageName(String packageName) {
        PackageName = packageName;
    }

    @Override
    public String toString() {
        return "GetClaimsResponse{" +
                "transID='" + transID + '\'' +
                ", HospitalTimeStamp='" + HospitalTimeStamp + '\'' +
                ", patientName='" + patientName + '\'' +
                ", DateofAdmission='" + DateofAdmission + '\'' +
                ", DateofDischarge='" + DateofDischarge + '\'' +
                ", hospitalName='" + hospitalName + '\'' +
                ", procedureName='" + procedureName + '\'' +
                ", amountNegotiated=" + amountNegotiated +
                ", patientAge=" + patientAge +
                ", patientGender='" + patientGender + '\'' +
                ", PackageName='" + PackageName + '\'' +
                '}';
    }
}
