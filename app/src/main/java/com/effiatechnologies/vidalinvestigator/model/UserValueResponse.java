package com.effiatechnologies.vidalinvestigator.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Administrator on 21-11-2016.
 */

public class UserValueResponse implements Serializable{

    @SerializedName("roleCode")
    private int roleCode;

    @SerializedName("roleName")
    private String roleName;


    public UserValueResponse()
    {

    }

    public UserValueResponse(int roleCode, String roleName)
    {
        this.roleCode = roleCode;
        this.roleName = roleName;
    }

    public int getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(int roleCode) {
        this.roleCode = roleCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public String toString() {
        return "UserValueResponse{" +
                "roleCode=" + roleCode +
                ", roleName='" + roleName + '\'' +
                '}';
    }
}
