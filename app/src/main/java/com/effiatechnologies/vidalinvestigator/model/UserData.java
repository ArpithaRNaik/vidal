package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 07-02-2017.
 */

public class UserData {

    private String Result;


    public UserData()
    {

    }

    public UserData(String Result)
    {

        this.Result = Result;


    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    @Override
    public String toString() {
        return "UserData{" +
                "Result='" + Result + '\'' +
                '}';
    }
}
