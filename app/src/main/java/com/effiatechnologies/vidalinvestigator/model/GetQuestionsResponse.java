package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 16-06-2017.
 */

public class GetQuestionsResponse {

    private int QuestionId;
    private String Question;
    private int StateCode;
    private int IsActive;

    private boolean selected;

    public GetQuestionsResponse(boolean selected) {
        this.selected = false;
    }

    public GetQuestionsResponse()
    {

    }

    public GetQuestionsResponse(int questionId, String question, int stateCode, int isActive) {
        QuestionId = questionId;
        Question = question;
        StateCode = stateCode;
        IsActive = isActive;
    }


    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(int questionId) {
        QuestionId = questionId;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public int getStateCode() {
        return StateCode;
    }

    public void setStateCode(int stateCode) {
        StateCode = stateCode;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    @Override
    public String toString() {
        return "GetQuestionsResponse{" +
                "QuestionId=" + QuestionId +
                ", Question='" + Question + '\'' +
                ", StateCode=" + StateCode +
                ", IsActive=" + IsActive +
                ", selected=" + selected +
                '}';
    }
}
