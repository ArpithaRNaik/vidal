package com.effiatechnologies.vidalinvestigator.model;

import org.json.JSONArray;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Administrator on 14-12-2016.
 */

public class SaveClaimsDetails {

    private String LoginId;
    private String TransID;
   // private String FileName;
    private String Description;
    private String Remarks;
    private String Longitude;
    private String Latitude;
    private List<QuestionAnswerTableList> questionAnswersList;

   // private String Document;

    //  private byte[] FileData;
  //  private String FileData;



    public SaveClaimsDetails() {

    }

    public SaveClaimsDetails(String LoginId, String TransID, List<QuestionAnswerTableList> questionAnswersList,
                             String Description, String Remarks, String Longitude, String Latitude) {
        this.LoginId = LoginId;
        this.TransID = TransID;
      //  this.FileName = FileName;
        this.Description = Description;
        this.Remarks = Remarks;
        this.Longitude = Longitude;
        this.Latitude = Latitude;
        this.questionAnswersList = questionAnswersList;
     //   this.FileData = FileData;

      //  this.Document=Document;

    }

    public String getTransID() {
        return TransID;
    }

    public void setTransID(String transID) {
        TransID = transID;
    }



    public List<QuestionAnswerTableList> getQuestionAnswersList() {
        return questionAnswersList;
    }

    public void setQuestionAnswersList(List<QuestionAnswerTableList> questionAnswersList) {
        this.questionAnswersList = questionAnswersList;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getRemarks() {
        return Remarks;
    }

    public void setRemarks(String remarks) {
        Remarks = remarks;
    }

    public String getLoginId() {
        return LoginId;
    }

    public void setLoginId(String loginId) {
        LoginId = loginId;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    @Override
    public String toString() {
        return "SaveClaimsDetails{" +
                "LoginId='" + LoginId + '\'' +
                ", TransID='" + TransID + '\'' +
                ", Description='" + Description + '\'' +
                ", Remarks='" + Remarks + '\'' +
                ", Longitude='" + Longitude + '\'' +
                ", Latitude='" + Latitude + '\'' +
                ", questionAnswersList=" + questionAnswersList +
                '}';
    }
}
