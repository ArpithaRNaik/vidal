package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Administrator on 03-12-2016.
 */

public class fg {

    private String Trans;
    private int stateCode;
    private int Generated;
    private int Pending;


    public fg()
    {

    }

    public fg(String Trans, int stateCode, int Generated, int Pending)
    {
        Trans = Trans;
        stateCode= stateCode;
        Generated = Generated;
        Pending = Pending;

    }

    public String getTrans() {
        return Trans;
    }

    public void setTrans(String trans) {
        Trans = trans;
    }

    public int getStateCode() {
        return stateCode;
    }

    public void setStateCode(int stateCode) {
        this.stateCode = stateCode;
    }

    public int getGenerated() {
        return Generated;
    }

    public void setGenerated(int generated) {
        Generated = generated;
    }

    public int getPending() {
        return Pending;
    }

    public void setPending(int pending) {
        Pending = pending;
    }

    @Override
    public String toString() {
        return "fg{" +
                "Trans='" + Trans + '\'' +
                ", stateCode=" + stateCode +
                ", Generated=" + Generated +
                ", Pending=" + Pending +
                '}';
    }
}
