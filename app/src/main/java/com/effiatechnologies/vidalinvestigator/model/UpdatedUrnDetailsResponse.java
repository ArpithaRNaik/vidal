package com.effiatechnologies.vidalinvestigator.model;

import java.util.List;

/**
 * Created by User3 on 5/27/2016.
 */
public class UpdatedUrnDetailsResponse {

    private List<UserData> Table1;

    public UpdatedUrnDetailsResponse()
    {

    }

    public UpdatedUrnDetailsResponse(List<UserData> table1) {
        Table1 = table1;
    }

    public List<UserData> getTable1() {
        return Table1;
    }

    public void setTable1(List<UserData> table1) {
        Table1 = table1;
    }

    @Override
    public String toString() {
        return "UpdatedUrnDetailsResponse{" +
                "Table1=" + Table1 +
                '}';
    }
}
