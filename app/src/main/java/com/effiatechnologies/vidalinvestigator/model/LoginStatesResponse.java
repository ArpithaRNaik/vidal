package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 09-06-2017.
 */

public class LoginStatesResponse {

    private int stateCode;
    private String stateName;



    public LoginStatesResponse()
    {

    }

    public LoginStatesResponse(int stateCode, String stateName) {
        this.stateCode = stateCode;
        this.stateName = stateName;
    }

    public int getStateCode() {
        return stateCode;
    }

    public void setStateCode(int stateCode) {
        this.stateCode = stateCode;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    @Override
    public String toString() {
        return "LoginStatesResponse{" +
                "stateCode=" + stateCode +
                ", stateName='" + stateName + '\'' +
                '}';
    }
}
