package com.effiatechnologies.vidalinvestigator.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Administrator on 29-11-2016.
 */

public class AllStateClaimsResponse implements Serializable {

    @SerializedName("Received")
    private int Received;

    @SerializedName("Approved")
    private int Approved;

    @SerializedName("Pending")
    private int Pending;

    @SerializedName("Rejected")
    private int Rejected;

    public AllStateClaimsResponse()
    {

    }

    public AllStateClaimsResponse(int Received, int Approved, int Pending, int Rejected)
    {
        Received = Received;
        Approved = Approved;
        Pending = Pending;
        Rejected = Rejected;
    }

    public int getReceived() {
        return Received;
    }

    public void setReceived(int received) {
        Received = received;
    }

    public int getApproved() {
        return Approved;
    }

    public void setApproved(int approved) {
        Approved = approved;
    }

    public int getPending() {
        return Pending;
    }

    public void setPending(int pending) {
        Pending = pending;
    }

    public int getRejected() {
        return Rejected;
    }

    public void setRejected(int rejected) {
        Rejected = rejected;
    }

    @Override
    public String toString() {
        return "AllStateClaimsResponse{" +
                "Received=" + Received +
                ", Approved=" + Approved +
                ", Pending=" + Pending +
                ", Rejected=" + Rejected +
                '}';
    }
}
