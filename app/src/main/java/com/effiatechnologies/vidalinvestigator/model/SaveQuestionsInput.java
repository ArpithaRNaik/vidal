package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 16-06-2017.
 */

public class SaveQuestionsInput {

    private String Question;
    private int StateCode;
    private int IsActive;

    public SaveQuestionsInput()
    {

    }

    public SaveQuestionsInput(String question, int stateCode, int isActive) {
        Question = question;
        StateCode = stateCode;
        IsActive = isActive;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }

    public int getStateCode() {
        return StateCode;
    }

    public void setStateCode(int stateCode) {
        StateCode = stateCode;
    }

    public int getIsActive() {
        return IsActive;
    }

    public void setIsActive(int isActive) {
        IsActive = isActive;
    }

    @Override
    public String toString() {
        return "SaveQuestionsInput{" +
                "Question='" + Question + '\'' +
                ", StateCode=" + StateCode +
                ", IsActive=" + IsActive +
                '}';
    }
}
