package com.effiatechnologies.vidalinvestigator.model;

/**
 * Created by Arpitha on 11-12-2017.
 */

public class UrnDetailsInput {

    private String URN;
    private String DistrictCode;

    public UrnDetailsInput()
    {

    }

    public UrnDetailsInput(String URN, String DistrictCode) {
        this.URN = URN;
        this.DistrictCode = DistrictCode;
    }


    public String getURN() {
        return URN;
    }

    public void setURN(String URN) {
        this.URN = URN;
    }

    public String getDistrictCode() {
        return DistrictCode;
    }

    public void setDistrictCode(String districtCode) {
        DistrictCode = districtCode;
    }

    @Override
    public String toString() {
        return "UrnDetailsInput{" +
                "URN='" + URN + '\'' +
                ", DistrictCode='" + DistrictCode + '\'' +
                '}';
    }
}




