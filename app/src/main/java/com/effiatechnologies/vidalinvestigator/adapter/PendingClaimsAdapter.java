package com.effiatechnologies.vidalinvestigator.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.activity.ClaimsDetailsActivity;
import com.effiatechnologies.vidalinvestigator.model.GetClaimsResponse;
import com.effiatechnologies.vidalinvestigator.model.PendingClaimsModel;

import java.util.List;

/**
 * Created by Administrator on 06-12-2016.
 */

public class PendingClaimsAdapter extends RecyclerView.Adapter<PendingClaimsAdapter.ItemViewHolder> {

   // private static final String TAG = "pendingclaims_adapter";
    private List<GetClaimsResponse> mDataItemsList;
    private Context mContext;
    private Intent claimsIntent;

    public PendingClaimsAdapter() {
    }

    public PendingClaimsAdapter(Context context, List<GetClaimsResponse> mDataItemsList) {

        this.mDataItemsList = mDataItemsList;
        this.mContext = context;

    }


    @Override
    public PendingClaimsAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.test_claims, parent, false);

        return new ItemViewHolder(itemLayoutView);

    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        final GetClaimsResponse searchResponseModel = mDataItemsList.get(position);
      //  Log.d(TAG, searchResponseModel + "");
        holder.tvClaimId.setText(mDataItemsList.get(position).getTransID());
       // holder.tvClaimId.setText(searchResponseModel.getRoleCode());
        holder.tvClaimDate.setText(searchResponseModel.getHospitalTimeStamp());
        holder.tvClaimId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String claimId = searchResponseModel.getTransID();
                String claimDate = searchResponseModel.getHospitalTimeStamp();
                String patientName = searchResponseModel.getPatientName();
                String dateOfAdmission = searchResponseModel.getDateofAdmission();
                String dateOfDischarge = searchResponseModel.getDateofDischarge();
                String hospitalName = searchResponseModel.getHospitalName();
                String procedureName = searchResponseModel.getProcedureName();
                String amountNegotiated = String.valueOf(searchResponseModel.getAmountNegotiated());
                String patientAge = String.valueOf(searchResponseModel.getPatientAge());
                String patientGender = searchResponseModel.getPatientGender();
                String packageName = searchResponseModel.getPackageName();


               /* Log.d(TAG, "claimId : " + claimId);
                Log.d(TAG, "claimDate : " + claimDate);
                Log.d(TAG, "patientName : " + patientName);
                Log.d(TAG, "dateOfAdmission : " + dateOfAdmission);
                Log.d(TAG, "dateOfDischarge : " + dateOfDischarge);
                Log.d(TAG, "hospitalName : " + hospitalName);
                Log.d(TAG, "procedureName : " + procedureName);
                Log.d(TAG, "amountNegotiated : " + amountNegotiated);
                Log.d(TAG, "patientAge : " + patientAge);
                Log.d(TAG, "patientGender : " + patientGender);
                Log.d(TAG, "packageName : " + packageName);*/


                claimsIntent = new Intent(mContext, ClaimsDetailsActivity.class);
                claimsIntent.putExtra("claimId", claimId);
                claimsIntent.putExtra("claimDate", claimDate);
                claimsIntent.putExtra("claimPatientName", patientName);
                claimsIntent.putExtra("claimDateOfAdmission", dateOfAdmission);
                claimsIntent.putExtra("claimDateOfDischarge", dateOfDischarge);
                claimsIntent.putExtra("claimHospitalName", hospitalName);
                claimsIntent.putExtra("claimsProcedureName", procedureName);
                claimsIntent.putExtra("claimsAmountNegotiated", amountNegotiated);
                claimsIntent.putExtra("claimsPatientAge", patientAge);
                claimsIntent.putExtra("claimsPatientGender", patientGender);
                claimsIntent.putExtra("claimsPackageName", packageName);

                claimsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                mContext.startActivity(claimsIntent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return mDataItemsList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        CardView result;
        TextView tvClaimId;
        TextView tvClaimDate;

        public ItemViewHolder(View itemView) {
            super(itemView);
            result = (CardView) itemView.findViewById(R.id.card_view);
            tvClaimId = (TextView) itemView.findViewById(R.id.tv_ClaimId);
            tvClaimDate = (TextView) itemView.findViewById(R.id.tv_ClaimDate);

            tvClaimId.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                  //  Log.d(TAG, "inside claimidclick");
                    String claimid = tvClaimId.getText().toString();
                    String claimdate = tvClaimDate.getText().toString();
                  //  Log.d(TAG, claimid + " :: claim id");
                  //  Log.d(TAG, claimdate + "  :: claim date");
                   /* claimsIntent = new Intent(mContext, ClaimsDetailsActivity.class);
                    claimsIntent.putExtra("claimId", claimid);
                    claimsIntent.putExtra("claimDate", claimdate);
                    claimsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                    mContext.startActivity(claimsIntent);*/
                  //  Log.d(TAG, "moving to claimsdetailsAnctivity");
                    //((Activity)mContext).finish();


                }
            });
        }
    }
}
