package com.effiatechnologies.vidalinvestigator.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.model.GetClaimsResponse;
import com.effiatechnologies.vidalinvestigator.model.LoginStatesResponse;

import java.util.ArrayList;
import java.util.List;


public class SearchPendingClaimsAdapter extends ArrayAdapter<GetClaimsResponse> {

   // private static final String TAG = "AutoComplete";

    private List<GetClaimsResponse> items;
    private ArrayList<GetClaimsResponse> itemsAll;
    private ArrayList<GetClaimsResponse> suggestions;
    private LayoutInflater vi;

    public SearchPendingClaimsAdapter(Context context, int resource, List<GetClaimsResponse> items) {
        super(context, resource);
        this.items = items;
        itemsAll = new ArrayList<GetClaimsResponse>(items);
        suggestions = new ArrayList<>();
        vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return items.size();
    }

    @Override
    public GetClaimsResponse getItem(int position) {
        return items.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {

            v = vi.inflate(R.layout.row_custom_spinner, parent, false);
        } else {
            v = convertView;
        }
        GetClaimsResponse collections = items.get(position);
        if (collections != null) {
            TextView categoryItemLabel = (TextView) v.findViewById(R.id.spnnier_row_name);
            if (categoryItemLabel != null) {
                categoryItemLabel.setText(collections.getHospitalName());
            }
        }
        return v;
    }


    @Override
    public Filter getFilter() {
        return itemFilter;
    }

    Filter itemFilter = new Filter() {

        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((GetClaimsResponse) (resultValue)).getHospitalName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null) {
                suggestions.clear();
                for (GetClaimsResponse collections : itemsAll) {

                    if (collections.getHospitalName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                     //   Log.i(TAG, "In Filter " + collections.getTransID() + ">>>>>" + constraint);
                        suggestions.add(collections);
                    }
                }
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, Filter.FilterResults results) {

            items = (ArrayList<GetClaimsResponse>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }

    };
}
