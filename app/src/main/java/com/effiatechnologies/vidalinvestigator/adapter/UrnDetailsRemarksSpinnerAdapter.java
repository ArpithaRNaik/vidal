package com.effiatechnologies.vidalinvestigator.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.model.LoginStatesResponse;
import com.effiatechnologies.vidalinvestigator.model.RemarksUrnResponse;

import java.util.List;


public class UrnDetailsRemarksSpinnerAdapter extends BaseAdapter {

   // private static final String TAG = "carrieradapter";
    private List<RemarksUrnResponse> generalItemList;
    private Context context = null;
    private LayoutInflater inflater = null;

    public UrnDetailsRemarksSpinnerAdapter(Context context, List<RemarksUrnResponse> generalItemList) {
        this.context = context;
        this.generalItemList = generalItemList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
     //   Log.i(TAG, generalItemList.size()+"");
        return generalItemList.size();
    }

    @Override
    public Object getItem(int i) {
        return generalItemList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        //  LayoutInflater inflater = ((Activity) context).getLayoutInflater();

        if (inflater != null) {
            View row = inflater.inflate(R.layout.row_custom_spinner, viewGroup, false);

            TextView spinnerItemName = (TextView) row.findViewById(R.id.spnnier_row_name);
            //Log.i(TAG,generalItemList.get(i).getCarrierType()+"");

            spinnerItemName.setText(generalItemList.get(i).getRemarks());
            return row;
        } else
            return null;
    }
}
