package com.effiatechnologies.vidalinvestigator.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.model.ReportResponseModel;
import com.effiatechnologies.vidalinvestigator.model.UrnReportCountResponse;

import java.util.List;

/**
 * Created by Administrator on 06-12-2016.
 */

public class ReportsDetailsAdapter extends RecyclerView.Adapter<ReportsDetailsAdapter.ItemViewHolder> {

   // private static final String TAG = "pendingclaims_adapter";
    private List<ReportResponseModel> mDataItemsList;
    private Context mContext;
    private Intent claimsIntent;

    public ReportsDetailsAdapter() {
    }

    public ReportsDetailsAdapter(Context context, List<ReportResponseModel> mDataItemsList) {

        this.mDataItemsList = mDataItemsList;
        this.mContext = context;

    }


    @Override
    public ReportsDetailsAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.report_detailed_view, parent, false);

        return new ItemViewHolder(itemLayoutView);

    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        ReportResponseModel searchResponseModel = mDataItemsList.get(position);
        // Log.d(TAG, "mDataItemsList : " + mDataItemsList);
        holder.tvDistrictName.setText(mDataItemsList.get(position).getDistrictName());
        holder.tvBlockName.setText(String.valueOf(mDataItemsList.get(position).getBlockName()));
        holder.tvPanchayathName.setText(mDataItemsList.get(position).getPanchayatName());
        holder.tvVillageName.setText(String.valueOf(mDataItemsList.get(position).getVillageName()));
        holder.tvTotalAadharSeeding.setText(String.valueOf(mDataItemsList.get(position).getTotalAadhaarSeeding()));

    }

    @Override
    public int getItemCount() {
        return mDataItemsList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        CardView result_rep;
        TextView tvDistrictName;
        TextView tvBlockName;
        TextView tvPanchayathName;
        TextView tvVillageName;
        TextView tvTotalAadharSeeding;


        public ItemViewHolder(View itemView) {
            super(itemView);
            result_rep = (CardView) itemView.findViewById(R.id.card_view_report_detailed);
            tvDistrictName = (TextView) itemView.findViewById(R.id.tv_DistrictName);
            tvBlockName = (TextView) itemView.findViewById(R.id.tv_BlockName);
            tvPanchayathName = (TextView) itemView.findViewById(R.id.tv_PanchayatName);
            tvVillageName = (TextView) itemView.findViewById(R.id.tv_VillageName);
            tvTotalAadharSeeding = (TextView) itemView.findViewById(R.id.tv_TotalAadhaarSeeding);

        }
    }
}
