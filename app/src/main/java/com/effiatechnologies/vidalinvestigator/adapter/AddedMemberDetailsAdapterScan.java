package com.effiatechnologies.vidalinvestigator.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.activity.DecoderActivity;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.helper.UrnDetailsHelperTable;
import com.effiatechnologies.vidalinvestigator.model.GenderResponse;
import com.effiatechnologies.vidalinvestigator.model.LocalMemDetailsResponse;
import com.effiatechnologies.vidalinvestigator.model.RelationsResponse;
import com.effiatechnologies.vidalinvestigator.model.RemarksUrnResponse;
import com.effiatechnologies.vidalinvestigator.model.UrnDetailsResponse;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;

/**
 * Created by Administrator on 31-12-2016.
 */

public class AddedMemberDetailsAdapterScan extends RecyclerView.Adapter<AddedMemberDetailsAdapterScan.ItemViewHolder> {

  //  private static final String TAG = "UrnDetailsAdapterScan";
    private List<LocalMemDetailsResponse> mDataItemsList;

    private Context mContext;
    private Activity mActivity;
    private AlertDialog.Builder builder = null;
    private ProgressDialog progressDialog = null;

    public AddedMemberDetailsAdapterScan(Activity mActivity, Context context, List<LocalMemDetailsResponse> mDataItemsList) {

        // Log.d(TAG, "inside 1");
        this.mDataItemsList = mDataItemsList;
        this.mContext = context;
        this.mActivity = mActivity;
        builder = new AlertDialog.Builder(mActivity);
        progressDialog = new ProgressDialog(mActivity);

        // Log.d(TAG, "scan_UID 1: " + scan_UID);
        // Log.d(TAG, "mDataItemsList : " + mDataItemsList);

    }

    @Override
    public AddedMemberDetailsAdapterScan.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.added_member_details_sheet, parent, false);

        // Log.d(TAG, "scan_UID 2: " + scan_UID);

        return new ItemViewHolder(itemLayoutView);

    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        LocalMemDetailsResponse searchResponseModel = mDataItemsList.get(position);
        // Log.d(TAG, "mDataItemsList : " + mDataItemsList);
        //holder.tvTimingsName.setText((mDataItemsList.get(position).getSubjectCode()));
        holder.tvURN.setText(mDataItemsList.get(position).getURN());
        //  holder.tv_memberName_scan.setText(mDataItemsList.get(position).getMemberName());
        holder.tv_memberId_scan.setText(String.valueOf(mDataItemsList.get(position).getMemberID()));
        holder.tv_memberName_scan.setText(mDataItemsList.get(position).getMemberName());
       // holder.tv_memberAge.setText(mDataItemsList.get(position).getMemberAge());
        holder.et_Aadhar_name.setText(mDataItemsList.get(position).getNameAsAadhaar());
        holder.tv_mobileNo_scan.setText(mDataItemsList.get(position).getMobileNO());
        holder.tv_AadharNum.setText(mDataItemsList.get(position).getAadhaarNo());


    }


    @Override
    public int getItemCount() {
        return mDataItemsList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        CardView urn_card_view_scan;
        TextView tvURN;
        TextView tv_memberId_scan;
        TextView tv_memberName_scan;
      //  EditText tv_memberAge;
      TextView et_Aadhar_name;
        TextView tv_mobileNo_scan;
        TextView tv_AadharNum;


        public ItemViewHolder(View itemView) {
            super(itemView);
            urn_card_view_scan = (CardView) itemView.findViewById(R.id.card_view_added_member_details);
            tvURN = (TextView) itemView.findViewById(R.id.tv_Added_URN);
            tv_memberId_scan = (TextView) itemView.findViewById(R.id.tv_Added_MemberId);
            tv_memberName_scan = (TextView) itemView.findViewById(R.id.tv_Added_MemberName);
          //  tv_memberAge = (EditText) itemView.findViewById(R.id//.tv_Added_MemberAge);
            et_Aadhar_name = (TextView) itemView.findViewById(R.id.tv_Added_AadharName);
            tv_mobileNo_scan = (TextView) itemView.findViewById(R.id.tv_Added_MobileNum);
            tv_AadharNum = (TextView) itemView.findViewById(R.id.tv_Added_AadharNum);

        }
    }

}
