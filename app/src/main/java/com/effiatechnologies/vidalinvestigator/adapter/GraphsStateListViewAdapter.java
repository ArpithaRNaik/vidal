package com.effiatechnologies.vidalinvestigator.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.model.LoginStatesResponse;
import com.effiatechnologies.vidalinvestigator.model.StatesModel;

import java.util.List;


public class GraphsStateListViewAdapter extends RecyclerView.Adapter<GraphsStateListViewAdapter.ItemViewHolder> {

   // private static final String TAG = "carrieradapter";
    private List<StatesModel> generalItemList;
    private Context context = null;
    private LayoutInflater inflater = null;

    public GraphsStateListViewAdapter(Context context, List<StatesModel> generalItemList) {
        this.context = context;
        this.generalItemList = generalItemList;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_custom_states, parent, false);

        return new ItemViewHolder(itemLayoutView);

    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        StatesModel searchResponseModel = generalItemList.get(position);
        //  Log.d(TAG, searchResponseModel + "");
        holder.tvLeaveTypeName.setText(generalItemList.get(position).getStateName());
        if (generalItemList.get(position).getStateName().contains("Select State"))
        {
            holder.tvLeaveTypeName.setText("ALL STATES");
        }
        else
        {
            holder.tvLeaveTypeName.setText(generalItemList.get(position).getStateName());
        }

    }


    @Override
    public int getItemCount() {
        return generalItemList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        TextView tvLeaveTypeName;


        public ItemViewHolder(View itemView) {
            super(itemView);
            tvLeaveTypeName = (TextView) itemView.findViewById(R.id.state_name);

        }
    }

}
