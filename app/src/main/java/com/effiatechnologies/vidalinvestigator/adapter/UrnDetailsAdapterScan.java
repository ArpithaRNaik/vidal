package com.effiatechnologies.vidalinvestigator.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.effiatechnologies.vidalinvestigator.MyApplication;
import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.activity.AadharSeedingActivity;
import com.effiatechnologies.vidalinvestigator.activity.AdminNavigationActivity;
import com.effiatechnologies.vidalinvestigator.activity.DecoderActivity;
import com.effiatechnologies.vidalinvestigator.activity.LoginActivity;
import com.effiatechnologies.vidalinvestigator.configs.UrlConfig;
import com.effiatechnologies.vidalinvestigator.helper.UrnDetailsHelperTable;
import com.effiatechnologies.vidalinvestigator.helper.XMLParser;
import com.effiatechnologies.vidalinvestigator.model.GenderResponse;
import com.effiatechnologies.vidalinvestigator.model.LoginStatesResponse;
import com.effiatechnologies.vidalinvestigator.model.RelationsResponse;
import com.effiatechnologies.vidalinvestigator.model.RemarksUrnResponse;
import com.effiatechnologies.vidalinvestigator.model.UrnDetailsResponse;
import com.effiatechnologies.vidalinvestigator.utils.AppUtil;
import com.effiatechnologies.vidalinvestigator.utils.SharedPreferenceUtil;
import com.google.android.gms.plus.model.people.Person;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

import static android.text.TextUtils.isEmpty;

/**
 * Created by Administrator on 31-12-2016.
 */

public class UrnDetailsAdapterScan extends RecyclerView.Adapter<UrnDetailsAdapterScan.ItemViewHolder> {

  //  private static final String TAG = "UrnDetailsAdapterScan";
    private static final int REQUEST_CODE_DOC = 101;
    private int UserCode;
    private String LoginId;
    private BasicDetailsServices mApiService = null;
    private ProgressDialog progressDialog = null;
    private List<UrnDetailsResponse> mDataItemsList;

    private Context mContext;

    private int memberId_scan = 0;
    private int memberId;
    private String memberName;
    private String age;
    private String relation;
    private String SINo;
    private String URN;
    private String IsEnrolled;
    private String userAadharNum;
    private String nameInAadhar;
    private String mobileNumber;
    private String familyId;

    private SQLiteDatabase SQLITEDATABASE;

    private String SQLiteQuery = null;
    String DeleteQuery;
    String GetSQliteQuery;
    Cursor cursor;


    private String scan_UID;

    UrnDetailsHelperTable URNHELPER;
    String GetSQliteQueryAssCls;
    Cursor cursorAssCls;
    SQLiteDatabase SQLITEDATABASEASSCLS;
    private String IMIENumber;
    private String formattedDate;
    private String currentDateTimeString;
    private AlertDialog.Builder builder;
    private Activity mActivity;
    private List<RemarksUrnResponse> remarksUrnResponseList = null;
    private String remarksStatus[] = {"Select", "Death", "Aadhar Not Available", "Split Of Family", "Member Not available"};
    private UrnDetailsRemarksSpinnerAdapter remarksSpinnerAda;
    private int selectedRemarksId;
    private String selectedRemarks;
    private List<GenderResponse> genderResponseList = null;
    private List<RelationsResponse> relationsResponseList = null;
    private GenderNameSpinnerAdapter genderNameSpinnerAdapter = null;
    private RelationsNameSpinnerAdapter relationsNameSpinnerAdapter = null;
    private RelationsResponse relationsResponse = null;
    private int relationId;
    private String relationDescription;
    private GenderResponse genderResponse = null;
    private int genderId;
    private String genderDescription;
    private String finalRelationDes;
    private String finalRelationCode;
    private String finalGenderDes;
    private int finalGenderCode;


    public UrnDetailsAdapterScan(Activity mActivity, Context context, List<UrnDetailsResponse> mDataItemsList) {

        // Log.d(TAG, "inside 1");
        this.mDataItemsList = mDataItemsList;
        this.mContext = context;
        this.mActivity = mActivity;
        IMIENumber = SharedPreferenceUtil.getIMIENum(mContext);
        // Log.d(TAG, "IMIENumber : " + IMIENumber);
        LoginId = SharedPreferenceUtil.getLoginId(mContext);
        // Log.d(TAG, "LoginId : " + LoginId);

        UserCode = SharedPreferenceUtil.getLoginCode(mContext);
        //  Log.d(TAG, "UserCode : " + UserCode);
        builder = new AlertDialog.Builder(mActivity);
        progressDialog = new ProgressDialog(mActivity);
        mApiService = MyApplication.getRetrofit().create(BasicDetailsServices.class);

        URNHELPER = new UrnDetailsHelperTable(context);
        GetSQliteQueryAssCls = "Select * from URNDetailsTable ";
        SQLITEDATABASEASSCLS = URNHELPER.getReadableDatabase();
        cursorAssCls = SQLITEDATABASEASSCLS.rawQuery(GetSQliteQueryAssCls, null);
        // Log.d(TAG, "scan_UID 1: " + scan_UID);
        // Log.d(TAG, "mDataItemsList : " + mDataItemsList);

        remarksUrnResponseList = new ArrayList<>();
        remarksUrnResponseList.add(0, new RemarksUrnResponse(0, "Select"));
        remarksUrnResponseList.add(1, new RemarksUrnResponse(1, "Death"));
        remarksUrnResponseList.add(2, new RemarksUrnResponse(2, "Aadhar Not Available"));
        remarksUrnResponseList.add(3, new RemarksUrnResponse(3, "Split Of Family"));
        remarksUrnResponseList.add(4, new RemarksUrnResponse(4, "Member Not available"));
        remarksSpinnerAda = new UrnDetailsRemarksSpinnerAdapter(mContext, remarksUrnResponseList);

        notifyDataSetChanged();

        getGenders();
        getRelations();

    }

    @Override
    public UrnDetailsAdapterScan.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.urn_details_scan_sheet, parent, false);

        // Log.d(TAG, "scan_UID 2: " + scan_UID);

        return new ItemViewHolder(itemLayoutView);

    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        UrnDetailsResponse searchResponseModel = mDataItemsList.get(position);
        // Log.d(TAG, "mDataItemsList : " + mDataItemsList);
        //holder.tvTimingsName.setText((mDataItemsList.get(position).getSubjectCode()));
        holder.tv_Urn_scan.setText(mDataItemsList.get(position).getURN());
        //  holder.tv_memberName_scan.setText(mDataItemsList.get(position).getMemberName());
        holder.tv_memberId_scan.setText(String.valueOf(mDataItemsList.get(position).getMemberID()));
        // holder.tv_relation_scan.setText(mDataItemsList.get(position).getRelation());
        holder.et_Aadhar_scan.setTag(position);
        holder.setIsRecyclable(true);
        // holder.et_Aadhar.setText(mDataItemsList.get(position).getAadhaarNo());
        holder.tv_nameInAadhar_scan.setText(mDataItemsList.get(position).getNameAsAadhaar());
        holder.tv_mobileNo_scan.setText(mDataItemsList.get(position).getMobileNO());
        holder.spinner_remarks.setAdapter(remarksSpinnerAda);

        /*-------member id-----------*/
        if (mDataItemsList.get(position).getMemberID() == 0) {
            // Log.d(TAG, "inside if");
            holder.tv_memberId_scan.setEnabled(true);
        } else {
            // Log.d(TAG, "inside else");
            holder.tv_memberId_scan.setEnabled(false);
            holder.tv_memberId_scan.setText(String.valueOf(mDataItemsList.get(position).getMemberID()));
        }

        /*-------member name-----------*/
        if (mDataItemsList.get(position).getMemberName() == null) {
            // Log.d(TAG, "inside if");
            holder.tv_memberName_scan.setEnabled(true);
        } else if (TextUtils.isEmpty(mDataItemsList.get(position).getMemberName())) {
            // Log.d(TAG, "inside else if");
            holder.tv_memberName_scan.setEnabled(true);
        } else if (mDataItemsList.get(position).getMemberName().equals("null")) {
            // Log.d(TAG, "inside else if");
            holder.tv_memberName_scan.setEnabled(true);
        } else {
            // Log.d(TAG, "inside else");
            holder.tv_memberName_scan.setEnabled(false);
            holder.tv_memberName_scan.setText(mDataItemsList.get(position).getMemberName());
        }

        /*----------relations---------------*/
        if (mDataItemsList.get(position).getRelation() == null) {
            // Log.d(TAG, "inside if");
            // holder.tv_relation_scan.setEnabled(true);
            holder.tv_relation_scan.setVisibility(View.GONE);
            holder.spinner_relations.setVisibility(View.VISIBLE);
            holder.spinner_relations.setAdapter(relationsNameSpinnerAdapter);
        } else if (TextUtils.isEmpty(mDataItemsList.get(position).getRelation())) {
            // Log.d(TAG, "inside else if");
            // holder.tv_relation_scan.setEnabled(true);
            holder.tv_relation_scan.setVisibility(View.GONE);
            holder.spinner_relations.setVisibility(View.VISIBLE);
            holder.spinner_relations.setAdapter(relationsNameSpinnerAdapter);

        } else if (mDataItemsList.get(position).getRelation().equals("null")) {
            // Log.d(TAG, "inside else if");
            // holder.tv_relation_scan.setEnabled(true);
            holder.tv_relation_scan.setVisibility(View.GONE);
            holder.spinner_relations.setVisibility(View.VISIBLE);
            holder.spinner_relations.setAdapter(relationsNameSpinnerAdapter);

        } else {
            // Log.d(TAG, "inside else");
            holder.tv_relation_scan.setEnabled(false);
            holder.tv_relation_scan.setVisibility(View.VISIBLE);
            holder.spinner_relations.setVisibility(View.GONE);
            holder.tv_relation_scan.setText(mDataItemsList.get(position).getRelation());
        }

        /*---------age---------*/
        if (mDataItemsList.get(position).getAge() == null) {
            // Log.d(TAG, "inside if");
            holder.tv_age_scan.setEnabled(true);
        } else if (TextUtils.isEmpty(mDataItemsList.get(position).getAge())) {
            // Log.d(TAG, "inside else if");
            holder.tv_age_scan.setEnabled(true);
        } else if (mDataItemsList.get(position).getAge().equals("null")) {
            // Log.d(TAG, "inside else if");
            holder.tv_age_scan.setEnabled(true);
        } else {
            // Log.d(TAG, "inside else");
            holder.tv_age_scan.setEnabled(false);
            holder.tv_age_scan.setText(mDataItemsList.get(position).getAge());
        }

        /*-----------gender-------------*/
        if (mDataItemsList.get(position).getGenderName() == null) {
            // Log.d(TAG, "inside if");
            // holder.tv_gender_scan.setEnabled(true);
            holder.tv_gender_scan.setVisibility(View.GONE);
            holder.spinner_gender.setVisibility(View.VISIBLE);
            holder.spinner_gender.setAdapter(genderNameSpinnerAdapter);
        } else if (TextUtils.isEmpty(mDataItemsList.get(position).getGenderName())) {
            // Log.d(TAG, "inside else if");
            // holder.tv_gender_scan.setEnabled(true);
            holder.tv_gender_scan.setVisibility(View.GONE);
            holder.spinner_gender.setVisibility(View.VISIBLE);
            holder.spinner_gender.setAdapter(genderNameSpinnerAdapter);
        } else if (mDataItemsList.get(position).getGenderName().equals("null")) {
            // Log.d(TAG, "inside else if");
            // holder.tv_gender_scan.setEnabled(true);
            holder.tv_gender_scan.setVisibility(View.GONE);
            holder.spinner_gender.setVisibility(View.VISIBLE);
            holder.spinner_gender.setAdapter(genderNameSpinnerAdapter);
        } else {
            // Log.d(TAG, "inside else");
            holder.tv_gender_scan.setEnabled(false);
            holder.tv_gender_scan.setVisibility(View.VISIBLE);
            holder.spinner_gender.setVisibility(View.GONE);
            holder.tv_gender_scan.setText(mDataItemsList.get(position).getGenderName());
        }

        /*------------name as aadhar--------------*/
        if (mDataItemsList.get(position).getNameAsAadhaar() == null) {
            // Log.d(TAG, "inside if");
            holder.tv_nameInAadhar_scan.setEnabled(true);
        } else if (TextUtils.isEmpty(mDataItemsList.get(position).getNameAsAadhaar())) {
            // Log.d(TAG, "inside else if");
            holder.tv_nameInAadhar_scan.setEnabled(true);
        } else if (mDataItemsList.get(position).getNameAsAadhaar().equals("null")) {
            // Log.d(TAG, "inside else if");
            holder.tv_nameInAadhar_scan.setEnabled(true);
        } else {
            // Log.d(TAG, "inside else");
            holder.tv_nameInAadhar_scan.setEnabled(false);
            holder.tv_nameInAadhar_scan.setText(mDataItemsList.get(position).getNameAsAadhaar());
        }

        /*------------mobile number----------------*/
        if (mDataItemsList.get(position).getMobileNO() == null) {
            // Log.d(TAG, "inside if");
            holder.tv_mobileNo_scan.setEnabled(true);
        } else if (TextUtils.isEmpty(mDataItemsList.get(position).getMobileNO())) {
            // Log.d(TAG, "inside else if");
            holder.tv_mobileNo_scan.setEnabled(true);
        } else if (mDataItemsList.get(position).getMobileNO().equals("null")) {
            // Log.d(TAG, "inside else if");
            holder.tv_mobileNo_scan.setEnabled(true);
        } else {
            // Log.d(TAG, "inside else");
            holder.tv_mobileNo_scan.setEnabled(false);
            holder.tv_mobileNo_scan.setText(mDataItemsList.get(position).getMobileNO());
        }

        /*-----------aadhar number-------------------*/
        if (mDataItemsList.get(position).getAadhaarNumber() == null) {
            // Log.d(TAG, "inside if");
            holder.et_Aadhar_scan.setEnabled(true);
            holder.spinner_remarks.setEnabled(true);
            holder.btn_ScanQrCode_scan.setEnabled(true);

            // holder.et_Aadhar.setText(mDataItemsList.get(position).getAadhaarNo());
        } else if (TextUtils.isEmpty(mDataItemsList.get(position).getAadhaarNumber())) {
            // Log.d(TAG, "inside else if");
            holder.et_Aadhar_scan.setEnabled(true);
            holder.spinner_remarks.setEnabled(true);
            holder.btn_ScanQrCode_scan.setEnabled(true);

            // holder.et_Aadhar.setText(mDataItemsList.get(position).getAadhaarNo());
        } else if (mDataItemsList.get(position).getAadhaarNumber().equals("null")) {
            // Log.d(TAG, "inside else if");
            holder.et_Aadhar_scan.setEnabled(true);
            holder.spinner_remarks.setEnabled(true);
            holder.btn_ScanQrCode_scan.setEnabled(true);

            // holder.et_Aadhar.setText(mDataItemsList.get(position).getAadhaarNo());
        } else {
            //  Log.d(TAG, "inside else");
            holder.et_Aadhar_scan.setEnabled(false);
            holder.spinner_remarks.setEnabled(false);
            holder.btn_ScanQrCode_scan.setEnabled(false);
            holder.et_Aadhar_scan.setText(mDataItemsList.get(position).getAadhaarNo());
        }


        /*----------remarks spinner------------*/
        holder.spinner_remarks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position >= 0) {

                    selectedRemarksId = remarksUrnResponseList.get(position).getRemarksId();
                    selectedRemarks = remarksUrnResponseList.get(position).getRemarks();
                    // Log.d(TAG, "selectedRemarksId 1: " + selectedRemarksId);
                    // Log.d(TAG, "selectedRemarks 1: " + selectedRemarks);
                    if (selectedRemarksId == 0) {
                        // Log.d(TAG, "inside 111");

                    } else {
                        // Log.d(TAG, "inside 112");
                        holder.et_Aadhar_scan.setEnabled(false);
                        holder.tv_nameInAadhar_scan.setEnabled(false);
                        holder.tv_mobileNo_scan.setEnabled(false);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        /*------------spinner relations-------------------*/
        holder.spinner_relations.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                {
                    relationsResponse = (RelationsResponse) parent.getItemAtPosition(position);
                    relationId = relationsResponse.getRelationCode();
                  //  Log.d(TAG, "relationId : " + relationId);
                    relationDescription = relationsResponse.getRelationDescription();
                  //  Log.d(TAG, "relationDescription : " + relationDescription);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

        /*------------spinner gender-------------------*/
        holder.spinner_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                {
                    genderResponse = (GenderResponse) parent.getItemAtPosition(position);
                    genderId = genderResponse.getGenderCode();
                 //   Log.d(TAG, "genderId : " + genderId);
                    genderDescription = genderResponse.getGenderName();
                  //  Log.d(TAG, "genderDescription : " + genderDescription);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });
        holder.checkAadhar_scan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // Log.d(TAG, "inside if (isChecked)");

                    boolean cancel = false;
                    mDataItemsList.get(position).setSelected(true);
                    memberId = mDataItemsList.get(position).getMemberID();
                    memberName = mDataItemsList.get(position).getMemberName();
                    //  age = mDataItemsList.get(position).getAge();
                    relation = mDataItemsList.get(position).getRelation();
                    SINo = mDataItemsList.get(position).getSINo();
                    URN = mDataItemsList.get(position).getURN();
                    IsEnrolled = mDataItemsList.get(position).getIsEnrolled();
                    familyId = mDataItemsList.get(position).getFamilyId();
                    // userAadharNum = holder.et_Aadhar.getText().toString();
                    if (mDataItemsList.get(position).getAadhaarNumber() == null) {
                        userAadharNum = holder.et_Aadhar_scan.getText().toString();
                    } else if (TextUtils.isEmpty(mDataItemsList.get(position).getAadhaarNumber())) {
                        userAadharNum = holder.et_Aadhar_scan.getText().toString();
                    } else if (mDataItemsList.get(position).getAadhaarNumber().equals("null")) {
                        userAadharNum = holder.et_Aadhar_scan.getText().toString();
                    } else {
                        userAadharNum = mDataItemsList.get(position).getAadhaarNumber();
                    }

                    if (mDataItemsList.get(position).getAge() == null) {
                        age = holder.tv_age_scan.getText().toString();
                    } else if (TextUtils.isEmpty(mDataItemsList.get(position).getAge())) {
                        age = holder.tv_age_scan.getText().toString();
                    } else if (mDataItemsList.get(position).getAge().equals("null")) {
                        age = holder.tv_age_scan.getText().toString();
                    } else {
                        age = mDataItemsList.get(position).getAge();
                    }

                    if (mDataItemsList.get(position).getRelation() == null) {
                        // Log.d(TAG, "inside if");
                        relation = relationDescription;
                        finalRelationCode = String.valueOf(relationId);
                    } else if (TextUtils.isEmpty(mDataItemsList.get(position).getRelation())) {
                        // Log.d(TAG, "inside else if");
                        relation = relationDescription;
                        finalRelationCode = String.valueOf(relationId);
                    } else if (mDataItemsList.get(position).getRelation().equals("null")) {
                        // Log.d(TAG, "inside else if");
                        relation = relationDescription;
                        finalRelationCode = String.valueOf(relationId);
                    } else {
                        // Log.d(TAG, "inside else");
                        relation = mDataItemsList.get(position).getRelation();
                        finalRelationCode = mDataItemsList.get(position).getRelationCode();

                    }

                    if (mDataItemsList.get(position).getGenderName() == null) {
                        // Log.d(TAG, "inside if");
                        finalGenderDes = genderDescription;
                        finalGenderCode = genderId;
                    } else if (TextUtils.isEmpty(mDataItemsList.get(position).getGenderName())) {
                        // Log.d(TAG, "inside else if");
                        finalGenderDes = genderDescription;
                        finalGenderCode = genderId;
                    } else if (mDataItemsList.get(position).getGenderName().equals("null")) {
                        // Log.d(TAG, "inside else if");
                        finalGenderDes = genderDescription;
                        finalGenderCode = genderId;
                    } else {
                        // Log.d(TAG, "inside else");
                        finalGenderDes = mDataItemsList.get(position).getGenderName();
                        finalGenderCode = Integer.parseInt(mDataItemsList.get(position).getGenderCode());

                    }
                    nameInAadhar = holder.tv_nameInAadhar_scan.getText().toString();
                    mobileNumber = holder.tv_mobileNo_scan.getText().toString();

                  //  Log.d(TAG, "mobileNumber size : " + mobileNumber.length());

                    Calendar c = Calendar.getInstance();
                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    formattedDate = df.format(c.getTime());
                    // Log.d(TAG, "formattedDate : " + formattedDate);


                    if (userAadharNum == null && selectedRemarksId == 0) {
                        showAadharRemarksNotAvailbale();
                        holder.checkAadhar_scan.setChecked(false);
                    } else if (TextUtils.isEmpty(userAadharNum) && selectedRemarksId == 0) {
                        showAadharRemarksNotAvailbale();
                        holder.checkAadhar_scan.setChecked(false);

                    } else if (userAadharNum.equals("null") && selectedRemarksId == 0) {
                        showAadharRemarksNotAvailbale();
                        holder.checkAadhar_scan.setChecked(false);

                    } else if (userAadharNum != null && nameInAadhar == null) {
                        showAadharNameNotAvailbale();
                        holder.checkAadhar_scan.setChecked(false);
                    } else if (!TextUtils.isEmpty(userAadharNum) && TextUtils.isEmpty(nameInAadhar)) {
                        showAadharNameNotAvailbale();
                        holder.checkAadhar_scan.setChecked(false);

                    } else if (!userAadharNum.equals("null") && nameInAadhar.equals("null")) {
                        showAadharNameNotAvailbale();
                        holder.checkAadhar_scan.setChecked(false);

                    } else if (userAadharNum != null && nameInAadhar != null && mobileNumber == null) {
                        showAadharNumNotAvailbale();
                        holder.checkAadhar_scan.setChecked(false);
                    } else if (!TextUtils.isEmpty(userAadharNum) && (!TextUtils.isEmpty(nameInAadhar)) && TextUtils.isEmpty(mobileNumber)) {
                        showAadharNumNotAvailbale();
                        holder.checkAadhar_scan.setChecked(false);
                    } else if (!userAadharNum.equals("null") && (!nameInAadhar.equals("null")) && mobileNumber.equals("null")) {
                        showAadharNumNotAvailbale();
                        holder.checkAadhar_scan.setChecked(false);
                    } else if (!(AppUtil.isMobileNumbervalid(mobileNumber))) {
                        showAadharNumNotAvailbale();
                        holder.checkAadhar_scan.setChecked(false);
                    } else {
                        DBCreate();
                        holder.et_Aadhar_scan.setEnabled(false);
                        holder.tv_nameInAadhar_scan.setEnabled(false);
                        holder.tv_mobileNo_scan.setEnabled(false);

                    }

                    /* Log.d(TAG, "memberId : " + memberId);
                    Log.d(TAG, "memberName : " + memberName);
                    Log.d(TAG, "age : " + age);
                    Log.d(TAG, "relation : " + relation);
                    Log.d(TAG, "SINo : " + SINo);
                    Log.d(TAG, "URN : " + URN);
                    Log.d(TAG, "IsEnrolled : " + IsEnrolled);
                    Log.d(TAG, "userAadharNum : " + userAadharNum);
                    Log.d(TAG, "nameInAadhar : " + nameInAadhar);
                    Log.d(TAG, "mobileNumber : " + mobileNumber);*/


                } else {

                    //  Log.d(TAG, "inside else");
                    mDataItemsList.get(position).setSelected(false);
                    memberId = mDataItemsList.get(position).getMemberID();
                    memberName = mDataItemsList.get(position).getMemberName();
                    // age = Integer.parseInt(mDataItemsList.get(position).getAge());
                    // relation = mDataItemsList.get(position).getRelation();
                    SINo = mDataItemsList.get(position).getSINo();
                    URN = mDataItemsList.get(position).getURN();
                    IsEnrolled = mDataItemsList.get(position).getIsEnrolled();
                    // userAadharNum = holder.et_Aadhar.getText().toString();
                    userAadharNum = mDataItemsList.get(position).getAadhaarNumber();
                    nameInAadhar = holder.tv_nameInAadhar_scan.getText().toString();
                    mobileNumber = holder.tv_mobileNo_scan.getText().toString();

                    //  Log.d(TAG, "QuestionId : " + QuestionId);

                    GetSQliteQuery = "SELECT * FROM URNDescriptionTable";
                    SQLITEDATABASE = mContext.openOrCreateDatabase("URNDescription.db", Context.MODE_PRIVATE, null);
                    cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
                    cursor.moveToFirst();
                    DeleteQuery = "DELETE FROM URNDescriptionTable WHERE MemberID=" + memberId + ";";
                    SQLITEDATABASE.execSQL(DeleteQuery);

                    // holder.tvTimingsAnswer.getText().clear();
                    Toast.makeText(mContext, "Record Deleted", Toast.LENGTH_LONG).show();
                    cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
                    holder.et_Aadhar_scan.setEnabled(true);
                    holder.tv_nameInAadhar_scan.setEnabled(true);
                    holder.tv_mobileNo_scan.setEnabled(true);
                    //holder.spinner_remarks.setEnabled(true);
                    // remarksUrnResponseList.clear();
                    // selectedRemarksId = 0;
                    // selectedRemarks = null;
                    // ((AdminUpdateQuestionFragment) mActivity).refreshList();

                }
            }
        });


        holder.btn_ScanQrCode_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                memberId_scan = mDataItemsList.get(position).getMemberID();
                //  Log.d(TAG, "memberId inside btn on click : " + memberId_scan);

                Intent intent = new Intent(mContext, DecoderActivity.class);
                //Activity origin = (Activity) mContext;
                Bundle bundle = new Bundle();
                bundle.putString("MEMBER_ID", String.valueOf(memberId_scan));
                intent.putExtras(bundle);
                mActivity.startActivityForResult(intent, REQUEST_CODE_DOC);
                // mActivity.startActivityForResult(new Intent(mContext, DecoderActivity.class), REQUEST_CODE_DOC);

            }
        });

    }

    private void getGenders() {

        //  Log.d(TAG, "inside getPeriodFaculyNames");
        if (AppUtil.isNetworkAvailable(mContext)) {
            progressDialog.setMessage("Please Wait....");
            progressDialog.show();
            progressDialog.setCancelable(false);

            Call<List<GenderResponse>> call = mApiService.getGenders();
            call.enqueue(new Callback<List<GenderResponse>>() {
                @Override
                public void onResponse(Call<List<GenderResponse>> call, Response<List<GenderResponse>> response) {

                    if (response.isSuccessful()) {
                        //  Log.d(TAG, "response is successfull");
                        progressDialog.dismiss();
                        genderResponseList = response.body();
                      //  Log.d(TAG, "genderResponseList : " + genderResponseList);
                        genderResponseList.add(0, new GenderResponse(0, " Select "));
                      //  Log.d(TAG, "genderResponseList : " + genderResponseList);
                        genderNameSpinnerAdapter = new GenderNameSpinnerAdapter(mContext, genderResponseList);
                        notifyDataSetChanged();

                    } else {

                        progressDialog.dismiss();
                        int statusCode = response.code();
                        //  Log.d(TAG, statusCode + "failed");
                        //  ResponseBody errorBody = response.errorBody();

                    }
                }

                @Override
                public void onFailure(Call<List<GenderResponse>> call, Throwable t) {
                    //  Log.d(TAG, "in failure" + t.getMessage());
                    showErrorDialog();
                    progressDialog.dismiss();
                }
            });
        } else {
            showInternetNotAvailbale();
            // AppUtil.toastMessage(AddStudentAttendanceActivity.this, "Check Internet connection");
        }
    }

    private void getRelations() {

       // Log.d(TAG, "inside getRelations");
        if (AppUtil.isNetworkAvailable(mContext)) {
            progressDialog.setMessage("Please Wait....");
            progressDialog.show();
            progressDialog.setCancelable(false);

            Call<List<RelationsResponse>> call = mApiService.getRelations();
            call.enqueue(new Callback<List<RelationsResponse>>() {
                @Override
                public void onResponse(Call<List<RelationsResponse>> call, Response<List<RelationsResponse>> response) {

                    if (response.isSuccessful()) {
                        //  Log.d(TAG, "response is successfull");
                        progressDialog.dismiss();
                        relationsResponseList = response.body();
                      //  Log.d(TAG, "relationsResponseList : " + relationsResponseList);
                        relationsResponseList.add(0, new RelationsResponse(0, " Select "));
                      //  Log.d(TAG, "relationsResponseList : " + relationsResponseList);
                        relationsNameSpinnerAdapter = new RelationsNameSpinnerAdapter(mContext, relationsResponseList);
                        notifyDataSetChanged();
                    } else {

                        progressDialog.dismiss();
                        int statusCode = response.code();
                        //  Log.d(TAG, statusCode + "failed");
                        //  ResponseBody errorBody = response.errorBody();

                    }
                }

                @Override
                public void onFailure(Call<List<RelationsResponse>> call, Throwable t) {
                    //  Log.d(TAG, "in failure" + t.getMessage());
                    showErrorDialog();
                    progressDialog.dismiss();
                }
            });
        } else {
            showInternetNotAvailbale();
            // AppUtil.toastMessage(AddStudentAttendanceActivity.this, "Check Internet connection");
        }
    }

    private void showErrorDialog() {

        builder.setCancelable(false);
        builder.setTitle("Network Failure");
        builder.setMessage("Please Try Again!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                //finish();
            }
        });

        builder.show();
    }

    private void showInternetNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Network Not Available");
        builder.setMessage("Please check your Internet Connection!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }

    public interface BasicDetailsServices {

        @GET(UrlConfig.GET_GENDERS)
        Call<List<GenderResponse>> getGenders();

        @GET(UrlConfig.GET_RELATIONS)
        Call<List<RelationsResponse>> getRelations();
    }

    private void showAadharNumNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Information");
        builder.setMessage("Please Enter Valid Mobile Number!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }

    private void showAadharRemarksNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Information");
        builder.setMessage("Please Enter AadharNumber or Select Remarks!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }

    private void showAadharNameNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Information");
        builder.setMessage("Please Enter Name As In Aadhar!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }

    private void showGenderNotAvailbale() {

        builder.setCancelable(false);
        builder.setTitle("Information");
        builder.setMessage("Please Select Gender!");
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //finish();
            }
        });

        builder.show();
    }

    public void DBCreate() {

        SQLITEDATABASE = mContext.openOrCreateDatabase("URNDescription.db", Context.MODE_PRIVATE, null);

        SQLITEDATABASE.execSQL("CREATE TABLE IF NOT EXISTS URNDescriptionTable(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,MemberID INTEGER,MemberName VARCHAR,URN VARCHAR,SINo VARCHAR,Age VARCHAR,Relation VARCHAR,IsEnrolled VARCHAR,AadhaarNo VARCHAR,NameAsAadhaar VARCHAR,MobileNO VARCHAR,IMIENumber VARCHAR,DATETIME VARCHAR,RemarksID INTEGER,RemarksName VARCHAR,LoginId VARCHAR,UserCode VARCHAR,RelationCode VARCHAR,GenderName VARCHAR,GenderCode INTEGER,FamilyId VARCHAR,Flag INTEGER);");

        SQLiteQuery = "INSERT INTO URNDescriptionTable(MemberID,MemberName,URN,SINo,Age,Relation,IsEnrolled,AadhaarNo,NameAsAadhaar,MobileNO,IMIENumber,DATETIME,RemarksID,RemarksName,LoginId,UserCode,RelationCode,GenderName,GenderCode,FamilyId,Flag) " +
                "VALUES('" + memberId + "','" + memberName + "','" + URN + "','" + SINo + "','" + age + "','" + relation + "','" + IsEnrolled + "','" + userAadharNum + "','" + nameInAadhar + "','" + mobileNumber + "','" + IMIENumber + "','" + formattedDate + "','" + selectedRemarksId + "','" + selectedRemarks + "','" + LoginId + "','" + UserCode + "','" + finalRelationCode + "','" + finalGenderDes + "','" + finalGenderCode + "','" + familyId + "','" + 0 + "');";

        SQLITEDATABASE.execSQL(SQLiteQuery);
        SQLITEDATABASE.close();
        Toast.makeText(mContext, "Data Added Successfully", Toast.LENGTH_LONG).show();
        //remarksUrnResponseList.clear();
        // selectedRemarksId = 0;
        // selectedRemarks = null;
    }

    @Override
    public int getItemCount() {
        return mDataItemsList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        CardView urn_card_view_scan;
        TextView tv_Urn_scan;
        TextView tv_memberName_scan;
        EditText et_Aadhar_scan;
        CheckBox checkAadhar_scan;
        EditText tv_nameInAadhar_scan;
        EditText tv_mobileNo_scan;
        Spinner spinner_remarks;
        Button btn_ScanQrCode_scan;

        EditText tv_memberId_scan;
        EditText tv_relation_scan;
        EditText tv_gender_scan;
        EditText tv_age_scan;
        Spinner spinner_relations;
        Spinner spinner_gender;


        public ItemViewHolder(View itemView) {
            super(itemView);
            urn_card_view_scan = (CardView) itemView.findViewById(R.id.card_view_urn_details_scan);
            tv_Urn_scan = (TextView) itemView.findViewById(R.id.tv_urn_scan);
            tv_memberName_scan = (TextView) itemView.findViewById(R.id.tv_MemberName_scan);
            checkAadhar_scan = (CheckBox) itemView.findViewById(R.id.aAdhar_check_scan);
            et_Aadhar_scan = (EditText) itemView.findViewById(R.id.et_AadharNum_scan);
            tv_nameInAadhar_scan = (EditText) itemView.findViewById(R.id.et_NameInAadhar_scan);
            tv_mobileNo_scan = (EditText) itemView.findViewById(R.id.et_MobileNo_scan);

            spinner_remarks = (Spinner) itemView.findViewById(R.id.spinner_remarks);
            btn_ScanQrCode_scan = (Button) itemView.findViewById(R.id.btn_ScanQRcode_scan);

            tv_memberId_scan = (EditText) itemView.findViewById(R.id.tv_MemberId_scan);
            tv_relation_scan = (EditText) itemView.findViewById(R.id.tv_Relation_scan);
            tv_age_scan = (EditText) itemView.findViewById(R.id.tv_Age_scan);
            tv_gender_scan = (EditText) itemView.findViewById(R.id.tv_Gender_scan);
            spinner_relations = (Spinner) itemView.findViewById(R.id.spinner_relations);
            spinner_gender = (Spinner) itemView.findViewById(R.id.spinner_gender);
        }
    }

}
