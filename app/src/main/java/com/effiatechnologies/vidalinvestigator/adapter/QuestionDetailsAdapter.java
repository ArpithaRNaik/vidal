package com.effiatechnologies.vidalinvestigator.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.activity.AdminNavigationActivity;
import com.effiatechnologies.vidalinvestigator.fragment.AdminUpdateQuestionFragment;
import com.effiatechnologies.vidalinvestigator.helper.RoleTypesSqliteHelper;
import com.effiatechnologies.vidalinvestigator.helper.SqliteStatesHelper;
import com.effiatechnologies.vidalinvestigator.model.GetQuestionsResponse;

import java.util.List;

/**
 * Created by Administrator on 31-12-2016.
 */

public class QuestionDetailsAdapter extends RecyclerView.Adapter<QuestionDetailsAdapter.ItemViewHolder>
       /* implements CompoundButton.OnCheckedChangeListener*/ {

  //  private static final String TAG = "questionsadapter";
    private List<GetQuestionsResponse> mDataItemsList;
    private Context mContext;
    private Intent claimsIntent;
    int position;
    String QuestionDeta;
    int QuestionId;
    int StateCode;
    int IsActiveValue;
    private RoleTypesSqliteHelper roleTypesSqliteHelper;
    private SQLiteDatabase SQLITEDATABASEADMIN;
    private String SQLiteQuery = null;
    String DeleteQuery;
    String GetSQliteQuery;
    Cursor cursor;
    Activity mActivity;




    public QuestionDetailsAdapter() {
    }

    public QuestionDetailsAdapter(Context context, Activity mActivity, List<GetQuestionsResponse> mDataItemsList) {

        this.mDataItemsList = mDataItemsList;
        this.mContext = context;
        this.mActivity = mActivity;
        roleTypesSqliteHelper = new RoleTypesSqliteHelper(mContext);


    }


    @Override
    public QuestionDetailsAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_details, parent, false);

        return new ItemViewHolder(itemLayoutView);

    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        GetQuestionsResponse searchResponseModel = mDataItemsList.get(position);
      //  Log.d(TAG, "searchResponseModel : " + searchResponseModel);
        holder.tvQuestionDetails.setText((mDataItemsList.get(position).getQuestion()));
        //  holder.checkQuestion.setTag(position);

        //searchResponseModel.getIsActive();
        holder.checkQuestion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                  //  Log.d(TAG, "inside if (isChecked)");

                    mDataItemsList.get(position).setSelected(true);
                    QuestionDeta = mDataItemsList.get(position).getQuestion();
                    QuestionId = mDataItemsList.get(position).getQuestionId();
                    StateCode = mDataItemsList.get(position).getStateCode();
                  //  IsActiveValue = mDataItemsList.get(position).getIsActive();
                    IsActiveValue = 1;

                    /*roleTypesSqliteHelper.addUserTypeList(QuestionDeta, QuestionId, StateCode, IsActiveValue);
                    Log.d(TAG,"response from sqlite " + roleTypesSqliteHelper.getQuestionsDetails().toString());
                    roleTypesSqliteHelper.close();*/

                    DBCreate();
                }
                else
                {

                  //  Log.d(TAG, "inside else");
                    mDataItemsList.get(position).setSelected(false);
                    QuestionDeta = mDataItemsList.get(position).getQuestion();
                    QuestionId = mDataItemsList.get(position).getQuestionId();
                    StateCode = mDataItemsList.get(position).getStateCode();
                  //  IsActiveValue = mDataItemsList.get(position).getIsActive();
                    IsActiveValue = 0;

                   // Log.d(TAG, "QuestionId : " + QuestionId);

                    GetSQliteQuery = "SELECT * FROM QuestionsTable";
                    SQLITEDATABASEADMIN = mContext.openOrCreateDatabase("QuestionsAdmin.db", Context.MODE_PRIVATE, null);
                    cursor = SQLITEDATABASEADMIN.rawQuery(GetSQliteQuery, null);
                    cursor.moveToFirst();
                    DeleteQuery = "DELETE FROM QuestionsTable WHERE QuestionId=" + QuestionId + ";";
                    SQLITEDATABASEADMIN.execSQL(DeleteQuery);
                  //  Toast.makeText(mContext, "Record Deleted", Toast.LENGTH_LONG).show();
                    cursor = SQLITEDATABASEADMIN.rawQuery(GetSQliteQuery, null);
                   // ((AdminUpdateQuestionFragment) mActivity).refreshList();
                }
            }
        });

    }

    public void DBCreate() {

        SQLITEDATABASEADMIN = mContext.openOrCreateDatabase("QuestionsAdmin.db", Context.MODE_PRIVATE, null);

        SQLITEDATABASEADMIN.execSQL("CREATE TABLE IF NOT EXISTS QuestionsTable(ids INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , QuestionId INTEGER , Question VARCHAR, StateCode INTEGER, IsActive INTEGER );");

        SQLiteQuery = "INSERT INTO QuestionsTable ( QuestionId , Question , StateCode , IsActive ) " +
                "VALUES('" + QuestionId + "', '" + QuestionDeta + "', '" + StateCode + "', '" + IsActiveValue + "');";

        SQLITEDATABASEADMIN.execSQL(SQLiteQuery);
        SQLITEDATABASEADMIN.close();
      //  Toast.makeText(mContext, "Data Added Successfully", Toast.LENGTH_LONG).show();
    }

    @Override
    public int getItemCount() {
        return mDataItemsList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        CardView resultQue;
        TextView tvQuestionDetails;
        CheckBox checkQuestion;


        public ItemViewHolder(View itemView) {
            super(itemView);
            resultQue = (CardView) itemView.findViewById(R.id.card_view_question_details);
            tvQuestionDetails = (TextView) itemView.findViewById(R.id.tv_Question);
            checkQuestion = (CheckBox) itemView.findViewById(R.id.question_check);

        }
    }
}
