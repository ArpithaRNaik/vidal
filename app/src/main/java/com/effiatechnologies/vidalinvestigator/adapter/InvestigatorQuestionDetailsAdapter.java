package com.effiatechnologies.vidalinvestigator.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.helper.RoleTypesSqliteHelper;
import com.effiatechnologies.vidalinvestigator.model.GetQuestionsResponse;

import java.util.List;

/**
 * Created by Arpitha on 31-12-2016.
 */

public class InvestigatorQuestionDetailsAdapter extends RecyclerView.Adapter<InvestigatorQuestionDetailsAdapter.ItemViewHolder>
       /* implements CompoundButton.OnCheckedChangeListener*/ {

   // private static final String TAG = "invQueDetails";
    private List<GetQuestionsResponse> mDataItemsList;
    private Context mContext;
    private Intent claimsIntent;
    int position;
    String QuestionDeta;
    int QuestionId;
    int StateCode;
    int IsActiveValue;
    String QuestionAnswer;
    private SQLiteDatabase SQLITEDATABASE;
    private String SQLiteQuery = null;
    String DeleteQuery;
    String GetSQliteQuery;
    Cursor cursor;
    Activity mActivity;




    public InvestigatorQuestionDetailsAdapter() {
    }

    public InvestigatorQuestionDetailsAdapter(Context context, Activity mActivity, List<GetQuestionsResponse> mDataItemsList) {

        this.mDataItemsList = mDataItemsList;
        this.mContext = context;
        this.mActivity = mActivity;

    }


    @Override
    public InvestigatorQuestionDetailsAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.investigator_questions, parent, false);

        return new ItemViewHolder(itemLayoutView);

    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, final int position) {
        GetQuestionsResponse searchResponseModel = mDataItemsList.get(position);
      //  Log.d(TAG, "searchResponseModel : " + searchResponseModel);
        holder.tvQuestionDetailsInv.setText((mDataItemsList.get(position).getQuestion()));
        //  holder.checkQuestion.setTag(position);

        //searchResponseModel.getIsActive();
       // holder.setIsRecyclable(true);
        holder.answerCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                  //  Log.d(TAG, "inside if (isChecked)");

                    mDataItemsList.get(position).setSelected(true);
                    QuestionDeta = mDataItemsList.get(position).getQuestion();
                    QuestionId = mDataItemsList.get(position).getQuestionId();
                    StateCode = mDataItemsList.get(position).getStateCode();
                    IsActiveValue = mDataItemsList.get(position).getIsActive();
                    QuestionAnswer = holder.etInvQuesAns.getText().toString();

                    DBCreate();
                }
                else
                {

                  //  Log.d(TAG, "inside else");
                    mDataItemsList.get(position).setSelected(false);
                    QuestionDeta = mDataItemsList.get(position).getQuestion();
                    QuestionId = mDataItemsList.get(position).getQuestionId();
                    StateCode = mDataItemsList.get(position).getStateCode();
                    IsActiveValue = mDataItemsList.get(position).getIsActive();

                  //  Log.d(TAG, "QuestionId : " + QuestionId);

                    GetSQliteQuery = "SELECT * FROM QuestionsAnswerTable";
                    SQLITEDATABASE = mContext.openOrCreateDatabase("QuestionsAnswer.db", Context.MODE_PRIVATE, null);
                    cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
                    cursor.moveToFirst();
                    DeleteQuery = "DELETE FROM QuestionsAnswerTable WHERE QuestionId=" + QuestionId + ";";
                    SQLITEDATABASE.execSQL(DeleteQuery);
                    holder.etInvQuesAns.getText().clear();
                   // Toast.makeText(mContext, "Record Deleted", Toast.LENGTH_LONG).show();
                    cursor = SQLITEDATABASE.rawQuery(GetSQliteQuery, null);
                   // ((AdminUpdateQuestionFragment) mActivity).refreshList();
                }
            }
        });

    }

    public void DBCreate() {

        SQLITEDATABASE = mContext.openOrCreateDatabase("QuestionsAnswer.db", Context.MODE_PRIVATE, null);

        SQLITEDATABASE.execSQL("CREATE TABLE IF NOT EXISTS QuestionsAnswerTable(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,QuestionId INTEGER,Question VARCHAR,StateCode INTEGER,IsActive INTEGER,QuestionAnswer VARCHAR);");

        SQLiteQuery = "INSERT INTO QuestionsAnswerTable(QuestionId,Question,StateCode,IsActive,QuestionAnswer) " +
                "VALUES('" + QuestionId + "','" + QuestionDeta + "','" + StateCode + "','" + IsActiveValue + "','" + QuestionAnswer + "');";

        SQLITEDATABASE.execSQL(SQLiteQuery);
        SQLITEDATABASE.close();
      //  Toast.makeText(mContext, "Data Added Successfully", Toast.LENGTH_LONG).show();
    }

    @Override
    public int getItemCount() {
        return mDataItemsList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        CardView resultQueInv;
        TextView tvQuestionDetailsInv;
        EditText etInvQuesAns;
        CheckBox answerCheckbox;


        public ItemViewHolder(View itemView) {
            super(itemView);
            resultQueInv = (CardView) itemView.findViewById(R.id.card_view_investigator_question);
            tvQuestionDetailsInv = (TextView) itemView.findViewById(R.id.tv_invQuestion);
            etInvQuesAns = (EditText) itemView.findViewById(R.id.et_invQuestionAns);
            answerCheckbox = (CheckBox) itemView.findViewById(R.id.answer_check);

        }
    }
}
