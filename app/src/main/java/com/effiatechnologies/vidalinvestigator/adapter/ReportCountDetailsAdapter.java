package com.effiatechnologies.vidalinvestigator.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.effiatechnologies.vidalinvestigator.R;
import com.effiatechnologies.vidalinvestigator.activity.ClaimsDetailsActivity;
import com.effiatechnologies.vidalinvestigator.model.GetClaimsResponse;
import com.effiatechnologies.vidalinvestigator.model.UrnDetailsResponse;
import com.effiatechnologies.vidalinvestigator.model.UrnReportCountResponse;

import java.util.List;

/**
 * Created by Administrator on 06-12-2016.
 */

public class ReportCountDetailsAdapter extends RecyclerView.Adapter<ReportCountDetailsAdapter.ItemViewHolder> {

   // private static final String TAG = "pendingclaims_adapter";
    private List<UrnReportCountResponse> mDataItemsList;
    private Context mContext;
    private Intent claimsIntent;

    public ReportCountDetailsAdapter() {
    }

    public ReportCountDetailsAdapter(Context context, List<UrnReportCountResponse> mDataItemsList) {

        this.mDataItemsList = mDataItemsList;
        this.mContext = context;

    }


    @Override
    public ReportCountDetailsAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.urn_report_count_details, parent, false);

        return new ItemViewHolder(itemLayoutView);

    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, final int position) {
        UrnReportCountResponse searchResponseModel = mDataItemsList.get(position);
        // Log.d(TAG, "mDataItemsList : " + mDataItemsList);
        holder.tvUserName_rep.setText(mDataItemsList.get(position).getUserName());
        holder.tvDistCode_rep.setText(String.valueOf(mDataItemsList.get(position).getDistrictCode()));
        holder.tvDisName_rep.setText(mDataItemsList.get(position).getDistrictName());
        holder.tvFamilySeed_rep.setText(String.valueOf(mDataItemsList.get(position).getFamilySeeding()));
        holder.tvBenSeed_rep.setText(String.valueOf(mDataItemsList.get(position).getBeneficiarySeeding()));

    }

    @Override
    public int getItemCount() {
        return mDataItemsList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        CardView result_rep;
        TextView tvUserName_rep;
        TextView tvDistCode_rep;
        TextView tvDisName_rep;
        TextView tvFamilySeed_rep;
        TextView tvBenSeed_rep;


        public ItemViewHolder(View itemView) {
            super(itemView);
            result_rep = (CardView) itemView.findViewById(R.id.card_urn_report_count_details);
            tvUserName_rep = (TextView) itemView.findViewById(R.id.tv_repUserName);
            tvDistCode_rep = (TextView) itemView.findViewById(R.id.tv_repDistrictCode);
            tvDisName_rep = (TextView) itemView.findViewById(R.id.tv_repDistrictName);
            tvFamilySeed_rep = (TextView) itemView.findViewById(R.id.tv_repFamilySeeding);
            tvBenSeed_rep = (TextView) itemView.findViewById(R.id.tv_repBeniSeeding);

        }
    }
}
