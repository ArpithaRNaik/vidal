package com.effiatechnologies.vidalinvestigator.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.effiatechnologies.vidalinvestigator.model.LoginStatesResponse;
import com.effiatechnologies.vidalinvestigator.model.StatesModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by moni on 12/9/2015.
 */

public class SqliteStatesHelper extends SQLiteOpenHelper {

  //  private Context context;
  //  private static final String TAG = "Sqlite";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "states.db";

    private static final String TABLE_PRODUCTS = "states";

    private static final String CREATE_TABLE  = "CREATE TABLE ";
    private static final String STATE_ID = "stateCode";
    private static final String STATE_NAME = "stateName";



    public SqliteStatesHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_PRODUCT = CREATE_TABLE + TABLE_PRODUCTS
                + " ("
                + STATE_ID + " INTEGER Primary Key,"
                + STATE_NAME + " TEXT )";



        try {
            db.execSQL(CREATE_TABLE_PRODUCT);


        } catch (SQLException msg) {
           // Toast.makeText(context, msg.getMessage(), Toast.LENGTH_LONG).show();
           // Log.d(TAG, "Sqlite error :   "+msg.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL( "DROP TABLE IF EXISTS " + TABLE_PRODUCTS );

        onCreate(db);
    }

    public void addProducts(List<LoginStatesResponse> generalItemList)
    {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_PRODUCTS);
        ContentValues values = new ContentValues();

        for(LoginStatesResponse product: generalItemList)
        {
            values.put(STATE_ID, product.getStateCode());
            values.put(STATE_NAME, product.getStateName());

            db.insert(TABLE_PRODUCTS, null, values);
        }
    }

    public List<StatesModel> getProducts()
    {
        List<StatesModel> productList = new ArrayList<>();
        productList.add(new StatesModel(0,"Select State"));
        String selectQuery = "SELECT * FROM " + TABLE_PRODUCTS;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst())
        {
            do {
                StatesModel product = new StatesModel();
                product.setStateCode(Integer.parseInt(cursor.getString(0)));
                product.setStateName(cursor.getString(1));

                productList.add(product);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return productList;
    }


}
