package com.effiatechnologies.vidalinvestigator.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.effiatechnologies.vidalinvestigator.model.GetQuestionsResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User3 on 5/27/2016.
 */
public class InvQuestionsAnswerSqliteHelper extends SQLiteOpenHelper {

  //  public static final String TAG = "sqliteQueAns";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "QuestionsAnswer.db";

    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS";
    public static final String TABLE_USERTYPE = "QuestionsAnswerTable";
    public static final String ASSIGN_KEY_ID = "id";
    public static final String QUESTION = "Question";
    public static final String QUESTION_ID = "QuestionId";
    public static final String STATE_CODE = "StateCode";
    public static final String IS_ACTIVE = "IsActive";
    public static final String QUESTION_ANSWER = "QuestionAnswer";



    public InvQuestionsAnswerSqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_USERTYPE = CREATE_TABLE+ TABLE_USERTYPE
                + " ("
                + ASSIGN_KEY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + QUESTION_ID + " INTEGER,"
                + QUESTION + " VARCHAR,"
                + STATE_CODE + " INTEGER,"
                + IS_ACTIVE + "INTEGER,"
                + QUESTION_ANSWER + "VARCHAR)";



        try {
            db.execSQL(CREATE_TABLE_USERTYPE);

        } catch (SQLException msg) {
            // Toast.makeText(context, msg.getMessage(), Toast.LENGTH_LONG).show();
          //  Log.d(TAG, "Sqlite error :   " + msg.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERTYPE);
        onCreate(db);
    }

    /*public void addUserTypeList(List<GetQuestionsResponse> userTypeList) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_USERTYPE);
        ContentValues values = new ContentValues();

        for (GetQuestionsResponse userType : userTypeList) {
            values.put(QUESTION, userType.getQuestion());
            values.put(QUESTION_ID, userType.getQuestionId());
            values.put(STATE_CODE, userType.getStateCode());
            values.put(IS_ACTIVE, userType.getIsActive());
            db.insert(TABLE_USERTYPE, null, values);

        }
    }*/
    public void addUserTypeList(String questionDeta, int questionId, int stateCode, int isActiveValue) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_USERTYPE);
        ContentValues values = new ContentValues();


        values.put(QUESTION, questionDeta);
        values.put(QUESTION_ID, questionId);
        values.put(STATE_CODE, stateCode);
        values.put(IS_ACTIVE, isActiveValue);
     //   Log.d(TAG, "values : " + values);
        db.insert(TABLE_USERTYPE, null, values);


    }

    public List<GetQuestionsResponse> getQuestionsDetails() {
        List<GetQuestionsResponse> productList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_USERTYPE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
      //  Log.d(TAG, "cursor : " + cursor);
        String question = cursor.getString(0);
      //  Log.d(TAG, "value 1 : " + question);

        if (cursor.moveToFirst()) {
          //  Log.d(TAG, "inside 1");
            do {
              //  Log.d(TAG, "inside 2");
                GetQuestionsResponse product = new GetQuestionsResponse();
              //  Log.d(TAG, "question : " + cursor.getString(0));
                product.setQuestion(cursor.getString(0));
                product.setQuestionId(cursor.getInt(1));
                product.setStateCode(cursor.getInt(2));
                product.setIsActive(cursor.getInt(3));


              //  Log.d(TAG, "product : " + product);
                productList.add(product);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
      //  Log.d(TAG, "productList : " + productList);
        return productList;
    }


}
