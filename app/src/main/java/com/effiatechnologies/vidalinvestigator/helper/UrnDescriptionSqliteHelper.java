package com.effiatechnologies.vidalinvestigator.helper;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by User3 on 5/27/2016.
 */
public class UrnDescriptionSqliteHelper extends SQLiteOpenHelper {

    //  public static final String TAG = "sqliteQueAns";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "URNDescription.db";

    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS";
    public static final String TABLE_USERTYPE = "URNDescriptionTable";
    public static final String DES_KEY_ID = "id";
    public static final String MEMBER_ID = "MemberID";
    public static final String MEMBER_NAME = "MemberName";
    public static final String URN = "URN";
    public static final String SINO = "SINo";
    public static final String AGE = "Age";
    public static final String RELATION = "Relation";
    public static final String ISENROLLED = "IsEnrolled";
    public static final String AADHARNO = "AadhaarNo";
    public static final String NAME_AS_AADHAR = "NameAsAadhaar";
    public static final String MOBILE_NUM = "MobileNO";
    public static final String IMIE_NUM = "IMIENumber";
    public static final String DATE_TIME = "DATETIME";
    public static final String REMARKS_ID = "RemarksID";
    public static final String REMARKS_NAME = "RemarksName";
    public static final String LOGIN_ID = "LoginId";
    public static final String USER_CODE = "UserCode";
    public static final String RELATION_CODE = "RelationCode";
    public static final String GENDER_NAME = "GenderName";
    public static final String GENDER_CODE = "GenderCode";
    public static final String FAMILY_ID = "FamilyId";
    public static final String FLAG = "Flag";

    public UrnDescriptionSqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_USERTYPE = CREATE_TABLE + TABLE_USERTYPE
                + " ("
                + DES_KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + MEMBER_ID + " INTEGER,"
                + MEMBER_NAME + " VARCHAR,"
                + URN + " VARCHAR,"
                + SINO + "VARCHAR,"
                + AGE + "VARCHAR,"
                + RELATION + "VARCHAR,"
                + ISENROLLED + "VARCHAR,"
                + AADHARNO + "VARCHAR,"
                + NAME_AS_AADHAR + "VARCHAR,"
                + MOBILE_NUM + "VARCHAR,"
                + IMIE_NUM + "VARCHAR,"
                + DATE_TIME + "VARCHAR,"
                + REMARKS_ID + "INTEGER,"
                + REMARKS_NAME + "VARCHAR,"
                + LOGIN_ID + "VARCHAR,"
                + USER_CODE + "VARCHAR,"
                + RELATION_CODE + "VARCHAR,"
                + GENDER_NAME + "VARCHAR,"
                + GENDER_CODE + "VARCHAR,"
                + FAMILY_ID + "VARCHAR,"
                + FLAG + "INTEGER)";

        try {
            db.execSQL(CREATE_TABLE_USERTYPE);

        } catch (SQLException msg) {
            // Toast.makeText(context, msg.getMessage(), Toast.LENGTH_LONG).show();
            //  Log.d(TAG, "Sqlite error :   " + msg.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERTYPE);
        onCreate(db);
    }

}
