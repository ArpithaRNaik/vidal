package com.effiatechnologies.vidalinvestigator.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.effiatechnologies.vidalinvestigator.model.LoginStatesResponse;
import com.effiatechnologies.vidalinvestigator.model.StatesModel;
import com.effiatechnologies.vidalinvestigator.model.UrnDetailsResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User3 on 5/27/2016.
 */
public class UrnDetailsHelperTable extends SQLiteOpenHelper {

  //  public static final String TAG = "sqliteurndetailstable";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "URNDetails.db";

    public static final String CREATE_TABLE = "CREATE TABLE ";
    public static final String TABLE_URN_DE = "URNDetailsTable";
    public static final String DES_KEY_ID = "id";
    public static final String URN = "URN";
    public static final String SINO = "SINo";
    public static final String MEMBER_NAME = "MemberName";
    public static final String MEMBER_ID = "MemberID";
    public static final String AGE = "Age";
    public static final String RELATION = "Relation";
    public static final String ISENROLLED = "IsEnrolled";
    public static final String AADHARNO = "AadhaarNo";
    public static final String NAME_AS_AADHAR = "NameAsAadhaar";
    public static final String MOBILE_NUM = "MobileNO";
    public static final String AADHAR_NUMBER = "AadhaarNumber";
    public static final String GENDER_CODE = "GenderCode";
    public static final String GENDER_NAME = "GenderName";
    public static final String RELATION_CODE = "RelationCode";
    public static final String FAMILY_ID = "FamilyId";
    public static final String FLAG = "Flag";


    private String SQLiteQuery = null;

    public UrnDetailsHelperTable(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_USERTYPE = CREATE_TABLE + TABLE_URN_DE
                + " ("
                + DES_KEY_ID + " INTEGER Primary Key AUTOINCREMENT NOT NULL,"
                + URN + " TEXT,"
                + SINO + " TEXT,"
                + MEMBER_NAME + " TEXT,"
                + MEMBER_ID + " INTEGER,"
                + AGE + " TEXT,"
                + RELATION + " TEXT,"
                + ISENROLLED + " TEXT,"
                + AADHARNO + " TEXT,"
                + NAME_AS_AADHAR + " TEXT,"
                + MOBILE_NUM + " TEXT,"
                + AADHAR_NUMBER + " TEXT,"
                + GENDER_CODE + " TEXT,"
                + GENDER_NAME + " TEXT,"
                + RELATION_CODE + " TEXT,"
                + FAMILY_ID + " TEXT,"
                + FLAG + " INTEGER )";

        try {
            db.execSQL(CREATE_TABLE_USERTYPE);

        } catch (SQLException msg) {
            // Toast.makeText(context, msg.getMessage(), Toast.LENGTH_LONG).show();
           // Log.d(TAG, "Sqlite error :   " + msg.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_URN_DE);
        onCreate(db);
    }


    public void addURNDetails(List<UrnDetailsResponse> generalItemList) {

        SQLiteDatabase db = this.getWritableDatabase();
        // db.execSQL("delete from " + TABLE_PRODUCTS);
        ContentValues values = new ContentValues();

        for (UrnDetailsResponse product : generalItemList) {

            SQLiteQuery = "INSERT INTO URNDetailsTable (URN,SINo,MemberName,MemberID,Age,Relation,IsEnrolled,AadhaarNo,NameAsAadhaar,MobileNO,AadhaarNumber,GenderCode,GenderName,RelationCode,FamilyId,Flag) " +
                    "VALUES ('" + product.getURN() + "','" + product.getSINo() + "','" + product.getMemberName() + "','" + product.getMemberID() + "','" + product.getAge() + "','" + product.getRelation() + "','" + product.getIsEnrolled() + "','" + product.getAadhaarNo() + "','" + product.getNameAsAadhaar() + "','" + product.getMobileNO() + "','" + product.getAadhaarNumber() + "','" + product.getGenderCode() + "','" + product.getGenderName() + "','" + product.getRelationCode() + "','" + product.getFamilyId() + "','" + product.getFlag() + "');";

           // Log.d(TAG, "SQLiteQuery : " + SQLiteQuery);
            db.execSQL(SQLiteQuery);

        }
    }

    public List<UrnDetailsResponse> getURNDetails() {
        List<UrnDetailsResponse> productList = new ArrayList<>();
        // productList.add(new facLeaveAssClsPeriods(0, "Select Department", false, 0));
        String selectQuery = "SELECT * FROM " + TABLE_URN_DE;
        // String selectQuery = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + LEAVE_DATE + "= '" + "2017-12-27T00:00:00" + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                UrnDetailsResponse product = new UrnDetailsResponse();
                product.setURN(cursor.getString(1));
                product.setSINo(cursor.getString(2));
                product.setMemberName(cursor.getString(3));
                product.setMemberID(cursor.getInt(4));
                product.setAge(cursor.getString(5));
                product.setRelation(cursor.getString(6));
                product.setIsEnrolled(cursor.getString(7));
                product.setAadhaarNo(cursor.getString(8));
                product.setNameAsAadhaar(cursor.getString(9));
                product.setMobileNO(cursor.getString(10));
                product.setAadhaarNumber(cursor.getString(11));
                product.setGenderCode(cursor.getString(12));
                product.setGenderName(cursor.getString(13));
                product.setRelationCode(cursor.getString(14));
                product.setFamilyId(cursor.getString(15));
                product.setFlag(cursor.getInt(16));

                // Log.d(TAG, "product : " + product);
                productList.add(product);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
      //  Log.d(TAG, "productList : " + productList);
        return productList;
    }
    public void removeProducts() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_URN_DE, null, null);
        db.close();
    }
    public void updateTableDetails(String aAdharNumber, String memberId){

        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("UPDATE " + TABLE_URN_DE + " SET " + AADHARNO + "= '" + aAdharNumber + "' WHERE " + MEMBER_ID + "= '" + memberId + "'");
        database.close();
    }

}
