package com.effiatechnologies.vidalinvestigator.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.effiatechnologies.vidalinvestigator.model.LocalMemDetailsResponse;
import com.effiatechnologies.vidalinvestigator.model.UrnDetailsResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User3 on 5/27/2016.
 */
public class LocalMemberDetailsHelperTable extends SQLiteOpenHelper {

  //  public static final String TAG = "sqliteurndetailstable";
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "LocalURNMemberDetails.db";

    public static final String CREATE_TABLE = "CREATE TABLE ";
    public static final String TABLE_URN_DE = "URNMemberDetailsTable";
    public static final String DES_KEY_ID = "id";
    public static final String URN = "URN";
    public static final String MEMBER_ID = "MemberID";
    public static final String MEMBER_NAME = "MemberName";
    public static final String AADHARNO = "AadhaarNo";
    public static final String NAME_AS_AADHAR = "NameAsAadhaar";
    public static final String MOBILE_NUM = "MobileNO";
    public static final String IMIE_NUMBER = "IMIENumber";
    public static final String DATE_TIME = "DATETIME";
    public static final String LOGIN_ID = "LoginId";
    public static final String USER_CODE = "UserCode";



    private String SQLiteQuery = null;

    public LocalMemberDetailsHelperTable(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_TABLE_USERTYPE = CREATE_TABLE + TABLE_URN_DE
                + " ("
                + DES_KEY_ID + " INTEGER Primary Key AUTOINCREMENT NOT NULL,"
                + URN + " TEXT,"
                + MEMBER_ID + " INTEGER,"
                + MEMBER_NAME + " TEXT,"
                + AADHARNO + " TEXT,"
                + NAME_AS_AADHAR + " TEXT,"
                + MOBILE_NUM + " TEXT,"
                + IMIE_NUMBER + " TEXT,"
                + DATE_TIME + " TEXT,"
                + LOGIN_ID + " TEXT,"
                + USER_CODE + " TEXT )";

        try {
            db.execSQL(CREATE_TABLE_USERTYPE);

        } catch (SQLException msg) {
            // Toast.makeText(context, msg.getMessage(), Toast.LENGTH_LONG).show();
           // Log.d(TAG, "Sqlite error :   " + msg.getMessage());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_URN_DE);
        onCreate(db);
    }


    public void addURNDetails(List<UrnDetailsResponse> generalItemList) {

        SQLiteDatabase db = this.getWritableDatabase();
        // db.execSQL("delete from " + TABLE_PRODUCTS);
        ContentValues values = new ContentValues();

        for (UrnDetailsResponse product : generalItemList) {

            SQLiteQuery = "INSERT INTO URNDetailsTable (URN,SINo,MemberName,MemberID,Age,Relation,IsEnrolled,AadhaarNo,NameAsAadhaar,MobileNO,AadhaarNumber,GenderCode,GenderName,RelationCode,FamilyId,Flag) " +
                    "VALUES ('" + product.getURN() + "','" + product.getSINo() + "','" + product.getMemberName() + "','" + product.getMemberID() + "','" + product.getAge() + "','" + product.getRelation() + "','" + product.getIsEnrolled() + "','" + product.getAadhaarNo() + "','" + product.getNameAsAadhaar() + "','" + product.getMobileNO() + "','" + product.getAadhaarNumber() + "','" + product.getGenderCode() + "','" + product.getGenderName() + "','" + product.getRelationCode() + "','" + product.getFamilyId() + "','" + product.getFlag() + "');";

           // Log.d(TAG, "SQLiteQuery : " + SQLiteQuery);
            db.execSQL(SQLiteQuery);

        }
    }

    public List<LocalMemDetailsResponse> getURNDetails() {
        List<LocalMemDetailsResponse> productList = new ArrayList<>();
        // productList.add(new facLeaveAssClsPeriods(0, "Select Department", false, 0));
        String selectQuery = "SELECT * FROM " + TABLE_URN_DE;
        // String selectQuery = "SELECT * FROM " + TABLE_PRODUCTS + " WHERE " + LEAVE_DATE + "= '" + "2017-12-27T00:00:00" + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                LocalMemDetailsResponse product = new LocalMemDetailsResponse();
                product.setURN(cursor.getString(1));
                product.setMemberID(cursor.getInt(2));
                product.setMemberName(cursor.getString(3));
                product.setMemberAge(cursor.getString(4));
                product.setAadhaarNo(cursor.getString(5));
                product.setNameAsAadhaar(cursor.getString(6));
                product.setMobileNO(cursor.getString(7));
                product.setIMIENumber(cursor.getString(8));
                product.setDateTime(cursor.getString(9));
                product.setLoginId(cursor.getString(10));
                product.setUserCode(cursor.getString(11));

                // Log.d(TAG, "product : " + product);
                productList.add(product);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
      //  Log.d(TAG, "productList : " + productList);
        return productList;
    }
    public void removeProducts() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_URN_DE, null, null);
        db.close();
    }
    public void updateTableDetails(String aAdharNumber, String memberId){

        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("UPDATE " + TABLE_URN_DE + " SET " + AADHARNO + "= '" + aAdharNumber + "' WHERE " + MEMBER_ID + "= '" + memberId + "'");
        database.close();
    }

}
