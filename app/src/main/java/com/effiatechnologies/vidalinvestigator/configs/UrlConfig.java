package com.effiatechnologies.vidalinvestigator.configs;

/**
 * Created by Arpitha on 3/8/2016.
 */

public class UrlConfig {

    /*VIDAL API FOR LOCAL*/
   // public static final String BASE_URL = "http://172.18.1.32:9006/API/";

    /*VIDAL API FOR LIVE */
    public static final String BASE_URL = "http://45.112.139.194:9008/API/";

    /*VIDAL API FOR LIVE OLD ECLAIMS APP*/
   // public static final String BASE_URL = "http://45.112.139.194:8056/api/";

    public static final String USER_LOGIN_IMEI_CHECK = "LoginDetails/GetIMEI_SIMDetails";
    public static final String USER_LOGIN_STATE_DETAILS = "LoginDetails/GetLoginStateDetails";
    public static final String USER_LOGIN_SESSION_DETAILS = "LoginDetails/GetLoginDetailsForSession";

    public static final String GET_PENDING_CLAIMS = "LoginSessionDetails/GetClaims";
    public static final String SAVE_CLAIMS_DETAILS = "LoginSessionDetails/SaveClaims";
    public static final String GET_INVESTIGATOR_STATE_QUESTIONS = "QuestionBank/GetQuestions";

    public static final String STATES_QUESTIONS = "QuestionBank/GetStates";
    public static final String SAVE_QUESTIONS = "QuestionBank/SaveQuestions";
    public static final String GET_STATES_QUESTIONS = "QuestionBank/GetQuestionsForUpdate";
    public static final String UPDATE_QUESTION_DETAILS = "QuestionBank/UpdateQuestions";
    public static final String UPLOAD_PDF_DOCUMENT = "DocumentUpload/MediaUpload";
    public static final String SAVE_INVESTIGATOR_QUESTION_ANSWERS = "QuestionBank/SaveQuestionsAnswer";

    public static final String GET_NATIVE_GRAPH_LIST = "Claims/ClaimsDetails";
    public static final String GET_DAILY_REPORT_LIST = "Claims/GetClaimsForState";

    /*------Aadhar Seeding APIs*/
    public static final String GET_URN_DETAILS = "AADHARSeeding/GetURNDetails";
    public static final String UPDATE_URN_DETAILS = "AADHARSeeding/UpdateMemberDetails";
    public static final String GET_URN_REPORT_COUNT = "AADHARSeeding/GetAADHARSeedingDetailsCount";
    public static final String GET_DISTRICTS = "AADHARSeeding/GetDistricts";
    public static final String GET_GENDERS = "AADHARSeeding/GetGender";
    public static final String GET_RELATIONS = "AADHARSeeding/GetRelations";
    public static final String UPDATE_NEW_MEMBER_DETAILS = "AADHARSeeding/InsertMemberDetails";

    public static final String GET_USER_TYPE_LIST = "Role";

}